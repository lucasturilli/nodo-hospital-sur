﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

﻿/*
 Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
 */

        CKEDITOR.editorConfig = function(config)
        {

            // Define changes to default configuration here. For example:
            config.language = 'es';
            config.skin = 'kama';
            config.extraPlugins = 'wenzgmap';
            // config.uiColor = '#AADC6E';
            var base_url = window.location.origin;
            var pathArray = window.location.pathname.split('/');
            var newPathname = "";
            for (i = 0; i < pathArray.length; i++) {
                if (pathArray[i] !== 'administrador') {
                    newPathname += pathArray[i];
                    newPathname += "/";
                }else{
                    break;
                }
            }
//            config.filebrowserBrowseUrl = base_url + newPathname + 'assets/grocery_crud/texteditor/kcfinder/browse.php?opener=ckeditor&type=files';
//            config.filebrowserImageBrowseUrl = base_url + newPathname + 'assets/grocery_crud/texteditor/kcfinder/browse.php?opener=ckeditor&type=images';
//            config.filebrowserFlashBrowseUrl = base_url +  newPathname + 'assets/grocery_crud/texteditor/kcfinder/browse.php?opener=ckeditor&type=flash';
//            config.filebrowserUploadUrl = base_url +  newPathname + 'assets/grocery_crud/texteditor/kcfinder/upload.php?opener=ckeditor&type=files';
//            config.filebrowserImageUploadUrl = base_url +  newPathname + 'assets/grocery_crud/texteditor/kcfinder/upload.php?opener=ckeditor&type=images';
//            config.filebrowserFlashUploadUrl = base_url + newPathname + 'assets/grocery_crud/texteditor/kcfinder/upload.php?opener=ckeditor&type=flash';
            
            // ...
   config.filebrowserBrowseUrl = base_url + newPathname +'kcfinder/browse.php?opener=ckeditor&type=files';
   config.filebrowserImageBrowseUrl =base_url + newPathname + 'kcfinder/browse.php?opener=ckeditor&type=images';
   config.filebrowserFlashBrowseUrl = base_url + newPathname +'kcfinder/browse.php?opener=ckeditor&type=flash';
   config.filebrowserUploadUrl = base_url + newPathname +'kcfinder/upload.php?opener=ckeditor&type=files';
   config.filebrowserImageUploadUrl = base_url + newPathname +'kcfinder/upload.php?opener=ckeditor&type=images';
   config.filebrowserFlashUploadUrl = base_url + newPathname +'kcfinder/upload.php?opener=ckeditor&type=flash';
// ...

            config.toolbar_Full =
                    [
                        {name: 'document', items: ['Source', '-', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates']},
                        {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                        {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
                        {name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']},
                        '/',
                        {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                        {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                        {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                        {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak']},
                        '/',
                        {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                        {name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About']}
                    ];
            //
        };
