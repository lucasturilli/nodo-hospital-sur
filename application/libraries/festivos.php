<?php

class Festivos {

    function __construct() {
        date_default_timezone_set("America/Bogota");
        $this->CI = & get_instance();
    }

    private $hoy;
    private $festivos;
    private $ano;
    private $pascua_mes;
    private $pascua_dia;

    public function getFestivos($ano = null) {
        $this->festivos_function($ano);
        return $this->festivos;
    }

    public function festivos_function($ano = null) {
        $this->hoy = date('d/n/Y');

//
//        


        $ano = 2019;
        for ($ano; $ano <= date('Y'); $ano++) {
            $this->ano = $ano;
            $this->pascua_mes = date("m", easter_date($this->ano));
            $this->pascua_dia = date("d", easter_date($this->ano));
            $this->festivos[$ano][1][1] = true;  // Primero de Enero año nuevo OK
            $this->festivos[$ano][5][1] = true;  // Dia del Trabajo 1 de Mayo  OK
            $this->festivos[$ano][7][20] = true;  // Independencia 20 de Julio  OK
            $this->festivos[$ano][8][7] = true;  // Batalla de Boyacá 7 de Agosto OK
            $this->festivos[$ano][12][8] = true;  // Maria Inmaculada 8 diciembre (religiosa) OK
            $this->festivos[$ano][12][25] = true;  // Navidad 25 de diciembre OK

            $this->calcula_emiliani(1, 6);    // Reyes Magos Enero 6 OK 
            $this->calcula_emiliani(3, 19);    // San Jose Marzo 19 OK
            $this->calcula_emiliani(6, 29);    // San Pedro y San Pablo Junio 29 OK
            $this->calcula_emiliani(8, 15);    // Asunción Agosto 15 OK
            $this->calcula_emiliani(10, 12);   // Descubrimiento de América Oct 12 OK
            $this->calcula_emiliani(11, 1);    // Todos los santos Nov 1 OK
            $this->calcula_emiliani(11, 11);   // Independencia de Cartagena Nov 11 OK
            //otras fechas calculadas a partir de la pascua.

            $this->otrasFechasCalculadas(-3);   //jueves santo OK
            $this->otrasFechasCalculadas(-2);   //viernes santo OK

            $this->otrasFechasCalculadas(43, true);  //Ascención el Señor pascua OK
            $this->otrasFechasCalculadas(64, true);  //Corpus Cristi OK
            $this->otrasFechasCalculadas(71, true);  //Sagrado Corazón OK
        }
//
        //
        //
        // otras fechas importantes que no son festivos
        // $this->otrasFechasCalculadas(-46);		// Miércoles de Ceniza
        // $this->otrasFechasCalculadas(-46);		// Miércoles de Ceniza
        // $this->otrasFechasCalculadas(-48);		// Lunes de Carnaval Barranquilla
        // $this->otrasFechasCalculadas(-47);		// Martes de Carnaval Barranquilla

        /* Agregamos los festivos ingresados a la DB */
    }

    protected function calcula_emiliani($mes_festivo, $dia_festivo) {
        // funcion que mueve una fecha diferente a lunes al siguiente lunes en el
        // calendario y se aplica a fechas que estan bajo la ley emiliani
        //global  $y,$dia_festivo,$mes_festivo,$festivo;
        // Extrae el dia de la semana
        // 0 Domingo  6 Sábado
        $dd = date("w", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano));
        switch ($dd) {
            case 0:                                    // Domingo
                $dia_festivo = $dia_festivo + 1;
                break;
            case 2:                                    // Martes.
                $dia_festivo = $dia_festivo + 6;
                break;
            case 3:                                    // Miércoles
                $dia_festivo = $dia_festivo + 5;
                break;
            case 4:                                     // Jueves
                $dia_festivo = $dia_festivo + 4;
                break;
            case 5:                                     // Viernes
                $dia_festivo = $dia_festivo + 3;
                break;
            case 6:                                     // Sábado
                $dia_festivo = $dia_festivo + 2;
                break;
        }
        $mes = date("n", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano)) + 0;
        $dia = date("d", mktime(0, 0, 0, $mes_festivo, $dia_festivo, $this->ano)) + 0;
        $this->festivos[$this->ano][$mes][$dia] = true;
    }

    protected function otrasFechasCalculadas($cantidadDias = 0, $siguienteLunes = false) {
        $mes_festivo = date("n", mktime(0, 0, 0, $this->pascua_mes, $this->pascua_dia + $cantidadDias, $this->ano));
        $dia_festivo = date("d", mktime(0, 0, 0, $this->pascua_mes, $this->pascua_dia + $cantidadDias, $this->ano));

        if ($siguienteLunes) {
            $this->calcula_emiliani($mes_festivo, $dia_festivo);
        } else {
            $this->festivos[$this->ano][$mes_festivo + 0][$dia_festivo + 0] = true;
        }
    }

    public function esFestivo($dia, $mes, $ano) {
        //echo (int)$mes;
//        if ($dia === '' or $mes === '') {
//            return FALSE;
//        }
//        if ($ano === '') {
//            $ano = $this->ano;
//        }
        $this->getFestivos($ano);
        if (isset($this->festivos[$ano][(int) $mes][(int) $dia])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //***************************************************************************************//
    //*****************FUNCION QUE HALLA DIAS HABILES ENTRE DOS FECHAS***********************//

    /**
     * function getDiasHabiles
     * 
     * Funcion que encuentra en numero de dias habiles entre dos fechas
     *
     * @param timestamp $fechainicio fecha inicial
     * @param timestamp $fechafin fecha final
     * @param array $diasferiados dias festivos $feriados
     * @return int  
     */
    public function getDiasHabiles($fechainicio, $fechafin, $year = null) {
        // Convirtiendo en timestamp las fechas
        $fechainicio = strtotime($fechainicio);
        $fechafin = strtotime($fechafin);
        $this->festivos_function($year);

        // Incremento en 1 dia
        $diainc = 24 * 60 * 60;

        // Arreglo de dias habiles, inicianlizacion
        $diashabiles = array();

        // Se recorre desde la fecha de inicio a la fecha fin, incrementando en 1 dia
        for ($midia = $fechainicio; $midia <= $fechafin; $midia += $diainc) {
            // Si el dia indicado, no es sabado o domingo es habil
            if (!in_array(date('N', $midia), array(6, 7))) {
                // Si no es un dia feriado entonces es habil
                if ($this->esFestivo(date('d', $midia), date('n', $midia), date('Y', $midia)) === FALSE) {
                    array_push($diashabiles, date('Y-n-d', $midia));
                }
            }
        }

        if (count($diashabiles) == NULL) {
            return array(1);
        } else {
            return $diashabiles;
        }
    }

}
