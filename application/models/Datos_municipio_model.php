<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_municipio_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_nombre_escudo_municipio() {

        $this->db->select('municipio_informacion_general_nombre, municipio_informacion_general_departamento,municipio_informacion_general_departamento_escudo,municipio_informacion_general_escudo');
        $query = $this->db->get('municipio_informacion_general');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }


    function obtener_informacion_general() {
        $query = $this->db->get('municipio_informacion_general');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_geografia() {
        $query = $this->db->get('municipio_informacion_geografia');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_sectores() {
        $query = $this->db->get('municipio_informacion_sectores');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_celebraciones() {
        $query = $this->db->get('municipio_celebraciones');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_galeria_mapas() {
        $query = $this->db->get('municipio_galeria_mapas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_barrios() {
        $this->db->where('municipio_territorios_tipo', 'Barrio');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_comunas() {
        $this->db->where('municipio_territorios_tipo', 'Comuna');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_corregimientos() {
        $this->db->where('municipio_territorios_tipo', 'Corregimiento');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_resguardos() {
        $this->db->where('municipio_territorios_tipo', 'Resguardo');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_urbanizaciones() {
        $this->db->where('municipio_territorios_tipo', 'Urbanización');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorios_veredas() {
        $this->db->where('municipio_territorios_tipo', 'Vereda');
        $this->db->order_by('municipio_territorios_nombre', 'asc');
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_territorio($id) {
        $this->db->where('id_municipio_territorios', $id);
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_sitios_interes() {
        $query = $this->db->get('municipio_sitios_interes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_directorio_turistico() {
        $this->db->order_by('municipio_directorio_turistico_clasificacion', 'asc');
        $query = $this->db->get('municipio_directorio_turistico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_directorio_turistico_unico($id) {
        $this->db->where('id_municipio_directorio_turistico', $id);
        $query = $this->db->get('municipio_directorio_turistico');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_audios() {
        $query = $this->db->get('municipio_galeria_audios');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }
      function obtener_videos() {
        $query = $this->db->get('municipio_galeria_videos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }
      function obtener_imagenes() {
        $query = $this->db->get('municipio_galeria_imagenes');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_documentos() {
        $this->db->order_by('municipio_documentos_nombre', 'asc');
        $query = $this->db->get('municipio_documentos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}

?>
