<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_normatividad_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_nomatividad_leyes($searh = NULL, $tipo = NULL) {

        if ($searh != NULL) {

            switch ($tipo) {
                case 'por_nombre':
                    $this->db->like('normatividad_leyes_nombre', $searh);
                    break;
                case 'por_tipo':
                    $this->db->like('normatividad_leyes_tipo', $searh);
                    break;
                case 'por_tematica':
                    $this->db->like('normatividad_leyes_tematica', $searh);
                    break;
                case 'por_fecha':
                    $this->db->like('normatividad_leyes_fecha_expedicion ', $searh);
                    break;
                default :
                    $this->db->like('normatividad_leyes_nombre', $searh);
                    $this->db->or_like('normatividad_leyes_tipo', $searh);
                    $this->db->or_like('normatividad_leyes_tematica', $searh);
                    $this->db->or_like('normatividad_leyes_fecha_expedicion ', $searh);
                    break;
            }
        }
        $this->db->order_by('normatividad_leyes_fecha_expedicion', 'desc');
        $query = $this->db->get('normatividad_leyes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_normatividad_ley($id) {
        $this->db->where('id_normatividad_leyes', $id);
        $query = $this->db->get('normatividad_leyes');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_nomatividad_decretos($searh = NULL, $tipo = NULL) {

        if ($searh != NULL) {

            switch ($tipo) {
                case 'por_nombre':
                    $this->db->like('normatividad_decretos_nombre', $searh);
                    break;
                case 'por_tematica':
                    $this->db->like('normatividad_decretos_tematica', $searh);
                    break;
                case 'por_fecha':
                    $this->db->like('normatividad_decretos_fecha_expedicion ', $searh);
                    break;
                default :
                    $this->db->like('normatividad_decretos_nombre', $searh);
                    $this->db->or_like('normatividad_decretos_tematica', $searh);
                    $this->db->or_like('normatividad_decretos_fecha_expedicion ', $searh);
                    break;
            }
        }
        $this->db->order_by('normatividad_decretos_fecha_expedicion', 'desc');
        $query = $this->db->get('normatividad_decretos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_normatividad_decreto($id) {
        $this->db->where('id_normatividad_decretos', $id);
        $query = $this->db->get('normatividad_decretos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_nomatividad_resoluciones($searh = NULL, $tipo = NULL) {

        if ($searh != NULL) {

            switch ($tipo) {
                case 'por_nombre':
                    $this->db->like('normatividad_resoluciones_nombre', $searh);
                    break;
                case 'por_tipo':
                    $this->db->like('normatividad_resoluciones_tipo', $searh);
                    break;
                case 'por_tematica':
                    $this->db->like('normatividad_resoluciones_tematica', $searh);
                    break;
                case 'por_fecha':
                    $this->db->like('normatividad_resoluciones_fecha_expedicion ', $searh);
                    break;
                default :
                    $this->db->like('normatividad_resoluciones_nombre', $searh);
                    $this->db->or_like('normatividad_resoluciones_tipo', $searh);
                    $this->db->or_like('normatividad_resoluciones_tematica', $searh);
                    $this->db->or_like('normatividad_resoluciones_fecha_expedicion ', $searh);
                    break;
            }
        }
        $this->db->order_by('normatividad_resoluciones_fecha_expedicion', 'desc');
        $query = $this->db->get('normatividad_resoluciones');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_normatividad_resolucion($id) {
        $this->db->where('id_normatividad_resoluciones', $id);
        $query = $this->db->get('normatividad_resoluciones');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_nomatividad_politicas($searh = NULL, $tipo = NULL) {

        if ($searh != NULL) {

            switch ($tipo) {
                case 'por_nombre':
                    $this->db->like('normatividad_politicas_nombre', $searh);
                    break;
                case 'por_tipo':
                    $this->db->like('normatividad_politicas_tipo', $searh);
                    break;
                case 'por_tematica':
                    $this->db->like('normatividad_politicas_tematica', $searh);
                    break;
                case 'por_fecha':
                    $this->db->like('normatividad_politicas_fecha_expedicion ', $searh);
                    break;
                default :
                    $this->db->like('normatividad_politicas_nombre', $searh);
                    $this->db->or_like('normatividad_politicas_tipo', $searh);
                    $this->db->or_like('normatividad_politicas_tematica', $searh);
                    $this->db->or_like('normatividad_politicas_fecha_expedicion ', $searh);
                    break;
            }
        }
        $this->db->order_by('normatividad_politicas_fecha_expedicion', 'desc');
        $query = $this->db->get('normatividad_politicas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_normatividad_politica($id) {
        $this->db->where('id_normatividad_politicas', $id);
        $query = $this->db->get('normatividad_politicas');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }
    
        function obtener_nomatividad_edictos() {

        $this->db->order_by('normatividad_edictos_fecha_expedicion', 'desc');
        $query = $this->db->get('normatividad_edictos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_normatividad_edicto($id) {
        $this->db->where('id_normatividad_edictos', $id);
        $query = $this->db->get('normatividad_edictos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

}


