<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Referenciales_nodo_model
 *
 * Clase donde reposan las sentencias de bases de datos utilizadas para la clase Login, donde se autentica
 * y se ingresa al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class App_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_aseguramiento() {
        $query = $this->db->get('aseguramiento');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function update_aseguramiento($data) {
        $this->db->where('id_aseguramiento', 1);
        $this->db->update('aseguramiento', $data);
    }

    function limipiar_tablas() {
        $this->limipiar_aseguramiento_clientes();
        $this->limipiar_entidades();
        $this->limipiar_fec_actualiza();
        return TRUE;
    }

    function limipiar_aseguramiento_clientes() {
        $this->db->empty_table("aseguramiento_pacientes");
    }

    function limipiar_entidades() {
        $this->db->empty_table("entidades");
    }

    function limipiar_fec_actualiza() {
        $this->db->empty_table("fec_actualiza");
    }

    function guardar_importacion($data, $table) {
        $this->db->insert_batch($table, $data);
    }

    function get_paciente($id) {
        $this->db->join('tipo_identificacion a', 'a.id_tipo_identificacion= paciente.id_tipo_identificacion', 'left');
        $this->db->select('a.tipo_identificacion_nombre as tipo_identificacion', FALSE);
        $this->db->join('ciudad b', 'b.id_ciudad= paciente.id_ciudad', 'left');
        $this->db->select('CONCAT(b.ciudad_pais_nombre," / ",b.ciudad_departamento_nombre," / ",b.ciudad_nombre) as ciudad', FALSE);
        $this->db->select('paciente.*', FALSE);
        $this->db->where('paciente_identificacion', $id);
        $query = $this->db->get('paciente');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_paciente_id($id) {
        $this->db->join('tipo_identificacion a', 'a.id_tipo_identificacion= paciente.id_tipo_identificacion', 'left');
        $this->db->select('a.tipo_identificacion_nombre as tipo_identificacion', FALSE);
        $this->db->join('ciudad b', 'b.id_ciudad= paciente.id_ciudad', 'left');
        $this->db->select('CONCAT(b.ciudad_pais_nombre," / ",b.ciudad_departamento_nombre," / ",b.ciudad_nombre) as ciudad', FALSE);
        $this->db->select('paciente.*', FALSE);
        $this->db->where('id_paciente', $id);
        $query = $this->db->get('paciente');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function update_paciente($id, $data) {
        $this->db->where('id_paciente', $id);
        $this->db->update('paciente', $data);
    }

    function guardar_paciente($datos) {
        $this->db->insert('paciente', $datos);
    }

    function get_tipo_identificacion() {
        $query = $this->db->get('tipo_identificacion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_paises() {
        $this->db->distinct();
        $this->db->query('SELECT * FROM ciudad ORDER BY FIELD(ciudad_nombre, "Colombia") ASC');
        $q = $this->db->get('ciudad');
        return $q->result_array();
    }

    function departamentos() {
        $this->db->distinct();
        $this->db->order_by("ciudad_departamento_nombre", "asc");
        $this->db->select('ciudad_departamento_nombre,ciudad_id_departamento');
        $q = $this->db->get('ciudad');
        return $q;
    }

    function departamentos_pais($id) {
        $this->db->distinct();
        $this->db->where('ciudad_id_pais', $id);
        $this->db->order_by("ciudad_departamento_nombre", "asc");
        $this->db->select('ciudad_departamento_nombre,ciudad_id_departamento');
        $q = $this->db->get('ciudad');
        return $q;
    }

    function municipios($id) {
        $this->db->where('ciudad_id_departamento', $id);
        $this->db->select('ciudad_nombre,id_ciudad');
        $q = $this->db->get('ciudad');
        return $q;
    }

    function get_municipios() {
        $query = $this->db->get('ciudad');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    function departamentosporpais($id) {
        $this->db->where('ciudad_id_departamento', $id);
        // $this->db->select('ciudad_nombre,id_ciudad');
        $q = $this->db->get('ciudad');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return FALSE;
        }
    }

    function municipiosporciudad($id) {
        $this->db->where('id_ciudad', $id);
        // $this->db->select('ciudad_nombre,id_ciudad');
        $q = $this->db->get('ciudad');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return FALSE;
        }
    }

    function get_ciudad($id) {
        $this->db->where('id_ciudad', $id);
        $q = $this->db->get('ciudad');
        if ($q->num_rows() == 1) {
            return $q->row();
        } else {
            return FALSE;
        }
    }

    function get_departamentos($id) {
        $this->db->where('ciudad_id_pais', $id);
        $q = $this->db->get('ciudad');
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return FALSE;
        }
    }

    function get_ciudades($id) {
        $this->db->where('ciudad_id_departamento', $id);
        $q = $this->db->get('ciudad');
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return FALSE;
        }
    }

    function obtener_ciudad_valor($id) {
        $this->db->where('id_ciudad', $id);
        $this->db->select('ciudad_nombre,ciudad_departamento_nombre');
        $query = $this->db->get('ciudad');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_servicios() {
        $this->db->where('id_estado', 1);
        $query = $this->db->get('servicios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_sedes() {
        $this->db->where('id_estado', 1);
        $query = $this->db->get('sedes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function check_setup($servicio, $sede, $start, $end, $id = NULL) {
        $this->db->where('id_servicios', $servicio);
        $this->db->where('id_sedes', $sede);
        if ($id != NULL) {
            $this->db->where('id_setup_servicios_x_sedes !=', $id);
        }
        $this->db->group_start();
        $this->db->group_start();
        $this->db->where('setup_servicios_x_sedes_fecha_inicio <=', $start);
        $this->db->where('setup_servicios_x_sedes_fecha_final >=', $start);
        $this->db->group_end();
        $this->db->or_group_start();
        $this->db->where('setup_servicios_x_sedes_fecha_inicio <=', $end);
        $this->db->where('setup_servicios_x_sedes_fecha_final >=', $end);
        $this->db->group_end();
        $this->db->group_end();
        $query = $this->db->get('setup_servicios_x_sedes');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function guardar_setup($datos) {
        $this->db->insert('setup_servicios_x_sedes', $datos);
    }

    function get_setup() {
        $this->db->join('servicios a', 'a.id_servicios= setup_servicios_x_sedes.id_servicios', 'left');
        $this->db->select('a.servicios_nombre as servicio', FALSE);
        $this->db->join('sedes b', 'b.id_sedes= setup_servicios_x_sedes.id_sedes', 'left');
        $this->db->select('b.sedes_nombre as sede', FALSE);
        $this->db->select('setup_servicios_x_sedes.*', FALSE);
        $this->db->order_by('setup_servicios_x_sedes.id_setup_servicios_x_sedes', 'DESC');
        $query = $this->db->get('setup_servicios_x_sedes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_un_setup($id) {
        $this->db->join('servicios a', 'a.id_servicios= setup_servicios_x_sedes.id_servicios', 'left');
        $this->db->select('a.servicios_nombre as servicio', FALSE);
        $this->db->join('sedes b', 'b.id_sedes= setup_servicios_x_sedes.id_sedes', 'left');
        $this->db->select('b.sedes_nombre as sede', FALSE);
        $this->db->select('setup_servicios_x_sedes.*', FALSE);
        $this->db->where('setup_servicios_x_sedes.id_setup_servicios_x_sedes', $id);
        $query = $this->db->get('setup_servicios_x_sedes');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_setup_by_sede_servicio($sede, $servicio) {
        $this->db->where('setup_servicios_x_sedes_fecha_inicio <=', date('Y-m-d'));
        $this->db->where('setup_servicios_x_sedes_fecha_final >=', date('Y-m-d'));
        $this->db->where('id_sedes', $sede);
        $this->db->where('id_servicios', $servicio);
        $query = $this->db->get('setup_servicios_x_sedes');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function delete_setup($id) {
        $this->db->delete('setup_servicios_x_sedes', array('id_setup_servicios_x_sedes' => $id));
        return TRUE;
    }

    function update_setup($id, $data) {
        $this->db->where('id_setup_servicios_x_sedes', $id);
        $this->db->update('setup_servicios_x_sedes', $data);
    }

    function get_citas_setup($id) {
        $this->db->where('id_setup_servicios_x_sedes', $id);
        $query = $this->db->get('citas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_citas_paciente($id, $sede = NULL, $servicio = NULL) {
        if ($sede != FALSE) {
            $this->db->where('a.id_sedes', $sede);
        }
        if ($servicio != FALSE) {
            $this->db->where('a.id_servicios', $servicio);
        }
        $this->db->where('citas.id_paciente', $id);
        $this->db->where('citas.citas_dia >=', date('Y-m-d'));
        $this->db->join('setup_servicios_x_sedes a', 'a.id_setup_servicios_x_sedes= citas.id_setup_servicios_x_sedes', 'left');
        $this->db->select('a.* ', FALSE);
        $this->db->join('sedes b', 'b.id_sedes= a.id_sedes', 'left');
        $this->db->select('b.sedes_nombre as sede ', FALSE);
        $this->db->join('servicios c', 'c.id_servicios= a.id_servicios', 'left');
        $this->db->select('c.servicios_nombre as servicio ', FALSE);
        $this->db->select('citas.*', FALSE);
        $query = $this->db->get('citas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_un_cita($id) {
        $this->db->where('id_citas', $id);
        $query = $this->db->get('citas');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_citas() {

        $this->db->where('citas.citas_dia >=', date('Y-m-d'));
        $this->db->join('setup_servicios_x_sedes a', 'a.id_setup_servicios_x_sedes= citas.id_setup_servicios_x_sedes', 'left');
        $this->db->select('a.* ', FALSE);
        $this->db->join('sedes b', 'b.id_sedes= a.id_sedes', 'left');
        $this->db->select('b.sedes_nombre as sede ', FALSE);
        $this->db->join('servicios c', 'c.id_servicios= a.id_servicios', 'left');
        $this->db->select('c.servicios_nombre as servicio ', FALSE);
        $this->db->join('paciente d', 'd.id_paciente= citas.id_paciente', 'left');
        $this->db->select('d.*', FALSE);
        $this->db->select('citas.*', FALSE);
        $query = $this->db->get('citas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function guardar_cita($datos) {
        $this->db->insert('citas', $datos);
    }

    function check_cita_hora($id, $dia, $hora) {
        $this->db->where('id_setup_servicios_x_sedes', $id);
        $this->db->where('citas_dia', $dia);
        $this->db->where('citas_hora', $hora);
        $query = $this->db->get('citas');
        return $query->num_rows();
    }

    function delete_cita($id) {
        $this->db->delete('citas', array('id_citas' => $id));
        return TRUE;
    }

    function update_cita($id, $data) {
        $this->db->where('id_citas', $id);
        $this->db->update('citas', $data);
    }

    function get_historico_citas() {

        $this->db->join('setup_servicios_x_sedes a', 'a.id_setup_servicios_x_sedes= citas.id_setup_servicios_x_sedes', 'left');
        $this->db->select('a.* ', FALSE);
        $this->db->join('sedes b', 'b.id_sedes= a.id_sedes', 'left');
        $this->db->select('b.sedes_nombre as sede ', FALSE);
        $this->db->join('servicios c', 'c.id_servicios= a.id_servicios', 'left');
        $this->db->select('c.servicios_nombre as servicio ', FALSE);
        $this->db->join('paciente d', 'd.id_paciente= citas.id_paciente', 'left');
        $this->db->select('d.*', FALSE);
        $this->db->select('citas.*', FALSE);
        $query = $this->db->get('citas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
