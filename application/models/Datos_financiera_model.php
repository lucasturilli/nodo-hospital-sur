<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_financiera_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_presupuesto() {
        $this->db->join('normatividad_leyes a', 'a.id_normatividad_leyes = financiera_presupuesto.id_normatividad_leyes', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('financiera_presupuesto.*', FALSE);
        $this->db->where('financiera_presupuesto.financiera_presupuesto_periodo', date('Y'));
        $this->db->limit(1);
        $query = $this->db->get('financiera_presupuesto');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_presupuesto_ejecucion($year) {
        $this->db->join('financiera_presupuesto a', 'a.id_financiera_presupuesto = financiera_presupuesto_ejecucion.id_financiera_presupuesto', 'left');
        $this->db->select('a.financiera_presupuesto_periodo AS periodo', FALSE);
        $this->db->select('financiera_presupuesto_ejecucion.*', FALSE);
        $this->db->where('a.financiera_presupuesto_periodo', $year);
        $this->db->order_by('financiera_presupuesto_ejecucion.financiera_presupuesto_ejecucion_trimestre', 'asc');
        $query = $this->db->get('financiera_presupuesto_ejecucion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_presupuesto_historico() {
        $this->db->join('normatividad_leyes a', 'a.id_normatividad_leyes = financiera_presupuesto.id_normatividad_leyes', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('financiera_presupuesto.*', FALSE);
        $this->db->where('financiera_presupuesto.financiera_presupuesto_periodo !=', date('Y'));
        $query = $this->db->get('financiera_presupuesto');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_estatuto_tributario() {
        $query = $this->db->get('financiera_estatuto_tributario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_estatuto_tributario_periodo() {
        $this->db->distinct('financiera_estatuto_tributario_periodo');
        $this->db->group_by("financiera_estatuto_tributario_periodo");
        $this->db->order_by('financiera_estatuto_tributario_periodo', 'DESC');
        $query = $this->db->get('financiera_estatuto_tributario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_historico_presupuesto() {
        $this->db->order_by('financiera_historico_presupuesto_periodo', 'desc');
        $query = $this->db->get('financiera_historico_presupuesto');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_estados_financieros() {
        $this->db->order_by('financiera_estados_financieros_periodo', 'desc');
        $query = $this->db->get('financiera_estados_financieros');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_estados_financieros() {
        $this->db->distinct('financiera_estados_financieros_periodo');
        $this->db->group_by("financiera_estados_financieros_periodo");
        $this->db->order_by('financiera_estados_financieros_periodo', 'DESC');
        $query = $this->db->get('financiera_estados_financieros');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
