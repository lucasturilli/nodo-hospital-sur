<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_contratacion_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_plan_compras() {
        $this->db->order_by('contratacion_plan_compras_periodo', 'desc');
        $query = $this->db->get('contratacion_plan_compras');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_plan_compras_avances($id) {
        $this->db->join('contratacion_plan_compras a', 'a.id_contratacion_plan_compras= contratacion_plan_compras_ejecucion.id_contratacion_plan_compras', 'left');
        $this->db->select('a.contratacion_plan_compras_periodo AS periodo', FALSE);
        $this->db->select('contratacion_plan_compras_ejecucion.*', FALSE);
        $this->db->where('contratacion_plan_compras_ejecucion.id_contratacion_plan_compras', $id);
        $query = $this->db->get('contratacion_plan_compras_ejecucion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_secop() {
        $query = $this->db->get('contratacion_contratos_secop');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_contratos() {
        $this->db->order_by('contratacion_contratos_fecha_publicacion', 'desc');
        $query = $this->db->get('contratacion_contratos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_avance_contractual() {
        $this->db->distinct('contratacion_avance_contractual_periodo');
        $this->db->group_by("contratacion_avance_contractual_periodo");
        $this->db->order_by('contratacion_avance_contractual_periodo', 'DESC');
        $query = $this->db->get('contratacion_avance_contractual');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_avance_contractual() {
        $query = $this->db->get('contratacion_avance_contractual');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_convenios() {
        $this->db->order_by('contratacion_convenios_fecha_publicacion', 'desc');
        $query = $this->db->get('contratacion_convenios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
       function obtener_lineamientos() {
        $query = $this->db->get('contratacion_lineamientos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
