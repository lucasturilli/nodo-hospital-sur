<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Login_model
 *
 * Clase donde reposan las sentencias de bases de datos utilizadas para la clase Login, donde se autentica
 * y se ingresa al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Funcion que verifica que el usuario si exista en la base de datos 

    function validar($id) {
        $this->db->where('administrador_identificacion', $id);
        $query = $this->db->get('administrador');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function update_log($datos) {
        $data = array(
            'audit_administrador_nombre' => $datos->administrador_nombres . ' ' . $datos->administrador_apellidos . ' - ' . $datos->administrador_identificacion,
            'audit_administrador_fecha' => date('Y-m-d H:i:s'),
            'audit_administrador_ip' => $_SERVER['REMOTE_ADDR'],
            'audit_administrador_agente' => $_SERVER['HTTP_USER_AGENT']);
        $this->db->insert('audit_administrador', $data);
    }

}
