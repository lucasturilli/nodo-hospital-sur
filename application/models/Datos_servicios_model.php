<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_servicios_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_servicios($searh = NULL) {

        if ($searh != NULL) {

            $this->db->like('servicios_tramites_nombre', $searh);
            $this->db->or_like('servicios_tramites_descripcion', $searh);
        }
        $this->db->order_by('servicios_tramites_nombre', 'asc');
        $query = $this->db->get('servicios_tramites');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_servicio($id) {
        $this->db->where('id_servicios_tramites', $id);
        $query = $this->db->get('servicios_tramites');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_pqrdf() {
        $this->db->order_by('servicios_pqrdf_periodo', 'desc');
        $query = $this->db->get('servicios_pqrdf');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_pqrdf_anexos($id) {
        $this->db->where('id_servicios_pqrdf',$id);
        $query = $this->db->get('servicios_pqrdf_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_inventario() {
        $query = $this->db->get('servicios_inventario');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_formularios() {
        $query = $this->db->get('servicios_formatos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_certificados() {
        $query = $this->db->get('servicios_certificados');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_tramites_en_linea($tipo = NULL) {
        $this->db->join('entidad_dependencia a', 'a.id_entidad_dependencia = servicios_tramites_en_linea.id_entidad_dependencia', 'left');
        $this->db->select('a.entidad_dependencia_nombre AS dependencia', FALSE);
        $this->db->select('servicios_tramites_en_linea.*', FALSE);
        if ($tipo != NULL) {
            $this->db->where('servicios_tramites_en_linea_tipo', $tipo);
        }
        $query = $this->db->get('servicios_tramites_en_linea');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_tramites_en_linea_dependencias() {
        $this->db->group_by('id_entidad_dependencia');
        $query = $this->db->get('servicios_tramites_en_linea');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_tramite_en_linea($id) {
        $this->db->where('id_servicios_tramites_en_linea', $id);
        $query = $this->db->get('servicios_tramites_en_linea');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

}

?>
