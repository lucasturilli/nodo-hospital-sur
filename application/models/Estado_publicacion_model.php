<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Estado_publicacion_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // 1 no hay registros // 2 ok // 3 Faltan campos // 4 no hay avances // 5 vencidos 

    function entidad_informacion() {
        $query = $this->db->get('entidad_informacion');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_contacto() {
        $query = $this->db->get('entidad_contacto');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_politicas_datos() {
        $query = $this->db->get('entidad_politicas_datos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_redes_sociales() {
        $query = $this->db->get('entidad_redes_sociales');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_dependencia() {
        $query = $this->db->get('entidad_dependencia');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_sucursales() {
        $query = $this->db->get('entidad_sucursales');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_entidades() {
        $query = $this->db->get('entidad_entidades');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_agremiaciones() {
        $query = $this->db->get('entidad_agremiaciones');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_preguntas_frecuentes() {
        $query = $this->db->get('entidad_preguntas_frecuentes');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_glosario() {
        $query = $this->db->get('entidad_glosario');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_noticias() {
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_calendario() {
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_ofertas_empleo() {
        $query = $this->db->get('entidad_ofertas_empleo');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_boletines() {
        $query = $this->db->get('entidad_boletines');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_periodico() {
        $query = $this->db->get('entidad_periodico');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function convocatorias() {
        $query = $this->db->get('convocatorias');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function estudios() {
        $query = $this->db->get('estudios');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function informacion_adicional() {
        $query = $this->db->get('informacion_adicional');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_funcionario() {
        $query = $this->db->get('entidad_funcionario');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_manual_funciones() {
        $query = $this->db->get('entidad_manual_de_funciones');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_asignaciones_salarial() {
        $query = $this->db->get('entidad_asignaciones_salarial');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_planta_personal() {
        $query = $this->db->get('entidad_planta_personal');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_evaluacion_desempeno() {
        $query = $this->db->get('entidad_evaluacion_desempeno');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_informacion_ninos() {
        $query = $this->db->get('entidad_informacion_ninos');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_noticias_ninos() {
        $query = $this->db->get('entidad_noticias_ninos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_calendario_ninos() {
        $query = $this->db->get('entidad_noticias_ninos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_juegos_ninos() {
        $query = $this->db->get('entidad_juegos_ninos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_sitios_ninos() {
        $query = $this->db->get('entidad_sitios_recomendados_ninos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function normatividad_leyes() {
        $query = $this->db->get('normatividad_leyes');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function normatividad_decretos() {
        $query = $this->db->get('normatividad_decretos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function normatividad_resoluciones() {
        $query = $this->db->get('normatividad_resoluciones');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function normatividad_edictos() {
        $query = $this->db->get('normatividad_edictos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function financiera_presupuesto() {
        $query = $this->db->get('financiera_presupuesto');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function financiera_presupuesto_ejecucion() {
        $query = $this->db->get('financiera_presupuesto');
        $query_2 = $this->db->get('financiera_presupuesto_ejecucion');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_financiera_presupuesto == $row_2->id_financiera_presupuesto) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function financiera_estados_financieros() {
        $query = $this->db->get('financiera_estados_financieros');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function financiera_estatuto_tributario() {
        $query = $this->db->get('financiera_estatuto_tributario');
         if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function normatividad_politicas() {
        $query = $this->db->get('normatividad_politicas');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function anticorrupcion_definicion() {
        $query = $this->db->get('anticorrupcion_definicion');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_plan_estrategico() {
        $query = $this->db->get('control_plan_estrategico');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function control_plan_accion() {
        $query = $this->db->get('control_plan_accion');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_plan_accion_avances() {
        $query = $this->db->get('control_plan_accion');
        $query_2 = $this->db->get('control_plan_accion_avances');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_control_plan_accion == $row_2->id_control_plan_accion) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_programas() {
        $query = $this->db->get('control_programas');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_programas_avances() {
        $query = $this->db->get('control_programas');
        $query_2 = $this->db->get('control_programas_avances');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_control_programas == $row_2->id_control_programas) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_indicadores_gestion() {
        $query = $this->db->get('control_indicadores_gestion');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_planes_otros() {
        $query = $this->db->get('control_planes_otros');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_empalme() {
        $query = $this->db->get('control_informe_empalme');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_empalme_anexos() {
        $query = $this->db->get('control_informe_empalme');
        $query_2 = $this->db->get('control_informe_empalme_anexos');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_control_informe_empalme == $row_2->id_control_informe_empalme) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_archivo() {
        $query = $this->db->get('control_informe_archivo');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function planeacion_plan_gasto_publico() {
        $query = $this->db->get('planeacion_plan_gasto_publico');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_entes() {
        $query = $this->db->get('control_entes');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_auditorias() {
        $query = $this->db->get('control_auditorias');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_contraloria() {
        $query = $this->db->get('control_informe_contraloria');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function control_informe_concejo() {
        $query = $this->db->get('control_informe_concejo');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_ciudadania() {
        $query = $this->db->get('control_informe_ciudadania');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_informe_fiscal() {
        $query = $this->db->get('control_informe_fiscal');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_planes_mejoramiento() {
        $query = $this->db->get('control_planes_mejoramiento');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_reportes_control_interno() {
        $query = $this->db->get('control_reportes_control_interno');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_poblacion_vulnerable() {
        $query = $this->db->get('control_poblacion_vulnerable');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function control_poblacion_vulnerable_programas() {
        $query = $this->db->get('control_poblacion_vulnerable_programas');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_poblacion_vulnerable_programas_avances() {
        $query = $this->db->get('control_poblacion_vulnerable_programas');
        $query_2 = $this->db->get('control_poblacion_vulnerable_programas_avances');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_control_poblacion_vulnerable_programas == $row_2->id_control_poblacion_vulnerable_programas) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_poblacion_vulnerable_proyectos() {
        $query = $this->db->get('control_poblacion_vulnerable_proyectos');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_poblacion_vulnerable_proyectos_avances() {
        $query = $this->db->get('control_poblacion_vulnerable_proyectos');
        $query_2 = $this->db->get('control_poblacion_vulnerable_proyectos_avances');
        if ($query->num_rows() > 0 && $query_2->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $flag = 4;
                foreach ($query_2->result() as $row_2) {
                    if ($row->id_control_poblacion_vulnerable_proyectos == $row_2->id_control_poblacion_vulnerable_proyectos) {
                        $flag = 2;
                    }
                }
                if ($flag == 4) {
                    return array($flag, $query_2->num_rows());
                    break;
                }
            }
            return array($flag, $query_2->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_programas_sociales() {
        $query = $this->db->get('control_programas_sociales');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_defensa_judicial() {
        $query = $this->db->get('control_defensa_judicial');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function control_otros_informes() {
        $query = $this->db->get('control_otros_informes');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function contratacion_plan_compras() {
        $query = $this->db->get('contratacion_plan_compras');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function contratacion_contratos_secop() {
        $query = $this->db->get('contratacion_contratos_secop');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function contratacion_contratos() {
        $query = $this->db->get('contratacion_contratos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function contratacion_avance_contractual() {
        $query = $this->db->get('contratacion_avance_contractual');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function contratacion_convenios() {
        $query = $this->db->get('contratacion_convenios');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function contratacion_lineamientos() {
        $query = $this->db->get('contratacion_lineamientos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function servicios_tramites() {
        $query = $this->db->get('servicios_tramites');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function servicios_tramites_en_linea() {
        $query = $this->db->get('servicios_tramites_en_linea');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function servicios_formatos() {
        $query = $this->db->get('servicios_formatos');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function servicios_certificados() {
        $query = $this->db->get('servicios_certificados');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function servicios_pqrdf() {
        $query = $this->db->get('servicios_pqrdf');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_calidad() {
        $query = $this->db->get('entidad_calidad');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_caracterizacion_procesos() {
        $query = $this->db->get('entidad_caracterizacion_procesos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function entidad_procedimientos() {
        $query = $this->db->get('entidad_procedimientos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function catalogo_datos_abiertos() {
        $query = $this->db->get('catalogo_datos_abiertos');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function archivos_datos_abiertos() {
        $query = $this->db->get('archivos_datos_abiertos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function mapas_datos_abiertos() {
        $query = $this->db->get('mapas_datos_abiertos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function registro_activos_informacion() {
        $query = $this->db->get('registro_activos_informacion');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function informacion_clasificada_reservada() {
        $query = $this->db->get('informacion_clasificada_reservada');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function esquema_publicacion() {
        $query = $this->db->get('esquema_publicacion');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function entidad_documentos() {
        $query = $this->db->get('entidad_documentos');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_informacion_general() {
        $query = $this->db->get('municipio_informacion_general');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function municipio_informacion_geografica() {
        $query = $this->db->get('municipio_informacion_geografia');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function municipio_informacion_sectores() {
        $query = $this->db->get('municipio_informacion_sectores');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_sitios_interes() {
        $query = $this->db->get('municipio_sitios_interes');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_galeria_imagenes() {
        $query = $this->db->get('municipio_galeria_imagenes');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function municipio_galeria_videos() {
        $query = $this->db->get('municipio_galeria_videos');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function municipio_galeria_audios() {
        $query = $this->db->get('municipio_galeria_audios');
        if ($query->num_rows() == 1) {
            $query = $query->row();
            $flag = 2;
            foreach ($query as $dato) {
                if ($dato == NULL) {
                    $flag = 3;
                }
            }
            return array($flag, 1);
        } else {
            return array(1, 0);
        }
    }

    function municipio_galeria_mapas() {
        $query = $this->db->get('municipio_galeria_mapas');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_directorio_turistico() {
        $query = $this->db->get('municipio_directorio_turistico');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_celebraciones() {
        $query = $this->db->get('municipio_celebraciones');
        if ($query->num_rows() > 0) {
            $flag = 2;
            foreach ($query->result() as $row) {
                foreach ($row as $dato) {
                    if ($dato == NULL) {
                        $flag = 3;
                    }
                }
            }
            return array($flag, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

    function municipio_territorios() {
        $query = $this->db->get('municipio_territorios');
        if ($query->num_rows() > 0) {
            return array(2, $query->num_rows());
        } else {
            return array(1, 0);
        }
    }

}
