<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Referenciales_nodo_model
 *
 * Clase donde reposan las sentencias de bases de datos utilizadas para la clase Login, donde se autentica
 * y se ingresa al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Aplicaciones_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update_audit($array, $key) {

        $last_query = $this->db->last_query();
        $porciones = explode(" ", str_replace(array("\r", "\n"), " ", $last_query));
        if ($porciones[0] == 'DELETE') {
            $key = $array;
            $data = json_encode($array);
        } else {

            $data = json_encode($array);
        }
        if ($porciones[0] == 'UPDATE') {
            $table = str_replace("`", "", $porciones[1]);
        } else {
            $table = str_replace("`", "", $porciones[2]);
        }

        $user_logs_insert = array(
            "audit_fecha" => date('Y-m-d H:i:s'),
            "audit_query" => $last_query,
            "audit_data" => $data,
            "audit_type" => $porciones[0],
            "audit_table" => $table,
            "audit_key_in_table" => $key,
            "audit_id_usuario" => $this->session->userdata('id_administrador')
        );
        $this->db->insert('audit', $user_logs_insert);
    }

    function obtener_audits($limit = FALSE) {
        $this->db->join('administrador a', 'a.id_administrador = audit.audit_id_usuario', 'left');
        $this->db->select('a.administrador_nombres AS nombres, a.administrador_apellidos AS apellidos', FALSE);
        $this->db->select('audit.*', FALSE);
        $this->db->order_by("audit.audit_fecha", "desc");
        if ($limit != FALSE) {
            $this->db->limit(200);
        }
        $query = $this->db->get('audit');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_datatables_registro_publicaciones($table = NULL, $column_order = NULL, $column_search = NULL, $order = NULL, $condiciones = NULL) {
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) { // loop column 
            if ($_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($order)) {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $secciones = array(
            "actualizacion",
            "administrador",
            "administrador_rol",
            "administrador_x_permisos",
            "audit",
            "audit_administrador",
            "ci_sessions",
            "embebido_home",
            "entidad_banners_imagenes",
            "entidad_banner_aviso",
            "entidad_carrousel",
            "entidad_contacto",
            "entidad_contador_visitas_dia",
            "entidad_contador_visitas_mes",
            "entidad_dependencia",
            "control_informe_archivo_tab",
            "entidad_enlaces_footer",
            "entidad_entidades",
            "entidad_iconos_home",
            "entidad_newsletter",
            "entidad_paginas",
            "entidad_redes_sociales",
            "entidad_redes_sociales_definicion",
            "entidad_seo",
            "entidad_sub_menu",
            "entidad_tipo_documentos",
            "entidad_widgets",
            "envio_newsletter",
            "notificaciones_electronicas_tipo",
            "permisos",
            "smtp",
            "survey",
            "survey_ip",
            "survey_preguntas",
            "animales_config",
            "config_general",
            "alertas_definicion",
            "AS"
        );
        $this->db->where_not_in('audit_table', $secciones);
        /* SELECCIONO LOS DATOS DE LA TABLA */
        $this->db->select("$table.*", FALSE);
        $query = $this->db->get();
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
        return array($query->result(), $query->num_rows(), $iFilteredTotal);
    }

    function obtener_banner($banner) {

        $this->db->where('entidad_banners_imagenes_banner', $banner);
        $this->db->order_by('id_entidad_banners_imagenes', 'DESC');
        $query = $this->db->get('entidad_banners_imagenes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_survey() {
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('survey_fecha_publicacion <=', date('Y-m-d'));
        $this->db->where('survey_fecha_expiracion >=', date('Y-m-d'));
        $this->db->limit(1);
        $query = $this->db->get('survey');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_survey_id($id) {
        $this->db->where('id_survey', $id);
        $query = $this->db->get('survey');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_survey_todos() {

        $query = $this->db->get('survey');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_survey_preguntas($id) {
        $this->db->where('id_survey', $id);
        $query = $this->db->get('survey_preguntas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function ingresar_votacion($id) {
        $this->db->where('id_survey_preguntas', $id);
        $this->db->set('survey_preguntas_respuestas', 'survey_preguntas_respuestas+1', FALSE);
        $this->db->update('survey_preguntas');
    }

    function obtenet_votaciones($id) {
        $this->db->where('id_survey', $id);
        $this->db->group_by("survey_preguntas_pregunta");
        $this->db->select('survey_preguntas_pregunta,sum(survey_preguntas_respuestas) as total_pregunta');
        $this->db->order_by("total_pregunta", "desc");
        $query = $this->db->get('survey_preguntas');
        $query2 = $this->obtener_total_votos($id);
        if ($query->num_rows() > 0) {
            return array($query->result(), $query2);
        } else {
            return FALSE;
        }
    }

    function obtener_total_votos($id) {
        $query3 = $this->db->query('select sum(survey_preguntas_respuestas) AS total from survey_preguntas WHERE id_survey =' . $id);
        if ($query3->num_rows() == 1) {
            return $query3->row()->total;
        } else {
            return FALSE;
        }
    }

    function ingresar_votacion_ip($ip, $id) {
        $data = array(
            'survey_ip' => $ip,
            'survey_ip_fecha' => date('Y-m-d H:i:s'),
            'id_survey_preguntas' => $id
        );
        $this->db->insert('survey_ip', $data);
    }

    //FUNCION QUE IDENTIFICA SI EN LAS ULTIMAS 24 HORAS UNA IP VISITO EL SITIO Y VOTO
    function get_ultima_visita_ip($ip) {
        $this->db->where('survey_ip_fecha > DATE_SUB(now(), INTERVAL 1 DAY)');
        $this->db->where('survey_ip ', $ip);
        $query = $this->db->get('survey_ip');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function obtener_carrousel() {
        $this->db->where('entidad_carrousel_tipo', 1);
        $query = $this->db->get('entidad_carrousel');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_enlaces() {
        $this->db->where('entidad_carrousel_tipo', 2);
        $this->db->order_by('entidad_carrousel_titulo', 'ASC');
        $query = $this->db->get('entidad_carrousel');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_widgets() {
        $query = $this->db->get('entidad_widgets');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_iconos() {
        $this->db->order_by('entidad_iconos_home_nombre', 'ASC');
        $query = $this->db->get('entidad_iconos_home');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_un_iconos($alias) {
        $this->db->order_by('entidad_iconos_home_nombre', 'ASC');
        $this->db->where('entidad_iconos_home_alias', $alias);
        $query = $this->db->get('entidad_iconos_home');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_iconos_tipo() {
        $this->db->order_by('entidad_iconos_home_tipo_orden', 'ASC');
        $query = $this->db->get('entidad_iconos_home_tipo');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_enlaces_footer() {
        $this->db->order_by('entidad_enlaces_footer_nombre', 'ASC');
        $query = $this->db->get('entidad_enlaces_footer');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_enlaces_sub_menu() {
        $this->db->order_by('entidad_sub_menu_nombre', 'ASC');
        $query = $this->db->get('entidad_sub_menu');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_pagina($alias) {
        $this->db->where('entidad_paginas_alias', $alias);
        $query = $this->db->get('entidad_paginas');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function check_alias($alias) {
        $this->db->where('entidad_paginas_alias', $alias);
        $query = $this->db->get('entidad_paginas');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function obtener_embebido_home() {
        $query = $this->db->get('embebido_home');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_animales_config() {
        $query = $this->db->get('animales_config');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_animales($par1 = NULL, $par2 = NULL, $par3 = NULL, $par4 = NULL) {
        $this->db->where('animales_adopcion_estado', 'Disponible');
        if ($par1 != '0') {
            $this->db->where('animales_adopcion_tipo', $par1);
        }
        if ($par2 != '0') {
            $this->db->where('animales_adopcion_sexo', $par2);
        }
        if ($par3 != '0') {
            $this->db->where('animales_adopcion_tamano', $par3);
        }
        if ($par4 != '0') {
            $this->db->where('animales_adopcion_edad', $par4);
        }
        $this->db->order_by('animales_adopcion_nombre', 'ASC');
        $query = $this->db->get('animales_adopcion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_una_animales($id) {
        $this->db->where('id_animales_adopcion', $id);
        $query = $this->db->get('animales_adopcion');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function guardar_clasificado($datos) {
        $this->db->insert('animales_clasificados', $datos);
        return TRUE;
    }

    function get_animales_clasificados() {
        $this->db->where('animales_clasificados_estado', 1);
        $this->db->order_by('animales_clasificados_fecha', 'DESC');
        $query = $this->db->get('animales_clasificados');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_un_animales_clasificados($id) {
        $this->db->where('id_animales_clasificados', $id);
        $query = $this->db->get('animales_clasificados');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function update_animales_clasificado($id, $estado) {
        $this->db->where('id_animales_clasificados', $id);
        $this->db->set('animales_clasificados_estado', $estado);
        $this->db->set('animales_clasificados_fecha_aprobacion', date('Y-m-d'));
        $this->db->update('animales_clasificados');
    }

    function buscar_notificaciones_electronicas($palabra) {
        $porciones = explode(" ", $palabra);
        $this->db->join('notificaciones_electronicas_tipo a', 'a.id_notificaciones_electronicas_tipo = notificaciones_electronicas.id_notificaciones_electronicas_tipo', 'left');
        $this->db->select('a.notificaciones_electronicas_tipo_nombre AS tipo', FALSE);
        $this->db->group_start();
        foreach ($porciones as $key => $porcion) {
            if ($porcion != '') {
                if (strlen($porcion) >= 4) {
                    if ($key == 0) {
                        $this->db->like('notificaciones_electronicas_identificacion', $porcion);
                        $this->db->or_like('notificaciones_electronicas_usuario', $porcion);
                        $this->db->or_like('notificaciones_electronicas_observaciones', $porcion);
                    } else {
                        $this->db->or_like('notificaciones_electronicas_identificacion', $porcion);
                        $this->db->or_like('notificaciones_electronicas_usuario', $porcion);
                        $this->db->or_like('notificaciones_electronicas_observaciones', $porcion);
                    }
                }
            }
        }
        $this->db->group_end();
        $this->db->select('notificaciones_electronicas.*', FALSE);
        $query = $this->db->get('notificaciones_electronicas');

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}
