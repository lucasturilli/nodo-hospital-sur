<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Referenciales_nodo_model
 *
 * Clase donde reposan las sentencias de bases de datos utilizadas para la clase Login, donde se autentica
 * y se ingresa al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Adicional_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_entidad_mipg_plan() {
        $query = $this->db->get('entidad_mipg_plan');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_entidad_mipg_planes() {
        $query = $this->db->get('entidad_mipg_planes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_avances_mipg($id) {
        $this->db->where('id_entidad_mipg_plan', $id);
        $query = $this->db->get('entidad_mipg_plan_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
