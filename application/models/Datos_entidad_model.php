<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_entidad_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function verificar_newsletter($email) {
        $this->db->where('entidad_newsletter_email', $email);
        $query = $this->db->get('entidad_newsletter');
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $data = array('entidad_newsletter_email' => $email);
            $this->db->insert('entidad_newsletter', $data);
            return TRUE;
        }
    }

    function obtener_entidad_seo() {
        $query = $this->db->get('entidad_seo');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_config() {
        $query = $this->db->get('config_general');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_actualizacion() {
        $query = $this->db->get('actualizacion');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function actualizar_actualizacion() {
        $data = array(
            'actualizacion' => date('Y-m-d')
        );
        $this->db->where('id_actualizacion', 1);
        $this->db->update('actualizacion', $data);
    }

    function obtener_entidad_redes_sociales() {
        $this->db->join('entidad_redes_sociales_definicion a', 'a.id_entidad_redes_sociales_definicion = entidad_redes_sociales.id_entidad_redes_sociales_definicion', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('entidad_redes_sociales.*', FALSE);
        $query = $this->db->get('entidad_redes_sociales');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_logo() {
        $this->db->select('entidad_informacion_logo');
        $query = $this->db->get('entidad_informacion');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_general() {
        $query = $this->db->get('entidad_informacion');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_noticias() {
        $this->db->order_by("entidad_noticias_fecha_ingreso", "desc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_noticias_tipo', 1);
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_noticias_relacionadas($tag, $id_principal) {
        $tags = explode(',', $tag);
        $this->db->order_by("entidad_noticias_fecha_ingreso", "desc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_noticias_tipo', 1);
        $this->db->where('id_entidad_noticias !=', $id_principal);
        $this->db->group_start();
        $this->db->or_where('YEAR(entidad_noticias_fecha_ingreso)', date('Y'));
        if ($tags != FALSE) {
            foreach ($tags as $data) {
                $this->db->or_like('entidad_noticias_tags', $data);
            }
        }
        $this->db->group_end();
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_noticia($alias) {
        $this->db->where('entidad_noticias_alias', $alias);
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function update_noticias($alias) {
        $this->db->where('entidad_noticias_alias', $alias);
        $this->db->set('entidad_noticias_newsletter', 1);
        $this->db->update('entidad_noticias');
    }

    function insert_cron_newsletter($datos) {
        $this->db->insert('cron_newsletter', $datos);
    }

    function get_cron_newsletter() {
        $this->db->where('cron_newsletter_estado', 2);
        $query = $this->db->get('cron_newsletter');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function update_cron_newsletter($datos, $alias) {
        $this->db->where('cron_newsletter_alias', $alias);
        $this->db->update('cron_newsletter', $datos);
    }

    function obtener_notificaciones() {
        $this->db->order_by("entidad_noticias_fecha_ingreso", "desc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_noticias_tipo', 2);
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_notificaciones_relacionadas($tag, $id_principal) {
        $tags = explode(',', $tag);
        $this->db->order_by("entidad_noticias_fecha_ingreso", "desc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_noticias_tipo', 2);
        $this->db->where('id_entidad_noticias !=', $id_principal);
        $this->db->group_start();
        $this->db->or_where('YEAR(entidad_noticias_fecha_ingreso)', date('Y'));
        if ($tags != FALSE) {
            foreach ($tags as $data) {
                $this->db->or_like('entidad_noticias_tags', $data);
            }
        }
        $this->db->group_end();
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_evento($alias) {
        $this->db->where('entidad_calendario_alias', $alias);
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_eventos() {
        $this->db->order_by("entidad_calendario_fecha_inicio", "asc");
        $this->db->where('id_estado_de_publicacion', 1);
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_eventos_mes() {
        $this->db->order_by("entidad_calendario_fecha_inicio", "asc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('YEAR(entidad_calendario_fecha_inicio) ', date('Y'));
        $this->db->where('MONTH(entidad_calendario_fecha_inicio) >=', date('m'));
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_eventos_mes_seleccion($y, $m) {
        $this->db->order_by("entidad_calendario_fecha_inicio", "asc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_calendario_fecha_inicio >=', date($y . '-' . $m) . '-01 00:00:00');
        $this->db->where('entidad_calendario_fecha_inicio <=', date($y . '-' . $m . '-t') . ' 23:59:59');
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_boletines() {

        $this->db->order_by('entidad_boletines_fecha', 'desc');
        $query = $this->db->get('entidad_boletines');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_boletines_periodos() {
        $query = $this->db->query('SELECT YEAR(entidad_boletines_fecha) as periodo FROM entidad_boletines GROUP BY YEAR(entidad_boletines_fecha) ORDER BY periodo DESC');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodico() {

        $this->db->order_by('entidad_periodico_fecha', 'desc');
        $query = $this->db->get('entidad_periodico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodico_periodos() {
        $query = $this->db->query('SELECT YEAR(entidad_periodico_fecha) as periodo FROM entidad_periodico GROUP BY YEAR(entidad_periodico_fecha) ORDER BY periodo DESC');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_datos_contacto() {

        $query = $this->db->get('entidad_contacto');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_politicas_datos() {
        $query = $this->db->get('entidad_politicas_datos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_informacion() {
        $query = $this->db->get('entidad_informacion');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_funcionarios() {
        $this->db->join('entidad_dependencia a', 'a.id_entidad_dependencia = entidad_funcionario.id_entidad_dependencia', 'left');
        $this->db->select('a.entidad_dependencia_nombre AS dependencia, a.id_entidad_dependencia AS id_dependencia', FALSE);
        $this->db->select('entidad_funcionario.*', FALSE);
        $this->db->order_by("a.entidad_dependencia_nombre", "asc");
        $query = $this->db->get('entidad_funcionario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_funcionario($id) {
        $this->db->join('entidad_dependencia a', 'a.id_entidad_dependencia = entidad_funcionario.id_entidad_dependencia', 'left');
        $this->db->select('a.entidad_dependencia_nombre AS dependencia, a.id_entidad_dependencia AS id_dependencia', FALSE);
        $this->db->select('entidad_funcionario.*', FALSE);
        $this->db->where('entidad_funcionario.id_entidad_funcionario', $id);
        $query = $this->db->get('entidad_funcionario');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_dependencias() {
        $this->db->join('entidad_funcionario a', 'a.id_entidad_funcionario = entidad_dependencia.id_entidad_funcionario', 'left');
        $this->db->select('a.entidad_funcionario_nombres AS nombres, a.entidad_funcionario_apellidos AS apellidos, a.id_entidad_funcionario AS id_funcionario', FALSE);
        $this->db->select('entidad_dependencia.*', FALSE);
        $this->db->order_by("entidad_dependencia.entidad_dependencia_nombre", "asc");
        $query = $this->db->get('entidad_dependencia');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_dependencia($id) {
        $this->db->join('entidad_funcionario a', 'a.id_entidad_funcionario = entidad_dependencia.id_entidad_funcionario', 'left');
        $this->db->select('a.entidad_funcionario_nombres AS nombres, a.entidad_funcionario_apellidos AS apellidos, a.id_entidad_funcionario AS id_funcionario', FALSE);
        $this->db->select('entidad_dependencia.*', FALSE);
        $this->db->where('entidad_dependencia.id_entidad_dependencia', $id);
        $query = $this->db->get('entidad_dependencia');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_listado_planta() {
        $query = $this->db->get('entidad_planta_personal');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_manual_funciones() {
        $query = $this->db->get('entidad_manual_de_funciones');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_definicion_manual_funciones() {
        $query = $this->db->get('entidad_definicion_manual_de_funciones');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_asiganciones_salarial() {
        $query = $this->db->get('entidad_asignaciones_salarial');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_evaluacion_desempeno() {
        $query = $this->db->get('entidad_evaluacion_desempeno');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_sucursales() {
        $this->db->order_by("entidad_sucursales_nombre", "asc");
        $query = $this->db->get('entidad_sucursales');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_sucursal($id) {
        $this->db->where('id_entidad_sucursales', $id);
        $query = $this->db->get('entidad_sucursales');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_entidades() {
        $this->db->order_by("entidad_entidades_nombre", "asc");
        $query = $this->db->get('entidad_entidades');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_entidad($id) {
        $this->db->where('id_entidad_entidades', $id);
        $query = $this->db->get('entidad_entidades');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_agremiaciones() {
        $this->db->order_by("entidad_agremiaciones_nombre", "asc");
        $query = $this->db->get('entidad_agremiaciones');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_agremiacion($id) {
        $this->db->where('id_entidad_agremiaciones', $id);
        $query = $this->db->get('entidad_agremiaciones');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_preguntas_frecuentes() {
        $this->db->order_by("entidad_preguntas_frecuentes_pregunta", "asc");
        $query = $this->db->get('entidad_preguntas_frecuentes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_preguntas_frecuentes_dependencias() {
        $this->db->join('entidad_dependencia a', 'a.id_entidad_dependencia = entidad_preguntas_frecuentes.id_dependencia', 'left');
        $this->db->select('a.entidad_dependencia_nombre AS dependencia', FALSE);
        $this->db->order_by("a.entidad_dependencia_nombre", "asc");
        $this->db->distinct();
        $this->db->select('entidad_preguntas_frecuentes.id_dependencia');
        $query = $this->db->get('entidad_preguntas_frecuentes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_glosario($letra) {
        $query = $this->db->query('SELECT entidad_glosario_palabra, entidad_glosario_descripcion  FROM entidad_glosario WHERE SUBSTRING( entidad_glosario_palabra, 1, 1 ) ="' . $letra . '" ORDER BY entidad_glosario_palabra asc; ');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_glosario_todos() {
        $this->db->order_by("entidad_glosario_palabra", "asc");
        $query = $this->db->get('entidad_glosario');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_ofertas_empleo() {
        $query = $this->db->get('entidad_ofertas_empleo');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_oferta_empleo($id) {
        $this->db->where('id_entidad_ofertas_empleo', $id);
        $query = $this->db->get('entidad_ofertas_empleo');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_documentoss($searh = NULL, $tipo = NULL) {
        $this->db->join('entidad_tipo_documentos a', 'a.id_entidad_tipo_documentos = entidad_documentos.id_entidad_tipo_documentos', 'left');
        $this->db->select('a.entidad_tipo_documentos_nombre AS tipo', FALSE);
        $this->db->select('entidad_documentos.*', FALSE);
        if ($searh != NULL) {

            switch ($tipo) {
                case 'por_nombre':
                    $this->db->like('entidad_documentos.entidad_documentos_nombre', $searh);
                    break;
                case 'por_tipo':
                    $this->db->like('a.entidad_tipo_documentos_nombre', $searh);
                    break;
                case 'por_tematica':
                    $this->db->like('entidad_documentos.entidad_documentos_descripcion', $searh);
                    break;
                default :
                    $this->db->like('entidad_documentos.entidad_documentos_nombre', $searh);
                    $this->db->or_like('a.entidad_tipo_documentos_nombre', $searh);
                    $this->db->or_like('entidad_documentos.entidad_documentos_descripcion', $searh);
                    break;
            }
        }
        $this->db->order_by("entidad_documentos.entidad_documentos_nombre", "asc");
        $query = $this->db->get('entidad_documentos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_noticias_ninos() {
        $this->db->order_by("id_entidad_noticias_ninos", "desc");
        $this->db->where('id_estado_de_publicacion', 1);
        $query = $this->db->get('entidad_noticias_ninos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_noticia_ninos($id) {

        $this->db->where('id_entidad_noticias_ninos', $id);
        $query = $this->db->get('entidad_noticias_ninos');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_evento_ninos($id) {
        $this->db->where('id_entidad_calendario_ninos', $id);
        $query = $this->db->get('entidad_calendario_ninos');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_eventos_mes_ninos() {
        $this->db->order_by("entidad_calendario_ninos_fecha_inicio", "asc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_calendario_ninos_fecha_inicio >=', date('Y-m') . '-01 00:00:00');
        $this->db->where('entidad_calendario_ninos_fecha_inicio <=', date('Y-m-t') . ' 23:59:59');
        $query = $this->db->get('entidad_calendario_ninos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_eventos_mes_seleccion_ninos($y, $m) {
        $this->db->order_by("entidad_calendario_ninos_fecha_inicio", "asc");
        $this->db->where('id_estado_de_publicacion', 1);
        $this->db->where('entidad_calendario_ninos_fecha_inicio >=', date($y . '-' . $m) . '-01 00:00:00');
        $this->db->where('entidad_calendario_ninos_fecha_inicio <=', date($y . '-' . $m . '-t') . ' 23:59:59');
        $query = $this->db->get('entidad_calendario_ninos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_ninos() {
        $query = $this->db->get('entidad_informacion_ninos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_sitios_ninos() {
        $query = $this->db->get('entidad_sitios_recomendados_ninos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_juegos_ninos() {
        $query = $this->db->get('entidad_juegos_ninos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function visitas($array = NULL) {
        $data = array(
            'entidad_contador_visitas_dia_fecha' => date('Y-m-d H-i-s'),
            'entidad_contador_visitas_dia_ip' => $array[0],
            'entidad_contador_visitas_dia_agente' => $array[1]
        );
        $this->db->insert('entidad_contador_visitas_dia', $data);
        $this->verificar_visita();
    }

    function verificar_visita() {
        $this->db->where('entidad_contador_visitas_mes_year', date('Y'));
        $this->db->where('entidad_contador_visitas_mes_mes', date('m'));
        $query = $this->db->get('entidad_contador_visitas_mes');

        if ($query->num_rows() > 0) {
            $this->db->set('entidad_contador_visitas_mes_visitas', 'entidad_contador_visitas_mes_visitas+1', false);
            $this->db->where('entidad_contador_visitas_mes_year', date('Y'));
            $this->db->where('entidad_contador_visitas_mes_mes', date('m'));
            $this->db->update('entidad_contador_visitas_mes');
        } else {
            $this->db->insert('entidad_contador_visitas_mes', array('entidad_contador_visitas_mes_year' => date('Y'),
                'entidad_contador_visitas_mes_mes' => date('m'),
                'entidad_contador_visitas_mes_visitas' => 0));
            $this->db->set('entidad_contador_visitas_mes_visitas', 'entidad_contador_visitas_mes_visitas+1', false);
            $this->db->where('entidad_contador_visitas_mes_year', date('Y'));
            $this->db->where('entidad_contador_visitas_mes_mes', date('m'));
            $this->db->update('entidad_contador_visitas_mes');
        }
    }

    function obtener_visitas() {
        $query = $this->db->query('SELECT SUM(entidad_contador_visitas_mes_visitas) as total FROM entidad_contador_visitas_mes');
        return $query->row()->total;
    }

    function eliminar_visitas_viejas() {
        $this->generar_csv_visitas();
        $this->db->where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%Y") <>', date('Y'));
        $this->db->or_where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%m") <>', date('m'));
        $this->db->or_where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%d") <>', date('d'));
        $this->db->delete('entidad_contador_visitas_dia');
    }

    function generar_csv_visitas() {
        $this->load->dbutil();
        $this->db->where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%Y") <>', date('Y'));
        $this->db->or_where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%m") <>', date('m'));
        $this->db->or_where('DATE_FORMAT(entidad_contador_visitas_dia_fecha,"%d") <>', date('d'));
        $query = $this->db->get("entidad_contador_visitas_dia");
        if ($query->num_rows() > 0) {
            $result = $this->dbutil->csv_from_result($query);
            $this->load->helper('file');
            write_file('./uploads/visitas/visitas' . date('Y-m-d') . '.csv', $result);
        }
    }

    function obtener_visitas_grafica() {
        $this->db->order_by('id_entidad_contador_visitas_mes', 'DESC');
        $this->db->limit(6);
        $query = $this->db->get('entidad_contador_visitas_mes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_ultima_visita_ip($ip) {
        $this->db->where('entidad_contador_visitas_dia_fecha > DATE_SUB(now(), INTERVAL 2 HOUR)');
        $this->db->where('entidad_contador_visitas_dia_ip ', $ip);
        $query = $this->db->get('entidad_contador_visitas_dia');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function obtener_newsletter() {
        //$this->db->limit(1);
        $query = $this->db->get('entidad_newsletter');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function delete_newsletter($id) {
        $this->db->where('id_entidad_newsletter', $id);
        $this->db->delete('entidad_newsletter');
    }

    function obtener_banner_aviso() {
        $query = $this->db->get('entidad_banner_aviso');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_calidad() {
        $query = $this->db->get('entidad_calidad');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_caracterizacion_procesos() {
        $this->db->order_by('entidad_caracterizacion_procesos_nombre', 'ASC');
        $query = $this->db->get('entidad_caracterizacion_procesos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entidad_procedimientos() {
        $this->db->order_by('entidad_procedimientos_nombre', 'ASC');
        $query = $this->db->get('entidad_procedimientos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function check_alias_noticias($alias) {
        $this->db->where('entidad_noticias_alias', $alias);
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_alias_calendario($alias) {
        $this->db->where('entidad_calendario_alias', $alias);
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

//    function insert_envio_newsletter($id) {
//        $data = array('envio_newsletter_fecha' => date('Y-m-d'),
//            'envio_newsletter_alias' => $id);
//        $this->db->insert('envio_newsletter', $data);
//    }
//
//    function get_envio_newsletter() {
//        $this->db->where('envio_newsletter_fecha', date('Y-m-d'));
//        $query = $this->db->get('envio_newsletter');
//        if ($query->num_rows() > 0) {
//            return $query->result();
//        } else {
//            return FALSE;
//        }
//    }

    function obtener_convocatorias() {
        $query = $this->db->get('convocatorias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_una_convocatoria($id) {
        $this->db->where('id_convocatorias', $id);
        $query = $this->db->get('convocatorias');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_convocatorias_anexos($id) {
        $this->db->where('id_convocatorias', $id);
        $query = $this->db->get('convocatorias_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_estudios() {
        $query = $this->db->get('estudios');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_adicional() {
        $query = $this->db->get('informacion_adicional');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
