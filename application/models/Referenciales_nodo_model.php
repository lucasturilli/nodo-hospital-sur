<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Referenciales_nodo_model
 *
 * Clase donde reposan las sentencias de bases de datos utilizadas para la clase Login, donde se autentica
 * y se ingresa al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Referenciales_nodo_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function verificar_administrador($id) {

        $this->db->where('administrador_identificacion', $id);
        $query = $this->db->get('administrador');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_permisos_usuario($id) {
        $this->db->where('id_administrador', $id);
        $query = $this->db->get('administrador_x_permisos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_smtp() {
        $query = $this->db->get('smtp');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_config() {
        $query = $this->db->get('config_general');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_administrador() {
        $query = $this->db->get('administrador');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function update_administrador($datos, $id) {
        $this->db->where('id_administrador', $id);
        $this->db->update('administrador', $datos);
    }

    /* ================ALRTAS Y NOTIFICACIONES================ */

    function get_last_check($id) {
        $this->db->join('audit a', 'a.audit_table= alertas_definicion.alertas_definicion_tabla', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('alertas_definicion.*', FALSE);
        $this->db->order_by("a.audit_fecha", "desc");
        $this->db->where('a.audit_type', 'INSERT');
        $this->db->where('id_alertas_definicion', $id);
        $this->db->limit(1);
        $query = $this->db->get('alertas_definicion');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_datos_alerta($id) {
        $this->db->where('id_alertas_definicion', $id);
        $query = $this->db->get('alertas_definicion');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_datos_tabla($tabla) {
        $query = $this->db->get($tabla);
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_alertas_definicion() {
        $query = $this->db->get('alertas_definicion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function update_alertas_definicion($datos, $id) {
        $this->db->where('id_alertas_definicion', $id);
        $this->db->update('alertas_definicion', $datos);
    }

}
