<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_buscar_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*     * *********************************************************INFORMACION GENERAL************************************************************** */

    function buscar_entidad_dependencias($palabra) {
        $data = array(
            'entidad_dependencia_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_dependencia');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_sucursales($palabra) {
        $data = array(
            'entidad_sucursales_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_sucursales');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_entidades($palabra) {
        $data = array(
            'entidad_entidades_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_entidades');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_agremiaciones($palabra) {
        $data = array(
            'entidad_agremiaciones_nombre' => $palabra,
            'entidad_agremiaciones_clasificacion' => $palabra,
            'entidad_agremiaciones_actividad' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_agremiaciones');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * *****************************************SERVICIOS DE INFORMACIONM ****************************************** */

    function buscar_entidad_preguntas_frecuentes($palabra) {
        $data = array(
            'entidad_preguntas_frecuentes_pregunta' => $palabra,
            'entidad_preguntas_frecuentes_respuesta' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_preguntas_frecuentes');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_glosario($palabra) {
        $data = array(
            'entidad_glosario_palabra' => $palabra,
            'entidad_glosario_descripcion' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_glosario');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_noticias($palabra) {
        $data = array(
            'entidad_noticias_titulo' => $palabra,
            'entidad_noticias_descripcion_corta' => $palabra,
            'entidad_noticias_tags' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_noticias');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_boletines($palabra) {
        $data = array(
            'entidad_boletines_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_boletines');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_periodico($palabra) {
        $data = array(
            'entidad_periodico_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_periodico');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_calendario($palabra) {
        $data = array(
            'entidad_calendario_nombre' => $palabra,
            'entidad_calendario_descripcion' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_calendario');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * *****************************TALENTO HUMANO************************************** */

    function buscar_entidad_funcionarios($palabra) {
        $data = array(
            'entidad_funcionario_nombres' => $palabra,
            'entidad_funcionario_apellidos' => $palabra,
            'entidad_funcionario_email' => $palabra,
            'entidad_funcionario_cargo' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_funcionario');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_manual_funciones($palabra) {
        $data = array(
            'entidad_manual_de_funciones_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_manual_de_funciones');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * *********************************NORMATIVIDAD ************************** */

    function buscar_normatividad_leyes($palabra) {
        $data = array(
            'normatividad_leyes_nombre' => $palabra,
            'normatividad_leyes_tematica' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('normatividad_leyes');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_normatividad_decretos($palabra) {
        $data = array(
            'normatividad_decretos_nombre' => $palabra,
            'normatividad_decretos_tematica' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('normatividad_decretos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_normatividad_circulares($palabra) {
        //   $palabra = trim($palabra);
        $this->db->where('normatividad_resoluciones_tipo = ("Circular" OR normatividad_resoluciones_tipo = "Otro") AND( normatividad_resoluciones_nombre LIKE "%' . $palabra . '%" OR normatividad_resoluciones_tematica LIKE "%' . $palabra . '%")');
        $query = $this->db->get('normatividad_resoluciones');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_normatividad_resoluciones($palabra) {
        //  $palabra = trim($palabra);
        $this->db->where('normatividad_resoluciones_tipo != "Circular" AND normatividad_resoluciones_tipo != "Otro" AND( normatividad_resoluciones_nombre LIKE "%' . $palabra . '%" OR normatividad_resoluciones_tematica LIKE "%' . $palabra . '%")');
        $query = $this->db->get('normatividad_resoluciones');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_normatividad_edictos($palabra) {
        $data = array(
            'normatividad_edictos_nombre' => $palabra,
            'normatividad_edictos_tematica' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('normatividad_edictos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * ****************************************PRESUPUESTO************************************************** */

    function buscar_financiera_estados_financieros($palabra) {
        $data = array(
            'financiera_estados_financieros_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('financiera_estados_financieros');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_financiera_estatuto_tributario($palabra) {
        $data = array(
            'financiera_estatuto_tributario_nombre' => $palabra,
            'financiera_estatuto_tributario_descripcion' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('financiera_estatuto_tributario');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * *******************************************************PLANEACION****************************** */

    function buscar_normatividad_politicas($palabra) {
        $data = array(
            'normatividad_politicas_nombre' => $palabra,
            'normatividad_politicas_tematica' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('normatividad_politicas');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_anticorrupcion_definicion($palabra) {
        $data = array(
            'anticorrupcion_definicion_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('anticorrupcion_definicion');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_plan_accion($palabra) {
        $data = array(
            'control_plan_accion_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_plan_accion');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_planeacion_plan_gasto_publico($palabra) {
        $data = array(
            'planeacion_plan_gasto_publico_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('planeacion_plan_gasto_publico');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_programas($palabra) {
        $data = array(
            'control_programas_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_programas');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_planes_otros($palabra) {
        $data = array(
            'control_planes_otros_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_planes_otros');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informe_empalme($palabra) {
        $data = array(
            'control_informe_empalme_nombre_archivo' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informe_empalme');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_metas($palabra) {
        $data = array(
            'control_indicadores_gestion_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_indicadores_gestion');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * **********************************************CONTROL*************************** */

    function buscar_control_entes($palabra) {
        $data = array(
            'control_entes_nombre' => $palabra,
            'control_entes_email_contacto' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_entes');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_auditorias($palabra) {
        $data = array(
            'control_auditorias_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_auditorias');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informe_concejo($palabra) {
        $data = array(
            'control_informe_concejo_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informe_concejo');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informe_fiscal($palabra) {
        $data = array(
            'control_informe_fiscal_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informe_fiscal');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informe_ciudadania($palabra) {
        $data = array(
            'control_informe_ciudadania_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informe_ciudadania');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informes_cronograma($palabra) {
        $data = array(
            'control_informes_cronograma_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informes_cronograma');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informes_gestion_ciudadania($palabra) {
        $data = array(
            'control_informes_gestion_ciudadania_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informes_gestion_ciudadania');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informes_ciudadania_evaluacion($palabra) {
        $data = array(
            'control_informes_ciudadania_evaluacion_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informes_ciudadania_evaluacion');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_planes_mejoramiento($palabra) {
        $data = array(
            'control_planes_mejoramiento_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_planes_mejoramiento');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_reportes_control_interno($palabra) {
        $data = array(
            'control_reportes_control_interno_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_reportes_control_interno');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_poblacion_vulnerable_programas($palabra) {
        $data = array(
            'control_poblacion_vulnerable_programas_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_poblacion_vulnerable_programas');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_poblacion_vulnerable_proyectos($palabra) {
        $data = array(
            'control_poblacion_vulnerable_proyectos_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_poblacion_vulnerable_proyectos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_programas_sociales($palabra) {
        $data = array(
            'control_programas_sociales_programa' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_programas_sociales');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_otros_informes($palabra) {
        $data = array(
            'control_otros_informes_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_otros_informes');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * ********************************************CONTRATACION**************************************************************** */

    function buscar_contratacion_plan_compras($palabra) {
        $data = array(
            'contratacion_plan_compras_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('contratacion_plan_compras');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_contratacion_contratos($palabra) {
        $data = array(
            'contratacion_contratos_numero_proceso' => $palabra,
            'contratacion_contratos_objeto' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('contratacion_contratos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_contratacion_avance_contractual($palabra) {
        $data = array(
            'contratacion_avance_contractual_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('contratacion_avance_contractual');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_contratacion_convenios($palabra) {
        $data = array(
            'contratacion_convenios_numero_proceso' => $palabra,
            'contratacion_convenios_objeto' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('contratacion_convenios');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_contratacion_lineamientos($palabra) {
        $data = array(
            'contratacion_lineamientos_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('contratacion_lineamientos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * ***********************************TRAMITES Y SERVICIOS****************************************** */

    function buscar_servicios_tramites($palabra) {
        $data = array(
            'servicios_tramites_nombre' => $palabra,
            'servicios_tramites_descripcion' => $palabra,
        );
        $this->db->or_like($data);
        $query = $this->db->get('servicios_tramites');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_servicios_tramites_en_linea($palabra) {
        $data = array(
            'servicios_tramites_en_linea_nombre' => $palabra,
            'servicios_tramites_en_linea_descripcion' => $palabra,
        );
        $this->db->or_like($data);
        $query = $this->db->get('servicios_tramites_en_linea');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_servicios_formatos($palabra) {
        $data = array(
            'servicios_formatos_nombre' => $palabra,
            'servicios_formatos_descripcion' => $palabra,
        );
        $this->db->or_like($data);
        $query = $this->db->get('servicios_formatos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_servicios_certificados($palabra) {
        $data = array(
            'servicios_certificados_nombre' => $palabra,
            'servicios_certificados_descripcion' => $palabra,
        );
        $this->db->or_like($data);
        $query = $this->db->get('servicios_certificados');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * ***********************CALIDAD***************************** */

    function buscar_entidad_caracterizacion_procesos($palabra) {
        $data = array(
            'entidad_caracterizacion_procesos_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_caracterizacion_procesos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_procedimientos($palabra) {
        $data = array(
            'entidad_procedimientos_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_procedimientos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * ************************************************************* */

    /*     * ***********************DATOS ABIERTOS***************************** */

    function buscar_archivos_datos_abiertos($palabra) {
        $data = array(
            'archivos_datos_abiertos_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('archivos_datos_abiertos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_entidad_documentos($palabra) {
        $data = array(
            'entidad_documentos_nombre' => $palabra,
            'entidad_documentos_descripcion' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_documentos');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_control_informe_archivo($palabra) {
        $data = array(
            'control_informe_archivo_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('control_informe_archivo');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_mipg_plan($palabra) {
        $data = array(
            'entidad_mipg_plan_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_mipg_plan');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_mipg_planes($palabra) {
        $data = array(
            'entidad_mipg_planes_nombre' => $palabra
        );
        $this->db->or_like($data);
        $query = $this->db->get('entidad_mipg_planes');
        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    /*     * **** *///

    function buscar_municipio_documentos($palabra) {
        $data = array(
            'municipio_documentos_tipo_documento' => $palabra,
            'municipio_documentos_nombre' => $palabra,
            'municipio_documentos_descripcion' => $palabra
        );

        $this->db->or_like($data);
        $query = $this->db->get('municipio_documentos');

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_municipio_galeria_mapas($palabra) {
        $data = array(
            'municipio_galeria_mapas_tipo_mapa' => $palabra,
            'municipio_galeria_mapas_nombre' => $palabra,
            'municipio_galeria_mapas_descripcion' => $palabra
        );

        $this->db->or_like($data);
        $query = $this->db->get('municipio_galeria_mapas');

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_municipio_sitios_interes($palabra) {
        $data = array(
            'municipio_sitios_interes_nombre' => $palabra,
            'municipio_sitios_interes_descripcion' => $palabra,
        );

        $this->db->or_like($data);
        $query = $this->db->get('municipio_sitios_interes');

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function buscar_municipio_territorios($palabra) {
        $data = array(
            'municipio_territorios_tipo' => $palabra,
            'municipio_territorios_nombre' => $palabra
        );

        $this->db->or_like($data);
        $query = $this->db->get('municipio_territorios');

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}
