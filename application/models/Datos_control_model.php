<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_control_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_politicas() {
        $this->db->order_by('control_politicas_periodo', 'desc');
        $query = $this->db->get('control_politicas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function plan_estrategico_anexos($id) {
        $this->db->where('id_control_plan_estrategico', $id);
        $query = $this->db->get('control_plan_estrategico_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_plan_estrategico() {
        $query = $this->db->get('control_plan_estrategico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_plan_estrategico_vigente() {
        if (date('Y') <= 2019 && date('Y') >= 2016) {
            $min = 2016;
            $max = 2019;
        } else {
            $min = 2020;
            $max = 2023;
        }
        $this->db->where("(control_plan_estrategico_periodo BETWEEN $min AND $max)");
        $query = $this->db->get('control_plan_estrategico');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_plan_accion() {
        $this->db->join('entidad_dependencia a', 'a.id_entidad_dependencia = control_plan_accion.id_entidad_dependencia', 'left');
        $this->db->select('a.entidad_dependencia_nombre AS dependencia', FALSE);
        $this->db->select('control_plan_accion.*', FALSE);
        $this->db->order_by('a.entidad_dependencia_nombre', 'asc');
        $query = $this->db->get('control_plan_accion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_plan_accion_dependencias() {
        $this->db->group_by('id_entidad_dependencia');
        $query = $this->db->get('control_plan_accion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_avances_plan_accion($id) {
        $this->db->where('control_plan_accion_avances.id_control_plan_accion', $id);
        $this->db->order_by('id_control_plan_accion_avances', 'asc');
        $query = $this->db->get('control_plan_accion_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_programas() {
        $this->db->order_by('control_programas_fecha_inscripcion', 'desc');
        $query = $this->db->get('control_programas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_programa_avances($id) {
        $this->db->join('control_programas a', 'a.id_control_programas = control_programas_avances.id_control_programas', 'left');
        $this->db->select('a.control_programas_nombre AS programa', FALSE);
        $this->db->select('control_programas_avances.*', FALSE);
        $this->db->order_by('control_programas_avances.control_programas_avances_periodo', 'desc');
        $this->db->where('control_programas_avances.id_control_programas', $id);
        $query = $this->db->get('control_programas_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_entes() {
        $query = $this->db->get('control_entes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_ente($id) {
        $this->db->where('id_control_entes', $id);
        $query = $this->db->get('control_entes');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_concejo() {
        $query = $this->db->get('control_informe_concejo');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_contraloria() {
        $query = $this->db->get('control_informe_contraloria');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_ciudadania() {
        $query = $this->db->get('control_informe_ciudadania');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_ciudadania_anexos($id) {
        $this->db->where('id_control_informe_ciudadania', $id);
        $query = $this->db->get('control_informe_ciudadania_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informes_gestion_ciudadania() {
        $this->db->join('control_informe_ciudadania b', 'b.id_control_informe_ciudadania = control_informes_gestion_ciudadania.id_control_informe_ciudadania', 'left');
        $this->db->select('b.control_informe_ciudadania_periodo AS periodo, b.control_informe_ciudadania_nombre as informe', FALSE);
        $this->db->select('control_informes_gestion_ciudadania.*', FALSE);
        $query = $this->db->get('control_informes_gestion_ciudadania');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informes_cronograma() {
        $query = $this->db->get('control_informes_cronograma');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informes_evaluacion() {
        $this->db->join('control_informe_ciudadania b', 'b.id_control_informe_ciudadania = control_informes_ciudadania_evaluacion.id_control_informe_ciudadania', 'left');
        $this->db->select('b.control_informe_ciudadania_periodo AS periodo, b.control_informe_ciudadania_nombre as informe', FALSE);
        $this->db->select('control_informes_ciudadania_evaluacion.*', FALSE);
        $query = $this->db->get('control_informes_ciudadania_evaluacion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_ciudadania_evaluacion_anexos($id) {
        $this->db->where('id_control_informes_ciudadania_evaluacion', $id);
        $query = $this->db->get('control_informes_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_fiscal() {
        $query = $this->db->get('control_informe_fiscal');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_indicadores_gestion() {
        $this->db->order_by('control_indicadores_gestion_periodo', 'desc');
        $query = $this->db->get('control_indicadores_gestion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_planes_mejoramiento() {
        $this->db->distinct('control_planes_mejoramiento_periodo');
        $this->db->group_by("control_planes_mejoramiento_periodo");
        $this->db->order_by('control_planes_mejoramiento_periodo', 'DESC');
        $query = $this->db->get('control_planes_mejoramiento');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_planes_mejoramiento() {
        $this->db->join('control_entes a', 'a.id_control_entes = control_planes_mejoramiento.id_control_entes', 'left');
        $this->db->select('a.control_entes_nombre AS ente, a.id_control_entes AS id_ente', FALSE);
        $this->db->join('entidad_dependencia b', 'b.id_entidad_dependencia = control_planes_mejoramiento.id_entidad_dependencia', 'left');
        $this->db->select('b.entidad_dependencia_nombre AS dependencia', FALSE);
        $this->db->select('control_planes_mejoramiento.*', FALSE);
        $this->db->order_by('control_planes_mejoramiento_fecha_ingreso', 'desc');
        $this->db->order_by('a.control_entes_nombre', 'desc');
        $query = $this->db->get('control_planes_mejoramiento');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_planes_otros() {
        $this->db->order_by('control_planes_otros_tipo', 'asc');
        $query = $this->db->get('control_planes_otros');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_reportes_control_interno() {
        $this->db->order_by('control_reportes_control_interno_periodo', 'desc');
        $query = $this->db->get('control_reportes_control_interno');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_reportes_control_interno_tab() {
        $query = $this->db->get('control_reportes_control_interno_tab');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_reportes_control_interno() {
        $this->db->distinct('control_reportes_control_interno_periodo');
        $this->db->group_by("control_reportes_control_interno_periodo");
        $this->db->order_by('control_reportes_control_interno_periodo', 'DESC');
        $query = $this->db->get('control_reportes_control_interno');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_empalme() {
        $this->db->order_by('control_informe_empalme_periodo', 'ASC');
        $query = $this->db->get('control_informe_empalme');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_anexos_obtener_informe_empalme($id) {
        $this->db->where('control_informe_empalme_anexos.id_control_informe_empalme', $id);
        $this->db->order_by('control_informe_empalme_anexos_nombre', 'asc');
        $query = $this->db->get('control_informe_empalme_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable() {
        $query = $this->db->get('control_poblacion_vulnerable');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_programas() {
        $this->db->order_by('control_poblacion_vulnerable_programas_fecha', 'desc');
        $query = $this->db->get('control_poblacion_vulnerable_programas');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_programa($id) {
        $this->db->where('id_control_poblacion_vulnerable_programas', $id);
        $query = $this->db->get('control_poblacion_vulnerable_programas');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_programas_avances($id) {
        $this->db->join('control_poblacion_vulnerable_programas a', 'a.id_control_poblacion_vulnerable_programas = control_poblacion_vulnerable_programas_avances.id_control_poblacion_vulnerable_programas', 'left');
        $this->db->select('a.control_poblacion_vulnerable_programas_nombre AS programa, a.id_control_poblacion_vulnerable_programas AS id_programa', FALSE);
        $this->db->select('control_poblacion_vulnerable_programas_avances.*', FALSE);
        $this->db->order_by('control_poblacion_vulnerable_programas_avances_periodo', 'desc');
        $this->db->where('control_poblacion_vulnerable_programas_avances.id_control_poblacion_vulnerable_programas', $id);
        $query = $this->db->get('control_poblacion_vulnerable_programas_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_proyectos() {
        $this->db->order_by('control_poblacion_vulnerable_proyectos_fecha', 'desc');
        $query = $this->db->get('control_poblacion_vulnerable_proyectos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_proyecto($id) {
        $this->db->where('id_control_poblacion_vulnerable_proyectos', $id);
        $query = $this->db->get('control_poblacion_vulnerable_proyectos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_poblacion_vulnerable_proyectos_avances($id) {
        $this->db->join('control_poblacion_vulnerable_proyectos a', 'a.id_control_poblacion_vulnerable_proyectos = control_poblacion_vulnerable_proyectos_avances.id_control_poblacion_vulnerable_proyectos', 'left');
        $this->db->select('a.control_poblacion_vulnerable_proyectos_nombre AS programa, a.id_control_poblacion_vulnerable_proyectos AS id_programa', FALSE);
        $this->db->select('control_poblacion_vulnerable_proyectos_avances.*', FALSE);
        $this->db->order_by('control_poblacion_vulnerable_proyectos_avances_periodo', 'desc');
        $this->db->where('control_poblacion_vulnerable_proyectos_avances.id_control_poblacion_vulnerable_proyectos', $id);
        $query = $this->db->get('control_poblacion_vulnerable_proyectos_avances');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_programas_sociales() {
        $this->db->order_by('control_programas_sociales_fecha', 'desc');
        $query = $this->db->get('control_programas_sociales');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_defensa_judicial() {
        $this->db->order_by('control_defensa_judicial_periodo', 'desc');
        $query = $this->db->get('control_defensa_judicial');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_archivo() {
        $this->db->order_by('control_informe_archivo_nombre', 'ASC');
        $query = $this->db->get('control_informe_archivo');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_archivo_tab() {
        $this->db->order_by('control_informe_archivo_tab_nombre', 'ASC');
        $query = $this->db->get('control_informe_archivo_tab');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informe_archivo_categoria() {
        $this->db->join('control_informe_archivo_categoria a', 'a.id_control_informe_archivo_categoria = control_informe_archivo.id_control_informe_archivo_categoria', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('control_informe_archivo.*', FALSE);
        $this->db->group_by('control_informe_archivo.id_control_informe_archivo_categoria');
        $query = $this->db->get('control_informe_archivo');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_anticorrupcion() {
        $this->db->distinct('anticorrupcion_definicion_periodo');
        $this->db->group_by("anticorrupcion_definicion_periodo");
        $this->db->order_by('anticorrupcion_definicion_periodo', 'DESC');
        $query = $this->db->get('anticorrupcion_definicion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function anticorrupcion_anexos($id) {
        $this->db->where('id_anticorrupcion_definicion', $id);
        $query = $this->db->get('anticorrupcion_definicion_anexos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_anticorrupcion_definicion() {
        $this->db->join('anticorrupcion_definicion_tipo a', 'a.id_anticorrupcion_definicion_tipo = anticorrupcion_definicion.id_anticorrupcion_definicion_tipo', 'left');
        $this->db->select('a.*', FALSE);
        $this->db->select('anticorrupcion_definicion.*', FALSE);
        $query = $this->db->get('anticorrupcion_definicion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_auditorias() {
        $this->db->distinct('control_auditorias_periodo');
        $this->db->group_by("control_auditorias_periodo");
        $this->db->order_by('control_auditorias_periodo', 'DESC');
        $query = $this->db->get('control_auditorias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_auditorias() {
        $this->db->join('control_entes a', 'a.id_control_entes = control_auditorias.id_control_entes', 'left');
        $this->db->select('a.control_entes_nombre AS ente, a.id_control_entes AS id_ente', FALSE);
        $this->db->select('control_auditorias.*', FALSE);
        $this->db->order_by('control_auditorias_fecha_ingreso', 'desc');
        $this->db->order_by('a.control_entes_nombre', 'desc');
        $query = $this->db->get('control_auditorias');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_periodos_plan_gasto_publico() {
        $this->db->distinct('planeacion_plan_gasto_publico_periodo');
        $this->db->group_by("planeacion_plan_gasto_publico_periodo");
        $this->db->order_by('planeacion_plan_gasto_publico_periodo', 'DESC');
        $query = $this->db->get('planeacion_plan_gasto_publico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_planeacion_plan_gasto_publico() {
        $query = $this->db->get('planeacion_plan_gasto_publico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_control_otros_informes() {
        $query = $this->db->get('control_otros_informes');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_control_anuario_estaditico_periodos() {
        $this->db->distinct('control_anuario_estadistico_periodo');
        $this->db->group_by("control_anuario_estadistico_periodo");
        $this->db->order_by('control_anuario_estadistico_periodo', 'DESC');
        $query = $this->db->get('control_anuario_estadistico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_control_anuario_estaditico() {

        $this->db->order_by('control_anuario_estadistico_nombre', 'asc');
        $query = $this->db->get('control_anuario_estadistico');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
