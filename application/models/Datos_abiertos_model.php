<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Datos_entidad_model
 *
 * Clase donde reposan las sentencias par aobtener toda la informacion de las tablas datos_entidad
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Datos_abiertos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function obtener_catalogo() {
        $this->db->order_by('catalago_datos_abiertos_nombre', 'desc');
        $query = $this->db->get('catalago_datos_abiertos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_archivos() {
        $this->db->order_by('archivos_datos_abiertos_nombre', 'desc');
        $query = $this->db->get('archivos_datos_abiertos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_mapas() {
        $this->db->order_by('mapas_datos_abiertos_nombre', 'desc');
        $query = $this->db->get('mapas_datos_abiertos');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_un_mapas($id) {
        $this->db->where('id_mapas_datos_abiertos' , $id);
        $query = $this->db->get('mapas_datos_abiertos');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function obtener_activos_informacion() {
        $query = $this->db->get('registro_activos_informacion');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_informacion_clasificada_reservada() {
        $query = $this->db->get('informacion_clasificada_reservada');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function obtener_esquema_publicacion() {
        $query = $this->db->get('esquema_publicacion');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

}
