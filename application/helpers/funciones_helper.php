<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('limitarPalabras')) {

    function limitarPalabras($cadena, $longitud, $lenght) {

        $palabras = explode(' ', $cadena);
        if ($longitud == NULL) {
            $longitud = count($palabras);
        } else {
            if (count($palabras) > $longitud) {
                $cadena = implode(' ', array_slice($palabras, 0, $longitud)) . ' ...';
            }
        }
        if ($lenght != NULL) {
            while (strlen($cadena) > $lenght) {
                $cadena = implode(' ', array_slice($palabras, 0, $longitud)) . ' ...';
                $longitud--;
            }
        }
        return $cadena;
    }

}

if (!function_exists('sanear_string')) {

    function sanear_string($string) {

        $string = trim($string);

        $string = str_replace(
                array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string
        );

        $string = str_replace(
                array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string
        );

        $string = str_replace(
                array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string
        );

        $string = str_replace(
                array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string
        );

        $string = str_replace(
                array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string
        );

        $string = str_replace(
                array('ñ', 'Ñ', 'ç', 'Ç'), array('n', 'N', 'c', 'C',), $string
        );
        return $string;
    }

}

if (!function_exists('obtenerFielType')) {

    function obtenerFielType($file) {
        $type = strtolower(substr($file, -4, 4));
        switch ($type) {
            case '.pdf':
                $fiel_type = 'pdf';
                break;
            case '.zip':
                $fiel_type = 'archive';
                break;
            case '.rar':
                $fiel_type = 'archive';
                break;
            case '.mp3':
                $fiel_type = 'audio';
                break;
            case '.wma':
                $fiel_type = 'audio';
                break;
            case '.xml':
                $fiel_type = 'excel';
                break;
            case 'xmlx':
                $fiel_type = 'excel';
                break;
            case '.doc':
                $fiel_type = 'word';
                break;
            case 'docx':
                $fiel_type = 'word';
                break;
            case '.avi':
                $fiel_type = 'video';
                break;
            case 'mpeg':
                $fiel_type = 'video';
                break;
            case '.mpg':
                $fiel_type = 'video';
                break;
            case '.jpg':
                $fiel_type = 'image';
                break;
            case '.png':
                $fiel_type = 'image';
                break;
            case '.gif':
                $fiel_type = 'image';
                break;
            case 'jpeg':
                $fiel_type = 'image';
                break;
            default :
                $fiel_type = 'alt';
                break;
        }
        return $fiel_type;
    }

}

if (!function_exists('periodicidad')) {

    function periodicidad() {
        $type = array(
            "Anual" => "Anual",
            "Primer Semestre" => "Primer Semestre",
            "Segundo Semestre" => "Segundo Semestre",
            "Primer Cuatrimestre" => "Primer Cuatrimestre",
            "Segundo Cuatrimestre" => "Segundo Cuatrimestre",
            "Tercer Cuatrimestre" => "Tercer Cuatrimestre",
            "Primer Trimestre" => "Primer Trimestre",
            "Segundo Trimestre" => "Segundo Trimestre",
            "Tercer Trimestre" => "Tercer Trimestre",
            "Cuatro Trimestre" => "Cuatro Trimestre",
            "Primer Bimestre" => "Primer Bimestre",
            "Segundo Bimestre" => "Segundo Bimestre",
            "Tercer Bimestre" => "Tercer Bimestre",
            "Cuarto Bimestre" => "Cuarto Bimestre",
            "Quinto Bimestre" => "Quinto Bimestre",
            "Sexto Bimestre" => "Sexto Bimestre",
            "Enero" => "Enero",
            "Febrero" => "Febrero",
            "Marzo" => "Marzo",
            "Abril" => "Abril",
            "Mayo" => "Mayo",
            "Junio" => "Junio",
            "Julio" => "Julio",
            "Agosto" => "Agosto",
            "Septiembre" => "Septiembre",
            "Octubre" => "Octubre",
            "Noviembre" => "Noviembre",
            "Diciembre" => "Diciembre",
            "No Aplica" => "No Aplica");
        return $type;
    }

}

if (!function_exists('periodos')) {

    function periodos() {
        $type = array(
            date('Y') => date('Y'),
            date('Y') - 1 => date('Y') - 1,
            date('Y') - 2 => date('Y') - 2,
            date('Y') - 3 => date('Y') - 3,
            date('Y') - 4 => date('Y') - 4,
            date('Y') - 5 => date('Y') - 5,
            date('Y') - 6 => date('Y') - 6,
            date('Y') - 7 => date('Y') - 7,
            date('Y') - 8 => date('Y') - 8,
            date('Y') - 9 => date('Y') - 9,
            date('Y') - 10 => date('Y') - 10,
            date('Y') - 11 => date('Y') - 11
        );
        return $type;
    }

}

if (!function_exists('jsonToTable')) {

    function jsonToTable($data) {
        $table = '
    <table class="json-table table table-sm  table-striped  table-bordered  " >
    ';
        foreach ($data as $key => $value) {
            $table .= '
        <tr valign="top">
        ';
            if (!is_numeric($key)) {
                $table .= '
            <td>
                <strong>' . $key . ':</strong>
            </td>
            <td>
            ';
            } else {
                $table .= '
            <td colspan="2">
            ';
            }
            if (is_object($value) || is_array($value)) {
                $table .= jsonToTable($value);
            } else {
                $table .= $value;
            }
            $table .= '
            </td>
        </tr>
        ';
        }
        $table .= '
    </table>
    ';
        return $table;
    }

}
if (!function_exists('json_validate')) {

    function json_validate($string) {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? TRUE : FALSE;
    }

}