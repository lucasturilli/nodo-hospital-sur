<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entidad extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $calendario = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->helper('cookie');
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index($item = NULL, $search = NULL) {

        $data['main_content'] = 'entidad/home';
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        switch ($item) {
            case 'mision_vision':
                $data['entidad_informacion'] = $this->Datos_entidad_model->obtener_entidad_informacion();
                $data['tab'] = 1;
                $this->breadcrumbs->unshift('Misión y Visión', '#');
                $this->breadcrumbs->unshift_clear('Información General', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'objetivos_funciones':
                $data['entidad_informacion'] = $this->Datos_entidad_model->obtener_entidad_informacion();
                $data['tab'] = 2;
                $this->breadcrumbs->unshift('Objetivos y Funciones', '#');
                $this->breadcrumbs->unshift_clear('Información General', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'organigrama':
                $data['entidad_informacion'] = $this->Datos_entidad_model->obtener_entidad_informacion();
                $data['tab'] = 3;
                $this->breadcrumbs->unshift('Organigrama', '#');
                $this->breadcrumbs->unshift_clear('Información General', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'funcionarios':
                $data['funcionarios'] = $this->Datos_entidad_model->obtener_entidad_funcionarios();
                $data['tab'] = 121;
                $this->breadcrumbs->unshift('Funcionarios', '#');
                $this->breadcrumbs->unshift_clear('Talento Humano ', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'dependencias':
                $data['dependencias'] = $this->Datos_entidad_model->obtener_entidad_dependencias();
                $data['tab'] = 5;
                $this->breadcrumbs->unshift('Dependencias', '#');
                $this->breadcrumbs->unshift_clear('Información General', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'listado_planta':
                $data['planta'] = $this->Datos_entidad_model->obtener_listado_planta();
                $data['tab'] = 122;
                $this->breadcrumbs->unshift('Listado de Planta', '#');
                $this->breadcrumbs->unshift_clear('Talento Humano ', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'asignaciones_salariales':
                $data['asignaciones_salariales'] = $this->Datos_entidad_model->obtener_asiganciones_salarial();
                $data['tab'] = 124;
                $this->breadcrumbs->unshift('Asignaciones Salariales', '#');
                $this->breadcrumbs->unshift_clear('Talento Humano ', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'evaluacion_desempeno':
                $data['evaluacion_desempeno'] = $this->Datos_entidad_model->obtener_evaluacion_desempeno();
                $data['tab'] = 125;
                $this->breadcrumbs->unshift('Evaluación de Desempeño ', '#');
                $this->breadcrumbs->unshift_clear('Talento Humano ', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'noticias':
                $data['tab'] = 23;
                $this->breadcrumbs->unshift('Noticias', '#');
                $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'notificaciones':
                $data['tab'] = 24;
                $this->breadcrumbs->unshift('Notificaciones y Anuncios', '#');
                $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'calendario':
                $data['tab'] = 25;
                $this->breadcrumbs->unshift('Calendario de Actividades', '#');
                $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'encuestas':
                $data['tab'] = 26;
                $this->breadcrumbs->unshift('Encuestas', '#');
                $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'portal_ninos':
                $data['tab'] = 27;
                $this->breadcrumbs->unshift('Portal para Niños y Niñas', '#');
                $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'historico_presupuesto':
                $this->load->model('Datos_financiera_model');
                $data['tab'] = 53;
                $data['historico_presupuesto'] = $this->Datos_financiera_model->obtener_historico_presupuesto();
                $this->breadcrumbs->unshift('Información Histórica de Presupuestos  ', '#');
                $this->breadcrumbs->unshift_clear('Presupuesto', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'politicas':
                $this->load->model('Datos_control_model');
                $data['tab'] = 61;
                $data['politicas'] = $this->Datos_control_model->obtener_politicas();
                $this->breadcrumbs->unshift('Políticas, Lineamientos y Manuales', '#');
                $this->breadcrumbs->unshift_clear('Planeación', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'informe_concejo':
                $this->load->model('Datos_control_model');
                $data['tab'] = 6066;
                $data['informe_concejo'] = $this->Datos_control_model->obtener_informe_concejo();
                $this->breadcrumbs->unshift('Informes al Honorable Concejo ', '#');
                $this->breadcrumbs->unshift_clear('Control', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'informe_fiscal':
                $this->load->model('Datos_control_model');
                $data['tab'] = 6069;
                $data['informe_fiscal'] = $this->Datos_control_model->obtener_informe_fiscal();
                $this->breadcrumbs->unshift('Informes de Rendición Fiscal', '#');
                $this->breadcrumbs->unshift_clear('Control', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'indicadores_gestion':
                $this->load->model('Datos_control_model');
                $data['tab'] = 69;
                $data['indicadores_gestion'] = $this->Datos_control_model->obtener_indicadores_gestion();
                $this->breadcrumbs->unshift('Metas e Indicadores de Gestión', '#');
                $this->breadcrumbs->unshift_clear('Planeación', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;

            case 'planes_otros':
                $this->load->model('Datos_control_model');
                $data['tab'] = 71;
                $data['planes_otros'] = $this->Datos_control_model->obtener_planes_otros();
                $this->breadcrumbs->unshift('Otros Planes', '#');
                $this->breadcrumbs->unshift_clear('Planeación', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;

            case 'poblacion_vulnerable':
                $this->load->model('Datos_control_model');
                $data['tab'] = 6074;
                $data['poblacion_vulnerable'] = $this->Datos_control_model->obtener_poblacion_vulnerable();
                $this->breadcrumbs->unshift('Información para Población Vulnerable', '#');
                $this->breadcrumbs->unshift_clear('Control', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'programas_sociales':
                $this->load->model('Datos_control_model');
                $data['tab'] = 6077;
                $data['programas_sociales'] = $this->Datos_control_model->obtener_programas_sociales();
                $this->breadcrumbs->unshift('Programas Sociales', '#');
                $this->breadcrumbs->unshift_clear('Control', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'defensa_judicial':
                $this->load->model('Datos_control_model');
                $data['tab'] = 6078;
                $data['defensa_judicial'] = $this->Datos_control_model->obtener_defensa_judicial();
                $this->breadcrumbs->unshift('Defensa Judicial', '#');
                $this->breadcrumbs->unshift_clear('Control', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'informe_archivo':
                $this->load->model('Datos_control_model');
                $data['tab'] = 79;
                $data['informe_archivo'] = $this->Datos_control_model->obtener_informe_archivo();
                $this->breadcrumbs->unshift('Informe de Archivo', '#');
                $this->breadcrumbs->unshift_clear('Planeación', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'secop':
                $this->load->model('Datos_contratacion_model');
                $data['tab'] = 92;
                $data['secop'] = $this->Datos_contratacion_model->obtener_secop();
                $this->breadcrumbs->unshift('Información SECOP', '#');
                $this->breadcrumbs->unshift_clear('Contratación', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'inventario_tramites':
                $this->load->model('Datos_servicios_model');
                $data['tab'] = 103;
                $data['inventario'] = $this->Datos_servicios_model->obtener_inventario();
                $this->breadcrumbs->unshift('Inventario de trámites y servicios', '#');
                $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'formularios_tramites':
                $this->load->model('Datos_servicios_model');
                $data['tab'] = 105;
                $data['formularios'] = $this->Datos_servicios_model->obtener_formularios();
                $this->breadcrumbs->unshift('Formularios de trámites', '#');
                $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'certificados':
                $this->load->model('Datos_servicios_model');
                $data['tab'] = 106;
                $data['certificados'] = $this->Datos_servicios_model->obtener_certificados();
                $this->breadcrumbs->unshift('Certificados en línea', '#');
                $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            case 'calidad':
                $data['tab'] = 131;
                $data['calidad'] = $this->Datos_entidad_model->obtener_entidad_calidad();
                $this->breadcrumbs->unshift('Calidad', '#');
                $this->breadcrumbs->unshift_clear('Calidad', '#');
                $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
                break;
            default :
                if ($item == NULL || $item == 0) {
                    $data['tab'] = 0;
                } else {
                    $data['tab'] = $item;
                }
                $this->breadcrumbs->unshift('Nuestra Institución', '#');
                $data['entidad_informacion'] = $this->Datos_entidad_model->obtener_entidad_informacion();
        }
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informacion_interes() {
        $data['main_content'] = 'entidad/informacion_interes';

        $this->breadcrumbs->unshift_clear('Información de Interés', '#');
        $this->breadcrumbs->unshift_clear('Atención al Ciudadano', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'informacion_interes';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function convocatorias() {
        $data['main_content'] = 'entidad/convocatorias';

        $this->breadcrumbs->unshift('Convocatorias ', '#');
        $this->breadcrumbs->unshift_clear('Información de Interés', "#");
        $this->breadcrumbs->unshift_clear('Atención al Ciudadano', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'convocatorias';
        $data['convocatorias'] = $this->Datos_entidad_model->obtener_convocatorias();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function convocatorias_ajax($id) {

        $dato = $this->Datos_entidad_model->obtener_una_convocatoria($id);
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                  <h4 class=" modal-title" id="myModalLabel">' . $dato->convocatorias_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm ">
                    <tr>
                        <th>Descripción</th>
                        <td>' . $dato->convocatorias_descripcion . '</td></tr><tr>
                        <th>Objetivos</th>
                        <td>' . $dato->convocatorias_objetivos . '</td></tr><tr>
                        <th>Requisitos</th>
                        <td>' . $dato->convocatorias_requisitos . '</td></tr><tr>
                        <th>Fecha de Inicio</th>
                        <td>' . $dato->convocatorias_fecha_inicio . '</td></tr><tr>
                        <th>Fecha de Finalización</th>
                        <td>' . $dato->convocatorias_fecha_fin . '</td></tr><tr>
                            <th>Archivo</th>
                        <td><a href="' . site_url('uploads/entidad/informacion_interes') . '/' . $dato->convocatorias_archivo . '" target="_blank" class="btn btn-primary btn-xs">Ver Archivo</a></td></tr><tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function convocatorias_anexos_ajax($id) {
        $this->load->model('Datos_financiera_model');
        $dato = $this->Datos_entidad_model->obtener_convocatorias_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">      
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                 <h4 class=" modal-title" id="myModalLabel">Anexos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->convocatorias_anexos_nombre . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/entidad/informacion_interes') . '/' . $row->convocatorias_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">       
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                 <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function estudios() {
        $data['main_content'] = 'entidad/estudios';

        $this->breadcrumbs->unshift('Estudios, Investigaciones y Otras Publicaciones ', '#');
        $this->breadcrumbs->unshift_clear('Información de Interés', "#");
        $this->breadcrumbs->unshift_clear('Atención al Ciudadano', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'estudios';
        $data['estudios'] = $this->Datos_entidad_model->obtener_estudios();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informacion_adicional() {
        $data['main_content'] = 'entidad/informacion_adicional';

        $this->breadcrumbs->unshift('Información Adicional ', '#');
        $this->breadcrumbs->unshift_clear('Información de Interés', "#");
        $this->breadcrumbs->unshift_clear('Atención al Ciudadano', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'informacion_adicional';
        $data['informacion_adicional'] = $this->Datos_entidad_model->obtener_informacion_adicional();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function mecanismos_contacto($section = NULL) {
        $data['main_content'] = 'entidad/mecanismos_contacto';

        $this->breadcrumbs->unshift('Mecanismo de Contacto', '#');
        $this->breadcrumbs->unshift_clear('Atención al Ciudadano', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'mecanismos_contacto';
        $data['section'] = $section;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function funcionario($id) {
        $data['main_content'] = 'entidad/funcionario';

        $this->breadcrumbs->unshift('Funcionario', '#');
        $this->breadcrumbs->unshift('Funcionarios', site_url('entidad/index/funcionarios'));
        $this->breadcrumbs->unshift_clear('Talento Humano ', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 121;
        $data['funcionario'] = $this->Datos_entidad_model->obtener_entidad_funcionario($id);
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function dependencia($id) {
        $data['main_content'] = 'entidad/dependencia';

        $this->breadcrumbs->unshift('Dependencia', '#');
        $this->breadcrumbs->unshift('Dependencias', site_url('entidad/index/dependencias'));
        $this->breadcrumbs->unshift_clear('Información General', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 5;
        $data['dependencia'] = $this->Datos_entidad_model->obtener_entidad_dependencia($id);
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function glosario($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['main_content'] = 'entidad/glosario';
        $data['letra'] = $search;
        $this->breadcrumbs->unshift('Glosario', '#');
        $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 22;
        $data['glosario'] = $this->Datos_entidad_model->obtener_entidad_glosario_todos();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function directorio_sucursales($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $this->load->model('Datos_normatividad_model');
        $this->breadcrumbs->unshift('Directorio de Sucursales', '#');
        $this->breadcrumbs->unshift_clear('Información General', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/directorio_sucursales';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 8;
        $data['sucursales'] = $this->Datos_entidad_model->obtener_entidad_sucursales();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function directorio_sucursales_ajax($id) {

        $dato = $this->Datos_entidad_model->obtener_entidad_sucursal($id);
        $email = ($dato->entidad_sucursales_email != NULL) ? $dato->entidad_sucursales_email : 'Información no disponible';
        $telefono = ($dato->entidad_sucursales_telefono != NULL) ? $dato->entidad_sucursales_telefono : 'Información no disponible';
        $linea_gratuita = ($dato->entidad_sucursales_linea_gratuita != NULL) ? $dato->entidad_sucursales_linea_gratuita : 'Información no disponible';
        $fax = ($dato->entidad_sucursales_fax != NULL) ? $dato->entidad_sucursales_fax : 'Información no disponible';
        $horarios = ($dato->entidad_sucursales_horarios_de_atencion != NULL) ? $dato->entidad_sucursales_horarios_de_atencion : 'Información no disponible';
        $direccion = ($dato->entidad_sucursales_direccion != NULL) ? $dato->entidad_sucursales_direccion : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class=" modal-title" id="myModalLabel">' . $dato->entidad_sucursales_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Nombre de la Sucursal o Regional</th>
                        <td>' . $dato->entidad_sucursales_nombre . '</td></tr><tr>
                        <th>Correo Electrónico</th>
                        <td>' . $email . '</td></tr><tr>
                        <th>Teléfono</th>
                        <td>' . $telefono . '</td></tr><tr>
                        <th>Línea Telefónica Gratuita</th>
                        <td>' . $linea_gratuita . '</td></tr><tr>
                        <th>Fax</th>
                        <td>' . $fax . '</td></tr><tr>
                        <th>Horarios de Atención</th>
                        <td>' . $horarios . '</td></tr><tr>
                        <th>Dirección o Ubicación</th>
                        <td>' . $direccion . '</td></tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function directorio_entidades($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $this->load->model('Datos_normatividad_model');

        $this->breadcrumbs->unshift('Directorio de Entidades', '#');
        $this->breadcrumbs->unshift_clear('Información General', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/directorio_entidades';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 9;
        $data['entidades'] = $this->Datos_entidad_model->obtener_entidad_entidades();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function directorio_entidades_ajax($id) {

        $dato = $this->Datos_entidad_model->obtener_entidad_entidad($id);
        $email = ($dato->entidad_entidades_email != NULL) ? $dato->entidad_entidades_email : 'Información no disponible';
        $url = ($dato->entidad_entidades_url != NULL) ? '<a href="' . prep_url($dato->entidad_entidades_url) . '" target="_blank">Visitar Sitio</a>' : 'Información no disponible';
        $telefono = ($dato->entidad_entidades_telefono != NULL) ? $dato->entidad_entidades_telefono : 'Información no disponible';
        $linea_gratuita = ($dato->entidad_entidades_linea_gratuita != NULL) ? $dato->entidad_entidades_linea_gratuita : 'Información no disponible';
        $fax = ($dato->entidad_entidades_fax != NULL) ? $dato->entidad_entidades_fax : 'Información no disponible';
        $horarios = ($dato->entidad_entidades_horarios_de_atencion != NULL) ? $dato->entidad_entidades_horarios_de_atencion : 'Información no disponible';
        $direccion = ($dato->entidad_entidades_direccion != NULL) ? $dato->entidad_entidades_direccion : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                        <h4 class=" modal-title" id="myModalLabel">' . $dato->entidad_entidades_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm ">
                    <tr>
                        <th>Nombre de Entidad</th>
                        <td>' . $dato->entidad_entidades_nombre . '</td></tr><tr>
                        <th>Correo Electrónico</th>
                        <td>' . $email . '</td></tr><tr>
                        <th>Sitio Web</th>
                        <td>' . $url . '</td></tr><tr>
                        <th>Teléfono</th>
                        <td>' . $telefono . '</td></tr><tr>
                        <th>Línea Telefónica Gratuita</th>
                        <td>' . $linea_gratuita . '</td></tr><tr>
                        <th>Fax</th>
                        <td>' . $fax . '</td></tr><tr>
                        <th>Horarios de Atención</th>
                        <td>' . $horarios . '</td></tr><tr>
                        <th>Dirección o Ubicación</th>
                        <td>' . $direccion . '</td></tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function directorio_agremiaciones($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $this->load->model('Datos_normatividad_model');

        $this->breadcrumbs->unshift('Directorio de Agremiaciones', '#');
        $this->breadcrumbs->unshift_clear('Información General', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/directorio_agremiaciones';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 10;
        $data['agremiaciones'] = $this->Datos_entidad_model->obtener_entidad_agremiaciones();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function directorio_agremiaciones_ajax($id) {

        $dato = $this->Datos_entidad_model->obtener_entidad_agremiacion($id);
        $tipo = ($dato->entidad_agremiaciones_clasificacion != NULL) ? $dato->entidad_agremiaciones_clasificacion : 'Información no disponible';
        $actividad = ($dato->entidad_agremiaciones_actividad != NULL) ? $dato->entidad_agremiaciones_actividad : 'Información no disponible';
        $email = ($dato->entidad_agremiaciones_email != NULL) ? $dato->entidad_agremiaciones_email : 'Información no disponible';
        $url = ($dato->entidad_agremiaciones_url != NULL) ? '<a href="' . prep_url($dato->entidad_agremiaciones_url) . '" target="_blank">Visitar Sitio</a>' : 'Información no disponible';
        $telefono = ($dato->entidad_agremiaciones_telefono != NULL) ? $dato->entidad_agremiaciones_telefono : 'Información no disponible';
        $fax = ($dato->entidad_agremiaciones_fax != NULL) ? $dato->entidad_agremiaciones_fax : 'Información no disponible';
        $direccion = ($dato->entidad_agremiaciones_direccion != NULL) ? $dato->entidad_agremiaciones_direccion : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                          <h4 class=" modal-title" id="myModalLabel">' . $dato->entidad_agremiaciones_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm">
                    <tr>
                        <th>Nombre de la Agremiación</th>
                        <td>' . $dato->entidad_agremiaciones_nombre . '</td></tr><tr>
                        <th>Clasificación</th>
                        <td>' . $tipo . '</td></tr><tr>
                        <th>Actividad Principal</th>
                        <td>' . $actividad . '</td></tr><tr>
                        <th>Correo Electrónico</th>
                        <td>' . $email . '</td></tr><tr>
                        <th>Sitio Web</th>
                        <td>' . $url . '</td></tr><tr>
                        <th>Teléfono</th>
                        <td>' . $telefono . '</td></tr><tr>
                        <th>Fax</th>
                        <td>' . $fax . '</td></tr><tr>
                        <th>Dirección o Ubicación</th>
                        <td>' . $direccion . '</td></tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function preguntas_frecuentes($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['tab_list'] = $tab_list;
        $data['search'] = $search;
        $this->load->library('pagination');
        $data['main_content'] = 'entidad/preguntas_frecuentes';

        $this->breadcrumbs->unshift('Preguntas y Respuestas Frecuentes', '#');
        $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 21;
        $data['preguntas_frecuentes'] = $this->Datos_entidad_model->obtener_entidad_preguntas_frecuentes();
        $data['dependencias'] = $this->Datos_entidad_model->obtener_entidad_preguntas_frecuentes_dependencias();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function boletines($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['main_content'] = 'entidad/boletines';

        $this->breadcrumbs->unshift('Boletines', '#');
        $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['boletines'] = $this->Datos_entidad_model->obtener_boletines();
        $data['periodos'] = $this->Datos_entidad_model->obtener_boletines_periodos();
        $data['tab'] = 29;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function periodico($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['main_content'] = 'entidad/periodico';
        $this->breadcrumbs->unshift('Periódicos', '#');
        $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['periodicos'] = $this->Datos_entidad_model->obtener_periodico();
        $data['periodos'] = $this->Datos_entidad_model->obtener_periodico_periodos();
        $data['tab'] = 30;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function ofertas_empleo() {

        $this->breadcrumbs->unshift('Ofertas de Empleo', '#');
        $this->breadcrumbs->unshift_clear('Servicios de Información', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/oferta_empleo';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 28;
        $data['empleo'] = $this->Datos_entidad_model->obtener_entidad_ofertas_empleo();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function ofertas_empleo_ajax($id) {

        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $dato = $this->Datos_entidad_model->obtener_entidad_oferta_empleo($id);
        $descripcion = ($dato->entidad_ofertas_empleo_descripcion != NULL) ? $dato->entidad_ofertas_empleo_descripcion : 'Información no disponible';
        $perfil = ($dato->entidad_ofertas_empleo_perfil != NULL) ? $dato->entidad_ofertas_empleo_perfil : 'Información no disponible';
        $fecha_inicio = ($dato->entidad_ofertas_empleo_fecha_inicio != NULL) ? date('d', strtotime($dato->entidad_ofertas_empleo_fecha_inicio)) . " de " . $meses[(date('n', strtotime($dato->entidad_ofertas_empleo_fecha_inicio)) - 1)] . " del " . date('Y', strtotime($dato->entidad_ofertas_empleo_fecha_inicio)) : 'Información no disponible';
        $fecha_fin = ($dato->entidad_ofertas_empleo_fecha_fin != NULL) ? date('d', strtotime($dato->entidad_ofertas_empleo_fecha_fin)) . " de " . $meses[date('n', (strtotime($dato->entidad_ofertas_empleo_fecha_fin)) - 1)] . " del " . date('Y', strtotime($dato->entidad_ofertas_empleo_fecha_fin)) : 'Información no disponible';
        $contacto = ($dato->entidad_ofertas_empleo_contacto != NULL) ? $dato->entidad_ofertas_empleo_contacto : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class=" modal-title" id="myModalLabel">' . $dato->entidad_ofertas_empleo_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Titulo</th>
                        <td>' . $dato->entidad_ofertas_empleo_nombre . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $descripcion . '</td></tr><tr>
                        <th>Perfil</th>
                        <td>' . $perfil . '</td></tr><tr>
                        <th>Fecha de Inicio</th>
                        <td>' . $fecha_inicio . '</td></tr><tr>
                        <th>Fecha de Finalización</th>
                        <td>' . $fecha_fin . '</td></tr><tr>
                        <th>Contacto</th>
                        <td>' . $contacto . '</td></tr><tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function manual_funciones($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['main_content'] = 'entidad/manual_funciones';
        $this->breadcrumbs->unshift('Manual de Funciones y Competencias', '#');
        $this->breadcrumbs->unshift_clear('Talento Humano', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['periodos'] = $this->Datos_entidad_model->obtener_definicion_manual_funciones();
        $data['manual_funciones'] = $this->Datos_entidad_model->obtener_manual_funciones();
        $data['tab'] = 123;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function leyes($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 3;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/leyes';
        $this->breadcrumbs->unshift('Leyes / Ordenanzas / Acuerdos', '#');
        $this->breadcrumbs->unshift_clear('Normatividad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 41;
        $data['leyes'] = $this->Datos_normatividad_model->obtener_nomatividad_leyes();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function leyes_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_ley($id);
        if ($dato->normatividad_leyes_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_leyes_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_leyes_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_leyes_nombre . '</td></tr><tr>
                        <th>Tipo de Norma</th>
                        <td>' . $dato->normatividad_leyes_tipo . '</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_leyes_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_leyes_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_leyes_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_leyes_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function decretos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 2;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/decretos';
        $this->breadcrumbs->unshift('Decretos', '#');
        $this->breadcrumbs->unshift_clear('Normatividad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 42;
        $data['decretos'] = $this->Datos_normatividad_model->obtener_nomatividad_decretos();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function decretos_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_decreto($id);
        if ($dato->normatividad_decretos_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_decretos_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_decretos_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_decretos_nombre . '</td></tr><tr>
                        <th>Tipo de Norma</th>
                        <td>Decreto</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_decretos_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_decretos_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_decretos_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_decretos_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function resoluciones($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 2;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/resoluciones';
        $this->breadcrumbs->unshift('Resoluciónes', '#');
        $this->breadcrumbs->unshift_clear('Normatividad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 43;
        $data['resoluciones'] = $this->Datos_normatividad_model->obtener_nomatividad_resoluciones();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function resoluciones_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_resolucion($id);
        if ($dato->normatividad_resoluciones_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_resoluciones_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_resoluciones_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_resoluciones_nombre . '</td></tr><tr>
                        <th>Tipo de Norma</th>
                        <td>Resolución</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_resoluciones_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_resoluciones_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_resoluciones_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_resoluciones_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function circulares($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 3;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/circulares';
        $this->breadcrumbs->unshift('Circulares / Otros', '#');
        $this->breadcrumbs->unshift_clear('Normatividad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 44;
        $data['resoluciones'] = $this->Datos_normatividad_model->obtener_nomatividad_resoluciones();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function circulares_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_resolucion($id);
        if ($dato->normatividad_resoluciones_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_resoluciones_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_resoluciones_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_resoluciones_nombre . '</td></tr><tr>
                        <th>Tipo de Norma</th>
                        <td>' . $dato->normatividad_resoluciones_tipo . '</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_resoluciones_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_resoluciones_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_resoluciones_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_resoluciones_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function politicas($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 3;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/politicas';
        $this->breadcrumbs->unshift('Políticas / Lineamientos / Manuales', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 89;
        $data['politicas'] = $this->Datos_normatividad_model->obtener_nomatividad_politicas();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function politicas_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_politica($id);
        if ($dato->normatividad_politicas_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_politicas_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_politicas_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_politicas_nombre . '</td></tr><tr>
                        <th>Tipo de Norma</th>
                        <td>' . $dato->normatividad_politicas_tipo . '</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_politicas_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_politicas_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_politicas_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_politicas_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function edictos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['shorting'] = 2;
        $data['shorting_type'] = 'desc';

        $this->load->model('Datos_normatividad_model');
        $data['main_content'] = 'entidad/edictos';
        $this->breadcrumbs->unshift('Edictos', '#');
        $this->breadcrumbs->unshift_clear('Normatividad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 45;
        $data['edictos'] = $this->Datos_normatividad_model->obtener_nomatividad_edictos();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function edictos_ajax($id) {
        $this->load->model('Datos_normatividad_model');
        $dato = $this->Datos_normatividad_model->obtener_normatividad_edicto($id);
        if ($dato->normatividad_edictos_fecha_ingreso != NULL) {
            $fecha = '    <tr>
        <th>Fecha de Ingreso</th>
        <td>' . $dato->normatividad_edictos_fecha_ingreso . '</td></tr>
    <tr>';
        } else {
            $fecha = '';
        }
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->normatividad_edictos_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $dato->normatividad_edictos_nombre . '</td></tr><tr>
                        <th>Temática</th>
                        <td>' . $dato->normatividad_edictos_tematica . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $dato->normatividad_edictos_descripcion . '</td></tr><tr>
                        <th>Fecha de Expedición</th>
                        <td>' . $dato->normatividad_edictos_fecha_expedicion . '</td></tr><tr>' . $fecha . '
                        <th>Archivo</th>
                        <td><a target="_blank"  href="' . site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_edictos_archivo . '"  class="btn btn-primary btn-xs"><i class="fas fa-file-pdf"> Ver Documento</i></a></td>
                            
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function presupuesto() {

        $this->load->model('Datos_financiera_model');
        $data['presupuesto'] = $this->Datos_financiera_model->obtener_presupuesto();
        $this->breadcrumbs->unshift('Presupuesto Aprobado en Ejercicio', '#');
        $this->breadcrumbs->unshift_clear('Presupuesto', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/presupuesto';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 51;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function presupuesto_ajax($year = NULL) {
        if ($year == NULL) {
            $y = date('Y');
        } else {
            $y = $year;
        }
        $this->load->model('Datos_financiera_model');
        $dato = $this->Datos_financiera_model->obtener_presupuesto_ejecucion($y);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Ejecuciones</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Periodo</th><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->periodo . '</td>';
                $texto .= '<td>' . $row->financiera_presupuesto_ejecucion_trimestre . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/entidad/financiera') . '/' . $row->financiera_presupuesto_ejecucion_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances en la ejecución del presupuesto  </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function presupuesto_historico() {

        $this->load->model('Datos_financiera_model');
        $data['presupuesto'] = $this->Datos_financiera_model->obtener_presupuesto_historico();
        $this->breadcrumbs->unshift('Información Histórica de Presupuestos ', '#');
        $this->breadcrumbs->unshift_clear('Presupuesto', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/presupuesto_historico';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 53;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function presupuesto_estatuto_tributario($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_financiera_model');
        $data['presupuesto'] = $this->Datos_financiera_model->obtener_estatuto_tributario();
        $data['periodos'] = $this->Datos_financiera_model->obtener_estatuto_tributario_periodo();
        $this->breadcrumbs->unshift('Estatuto Tributario', '#');
        $this->breadcrumbs->unshift_clear('Presupuesto', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/estatuto_tributario';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 55;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function estados_financieros($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_financiera_model');
        $data['periodos_financieros'] = $this->Datos_financiera_model->obtener_periodos_estados_financieros();
        $data['estados_financieros'] = $this->Datos_financiera_model->obtener_estados_financieros();
        $this->breadcrumbs->unshift('Estados Financieros ', '#');
        $this->breadcrumbs->unshift_clear('Presupuesto', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/estados_financieros';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 54;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function plan_anticorrupcion($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/plan_anticorrupcion';
        $this->load->model('Datos_control_model');

        $data['tab'] = 65;
        $data['periodos'] = $this->Datos_control_model->obtener_periodos_anticorrupcion();
        $data['anticorrupcion'] = $this->Datos_control_model->obtener_anticorrupcion_definicion();
        $this->breadcrumbs->unshift('Plan Anticorrupción', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function plan_anticorrupcion_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->anticorrupcion_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<td>' . $row->anticorrupcion_definicion_anexos_nombre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/anticorrupcion') . '/' . $row->anticorrupcion_definicion_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function plan_estrategico($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/plan_estrategico';
        $this->load->model('Datos_control_model');
        $data['tab'] = 62;
        $data['plan_estrategico'] = $this->Datos_control_model->obtener_plan_estrategico();
        $data['plan_estrategico_vigente'] = $this->Datos_control_model->obtener_plan_estrategico_vigente();
        $this->breadcrumbs->unshift('Plan de Desarrollo ', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function plan_estrategico_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->plan_estrategico_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<td>' . $row->control_plan_estrategico_avances_nombre . '</td>';
                $texto .= '<td>' . $row->control_plan_estrategico_avances_periodicidad . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $row->control_plan_estrategico_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function plan_accion($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['shorting'] = 2;
        $data['shorting_type'] = 'desc';
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/plan_accion';
        $this->load->model('Datos_control_model');

        $data['tab'] = 63;
        $data['plan_accion'] = $this->Datos_control_model->obtener_plan_accion();
        $data['dependencias'] = $this->Datos_control_model->obtener_plan_accion_dependencias();
        $this->breadcrumbs->unshift('Plan de Acción', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    //$data['plan_accion_avances'] = $this->Datos_control_model->obtener_avances_plan_accion(date('Y'));


    public function plan_accion_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_avances_plan_accion($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Avances del Plan de Acción</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {

                $texto .= '<td>' . $row->control_plan_accion_avances_trimestre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $row->control_plan_accion_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances para el plan de acción</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function informe_empalme($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['main_content'] = 'entidad/empalme';

        $this->load->model('Datos_control_model');
        $data['tab'] = 73;
        $data['informe_empalme'] = $this->Datos_control_model->obtener_informe_empalme();
        $this->breadcrumbs->unshift('Informes de Empalme', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informe_empalme_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_anexos_obtener_informe_empalme($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Fecha de Publicación</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {

                $texto .= '<td>' . $row->control_informe_empalme_anexos_nombre . '</td>';
                $texto .= '<td>' . $row->control_informe_empalme_anexos_fecha . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $row->control_informe_empalme_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos para el informe</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function control_archivo($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = NULL;
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/control_archivo';

        $this->breadcrumbs->unshift('Informes de Archivo', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['archivos'] = $this->Datos_control_model->obtener_informe_archivo();
        $data['tabs'] = $this->Datos_control_model->obtener_informe_archivo_tab();
        $data['categorias'] = $this->Datos_control_model->obtener_informe_archivo_categoria();
        $data['tab'] = 79;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function reportes_control_interno($tab_list = NULL, $period = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['period'] = $period;
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/control_interno';
        $this->breadcrumbs->unshift('Reportes de Control Interno', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 6072;
        $data['tabs'] = $this->Datos_control_model->obtener_reportes_control_interno_tab();
        $data['reportes_control_interno'] = $this->Datos_control_model->obtener_reportes_control_interno();
        $data['periodos_control_interno'] = $this->Datos_control_model->obtener_periodos_reportes_control_interno();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function planeacion_plan_gasto_publico($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['main_content'] = 'entidad/plan_gasto_publico';
        $this->load->model('Datos_control_model');

        $data['tab'] = 80;
        $data['periodos'] = $this->Datos_control_model->obtener_periodos_plan_gasto_publico();
        $data['planes'] = $this->Datos_control_model->obtener_planeacion_plan_gasto_publico();
        $this->breadcrumbs->unshift('Plan de Gasto Publico', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function programas_proyectos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/programas_proyectos';
        $this->load->model('Datos_control_model');
        $data['tab'] = 64;
        $data['programas'] = $this->Datos_control_model->obtener_programas();
        $this->breadcrumbs->unshift('Programas y Proyectos en Ejecución', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function programas_proyectos_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_programa_avances($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Avances del Proyecto</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                <tr><th>Periodo</th><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->control_programas_avances_periodo . '</td>';
                $texto .= '<td>' . $row->control_programas_avances_trimestre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $row->control_programas_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances para el programa</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function entes($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/entes';
        $this->load->model('Datos_control_model');
        $data['tab'] = 6065;
        $data['entes'] = $this->Datos_control_model->obtener_entes();
        $this->breadcrumbs->unshift('Entes de Control que Vigilan la Entidad ', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function entes_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_ente($id);
        $nombre = ($dato->control_entes_nombre != NULL) ? $dato->control_entes_nombre : 'Información no disponible';
        $tipo = ($dato->control_entes_tipo_control != NULL) ? $dato->control_entes_tipo_control : 'Información no disponible';
        $descripcion = ($dato->control_entes_descripcion != NULL) ? $dato->control_entes_descripcion : 'Información no disponible';
        $url = ($dato->control_entes_url != NULL) ? '<a target="_blank" href="' . prep_url($dato->control_entes_url) . '">' . $dato->control_entes_url . '</a>' : 'Información no disponible';
        $telefono = ($dato->control_entes_telefono_contacto != NULL) ? $dato->control_entes_telefono_contacto : 'Información no disponible';
        $fax = ($dato->control_entes_fax_contacto != NULL) ? $dato->control_entes_fax_contacto : 'Información no disponible';
        $direccion = ($dato->control_entes_direccion != NULL) ? $dato->control_entes_direccion : 'Información no disponible';
        $ciudad = ($dato->control_entes_ciudad != NULL) ? $dato->control_entes_ciudad : 'Información no disponible';
        $email = ($dato->control_entes_email_contacto != NULL) ? $dato->control_entes_email_contacto : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->control_entes_nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $nombre . '</td></tr><tr>
                        <th>Tipo de Control</th>
                        <td>' . $tipo . '</td></tr><tr>
                        <th>Descripción del Ente</th>
                        <td>' . $descripcion . '</td></tr><tr>
                        <th>Sitio Web</th>
                        <td>' . $url . '</td></tr><tr>
                        <th>Correo Electrónico </th>
                        <td>' . $email . '</td></tr><tr>
                              <th>Teléfono </th>
                        <td>' . $telefono . '</td></tr><tr>
                              <th>Fax</th>
                        <td>' . $fax . '</td></tr><tr>
                                        <th>Dirección </th>
                        <td>' . $direccion . '</td></tr><tr>
                                        <th>Ciudad</th>
                        <td>' . $ciudad . '</td></tr><tr>
       
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function informe_ciudadania($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['informe_ciudadania'] = $this->Datos_control_model->obtener_informe_ciudadania();
        $data['informe_gestion'] = $this->Datos_control_model->obtener_informes_gestion_ciudadania();
        $data['informe_cronograma'] = $this->Datos_control_model->obtener_informes_cronograma();
        $data['informe_evaluacion'] = $this->Datos_control_model->obtener_informes_evaluacion();
        $this->breadcrumbs->unshift('Informes de Rendición de Cuenta a los Ciudadanos', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/informe_ciudadania';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 6068;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informe_ciudadania_anexos_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_informe_ciudadania_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos del Informe</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->control_informe_ciudadania_anexos_nombre . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/entidad/control') . '/' . $row->control_informe_ciudadania_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos para el informe</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function informe_ciudadania_evaluacion_anexos_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_informe_ciudadania_evaluacion_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos del Informe</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->control_informes_anexos_nombre . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/entidad/control') . '/' . $row->control_informes_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos para el informe</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function auditorias($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['auditorias_periodos'] = $this->Datos_control_model->obtener_periodos_auditorias();
        $data['informe_contraloria'] = $this->Datos_control_model->obtener_informe_contraloria();
        $data['auditorias'] = $this->Datos_control_model->obtener_auditorias();
        $this->breadcrumbs->unshift('Auditorías ', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/auditorias';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 6067;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function planes_mejoramiento($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Datos_control_model');
        $data['tab'] = 6070;
        $data['planes_mejoramiento'] = $this->Datos_control_model->obtener_planes_mejoramiento();
        $data['planes_mejoramiento_periodo'] = $this->Datos_control_model->obtener_periodos_planes_mejoramiento();
        $this->breadcrumbs->unshift('Planes de Mejoramiento', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/planes_mejoramiento';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function programas_poblacion_vulnerable($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/programas_poblacion_vulnerable';
        $this->load->model('Datos_control_model');
        $data['tab'] = 6074;
        $data['programas'] = $this->Datos_control_model->obtener_poblacion_vulnerable_programas();
        $this->breadcrumbs->unshift('Programas para la Población Vulnerable', '#');
        $this->breadcrumbs->unshift('Población Vulnerable', site_url('entidad/index/poblacion_vulnerable'));
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function programas_poblacion_vulnerable_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_poblacion_vulnerable_programa($id);
        $nombre = ($dato->control_poblacion_vulnerable_programas_nombre != NULL) ? $dato->control_poblacion_vulnerable_programas_nombre : 'Información no disponible';
        $descripcion = ($dato->control_poblacion_vulnerable_programas_descripcion != NULL) ? $dato->control_poblacion_vulnerable_programas_descripcion : 'Información no disponible';
        $archivo = ($dato->control_poblacion_vulnerable_programas_archivo != NULL) ? '<a class="btn btn-primary btn-xs"  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $dato->control_poblacion_vulnerable_programas_archivo . '"><i  class="fas fa-file-pdf"> </i> Ver Archivo</a>' : 'Información no disponible';
        $fecha = ($dato->control_poblacion_vulnerable_programas_fecha != NULL) ? $dato->control_poblacion_vulnerable_programas_fecha : 'Información no disponible';
        $dirigido_a = ($dato->control_poblacion_vulnerable_programas_dirigido_a != NULL) ? $dato->control_poblacion_vulnerable_programas_dirigido_a : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $nombre . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $descripcion . '</td></tr><tr>
                        <th>Dirigido a la Población </th>
                        <td>' . $dirigido_a . '</td></tr><tr>
                        <th>Fecha</th>
                        <td>' . $fecha . '</td></tr><tr>
                        <th>Archivo</th>
                        <td>' . $archivo . '</td></tr><tr>

              
       
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function programas_poblacion_vulnerable_avances_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_poblacion_vulnerable_programas_avances($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Avances del Programa</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                <tr><th>Periodo</th><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->control_poblacion_vulnerable_programas_avances_periodo . '</td>';
                $texto .= '<td>' . $row->control_poblacion_vulnerable_programas_avances_trimestre . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/entidad/control') . '/' . $row->control_poblacion_vulnerable_programas_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances para el programa</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function proyectos_poblacion_vulnerable($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/proyectos_poblacion_vulnerable';
        $this->load->model('Datos_control_model');
        $data['tab'] = 6074;
        $data['proyectos'] = $this->Datos_control_model->obtener_poblacion_vulnerable_proyectos();
        $this->breadcrumbs->unshift('Proyectos para la Población Vulnerable', '#');
        $this->breadcrumbs->unshift('Población Vulnerable', site_url('entidad/index/poblacion_vulnerable'));
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function proyectos_poblacion_vulnerable_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_poblacion_vulnerable_proyecto($id);
        $nombre = ($dato->control_poblacion_vulnerable_proyectos_nombre != NULL) ? $dato->control_poblacion_vulnerable_proyectos_nombre : 'Información no disponible';
        $descripcion = ($dato->control_poblacion_vulnerable_proyectos_descripcion != NULL) ? $dato->control_poblacion_vulnerable_proyectos_descripcion : 'Información no disponible';
        $archivo = ($dato->control_poblacion_vulnerable_proyectos_archivo != NULL) ? '<a class="btn  btn-primary btn-xs" target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $dato->control_poblacion_vulnerable_proyectos_archivo . '"><i class="fas fa-file-pdf"> Ver Archivo</a>' : 'Información no disponible';
        $fecha = ($dato->control_poblacion_vulnerable_proyectos_fecha != NULL) ? $dato->control_poblacion_vulnerable_proyectos_fecha : 'Información no disponible';
        $dirigido_a = ($dato->control_poblacion_vulnerable_proyectos_dirigido_a != NULL) ? $dato->control_poblacion_vulnerable_proyectos_dirigido_a : 'Información no disponible';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $nombre . '</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                    <tr>
                        <th>Nombre</th>
                        <td>' . $nombre . '</td></tr><tr>
                        <th>Descripción</th>
                        <td>' . $descripcion . '</td></tr><tr>
                        <th>Dirigido a la Población </th>
                        <td>' . $dirigido_a . '</td></tr><tr>
                        <th>Fecha</th>
                        <td>' . $fecha . '</td></tr><tr>
                        <th>Archivo</th>
                        <td>' . $archivo . '</td></tr><tr>

              
       
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function proyectos_poblacion_vulnerable_avances_ajax($id) {
        $this->load->model('Datos_control_model');
        $dato = $this->Datos_control_model->obtener_poblacion_vulnerable_proyectos_avances($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Avances del Proyecto</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover">
                <tr><th>Periodo</th><th>Periodicidad</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->control_poblacion_vulnerable_proyectos_avances_periodo . '</td>';
                $texto .= '<td>' . $row->control_poblacion_vulnerable_proyectos_avances_trimestre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/control') . '/' . $row->control_poblacion_vulnerable_proyectos_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances para el programa</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function otros_informes($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $this->load->model('Datos_control_model');
        $data['tab'] = 6080;
        $data['informes'] = $this->Datos_control_model->obtener_control_otros_informes();
        $this->breadcrumbs->unshift('Otros Informes', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/control_otros_informes';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function control_anuario_estadistico($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['main_content'] = 'entidad/anuario_estadistico';
        $this->load->model('Datos_control_model');
        $data['tab'] = NULL;
        $data['anuarios'] = $this->Datos_control_model->obtener_control_anuario_estaditico();
        $data['periodos'] = $this->Datos_control_model->obtener_control_anuario_estaditico_periodos();
        $this->breadcrumbs->unshift('Anuarios Estadísticos', '#');
        $this->breadcrumbs->unshift_clear('Control', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function plan_compras($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $this->load->model('Datos_contratacion_model');
        $data['tab'] = 91;
        $data['shorting'] = 1;
        $data['shorting_type'] = 'desc';
        $data['plan_compras'] = $this->Datos_contratacion_model->obtener_plan_compras();
        $this->breadcrumbs->unshift('Plan de Adquisiciones', '#');
        $this->breadcrumbs->unshift_clear('Contratación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['main_content'] = 'entidad/plan_compras';
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function contratos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/contratos';
        $this->load->model('Datos_contratacion_model');
        $data['tab'] = 93;
        $data['shorting'] = 3;
        $data['shorting_type'] = 'desc';
        $data['contratos'] = $this->Datos_contratacion_model->obtener_contratos();
        $this->breadcrumbs->unshift('Contratos', '#');
        $this->breadcrumbs->unshift_clear('Contratación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function avance_contractual($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $data['main_content'] = 'entidad/avance_contractual';
        $this->load->model('Datos_contratacion_model');
        $data['tab'] = 95;
        $data['avance_contractual'] = $this->Datos_contratacion_model->obtener_avance_contractual();
        $data['periodos'] = $this->Datos_contratacion_model->obtener_periodos_avance_contractual();
        $this->breadcrumbs->unshift('Contratos', '#');
        $this->breadcrumbs->unshift_clear('Contratación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function convenios($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/convenios';
        $this->load->model('Datos_contratacion_model');
        $data['tab'] = 94;
        $data['shorting'] = 2;
        $data['shorting_type'] = 'desc';
        $data['convenios'] = $this->Datos_contratacion_model->obtener_convenios();
        $this->breadcrumbs->unshift('Convenios', '#');
        $this->breadcrumbs->unshift_clear('Contratación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function contratacion_lineamientos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/contratacion_lineamientos';
        $this->load->model('Datos_contratacion_model');
        $data['tab'] = 96;
        $data['lineamientos'] = $this->Datos_contratacion_model->obtener_lineamientos();
        $this->breadcrumbs->unshift('Lineamientos de Adquisiciones  y Compras', '#');
        $this->breadcrumbs->unshift_clear('Contratación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informe_pqrs() {
        $data['main_content'] = 'entidad/informe_pqrs';
        $this->load->model('Datos_servicios_model');
        $data['pqrdf'] = $this->Datos_servicios_model->obtener_pqrdf();
        $this->breadcrumbs->unshift('Informe de peticiones, quejas y reclamos', '#');
        $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'convocatorias';
        $data['tab'] = 6079;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informe_pqrs_ajax($id) {
        $this->load->model('Datos_servicios_model');
        $dato = $this->Datos_servicios_model->obtener_pqrdf_anexos($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Anexos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {
                $texto .= '<td>' . $row->servicios_pqrdf_anexos_nombre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/servicios') . '/' . $row->servicios_pqrdf_anexos_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron anexos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

    public function tramites($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $this->load->library('pagination');
        $data['main_content'] = 'entidad/tramites';
        $this->load->model('Datos_servicios_model');
        $data['tab'] = 101;
        $data['tramites'] = $this->Datos_servicios_model->obtener_servicios();
        $this->breadcrumbs->unshift('Listado de Trámites y Servicios', '#');
        $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function tramites_ajax($id) {
        $this->load->model('Datos_servicios_model');
        $dato = $this->Datos_servicios_model->obtener_servicio($id);
        $enlinea = ($dato->servicios_tramites_enlace != NULL) ? '<a  target="_blank" href="' . prep_url($dato->servicios_tramites_enlace) . '"  class="btn btn-primary btn-xs">Realizar tramite en línea</a>' : 'No';
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Descripción del tramite</h4>
            </div>
            <div class="modal-body">
<dl>

        <dt>Tramite<br /></dt>
  <dd>' . $dato->servicios_tramites_nombre . '<br /><br /></dd>
        <dt>Descripción<br /></dt>
  <dd>' . strip_tags($dato->servicios_tramites_descripcion) . '</dd>
</dl>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    function tramites_en_linea($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->library('pagination');
        $data['main_content'] = 'entidad/tramites_en_linea';
        $this->load->model('Datos_servicios_model');
        $data['tab'] = 104;
        $data['tramites_en_linea'] = $this->Datos_servicios_model->obtener_tramites_en_linea();
        $data['dependencias'] = $this->Datos_servicios_model->obtener_tramites_en_linea_dependencias();
        $this->breadcrumbs->unshift('Trámites y Servicios en línea ', '#');
        $this->breadcrumbs->unshift_clear('Trámites y Servicios', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function tramites_en_linea_ajax($id) {
        $this->load->model('Datos_servicios_model');
        $dato = $this->Datos_servicios_model->obtener_tramite_en_linea($id);
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Descripción del tramite</h4>
            </div>
            <div class="modal-body">
<dl>

        <dt>Tramite<br /></dt>
  <dd>' . $dato->servicios_tramites_en_linea_nombre . '<br /><br /></dd>
        <dt>Descripción<br /></dt>
  <dd>' . strip_tags($dato->servicios_tramites_en_linea_descripcion) . '<br /><br /></dd>
              <dt>Requisitos<br /></dt>
  <dd>' . $dato->servicios_tramites_en_linea_requisitos . '</dd>
</dl>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function tramites_en_linea_embebbed($id) {
        $this->load->model('Datos_servicios_model');
        $tramite = $this->Datos_servicios_model->obtener_tramite_en_linea($id);
        echo $tramite->servicios_tramites_en_linea_embedded;
    }

    public function documentos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['main_content'] = 'entidad/documentos';
        $this->breadcrumbs->unshift(' Documentos de la Alcaldía', '#');
        $this->breadcrumbs->unshift_clear('Documentos', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab'] = 111;
        $data['documentos'] = $this->Datos_entidad_model->obtener_entidad_documentoss();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function transparencia() {

        $data['main_content'] = 'entidad/transparencia';
        $this->breadcrumbs->unshift('Transparencia y acceso a información publica', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['tab_menu'] = 'transparencia';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function datos_abiertos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Nuestra Institución', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/datos_abiertos';
        $data['archivos'] = $this->Datos_abiertos_model->obtener_archivos();
        $data['tab'] = 141;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function catalogo_abiertos($tab = NULL) {

        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Catálogo de Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Nuestra Institución', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/catalogo_abiertos';
        $data['catalogo'] = $this->Datos_abiertos_model->obtener_catalogo();
        $data['tab'] = 142;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function mapas_abiertos($tab = NULL) {

        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Mapas de Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Datos Abiertos', '#');
        $this->breadcrumbs->unshift_clear('Nuestra Institución', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/mapas_abiertos';
        $data['mapas'] = $this->Datos_abiertos_model->obtener_mapas();
        $data['tab'] = 143;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function mapas_ajax($id) {
        $this->load->model('Datos_abiertos_model');
        $dato = $this->Datos_abiertos_model->obtener_un_mapas($id);
        $texto = '        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">' . $dato->mapas_datos_abiertos_nombre . '</h4>
            </div>
            <div class="modal-body">' . $dato->mapas_datos_abiertos_embebido . '</div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function registro_activos_informacion($tab = NULL) {

        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Registro de Activos de Información', '#');
        $this->breadcrumbs->unshift('Transparencia y acceso a información publica', site_url("entidad/transparencia"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/registro_activos_informacion';
        $data['registros'] = $this->Datos_abiertos_model->obtener_activos_informacion();
        $data['tab'] = $tab;
        $data['tab_menu'] = 'transparencia';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function informacion_clasificada_reservada($tab = NULL) {
        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Índice de Información Clasificada y Reservada', '#');
        $this->breadcrumbs->unshift('Transparencia y acceso a información publica', site_url("entidad/transparencia"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/informacion_clasificada_reservada';
        $data['registros'] = $this->Datos_abiertos_model->obtener_informacion_clasificada_reservada();
        $data['tab'] = $tab;
        $data['tab_menu'] = 'transparencia';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function esquema_publicacion($tab = NULL) {
        $this->load->model('Datos_abiertos_model');
        $this->breadcrumbs->unshift_clear('Esquema de Publicación ', '#');
        $this->breadcrumbs->unshift('Transparencia y acceso a información publica', site_url("entidad/transparencia"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/esquema_publicacion';
        $data['registros'] = $this->Datos_abiertos_model->obtener_esquema_publicacion();
        $data['tab'] = $tab;
        $data['tab_menu'] = 'transparencia';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function registro_publicaciones($tab = NULL) {

        $this->breadcrumbs->unshift_clear('Registro de Publicaciones', '#');
        $this->breadcrumbs->unshift('Transparencia y acceso a información publica', site_url("entidad/transparencia"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'entidad/registro_publicaciones';
        $data['audits'] = $this->Aplicaciones_model->obtener_audits();
        $data['tab'] = $tab;
        $data['tab_menu'] = 'transparencia';
        $data['columns'] = array('Fecha', 'Tipo', 'Sección', 'Identificador', 'Datos');
        $data['ajax'] = 'registro_publicaciones_ajax_list';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function registro_publicaciones_ajax_list() {
        $table = 'audit';
        $column_order = array(
            'audit_fecha',
            'audit_type',
            'audit_table',
            'audit_key_in_table',
            NULL
        ); //set column field database for datatable orderable
        $column_search = array(
            'audit_fecha',
            'audit_type',
            'audit_table',
            'audit_key_in_table',
            'audit_data'
        ); //set column field database for datatable searchable 
        $order = array('audit_fecha' => 'desc'); // default order 
        $condiciones = array();
        $list = $this->Aplicaciones_model->get_datatables_registro_publicaciones($table, $column_order, $column_search, $order, $condiciones);
        $data = array();
        $no = $_POST['start'];
        foreach ($list[0] as $dato) {
            $no++;
            $table = json_validate($dato->audit_data) == FALSE ? $dato->audit_data : jsonToTable(json_decode($dato->audit_data));
            switch ($dato->audit_type) {
                case "UPDATE":
                    $accion = 'Actualización';
                    break;
                case 'DELETE':
                    $accion = 'Eliminación';
                    break;
                case "INSERT":
                    $accion = 'Inserción';
                    break;
                default:
                    $accion = "N/A";
                    break;
            }
            $row = array();
            $row[] = $dato->audit_fecha;
            $row[] = $accion;
            $row[] = str_replace("_", " ", "$dato->audit_table");
            $row[] = $dato->audit_key_in_table;
            $row[] = $table;
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $list[1],
            "recordsFiltered" => $list[2],
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    /*     * ****************************CALIDAD******************************************* */

    public function caracterizacion_procesos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/caracterizacion_procesos';
        $this->load->model('Datos_entidad_model');
        $data['tab'] = 132;
        $data['contratos'] = $this->Datos_entidad_model->obtener_entidad_caracterizacion_procesos();
        $this->breadcrumbs->unshift('Caracterización de procesos', '#');
        $this->breadcrumbs->unshift_clear('Calidad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function procedimientos($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;

        $data['main_content'] = 'entidad/procedimientos';
        $this->load->model('Datos_entidad_model');
        $data['tab'] = 133;
        $data['contratos'] = $this->Datos_entidad_model->obtener_entidad_procedimientos();
        $this->breadcrumbs->unshift('Procedimientos', '#');
        $this->breadcrumbs->unshift_clear('Calidad', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    /* ===============================ADICIONAL================================================ */

    public function mipg($tab_list = NULL, $search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $data['tab_list'] = $tab_list;
        $this->load->model('Adicional_model');
        $data['main_content'] = 'entidad/mipg';
        $data['tab'] = 81;
        $data['plan'] = $this->Adicional_model->get_entidad_mipg_plan();
        $data['planes'] = $this->Adicional_model->get_entidad_mipg_planes();
        $this->breadcrumbs->unshift('Modelo Integrado de Planeación y Gestión “MIPG”', '#');
        $this->breadcrumbs->unshift_clear('Planeación', '#');
        $this->breadcrumbs->unshift('Nuestra Institución', site_url('entidad'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->view('includes/template', ($data + $this->add));
    }

    //$data['plan_accion_avances'] = $this->Datos_control_model->obtener_avances_plan_accion(date('Y'));


    public function mipg_ajax($id) {
        $this->load->model('Adicional_model');
        $dato = $this->Adicional_model->obtener_avances_mipg($id);
        if ($dato != FALSE) {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Avances del Plan de Acción</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm table-hover basic_table">
                <tr><th>Nombre</th><th>Archivo</th></tr>
                    ';
            foreach ($dato as $row) {

                $texto .= '<td>' . $row->entidad_mipg_plan_avances_nombre . '</td>';
                $texto .= '<td><a  target="_blank" href="' . site_url('uploads/entidad/adicional') . '/' . $row->entidad_mipg_plan_avances_archivo . '" class="btn btn-primary btn-xs" ><i class="fas fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' 
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        } else {
            $texto = '<div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                            <h4 class=" modal-title" id="myModalLabel">Información no disponible </h4>
            </div>
            <div class="modal-body">
         <p>No se encontraron avances para el plan de acción</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        }
        echo json_encode($texto);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */    