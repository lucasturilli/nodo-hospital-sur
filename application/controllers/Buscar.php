<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Buscar extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->model('Datos_buscar_model');
        $this->load->helper('cookie');
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index($texto = NULL) {

        $this->breadcrumbs->unshift('Buscar', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'buscar';
        $texto = "";
        if ($this->input->post('texto') != "") {
            $texto = $this->input->post('texto');
        } elseif ($this->input->post('texto2') != "") {
            $texto = $this->input->post('texto2');
        } else {
            $texto = $this->input->post('texto3');
        }
        $palabra = strtolower($texto);
        $data['texto'] = $palabra;
        $data['texto_plain'] = $texto;
        $secciones = array(
            /* GACETA */
            'sitio/pagina/gaceta-municipal' => array('Gaceta gaceta municipal Municipal', 'Gaceta Municipal'),
            /* INFORMACION GENERAL */
            'entidad/index/mision_vision' => array('misión visión mision vision', 'Misión y Visión'),
            'entidad/index/objetivos_funciones' => array('objetivos funciones', 'Objetivos y Funciones'),
            'entidad/index/organigrama' => array('organigrama', 'Organigrama'),
            'entidad/index/dependencias' => array('dependencias secretarias', 'Dependencias'),
            'entidad/directorio_sucursales' => array('directorio sucursales regionales', 'Directorio de Sucursales'),
           'entidad/directorio_entidades' => array('directorio entidades', 'Directorio de Entidades'),
            'entidad/directorio_agremiaciones' => array('directorio agremiaciones asociaciones', 'Directorio de agremiaciones, asociaciones y otros grupos de interés'),
            /* SERVICIOS DE INFORMACION */
            'entidad/preguntas_frecuentes' => array('preguntas respuestas frecuentes', 'Preguntas y Respuestas Frecuentes'),
            'entidad/glosario' => array('glosario', 'Glosario'),
            'sitio/noticias' => array('noticias noticia', 'Noticias'),
            'sitio/notificaciones' => array('anuncios notificaciones', 'Notificaciones y Anuncios'),
            'entidad/boletines' => array('Boletines boletines informativos', 'Boletines Informativos'),
            'sitio/calendario' => array('eventos calendario actividades', 'Calendario de Actividades'),
            'sitio/encuestas' => array('encuestas', 'Encuestas'),
            'sitio/pagina/blog-ninos' => array('portal niños niñas', 'Portal para Niños y Niñas'),
            'sitio/pagina/ofertas-de-empleo' => array('ofertas de empleo', 'Ofertas de Empleo'),
            /* TALENTO HUMANO */
            'entidad/index/funcionarios' => array('funcionarios funcionario', 'Funcionarios Principales'),
            'entidad/index/listado_planta' => array('funcionarios Listado de Personal personal', 'Listado de Personal'),
            'entidad/manual_funciones' => array('manual funciones', 'Manual de Funciones'),
            'entidad/index/asignaciones_salariales' => array('asignaciones salariales viaticos', 'Asignaciones Salariales'),
            'entidad/index/evaluacion_desempeno' => array('Evaluación Desempeño evaluacion', 'Evaluación de Desempeño'),
            /* NORMATIVIDAD */
            'entidad/leyes' => array('leyes ordenanzas acuerdos', 'Leyes / Ordenanzas / Acuerdos'),
            'entidad/decretos' => array('decretos', 'Decretos'),
            'entidad/circulares' => array('circulares actos administrativos', 'Circulares y/u otros actos administrativos de carácter general'),
            'entidad/resoluciones' => array('resoluciones Resoluciones', 'Resoluciones'),
            'entidad/edictos' => array('edictos Edictos', 'Edictos'),
            /* PRESUPUESTO */
            'entidad/presupuesto' => array('presupuesto aprobado en ejercicio', 'Presupuesto Aprobado en Ejercicio'),
            'entidad/index/historico_presupuesto' => array('histórica historica de presupuestos', 'Información Histórica de Presupuestos'),
            'entidad/estados_financieros' => array('estados financieros', 'Estados Financieros'),
            'entidad/presupuesto_estatuto_tributario' => array('estatuto tributario normatividad', 'Estatuto Tributario'),
            /* PLANEACION */
            'entidad/politicas' => array('políticas politicas lineamientos manuales', 'Políticas / Lineamientos / Manuales'),
            'entidad/plan_anticorrupcion' => array('Plan Anticorrupción plan anticorrupcion', 'Plan Anticorrupción'),
            'entidad/plan_estrategico' => array('Plan Desarrollo plan desarrollo', 'Plan de Desarrollo'),
            'entidad/plan_accion' => array('plan acción accion planes Plan de Acción', 'Plan de Acción'),
            'entidad/planeacion_plan_gasto_publico' => array('Plan Gasto Público plan gasto publico', 'Plan de Gasto Público'),
            'entidad/programas_proyectos' => array('programas proyectos  ejecución ejecucion', 'Programas y Proyectos en Ejecución'),
            'entidad/index/indicadores_gestion' => array('metas indicadores', 'Metas e Indicadores de Gestión'),
            'entidad/index/planes_otros' => array('Otros Planes otros planes', 'Otros Planes'),
            'entidad/informe_empalme' => array('informe empalme', 'Informes de Empalme'),
            'entidad/control_archivo' => array('informe archivo retencion documental Gestión gestion Documental', 'Gestión Documental'),
//            'entidad/mipg' => array('Modelo Integrado Planeación Gestión MIPG ', 'Modelo Integrado de Planeación y Gestión “MIPG”'),
            /* CONTROL */
            'entidad/entes' => array('entes control vigilan', 'Entes de Control que Vigilan la Entidad'),
            'entidad/auditorias' => array('Auditorias auditorias', 'Auditorías '),
            'entidad/index/informe_concejo' => array('concejo rendicion informe informes', 'Informes al Honorable Concejo'),
            'entidad/index/informe_contraloria' => array('Informes Rendición Cuenta Fiscal rendicion cuentas fiscal', 'Informes de Rendición de Cuenta Fiscal'),
            'entidad/informe_ciudadania' => array('ciudadania rendicion cuenta cuentas informe informes gestion cronograma evaluacion evaluaciones', 'Informes de Rendición de Cuenta a los Ciudadanos'),
            'entidad/planes_mejoramiento' => array('planes mejoramiento', 'Planes de Mejoramiento'),
            'entidad/reportes_control_interno' => array('control interno reportes', 'Reportes de Control Interno'),
            'entidad/index/poblacion_vulnerable' => array('poblacion vulnerable población', 'Información para Población Vulnerable'),
            'entidad/programas_poblacion_vulnerable' => array('programas poblacion vulnerable población', 'Programas para la Población Vulnerable'),
            'entidad/proyectos_poblacion_vulnerable' => array('proyectos poblacion vulnerable población', 'Proyectos para la Población Vulnerable'),
            'entidad/index/programas_sociales' => array('programas sociales', 'Programas Sociales'),
            'entidad/index/defensa_judicial' => array('defensa judicial', 'Defensa Judicial'),
            'entidad/otros_informes' => array('Otros Informes', 'Otros Informes'),
            /* CONTRATACION */
            'entidad/plan_compras' => array('plan compras adquisiciones', 'Plan de Adquisiciones'),
            'entidad/index/secop' => array('secop', 'Información SECOP'),
            'entidad/contratos' => array('contratos procesos contratación contratacion', 'Procesos de Contratación'),
            'entidad/avance_contractual' => array('Avances Contractuales avances contractuales', 'Avances Contractuales'),
//            'entidad/convenios' => array('contratos procesos contratación convenios', 'Convenios'),
            'entidad/contratacion_lineamientos' => array('Lineamientos de Adquisiciones Compras adquisiciones compras', 'Lineamientos de Adquisiciones y Compras'),
            /* TRAMITES Y SERVICIOS */
            'entidad/tramites' => array('listado tramites servicios tramites', 'Listado de Trámites y Servicios'),
            'entidad/tramites_en_linea' => array('Trámites Servicios línea servicios', 'Trámites y Servicios en línea'),
            'entidad/index/formularios_tramites' => array('Formularios Trámites Servicios formularios tramites servicios', 'Formularios de Trámites y Servicios'),
            'entidad/informe_pqrs' => array('peticiones quejas reclamos denuncias felicitaciones pqr pqrsf', 'Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRSF)'),
            /* CALIDAD */
            'entidad/index/calidad' => array('Calidad calidad', 'Calidad'),
            'entidad/caracterizacion_procesos' => array('Caracterización de Procesos caracterizacion de procesos', 'Caracterización de Procesos'),
            'entidad/procedimientos' => array('Procedimientos procedimientos', 'Procedimientos'),
            /* DOCUMENTOS DE LA ALCALDIA */
            'entidad/documentos' => array('documentos', 'Documentos de la Alcaldía'),
            /* INFORMACION DE INTEREZ */
//            'entidad/index/informacion_interes' => array('Ayuda para navegar en el sitio', 'Ayuda para Navegar en el Sitio'),
            'entidad/convocatorias' => array('Convocatorias', 'Convocatorias'),
            'entidad/estudios' => array('Estudios Investigaciones Otras Publicaciones', 'Estudios, Investigaciones y Otras Publicaciones'),
            'entidad/informacion_adicional' => array('Información Adicional informacion', 'Información Adicional'),
            /* MECANISMOS DE CONTACO */
            'entidad/mecanismos_contacto' => array('Políticas  seguridad  información  protección  datos personales Correo electrónico  notificaciones judiciales Mecanismo de Contacto Atención  Línea Buzón  Contacto Localización física días  atención  público', 'Mecanismo de Contacto'),
            /* DATOS ABIERTOS */
            'entidad/esquema_publicacion' => array('Datos Abiertos Esquema Publicación esquema publicación publicacion', 'Esquema de Publicación '),
            'entidad/datos_abiertos' => array('Datos Abiertos Archivos Catálogo catalogo', 'Datos Abiertos'),
            'entidad/catalogo_abiertos' => array('Datos Abiertos Archivos Catálogo catalogo', 'Catálogo de Datos Abiertos'),
//            'entidad/mapas_abiertos' => array('Datos Abiertos Archivos mapas Mapas', 'Mapas de Datos Abiertos')
                /* INFORMACION MUNICIPIO */
//            'municipio/index' => array('codigo dane código dane gentilicio escudo himno bandera fundadores fundación temperatura altura límites extensión sectores celebraciones fiestas', 'Información General del Municipio'),
//            'municipio/mapas' => array('mapas topográfico geográfico territorial', 'Mapas'),
//            'municipio/territorios' => array('territorios barrios comunas corregimientos resguardos urbanizaciones veredas', 'Territorios'),
//            'municipio/sitios' => array('sitios interés interes', 'Sitios de Interés'),
//            'municipio/directorio' => array('directorio turístico turismo  turistico', 'Directorio Turístico'),
//            'municipio/media' => array('videos fotos audios fotografias albumes galerias galeria audios video foto', 'Media'),
//            'municipio/documentos' => array('documentos municipio', 'Documentos del Municipio')
        );
        $pieces = explode(" ", $palabra);
        $resultados_secciones = array();
        foreach ($pieces as $piece) {
            if ($piece != FALSE) {
                $resultados_secciones = array_merge($resultados_secciones, $this->search_array($piece, $secciones));
            }
        }
        asort($resultados_secciones);
        $data['secciones'] = array_unique($resultados_secciones);

        /* INFORMACION GENERAL */
        $data['dependencias'] = $this->Datos_buscar_model->buscar_entidad_dependencias($palabra);
        $data['sucursales'] = $this->Datos_buscar_model->buscar_entidad_sucursales($palabra);
        $data['entidades'] = NULL;
        $data['agremiaciones'] = NULL;
        /* SERVICOS DE INFORMACION */
        $data['preguntas_frecuentes'] = $this->Datos_buscar_model->buscar_entidad_preguntas_frecuentes($palabra);
        $data['glosario'] = $this->Datos_buscar_model->buscar_entidad_glosario($palabra);
        $data['noticias'] = $this->Datos_buscar_model->buscar_entidad_noticias($palabra);
        $data['boletines'] = $this->Datos_buscar_model->buscar_entidad_boletines($palabra);
        //  $data['periodicos'] = $this->Datos_buscar_model->buscar_entidad_periodico($palabra);
        $data['calendario'] = $this->Datos_buscar_model->buscar_entidad_calendario($palabra);
        /* TALENTO HUMANO */
        $data['funcionarios'] = $this->Datos_buscar_model->buscar_entidad_funcionarios($palabra);
        $data['manual_funciones'] = $this->Datos_buscar_model->buscar_entidad_manual_funciones($palabra);
        /* NORMATIVIDAD */
        $data['leyes'] = $this->Datos_buscar_model->buscar_normatividad_leyes($palabra);
        $data['decretos'] = $this->Datos_buscar_model->buscar_normatividad_decretos($palabra);
        $data['circulares'] = $this->Datos_buscar_model->buscar_normatividad_circulares($palabra);
        $data['resoluciones'] = $this->Datos_buscar_model->buscar_normatividad_resoluciones($palabra);
        $data['edictos'] = $this->Datos_buscar_model->buscar_normatividad_edictos($palabra);
        /* PRESUPUESTO */
        $data['estados_financieros'] = $this->Datos_buscar_model->buscar_financiera_estados_financieros($palabra);
        $data['estatuto_tributario'] = $this->Datos_buscar_model->buscar_financiera_estatuto_tributario($palabra);
        /* PLANEACION */
        $data['politicas'] = $this->Datos_buscar_model->buscar_normatividad_politicas($palabra);
        $data['anticorrupcion'] = $this->Datos_buscar_model->buscar_anticorrupcion_definicion($palabra);
        $data['plan_accion'] = $this->Datos_buscar_model->buscar_control_plan_accion($palabra);
        $data['plan_gasto_publico'] = $this->Datos_buscar_model->buscar_planeacion_plan_gasto_publico($palabra);
        $data['programas'] = $this->Datos_buscar_model->buscar_control_programas($palabra);
        $data['otros_planes'] = $this->Datos_buscar_model->buscar_control_planes_otros($palabra);
        $data['infore_enpalme'] = $this->Datos_buscar_model->buscar_control_informe_empalme($palabra);
        $data['gestion_documental'] = $this->Datos_buscar_model->buscar_control_informe_archivo($palabra);
        $data['mipg_plan'] = NULL;
        $data['mipg_planes'] = NULL;
        $data['metas'] = $this->Datos_buscar_model->buscar_metas($palabra);
        /* CONTROL */
        $data['entes'] = $this->Datos_buscar_model->buscar_control_entes($palabra);
        $data['auditorias'] = $this->Datos_buscar_model->buscar_control_auditorias($palabra);
        $data['informe_concejo'] = $this->Datos_buscar_model->buscar_control_informe_concejo($palabra);
        $data['informe_fiscal'] = $this->Datos_buscar_model->buscar_control_informe_fiscal($palabra);
        $data['informe_ciudadania'] = $this->Datos_buscar_model->buscar_control_informe_ciudadania($palabra);
        $data['informe_ciudadania_gestion'] = $this->Datos_buscar_model->buscar_control_informes_gestion_ciudadania($palabra);
        $data['informe_ciudadania_evaluacion'] = $this->Datos_buscar_model->buscar_control_informes_ciudadania_evaluacion($palabra);
        $data['informes_cronograma'] = $this->Datos_buscar_model->buscar_control_informes_cronograma($palabra);
        $data['planes_mejoramiento'] = $this->Datos_buscar_model->buscar_control_planes_mejoramiento($palabra);
        $data['reportes_control_interno'] = $this->Datos_buscar_model->buscar_control_reportes_control_interno($palabra);
        $data['poblacion_vulnerable_programas'] = $this->Datos_buscar_model->buscar_control_poblacion_vulnerable_programas($palabra);
        $data['poblacion_vulnerable_proyectos'] = $this->Datos_buscar_model->buscar_control_poblacion_vulnerable_proyectos($palabra);
        $data['programas_sociales'] = $this->Datos_buscar_model->buscar_control_programas_sociales($palabra);
        $data['otros_informes'] = $this->Datos_buscar_model->buscar_control_otros_informes($palabra);
        /* CONTRATACION */
        $data['plan_compras'] = $this->Datos_buscar_model->buscar_contratacion_plan_compras($palabra);
        $data['contratos'] = $this->Datos_buscar_model->buscar_contratacion_contratos($palabra);
        $data['avances_contractuales'] = $this->Datos_buscar_model->buscar_contratacion_avance_contractual($palabra);
        $data['convenios'] = NULL;
        $data['lineamientos'] = $this->Datos_buscar_model->buscar_contratacion_lineamientos($palabra);
        /* TRAMITES Y SERVICIOS */
        $data['tramites'] = $this->Datos_buscar_model->buscar_servicios_tramites($palabra);
        $data['tramites_en_linea'] = $this->Datos_buscar_model->buscar_servicios_tramites_en_linea($palabra);
        $data['formatos'] = $this->Datos_buscar_model->buscar_servicios_formatos($palabra);
        /* DOCUMENTOS */
        $data['documentos'] = $this->Datos_buscar_model->buscar_entidad_documentos($palabra);
        /* CALIDAD */
        $data['caracterizacion_procesos'] = $this->Datos_buscar_model->buscar_entidad_caracterizacion_procesos($palabra);
        $data['procedimientos'] = $this->Datos_buscar_model->buscar_entidad_procedimientos($palabra);

        /* DATOS ABIERTOS */
        $data['archivos_datos_abiertos'] = $this->Datos_buscar_model->buscar_archivos_datos_abiertos($palabra);
        $data['documentos_municipio'] = NULL;
        $data['mapas'] = NULL;
        $data['sitios'] = NULL;
        $data['territorios'] = NULL;

        $this->load->view('includes/template', ($data + $this->add));
    }

    public function search_array($texto, $array) {
        $matches = array();
        foreach ($array as $key => $c) {
            if (strpos(strtolower($c[0]), strtolower($texto)) !== FALSE) {
                $matches[$key] = $c[1];
            }
        }
        return $matches;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
