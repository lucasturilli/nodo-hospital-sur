<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Portal_ninos extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $prefs = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => site_url('portal_ninos/calendario')
        );
        $this->load->library('calendar', $prefs);
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'nombre_escudo_municipio' => $this->Datos_municipio_model->obtener_nombre_escudo_municipio(),
            'titulo' => 'Alcaldía de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre,
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo()
        );
        $this->footer = array(
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->add = $this->header + $this->footer + $this->dias + $this->meses;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index() {

        $data['banner_ninos'] = $this->Aplicaciones_model->obtener_banner('ninos');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/home';
        $data['tab'] = 'index';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function noticias($paginas = 0) {
        $this->load->library('pagination');
        $this->breadcrumbs->unshift('Noticias', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['titulo'] = 'Noticias';
        $data['main_content'] = 'portal_ninos/noticias';
        $data['noticias'] = $this->Datos_entidad_model->obtener_noticias_ninos();
        $config['base_url'] = site_url('sitio/noticias');
        $config['total_rows'] = count($data['noticias']);
        $config['per_page'] = 8;
        $config['full_tag_open'] = '<ul class="pagination paginacion">';
        $config['full_tag_close'] = '</ul>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['paginacion'] = $this->pagination->create_links();
        $data['paginas'] = $paginas;
        $data['tab'] = 'noticias';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function ver_noticia($id) {
        $this->breadcrumbs->unshift('Noticia', '#');
        $this->breadcrumbs->unshift('Noticias', site_url('sitio/noticias'));
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['titulo'] = 'Noticia';
        $data['tab'] = 'noticias';
        $data['main_content'] = 'portal_ninos/ver_noticia';
        $data['noticia'] = $this->Datos_entidad_model->obtener_noticia_ninos($id);
        $data['noticias'] = $this->Datos_entidad_model->obtener_noticias_ninos();
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function calendario($y = NULL, $m = NULL) {
        $this->breadcrumbs->unshift('Calendario de Eventos', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['titulo'] = 'Calendario de Eventos';
        $data['tab'] = 'eventos';
        $data['main_content'] = 'portal_ninos/calendario';
        $data['calendario_mes'] = $this->Datos_entidad_model->obtener_eventos_mes_seleccion_ninos($y, $m);
        $data_calendar = array();
        $title_calendar = array();
        if ($data['calendario_mes'] != FALSE) {

            foreach ($data['calendario_mes'] as $dato) {
                $title_calendar = ($title_calendar + array(date("j", strtotime($dato->entidad_calendario_ninos_fecha_inicio)) => $dato->entidad_calendario_ninos_nombre));
                $data_calendar = ($data_calendar + array(date("j", strtotime($dato->entidad_calendario_ninos_fecha_inicio)) => site_url('portal_ninos/calendario_evento') . '/' . $dato->id_entidad_calendario_ninos));
            }
        }
        $data['calendario'] = $this->calendar->generate($y, $m, $data_calendar, $title_calendar);
        $data['m'] = $m;
        $data['y'] = $y;
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function calendario_evento($id) {
        $this->breadcrumbs->unshift('Evento ', '#');
        $this->breadcrumbs->unshift('Calendario de Eventos', site_url('portal_ninos/calendario') . '/' . date('Y') . '/' . date('m'));
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['titulo'] = 'Evento';
        $data['tab'] = 'eventos';
        $data['main_content'] = 'portal_ninos/ver_evento';
        $data['evento'] = $this->Datos_entidad_model->obtener_evento_ninos($id);
        $data['calendario'] = $this->Datos_entidad_model->obtener_eventos_mes_ninos();
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function nuestro_municipio() {
        //$data['municipio'] = $this->Aplicaciones_model->obtener_banner('ninos');
        $this->breadcrumbs->unshift('Nuestro Municipio ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/municipio';
        $data['tab'] = 'municipio';
        $data['infomacion_general'] = $this->Datos_municipio_model->obtener_informacion_general();
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function nuestra_alcaldia() {
        $data['informacion'] = $this->Datos_entidad_model->obtener_informacion_ninos();
        $this->breadcrumbs->unshift('Nuestra Alcaldía ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/alcaldia';
        $data['tab'] = 'alcaldia';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function sitios_recomendados() {
        $data['sitios'] = $this->Datos_entidad_model->obtener_sitios_ninos();
        $this->breadcrumbs->unshift('Sitios Recomendados ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/sitios_recomendados';
        $data['tab'] = 'sitios';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function servicios() {
        $data['informacion'] = $this->Datos_entidad_model->obtener_informacion_ninos();
        $this->breadcrumbs->unshift('Servicios ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/servicios';
        $data['tab'] = 'servicios';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function video() {
        $data['informacion'] = $this->Datos_entidad_model->obtener_informacion_ninos();
        $this->breadcrumbs->unshift('Videos ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/video';
        $data['tab'] = 'video';
        $data['video_widget'] = $this->Datos_municipio_model->obtener_videos();
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function fotos() {
        $this->breadcrumbs->unshift('Fotos ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/fotos';
        $data['tab'] = 'fotos';
        $data['imagenes_widget'] = $this->Datos_municipio_model->obtener_imagenes();
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function juegos() {
        $data['juegos'] = $this->Datos_entidad_model->obtener_juegos_ninos();
        $this->breadcrumbs->unshift('Juegos ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/juegos';
        $data['tab'] = 'juegos';
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    function contacto($mensaje = NULL) {
        $this->breadcrumbs->unshift('Contacto ', '#');
        $this->breadcrumbs->unshift('<img height="20" src="' . site_url('images') . '/home_ninos.png"> Inicio', site_url('portal_ninos'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_ninos/contacto';
        $data['tab'] = 'contacto';
        $data['mensaje'] = $mensaje;
        $this->load->view('portal_ninos/includes/template', ($data + $this->add));
    }

    public function enviar_email() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('email', 'Correo electrónico ', 'required|valid_email');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->contacto();
        } else {
            $this->load->library('email');
            $email_setting = array('mailtype' => 'html');
            $this->email->initialize($email_setting);
            $this->email->from($this->input->post('email'), $this->input->post('nombre'));
            $this->email->to($this->Datos_entidad_model->obtener_datos_contacto()->entidad_contacto_email);
            $this->email->subject('Nuevo mensaje generado desde el portal de Niños');
            $this->email->message($this->input->post('mensaje') . '<br /><br /><br />Enviado por<br />' . $this->input->post('nombre') . '<br />Correo electrónico<br />' . $this->input->post('email') . '<br />Teléfono de contacto <br />' . $this->input->post('telefono'));
            $this->email->send();
            $this->contacto(TRUE);
        }
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    