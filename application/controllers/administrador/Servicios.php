<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Servicios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function servicios_tramites() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_tramites');
        $crud->set_subject('TRÁMITE O SERVICIO');
        $crud->display_as('servicios_tramites_nombre', 'Nombre');
        $crud->display_as('servicios_tramites_enlace', 'Enlace');
        $crud->display_as('servicios_tramites_descripcion', 'Descripción ');
        $crud->required_fields('servicios_tramites_nombre', 'servicios_tramites_descripcion');
        //  $crud->unset_texteditor('servicios_tramites_descripcion');
        $ruta = 'Listado de Trámites y Servicios ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'La entidad publica, en un lugar plenamente visible de la página inicial, un enlace a la sección de trámites y servicios, donde se proporcione un listado con el nombre de cada trámite o servicio, enlazado a la información sobre éste en el Portal del Estado Colombiano (PEC), en el cual cada entidad debe subir, a través del SUIT, los formatos que exige para cada trámite o servicio. En los casos en que el trámite o servicio pueda realizarse en línea, deberá existir junto a su nombre un enlace al punto exacto para gestionarlo en línea. Este enlace debe estar disponible de igual forma en el PEC.La información de los trámites y servicios que se publica en el Sistema Único de Información de Trámites – SUIT, debe estar actualizada, de acuerdo con los lineamientos establecidos por el Departamento Administrativo de la Función Pública.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 451, $ruta, $ruta_tab, $texto);
    }

    public function servicios_inventario() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_inventario');
        $crud->set_subject('INVENTARIO');
        $crud->display_as('servicios_inventario_archivo', 'Archivo');
        $crud->set_field_upload('servicios_inventario_archivo', 'uploads/entidad/servicios');
        $crud->required_fields('servicios_inventario_archivo');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Inventario de trámites y servicios  ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'Inventario que contiene un listado de todos los trámites y servicios prestados por la entidad.';
        $output = $crud->render();
        $this->_example_output($output, 501, $ruta, $ruta_tab, $texto);
    }

    public function servicios_tramites_en_linea() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_tramites_en_linea');
        $crud->set_subject('TRÁMITE O SERVICIO');
        $crud->display_as('servicios_tramites_en_linea_nombre', 'Nombre');
        $crud->display_as('servicios_tramites_en_linea_enlace', 'Enlace');
        $crud->display_as('servicios_tramites_en_linea_descripcion', 'Descripción ');
        $crud->display_as('servicios_tramites_en_linea_requisitos', 'Requisitos ');
        $crud->display_as('servicios_tramites_en_linea_tematica', 'Temática ');
        $crud->display_as('servicios_tramites_en_linea_tipo', 'Tipo ');
        $crud->display_as('id_entidad_dependencia', 'Dependencia');
        $crud->set_relation('id_entidad_dependencia', 'entidad_dependencia', 'entidad_dependencia_nombre');
        $crud->field_type('servicios_tramites_en_linea_tipo', 'dropdown', array(1 => 'Tramite', 2 => 'Servicio'));
        $crud->required_fields('servicios_tramites_en_linea_nombre', 'servicios_tramites_en_linea_enlace', 'servicios_tramites_en_linea_descripcion', 'servicios_tramites_en_linea_requisitos', 'servicios_tramites_en_linea_tematica', 'servicios_tramites_en_linea_tipo', 'id_entidad_dependencia');
        $crud->unset_texteditor('servicios_tramites_en_linea_embedded');
        $ruta = 'Listado de Trámites y Servicios en línea  ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'Listado de trámites y servicios en línea';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 452, $ruta, $ruta_tab, $texto);
    }

    public function servicios_formatos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_formatos');
        $crud->set_subject('FORMATO DE TRAMITE O SERVICIO');
        $crud->display_as('servicios_formatos_nombre', 'Nombre');
        $crud->display_as('servicios_formatos_archivo', 'Archivo');
        $crud->display_as('servicios_formatos_descripcion', 'Descripción ');
        $crud->required_fields('servicios_formatos_nombre', 'servicios_formatos_archivo');
        $crud->set_field_upload('servicios_formatos_archivo', 'uploads/entidad/servicios');
        $crud->unset_texteditor('servicios_formatos_descripcion');
        $ruta = 'Formularios de trámites y servicios ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'Formatos de descarga necesarios para la realización de trámites y servicios con la entidad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 453, $ruta, $ruta_tab, $texto);
    }

    public function servicios_certificados() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_certificados');
        $crud->set_subject('CERTIFICADO');
        $crud->display_as('servicios_certificados_nombre', 'Nombre');
        $crud->display_as('servicios_certificados_embedded', 'Embebido');
        $crud->display_as('servicios_certificados_descripcion', 'Descripción ');
        $crud->required_fields('servicios_certificados_nombre', 'servicios_certificados_embedded');
        $crud->unset_texteditor('servicios_certificados_embedded');
        $ruta = 'Certificados en línea  ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'Listado de certificados en línea disponibles.<br /><br />Plantilla embebido:<br/><code>&lt;object data=&quot;AQUI LA URL&quot; width=&quot;100%&quot;&gt;&lt;/object&gt;</code>';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 454, $ruta, $ruta_tab, $texto);
    }

    public function servicios_pqrdf() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_pqrdf');
        $crud->set_subject('INFORME DE PQRDF');
        $crud->display_as('servicios_pqrdf_archivo', 'Archivo');
        $crud->display_as('servicios_pqrdf_semestre', 'Periodicidad');
        $crud->display_as('servicios_pqrdf_periodo', 'Periodo ');
        $crud->required_fields('servicios_pqrdf_archivo', 'servicios_pqrdf_semestre', 'servicios_pqrdf_periodo');
        $crud->set_field_upload('servicios_pqrdf_archivo', 'uploads/entidad/servicios');
        $crud->field_type('servicios_pqrdf_semestre', 'dropdown', periodicidad());
        $crud->field_type('servicios_pqrdf_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'La entidad publica semestralmente un informe de todas las peticiones recibidas y los tiempos de respuesta relacionados, junto con un análisis resumido de este mismo tema.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 455, $ruta, $ruta_tab, $texto);
    }

    public function servicios_pqrdf_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('servicios_pqrdf_anexos');
        $crud->set_subject('ANEXOS AL INFORME DE PQRDF');
        $crud->display_as('servicios_pqrdf_anexos_archivo', 'Archivo');
        $crud->display_as('servicios_pqrdf_anexos_nombre', 'Nombre');
        $crud->display_as('id_servicios_pqrdf_anexos', 'Informe');
        $crud->display_as('servicios_pqrdf_anexos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('servicios_pqrdf_anexos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('servicios_pqrdf_anexos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('servicios_pqrdf_anexos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('servicios_pqrdf_anexos_archivo', 'servicios_pqrdf_anexos_nombre', 'id_servicios_pqrdf_anexos');
        $crud->set_field_upload('servicios_pqrdf_anexos_archivo', 'uploads/entidad/servicios');
        $crud->edit_fields('servicios_pqrdf_anexos_nombre','servicios_pqrdf_anexos_archivo','id_servicios_pqrdf_anexos','servicios_pqrdf_anexos_fecha_actualizacion');
        $crud->set_relation('id_servicios_pqrdf', 'servicios_pqrdf', '{servicios_pqrdf_periodo} - {servicios_pqrdf_semestre}');
        $ruta = 'Anexos al Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) ';
        $ruta_tab = 'Trámites y Servicios ';
        $texto = 'Anexos al Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 456, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {
        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);
        return true;
    }

}
