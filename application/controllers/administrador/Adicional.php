<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adicional extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('image_moo');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function entidad_mipg_plan() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_mipg_plan');
        $crud->set_subject('PLAN DE ACCIÓN MIPG');
        $crud->display_as('entidad_mipg_plan_nombre', 'Nombre');
        $crud->display_as('entidad_mipg_plan_archivo', 'Archivo');
        $crud->display_as('entidad_mipg_plan_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_mipg_plan_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('entidad_mipg_plan_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_mipg_plan_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->edit_fields('entidad_mipg_plan_nombre', 'entidad_mipg_plan_archivo', 'entidad_mipg_plan_fecha_actualizacion');
        $crud->required_fields('entidad_mipg_plan_nombre', 'entidad_mipg_plan_archivo', 'entidad_mipg_plan_fecha_actualizacion');
        $crud->set_field_upload('entidad_mipg_plan_archivo', 'uploads/entidad/adicional');
        //$crud->unset_delete();
        // $crud->unset_add();
        $ruta = 'Plan de Acción MIPG';
        $ruta_tab = 'Planeación';
        $texto = 'Plan de Acción MIPG';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 317, $ruta, $ruta_tab, $texto);
    }

    public function entidad_mipg_plan_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_mipg_plan_avances');
        $crud->set_subject('AVANCES PLAN DE ACCIÓN MIPG');
        $crud->display_as('entidad_mipg_plan_avances_nombre', 'Nombre');
        $crud->display_as('entidad_mipg_plan_avances_archivo', 'Archivo');
         $crud->display_as('id_entidad_mipg_plan', 'Plan de Acción');
        $crud->display_as('entidad_mipg_plan_avances_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_mipg_plan_avances_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('entidad_mipg_plan_avances_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_mipg_plan_avances_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_relation('id_entidad_mipg_plan', 'entidad_mipg_plan', 'entidad_mipg_plan_nombre');
        $crud->edit_fields('entidad_mipg_plan_avances_nombre', 'entidad_mipg_plan_avances_archivo', 'entidad_mipg_plan_avances_fecha_actualizacion','id_entidad_mipg_plan');
        $crud->required_fields('entidad_mipg_plan_avances_nombre', 'entidad_mipg_plan_avances_archivo', 'entidad_mipg_plan_avances_fecha_actualizacion','id_entidad_mipg_plan');
        $crud->set_field_upload('entidad_mipg_plan_avances_archivo', 'uploads/entidad/adicional');
       // $crud->unset_delete();
        // $crud->unset_add();
        $ruta = 'Avances del Plan de Acción MIPG';
        $ruta_tab = 'Planeación';
        $texto = 'Avances del Plan de Acción MIPG';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 318, $ruta, $ruta_tab, $texto);
    }
    
    
     public function entidad_mipg_planes() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_mipg_planes');
        $crud->set_subject('PLANES DE  MIPG');
        $crud->display_as('entidad_mipg_planes_nombre', 'Nombre');
        $crud->display_as('entidad_mipg_planes_archivo', 'Archivo');
        $crud->display_as('entidad_mipg_planes_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_mipg_planes_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('entidad_mipg_planes_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_mipg_planes_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->edit_fields('entidad_mipg_planes_nombre', 'entidad_mipg_planes_archivo', 'entidad_mipg_planes_fecha_actualizacion');
        $crud->required_fields('entidad_mipg_planes_nombre', 'entidad_mipg_planes_archivo', 'entidad_mipg_planes_fecha_actualizacion');
        $crud->set_field_upload('entidad_mipg_planes_archivo', 'uploads/entidad/adicional');
        //$crud->unset_delete();
        // $crud->unset_add();
        $ruta = 'Planes de MIPG';
        $ruta_tab = 'Planeación';
        $texto = 'Planes de MIPG';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 319, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
