<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contratacion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function contratacion_plan_compras() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_plan_compras');
        $crud->set_subject('PLAN DE COMPRAS');
        $crud->display_as('contratacion_plan_compras_nombre', 'Nombre');
        $crud->display_as('contratacion_plan_compras_archivo', 'Archivo');
        $crud->display_as('contratacion_plan_compras_periodo', 'Periodo');
        $crud->set_field_upload('contratacion_plan_compras_archivo', 'uploads/entidad/contratacion');
        $crud->required_fields('contratacion_plan_compras_nombre','contratacion_plan_compras_archivo', 'contratacion_plan_compras_periodo');
        $crud->field_type('contratacion_plan_compras_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Plan de Compras';
        $ruta_tab = 'Contratación ';
        $texto = 'La entidad publica el Plan de compras de la vigencia respectiva.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 401, $ruta, $ruta_tab, $texto);
    }


    public function contratacion_contratos_secop() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_contratos_secop');
        $crud->set_subject('INFORMACIÓN SECOP');
        $crud->display_as('contratacion_contratos_secop_enlace', 'Enlace a información de la entidad en el SECOP ');
        $crud->display_as('contratacion_contratos_secop_descripcion', 'Descripción SECOP');
        $crud->required_fields('contratacion_contratos_secop_enlace', 'contratacion_contratos_secop_descripcion');
        // $crud->unset_texteditor('contratacion_contratos_secop_descripcion');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Información SECOP';
        $ruta_tab = 'Contratación ';
        $texto = 'La entidad debe publicar en el SECOP todos sus procesos de contratación según lo establecido en el artículo 2.2.5 del Decreto 734 de 2012. <br>Para facilitar el acceso a esta información la entidad debe contar con un enlace  que direccione a la información publicada por la entidad en el SECOP.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 403, $ruta, $ruta_tab, $texto);
    }

    public function contratacion_contratos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_contratos');
        $crud->set_subject('PROCESO DE CONTRATACIÓN');
        $crud->display_as('contratacion_contratos_numero_proceso', 'Identificación o Número del Proceso ');
        $crud->display_as('contratacion_contratos_objeto', 'Objeto');
        $crud->display_as('contratacion_contratos_tipo', 'Tipo');
        $crud->display_as('contratacion_contratos_fecha_publicacion', 'Fecha  de Publicación');
        $crud->display_as('contratacion_contratos_enlace', 'Enlace al SECOP');
        $crud->required_fields('contratacion_contratos_numero_proceso','contratacion_contratos_tipo', 'contratacion_contratos_objeto', 'contratacion_contratos_fecha_publicacion', 'contratacion_contratos_enlace');
        $crud->unset_texteditor('contratacion_contratos_objeto');
        $crud->field_type('contratacion_contratos_tipo', 'dropdown', array('Adquisición de Bienes' => 'Adquisición de Bienes', 'Arrendamiento'=> 'Arrendamiento', 'Funcionamiento'=> 'Funcionamiento', 'Inversión' => 'Inversión', 'Obras Públicas' => 'Obras Públicas','Prestación de Servicios'=>'Prestación de Servicios','Otros'=>'Otros'));
        $ruta = 'Procesos de Contratación ';
        $ruta_tab = 'Contratación ';
        $texto = 'La entidad debe publicar en el SECOP todos sus procesos de contratación según lo establecido en el artículo 2.2.5 del Decreto 734 de 2012. <br> Se podrá presentar un listado de procesos de contratación (con la identificación y objeto del proceso) en el que cada proceso se enlace con la información respectiva publicada en el SECOP.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 404, $ruta, $ruta_tab, $texto);
    }

    public function contratacion_convenios() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_convenios');
        $crud->set_subject('CONVENIOS');
        $crud->display_as('contratacion_convenios_numero_proceso', 'Identificación o Número del Proceso ');
        $crud->display_as('contratacion_convenios_objeto', 'Objeto');
        $crud->display_as('contratacion_convenios_fecha_publicacion', 'Fecha  de Publicación');
        $crud->display_as('contratacion_convenios_archivo', 'Archivo');
        $crud->required_fields('contratacion_convenios_numero_proceso', 'contratacion_convenios_objeto', 'contratacion_convenios_fecha_publicacion', 'contratacion_convenios_archivo');
        $crud->unset_texteditor('contratacion_convenios_objeto');
        $crud->set_field_upload('contratacion_convenios_archivo', 'uploads/entidad/contratacion');
        $ruta = 'Convenios ';
        $ruta_tab = 'Contratación ';
        $texto = 'Convenios.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 405, $ruta, $ruta_tab, $texto);
    }

    public function contratacion_avance_contractual() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_avance_contractual');
        $crud->set_subject('AVANCES CONTRACTUALES');
        $crud->display_as('contratacion_avance_contractual_nombre', 'Nombre');
        $crud->display_as('contratacion_avance_contractual_periodo', 'Periodo');
        $crud->display_as('contratacion_avance_contractual_archivo', 'Archivo');
        $crud->display_as('contratacion_avance_contractual_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('contratacion_avance_contractual_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('contratacion_avance_contractual_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('contratacion_avance_contractual_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('contratacion_avance_contractual_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->required_fields('contratacion_avance_contractual_nombre', 'contratacion_avance_contractual_periodo', 'contratacion_avance_contractual_archivo');
        $crud->edit_fields('contratacion_avance_contractual_nombre', 'contratacion_avance_contractual_periodo', 'contratacion_avance_contractual_archivo', 'contratacion_avance_contractual_fecha_actualizacion');
        $crud->set_field_upload('contratacion_avance_contractual_archivo', 'uploads/entidad/contratacion');
        $ruta = 'Avances Contractuales';
        $ruta_tab = 'Contratación ';
        $texto = 'Avances Contractuales';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 406, $ruta, $ruta_tab, $texto);
    }
    
    
        public function contratacion_lineamientos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('contratacion_lineamientos');
        $crud->set_subject('LINEAMIENTOS DE ADQUISICIONES Y COMPRAS');
        $crud->display_as('contratacion_lineamientos_nombre', 'Nombre');
        $crud->display_as('contratacion_lineamientos_archivo', 'Archivo');
        $crud->display_as('contratacion_lineamientos_tipo', 'Tipo');
        $crud->display_as('contratacion_lineamientos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('contratacion_lineamientos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('contratacion_lineamientos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('contratacion_lineamientos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('contratacion_lineamientos_tipo', 'dropdown', array('Lineamiento' => 'Lineamiento','Proceso'=> 'Proceso', 'Procedimiento' => 'Procedimiento',"Decreto"=>"Decreto"));
        $crud->required_fields('contratacion_lineamientos_nombre', 'contratacion_lineamientos_archivo', 'contratacion_lineamientos_tipo');
        $crud->edit_fields('contratacion_lineamientos_nombre', 'contratacion_lineamientos_archivo', 'contratacion_lineamientos_tipo', 'contratacion_lineamientos_fecha_actualizacion');
        $crud->set_field_upload('contratacion_lineamientos_archivo', 'uploads/entidad/contratacion');
        $ruta = 'Lineamientos de Adquisiciones  y Compras';
        $ruta_tab = 'Contratación ';
        $texto = 'Lineamientos de Adquisiciones  y Compras';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 407, $ruta, $ruta_tab, $texto);
    }
    

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
