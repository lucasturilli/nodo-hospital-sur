<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Login
 *
 * Clase que me controla la autenticacion y elfomulario de ingreso al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Funciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->model('Referenciales_nodo_model');
    }

    function reiniciar_accesos() {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $administradores = $this->Referenciales_nodo_model->get_administrador();
        foreach ($administradores as $row) {
            $pass = substr(str_shuffle($permitted_chars), 0, 6);
            $datos = array(
                'administrador_password' => $this->encryption->encrypt($pass)
            );
            $this->Referenciales_nodo_model->update_administrador($datos, $row->id_administrador);
            echo "Usuario $row->administrador_identificacion  - OK <br>";
        }
    }

}
