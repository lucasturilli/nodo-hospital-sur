<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Principal extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->helper('url');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->model('Estado_publicacion_model');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function index() {
        $data = $this->check_publicacion();
        $this->Datos_entidad_model->actualizar_actualizacion();
        $this->breadcrumbs->push('<i class="fas fa-tachometer-alt"></i>', '<i class="fas fa-tachometer-alt"></i>');
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/home';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['entidad_logo'] = $this->Datos_entidad_model->obtener_entidad_logo();
        $data['menu'] = 'entidad';
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['audits'] = $this->Aplicaciones_model->obtener_audits();
        $data['audits_count'] = count($data['audits']);
        $data['visitas'] = $this->Datos_entidad_model->obtener_visitas();
        $datos_grafica = $this->Datos_entidad_model->obtener_visitas_grafica();
        $data['graficas'] = array();
        foreach ($datos_grafica as $dato) {
            $data['graficas'][$dato->entidad_contador_visitas_mes_mes] = array($dato->entidad_contador_visitas_mes_year, $dato->entidad_contador_visitas_mes_visitas, $dato->entidad_contador_visitas_mes_mes);
        }
        $data['suscriptores'] = $this->Datos_entidad_model->obtener_newsletter();
        if ($data['suscriptores'] != FALSE) {
            $data['suscriptores'] = count($data['suscriptores']);
        } else {
            $data['suscriptores'] = 0;
        }
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', ($data)); //Vista que se carga
    }

    public function estado_publicacion() {

        $data = $this->check_publicacion();
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $this->breadcrumbs->unshift_clear('Estado de Publicación', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/estado_publicaciones';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = FALSE;
        $data['menu'] = 'entidad';
        $data['audits'] = $this->Aplicaciones_model->obtener_audits(TRUE);
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function check_publicacion() {
        // 1 sin registro // 2 ok // 3 Faltan campos // 4 no hay avances // 5 vencidos  
        $data['entidad_informacion'] = $this->Estado_publicacion_model->entidad_informacion();
        $data['entidad_contacto'] = $this->Estado_publicacion_model->entidad_contacto();
        $data['entidad_politicas_datos'] = $this->Estado_publicacion_model->entidad_politicas_datos();
        $data['entidad_redes_sociales'] = $this->Estado_publicacion_model->entidad_redes_sociales();
        $data['entidad_dependencia'] = $this->Estado_publicacion_model->entidad_dependencia();
        $data['entidad_sucursales'] = $this->Estado_publicacion_model->entidad_sucursales();
//        $data['entidad_entidades'] = $this->Estado_publicacion_model->entidad_entidades();
//        $data['entidad_agremiaciones'] = $this->Estado_publicacion_model->entidad_agremiaciones();

        $data['entidad_preguntas_frecuentes'] = $this->Estado_publicacion_model->entidad_preguntas_frecuentes();
        $data['entidad_glosario'] = $this->Estado_publicacion_model->entidad_glosario();
        $data['entidad_noticias'] = $this->Estado_publicacion_model->entidad_noticias();
        $data['entidad_calendario'] = $this->Estado_publicacion_model->entidad_calendario();
        $data['entidad_ofertas_empleo'] = $this->Estado_publicacion_model->entidad_ofertas_empleo();
        $data['entidad_boletines'] = $this->Estado_publicacion_model->entidad_boletines();
        $data['convocatorias'] = $this->Estado_publicacion_model->convocatorias();
        $data['estudios'] = $this->Estado_publicacion_model->estudios();
        $data['informacion_adicional'] = $this->Estado_publicacion_model->informacion_adicional();

        $data['entidad_funcionario'] = $this->Estado_publicacion_model->entidad_funcionario();
        $data['entidad_manual_funciones'] = $this->Estado_publicacion_model->entidad_manual_funciones();
        $data['entidad_asignaciones_salarial'] = $this->Estado_publicacion_model->entidad_asignaciones_salarial();
        $data['entidad_planta_personal'] = $this->Estado_publicacion_model->entidad_planta_personal();
        $data['entidad_evaluacion_desempeno'] = $this->Estado_publicacion_model->entidad_evaluacion_desempeno();


        $data['normatividad_leyes'] = $this->Estado_publicacion_model->normatividad_leyes();
        $data['normatividad_decretos'] = $this->Estado_publicacion_model->normatividad_decretos();
        $data['normatividad_resoluciones'] = $this->Estado_publicacion_model->normatividad_resoluciones();
        $data['normatividad_edictos'] = $this->Estado_publicacion_model->normatividad_edictos();

        $data['financiera_presupuesto'] = $this->Estado_publicacion_model->financiera_presupuesto();
        $data['financiera_presupuesto_ejecucion'] = $this->Estado_publicacion_model->financiera_presupuesto_ejecucion();
        $data['financiera_estados_financieros'] = $this->Estado_publicacion_model->financiera_estados_financieros();
        $data['financiera_estatuto_tributario'] = $this->Estado_publicacion_model->financiera_estatuto_tributario();

        $data['normatividad_politicas'] = $this->Estado_publicacion_model->normatividad_politicas();
        $data['anticorrupcion_definicion'] = $this->Estado_publicacion_model->anticorrupcion_definicion();
        $data['control_plan_estrategico'] = $this->Estado_publicacion_model->control_plan_estrategico();
        $data['control_plan_accion'] = $this->Estado_publicacion_model->control_plan_accion();
        $data['control_plan_accion_avances'] = $this->Estado_publicacion_model->control_plan_accion_avances();
        $data['control_programas'] = $this->Estado_publicacion_model->control_programas();
        $data['control_programas_avances'] = $this->Estado_publicacion_model->control_programas_avances();
        $data['control_indicadores_gestion'] = $this->Estado_publicacion_model->control_indicadores_gestion();
        $data['control_planes_otros'] = $this->Estado_publicacion_model->control_planes_otros();
        $data['control_informe_empalme'] = $this->Estado_publicacion_model->control_informe_empalme();
        $data['control_informe_empalme_anexos'] = $this->Estado_publicacion_model->control_informe_empalme_anexos();
        $data['control_informe_archivo'] = $this->Estado_publicacion_model->control_informe_archivo();
        $data['planeacion_plan_gasto_publico'] = $this->Estado_publicacion_model->planeacion_plan_gasto_publico();

        $data['control_entes'] = $this->Estado_publicacion_model->control_entes();
        $data['control_auditorias'] = $this->Estado_publicacion_model->control_auditorias();
        $data['control_informe_concejo'] = $this->Estado_publicacion_model->control_informe_concejo();
        $data['control_informe_ciudadania'] = $this->Estado_publicacion_model->control_informe_ciudadania();
        $data['control_informe_fiscal'] = $this->Estado_publicacion_model->control_informe_fiscal();
        $data['control_planes_mejoramiento'] = $this->Estado_publicacion_model->control_planes_mejoramiento();
        $data['control_reportes_control_interno'] = $this->Estado_publicacion_model->control_reportes_control_interno();
        $data['control_poblacion_vulnerable'] = $this->Estado_publicacion_model->control_poblacion_vulnerable();
        $data['control_poblacion_vulnerable_programas'] = $this->Estado_publicacion_model->control_poblacion_vulnerable_programas();
        $data['control_poblacion_vulnerable_programas_avances'] = $this->Estado_publicacion_model->control_poblacion_vulnerable_programas_avances();
        $data['control_poblacion_vulnerable_proyectos'] = $this->Estado_publicacion_model->control_poblacion_vulnerable_proyectos();
        $data['control_poblacion_vulnerable_proyectos_avances'] = $this->Estado_publicacion_model->control_poblacion_vulnerable_proyectos_avances();
        $data['control_programas_sociales'] = $this->Estado_publicacion_model->control_programas_sociales();
        $data['control_defensa_judicial'] = $this->Estado_publicacion_model->control_defensa_judicial();
        $data['control_otros_informes'] = $this->Estado_publicacion_model->control_otros_informes();

        $data['contratacion_plan_compras'] = $this->Estado_publicacion_model->contratacion_plan_compras();
        $data['contratacion_contratos_secop'] = $this->Estado_publicacion_model->contratacion_contratos_secop();
        $data['contratacion_contratos'] = $this->Estado_publicacion_model->contratacion_contratos();
        $data['contratacion_avance_contractual'] = $this->Estado_publicacion_model->contratacion_avance_contractual();
//        $data['contratacion_convenios'] = $this->Estado_publicacion_model->contratacion_convenios();
        $data['contratacion_lineamientos'] = $this->Estado_publicacion_model->contratacion_lineamientos();

        $data['servicios_tramites'] = $this->Estado_publicacion_model->servicios_tramites();
        $data['servicios_tramites_en_linea'] = $this->Estado_publicacion_model->servicios_tramites_en_linea();
        $data['servicios_formatos'] = $this->Estado_publicacion_model->servicios_formatos();
        $data['servicios_pqrdf'] = $this->Estado_publicacion_model->servicios_pqrdf();

        $data['entidad_calidad'] = $this->Estado_publicacion_model->entidad_calidad();
        $data['entidad_caracterizacion_procesos'] = $this->Estado_publicacion_model->entidad_caracterizacion_procesos();
        $data['entidad_procedimientos'] = $this->Estado_publicacion_model->entidad_procedimientos();

        $data['catalogo_datos_abiertos'] = $this->Estado_publicacion_model->entidad_procedimientos();
        $data['archivos_datos_abiertos'] = $this->Estado_publicacion_model->archivos_datos_abiertos();
//        $data['mapas_datos_abiertos'] = $this->Estado_publicacion_model->mapas_datos_abiertos();

        $data['registro_activos_informacion'] = $this->Estado_publicacion_model->registro_activos_informacion();
        $data['informacion_clasificada_reservada'] = $this->Estado_publicacion_model->informacion_clasificada_reservada();
        $data['esquema_publicacion'] = $this->Estado_publicacion_model->esquema_publicacion();

        $data['entidad_documentos'] = $this->Estado_publicacion_model->entidad_documentos();

//        $data['municipio_informacion_general'] = $this->Estado_publicacion_model->municipio_informacion_general();
        //     $data['municipio_informacion_geografica'] = $this->Estado_publicacion_model->municipio_informacion_geografica();
        //      $data['municipio_informacion_sectores'] = $this->Estado_publicacion_model->municipio_informacion_sectores();
//        $data['municipio_sitios_interes'] = $this->Estado_publicacion_model->municipio_sitios_interes();
//        $data['municipio_galeria_imagenes'] = $this->Estado_publicacion_model->municipio_galeria_imagenes();
//        $data['municipio_galeria_videos'] = $this->Estado_publicacion_model->municipio_galeria_videos();
//        $data['municipio_galeria_audios'] = $this->Estado_publicacion_model->municipio_galeria_audios();
//        $data['municipio_galeria_mapas'] = $this->Estado_publicacion_model->municipio_galeria_mapas();
//        $data['municipio_directorio_turistico'] = $this->Estado_publicacion_model->municipio_directorio_turistico();
        //       $data['municipio_celebraciones'] = $this->Estado_publicacion_model->municipio_celebraciones();
//        $data['municipio_territorios'] = $this->Estado_publicacion_model->municipio_territorios();
        $contador = array(1 => 0, 2 => 0, 3 => 0, 4 => 0);
        $total = 0;
        $registros = 0;
        foreach ($data as $dato) {
            $contador[$dato[0]] ++;
            $total++;
            $registros += $dato[1];
        }
        $data['contador'] = $contador;
        $data['registros'] = $registros;
        $data['total'] = $total;
        return $data;
    }

    public function audits() {
        $this->Datos_entidad_model->actualizar_actualizacion();
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $this->breadcrumbs->unshift_clear('Registro de Publicaciones ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/registro_publicaciones';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = FALSE;
        $data['menu'] = 'entidad';
        $data['audits'] = $this->Aplicaciones_model->obtener_audits(TRUE);
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function recordatorios() {
        $this->breadcrumbs->unshift_clear('Listado de Recordatorios ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/registro_publicaciones';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = FALSE;
        $data['menu'] = 'entidad';
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function filemanager() {
        $this->breadcrumbs->unshift_clear('Administrador de Archivos', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/filemanager';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['entidad_logo'] = $this->Datos_entidad_model->obtener_entidad_logo();
        $data['menu'] = 'entidad';
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

}
