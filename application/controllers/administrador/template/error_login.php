<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co

 * @package    PQRS
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
$data = '<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Nodo - Error de autenticaci&oacute;n </title>
        <!-- Bootstrap -->
        <link href="' . site_url() . 'css/administrador/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,900|Roboto:400,500,700" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="' . site_url() . 'js/jquery.js" type="text/javascript"></script>
        <script src="' . site_url() . 'js/administrador/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>

    </head>
    <body>
    <body>
        <div class="row">
            <div class="container">
                <div class="col-12 center-block text-center">
<div class="jumbotron">
                                <img class="img-responsive center-block" src="' . site_url() . 'images/administrador/nodo.png" height="120" />
                                <h1>Error de autenticaci&oacute;n </h1>
                                <div class="alert alert-danger" role="alert"><i class="fas fa-exclamation-triangle"></i> Para poder ingresar, primero debes de autenticarte en el sistema.</div>
                                <a class="btn btn-primary btn-lg" href="' . site_url('administrador/login') . '"><i class="fas fa-sign-in-alt"></i> Ingresar Aquí</a>
</div>               
</div>
            </div>
        </div>
        <style>
        .jumbotron {
  position: absolute;
  top: 50%;
  left:50%;
  transform: translate(-50%,-50%);
  border: 1px solid deeppink;
}
        </style>
    </body>
</html>';
