<?php

$contenido = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>PQRS SOFTWARE</title>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Lato:400);
            img {
                max-width: 600px;
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
            }
            a {
                text-decoration: none;
                border: 0;
                outline: none;
            }
            a img {
                border: none;
            }
            td, h1, h2, h3  {
                font-family: Helvetica, Arial, sans-serif;
                font-weight: 400;
            }
            body {
                -webkit-font-smoothing:antialiased;
                -webkit-text-size-adjust:none;
                width: 100%;
                height: 100%;
                color: #37302d;
                background: #ffffff;
            }
            table {
                border-collapse: collapse !important;
            }
            h1, h2, h3 {
                padding: 0;
                margin: 0;
                color: #ffffff;
                font-weight: 400;
            }
            h3 {
                color: #21c5ba;
                font-size: 24px;
            }
            .important-font {
                color: #21BEB4;
                font-weight: bold;
            }
            .hide {
                display: none !important;
            }
            .force-full-width {
                width: 100% !important;
            }
        </style>
        <style type="text/css" media="screen">
            @media screen {
                /* Thanks Outlook 2013! http://goo.gl/XLxpyl*/
                td, h1, h2, h3 {
                    font-family: "Lato", "Helvetica Neue", "Arial", "sans-serif" !important;
                }
            }
        </style>
        <style type="text/css" media="only screen and (max-width: 480px)">
            /* Mobile styles */
            @media only screen and (max-width: 480px) {
                table[class="w320"] {
                    width: 320px !important;
                }
                table[class="w300"] {
                    width: 300px !important;
                }
                table[class="w290"] {
                    width: 290px !important;
                }
                td[class="w320"] {
                    width: 320px !important;
                }
                td[class="mobile-center"] {
                    text-align: center !important;
                }
                td[class*="mobile-padding"] {
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                    padding-bottom: 20px !important;
                }
                td[class*="mobile-block"] {
                    display: block !important;
                    width: 100% !important;
                    text-align: left !important;
                    padding-bottom: 20px !important;
                }
                td[class*="mobile-border"] {
                    border: 0 !important;
                }
                td[class*="reveal"] {
                    display: block !important;
                }
            }
        </style>
</head>


    <div>
        <img height="150" src="' . site_url('uploads/entidad/') . '/' . $this->Datos_entidad_model->obtener_entidad_logo()->entidad_informacion_logo . '" />
        <br />
<p>Este correo es generado automáticamente por el sitio web y tiene como fin informarle sobre la publicación de documentos asociados a su área y que deben de ser publicados en el sitio web.
<br><br>La documentación en la siguiente:
<br>
<br>
</p>
<table  class="force-full-width" cellspacing="10" border="1" cellpadding="10">';

if ($row->alertas_definicion_texto != NULL) {
    $contenido .= '<tr>
                        <td style="text-align: left;"><span class="important-font">Mensaje</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $row->alertas_definicion_texto . '</td>
                        </tr>';
}

$contenido .= '<tr>
                        <td style="text-align: left;"><span class="important-font">Sección</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $nombre_tabla . '</td>
                        </tr>
                                                <tr>
                        <td style="text-align: left;"><span class="important-font">Fecha ultimo ingreso</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $ultimo_ingreso . '</td>
                        </tr>
                                                <tr>
                        <td style="text-align: left;"><span class="important-font">Periodicidad  de publicación</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $periodicidad . '</td>
                        </tr>
                                                <tr>
                        <td style="text-align: left;"><span class="important-font">Diferencia</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $resultado[4] . '</td>
                        </tr>
                                                                        <tr>
                        <td style="text-align: left;"><span class="important-font">Días restantes</span></>
                        <td style="text-align: justify; vertical-align:top;">' . $resultado[2] . ' Días</td>
                        </tr>
</table>
<br>
<br>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
        <div style="background:#F1F1F1; color:#333; text-decoration:none; padding:20px;">
   <address> <strong>Alcaldía de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre . '<br />' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_departamento . ' - Colombia<br /></strong>
       <br /><strong>Dirección:</strong>' . $entidad->entidad_contacto_direccion . '<br /><strong>Correo electrónico:</strong>
           <a href="mailto:' . $entidad->entidad_contacto_email . '">' . $entidad->entidad_contacto_email . '</a><br />
               <strong>Teléfono:</strong>' . $entidad->entidad_contacto_telefono . '<br/>
                   <strong>Fax:</strong>' . $entidad->entidad_contacto_fax . '<br /><br />
                       <strong>Horarios de atención:</strong>' . $entidad->entidad_contacto_horarios_atencion . '</address>
        
                <br /><b>© Diseño desarrollado e implementación por <a href="http://codweb.co" target="_blanck">CodWeb.</a></b><br />
            </p>
        </div>
        </body>
';

