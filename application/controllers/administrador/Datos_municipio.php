<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datos_municipio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->library('grocery_CRUD');
        $this->load->library('image_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Municipio ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'municipio';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function municipio_informacion_general() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_informacion_general');
        $crud->set_subject('INFORMACIÓN GENERAL');
        $crud->display_as('municipio_informacion_general_nombre', 'Nombre del Municipio');
        $crud->display_as('municipio_informacion_general_departamento', 'Departamento al que Pertenece');
        $crud->display_as('municipio_informacion_general_codigo_dane', 'Código DANE');
        $crud->display_as('municipio_informacion_general_gentilicio', 'Gentilicio');
        $crud->display_as('municipio_informacion_general_presentacion', 'Presentación');
        $crud->display_as('municipio_informacion_general_mapa', 'Mapa');
        $crud->display_as('municipio_informacion_general_bandera', 'Bandera');
        $crud->display_as('municipio_informacion_general_bandera_descripcion', 'Descripción de la Bandera');
        $crud->display_as('municipio_informacion_general_escudo', 'Escudo');
        $crud->display_as('municipio_informacion_general_escudo_descripcion', 'Descripción del Escudo');
        $crud->display_as('municipio_informacion_general_himno', 'Himno');
        $crud->display_as('municipio_informacion_general_himno_autor', 'Autor del Himno');
        $crud->display_as('municipio_informacion_general_himno_audio', 'Audio del himno');
        $crud->display_as('municipio_informacion_general_fecha_fundacion', 'Año de Fundación');
        $crud->display_as('municipio_informacion_general_fundadores', 'Fundadores');
        $crud->display_as('municipio_informacion_general_historia', 'Historia del Municipio ');
        $crud->display_as('municipio_informacion_general_departamento_escudo', 'Escudo del Departamento ');
        $crud->set_field_upload('municipio_informacion_general_mapa', 'uploads/municipio');
        $crud->set_field_upload('municipio_informacion_general_departamento_escudo', 'uploads/municipio');
        $crud->set_field_upload('municipio_informacion_general_bandera', 'uploads/municipio');
        $crud->set_field_upload('municipio_informacion_general_escudo', 'uploads/municipio');
        $crud->set_field_upload('municipio_informacion_general_himno_audio', 'uploads/municipio');
        $crud->unset_texteditor('municipio_informacion_general_bandera_descripcion', 'municipio_informacion_general_escudo_descripcion', 'municipio_informacion_general_fundadores');
        $crud->required_fields('municipio_informacion_general_departamento_escudo', 'municipio_informacion_general_bandera', 'municipio_informacion_general_escudo', 'municipio_informacion_general_presentacion', 'municipio_informacion_general_codigo_dane', 'municipio_informacion_general_departamento', 'municipio_informacion_general_nombre');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Información General';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Información general del municipio, donde se especifica nombre, departamento al que pertenece, la presentación del municipio y su historia.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 651, $ruta, $ruta_tab, $texto);
    }

    public function municipio_informacion_geografica() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_informacion_geografia');
        $crud->set_subject('INFORMACIÓN GEOGRÁFICA');
        $crud->display_as('municipio_informacion_geografia_descripcion', 'Descripción de la Geografía del Municipio ');
        $crud->display_as('municipio_informacion_geografia_temperatura', 'Temperatura Promedio ');
        $crud->display_as('municipio_informacion_geografia_altura', 'Altura');
        $crud->display_as('municipio_informacion_geografia_extencion_total', 'Extensión Total');
        $crud->display_as('municipio_informacion_geografia_extencion_urbana', 'Extensión Urbana');
        $crud->display_as('municipio_informacion_geografia_extencion_rural', 'Extensión Rural');
        $crud->display_as('municipio_informacion_geografia_limites', 'Límites ');
        $crud->unset_texteditor('municipio_informacion_geografia_limites');
        $crud->required_fields('municipio_informacion_geografia_descripcion');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Información Geográfica ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Información general del municipio, donde se especifica nombre, departamento al que pertenece, la presentación del municipio y su historia.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 652, $ruta, $ruta_tab, $texto);
    }

    public function municipio_informacion_sectores() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_informacion_sectores');
        $crud->set_subject('SECTOR AGROECONÓMICO/ECOLOGICO');
        $crud->display_as('municipio_informacion_sectores_descripcion', 'Descripción');
        $crud->display_as('municipio_informacion_sectores_nombre', 'Nombre');
        $crud->unset_texteditor('municipio_informacion_sectores_descripcion');
        $crud->required_fields('municipio_informacion_sectores_descripcion', 'municipio_informacion_sectores_nombre');
        $ruta = 'Sectores Agroeconómicos/Ecologicos';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Descripción de cada uno de los sectores más importantes que constituyen la economía y ecología del municipio.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 653, $ruta, $ruta_tab, $texto);
    }

    public function municipio_sitios_interes() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_sitios_interes');
        $crud->set_subject('SITIO DE INTERÉS ');
        $crud->display_as('municipio_sitios_interes_descripcion', 'Descripción');
        $crud->display_as('municipio_sitios_interes_nombre', 'Nombre');
        $crud->display_as('municipio_sitios_interes_imagen', 'Imagen');
        $crud->required_fields('municipio_sitios_interes_descripcion', 'municipio_sitios_interes_nombre', 'municipio_sitios_interes_imagen');
        $crud->set_field_upload('municipio_sitios_interes_imagen', 'uploads/municipio/sitios');
        $crud->unset_texteditor('municipio_sitios_interes_descripcion');
        $ruta = 'Sitios de Interés ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Principales sitios representativos del municipio, ya sea por su significado histórico, recreacional o atracción.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 654, $ruta, $ruta_tab, $texto);
    }

    public function municipio_galeria_imagenes() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_galeria_imagenes');
        $crud->set_subject('WIDGET IMAGENES');
        $crud->display_as('municipio_galeria_imagenes_widget', 'Widget');
        $crud->unset_texteditor('municipio_galeria_imagenes_widget');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Galería de Imágenes ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Galería de imágenes referente al municipio y sus características.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 655, $ruta, $ruta_tab, $texto);
    }

    public function municipio_galeria_videos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_galeria_videos');
        $crud->set_subject('WIDGET VIDEO');
        $crud->display_as('municipio_galeria_videos_widget', 'Widget');
        $crud->unset_texteditor('municipio_galeria_videos_widget');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Galería de Videos ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Galería de videos referente al municipio y sus características.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 656, $ruta, $ruta_tab, $texto);
    }

    public function municipio_galeria_audios() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_galeria_audios');
        $crud->set_subject('WIDGET AUDIO');
        $crud->display_as('municipio_galeria_audios_widget', 'Widget');
        $crud->unset_texteditor('municipio_galeria_audios_widget');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Galería de Audios ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Galería de audios referente al municipio y sus características.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 657, $ruta, $ruta_tab, $texto);
    }

    public function municipio_galeria_mapas() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_galeria_mapas');
        $crud->set_subject('MAPA');
        $crud->display_as('municipio_galeria_mapas_nombre', 'Nombre');
        $crud->display_as('municipio_galeria_mapas_imagen', 'Archivo');
        $crud->display_as('municipio_galeria_mapas_descripcion', 'Descripción');
        $crud->display_as('municipio_galeria_mapas_tipo_mapa', 'Clasificación');
        $crud->required_fields('municipio_galeria_mapas_nombre', 'municipio_galeria_mapas_imagen');
        $crud->unset_texteditor('municipio_galeria_mapas_descripcion');
        $crud->set_field_upload('municipio_galeria_mapas_imagen', 'uploads/municipio/mapas');
        $crud->field_type('municipio_galeria_mapas_tipo_mapa', 'dropdown', array('Mapa Geográfico' => 'Mapa Geográfico', 'Mapa Político' => 'Mapa Político', 'Mapa territorial' => 'Mapa territorial', 'Mapa Climático' => 'Mapa Climático'
            , 'Mapa Geológico ' => 'Mapa Geológico ', 'Mapa Topográfico' => 'Mapa Topográfico', 'Mapa Económico' => 'Mapa Económico', 'Mapa Urbano' => 'Mapa Urbano', 'Mapa Rural' => 'Mapa Rural'));
        $ruta = 'Galería de Mapas ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Galería de mapas referente al municipio y sus características.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 658, $ruta, $ruta_tab, $texto);
    }

    public function municipio_directorio_turistico() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_directorio_turistico');
        $crud->set_subject('INFORMACIÓN TURÍSTICA/COMERCIAL');
        $crud->display_as('municipio_directorio_turistico_nombre', 'Nombre');
        $crud->display_as('municipio_directorio_turistico_clasificacion', 'Clasificación ');
        $crud->display_as('municipio_directorio_turistico_direccion', 'Dirección ');
        $crud->display_as('municipio_directorio_turistico_telefono', 'Teléfono ');
        $crud->display_as('municipio_directorio_turistico_email', 'Correo Electrónico');
        $crud->display_as('municipio_directorio_turistico_web', 'Sitio WEB');
        $crud->required_fields('municipio_directorio_turistico_nombre', 'municipio_directorio_turistico_clasificacion', 'municipio_directorio_turistico_telefono');
        $crud->set_rules('municipio_directorio_turistico_email', 'Correo Electrónico', 'valid_email');
        $crud->field_type('municipio_directorio_turistico_clasificacion', 'dropdown', array('Almacén Comercial' => 'Almacén Comercial', 'Centro de Recreación' => 'Centro de Recreación', 'Centro Turístico' => 'Centro Turístico', 'Cine/Teatro' => 'Cine/Teatro', 'Entidad Bancaria' => 'Entidad Bancaria', 'Hotel' => 'Hotel', 'Iglesia' => 'Iglesia', 'Museo' => 'Museo', 'Restaurante/Bares' => 'Restaurante/Bares', 'Otro' => 'Otro'));
        $ruta = 'Directorio Turístico/Comercial  ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Directorio de los principales establecimientos y centros que posee el municipio que permita a los visitantes facilitar su estadía o paso por el municipio.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 659, $ruta, $ruta_tab, $texto);
    }

    public function municipio_territorios() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_territorios');
        $crud->set_subject('TERRITORIO');
        $crud->display_as('municipio_territorios_nombre', 'Nombre');
        $crud->display_as('municipio_territorios_tipo', 'Tipo ');
        $crud->display_as('municipio_territorios_descripcion', 'Descripción ');
        $crud->display_as('municipio_territorios_mapa', 'Mapa Embedded');
        $crud->display_as('municipio_territorios_foto', 'Foto ');
       // $crud->set_field_upload('municipio_territorios_mapa', 'uploads/municipio/territorios');
        $crud->set_field_upload('municipio_territorios_foto', 'uploads/municipio/territorios');
        $crud->required_fields('municipio_territorios_nombre', 'municipio_territorios_tipo');
        $crud->unset_texteditor('municipio_territorios_descripcion','municipio_territorios_mapa');
        $crud->field_type('municipio_territorios_tipo', 'dropdown', array('Barrio' => 'Barrio', 'Comuna' => 'Comuna', 'Corregimiento' => 'Corregimiento', 'Urbanización ' => 'Urbanización ', 'Vereda' => 'Vereda'));
        $ruta = 'Territorios';
        $ruta_tab = 'Territorios';
        $texto = 'información de los territorios del municipio clasificados por comunas, barrios, corregimientos y veredas. Cada territorio contiene información general, fotos, mapas e información de población.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 701, $ruta, $ruta_tab, $texto);
    }

    public function municipio_celebraciones() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_celebraciones');
        $crud->set_subject('CELEBRACIONES');
        $crud->display_as('municipio_celebraciones_nombre', 'Nombre');
        $crud->display_as('municipio_celebraciones_fecha', 'Fecha ');
        $crud->display_as('municipio_celebraciones_descripcion', 'Descripción ');
        $crud->required_fields('municipio_celebraciones_nombre', 'municipio_celebraciones_fecha', 'municipio_celebraciones_descripcion');
        $crud->unset_texteditor('municipio_celebraciones_descripcion');
        $ruta = 'Fiestas y Celebraciones ';
        $ruta_tab = 'Información General del Municipio';
        $texto = 'Fiestas y celebraciones que se celebran en el municipio o aledaños.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 660, $ruta, $ruta_tab, $texto);
    }

    public function municipio_documentos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('municipio_documentos');
        $crud->set_subject('DOCUMENTO');
        $crud->display_as('municipio_documentos_nombre', 'Nombre');
        $crud->display_as('municipio_documentos_descripcion', 'Descripción');
        $crud->display_as('municipio_documentos_archivo', 'Archivo');
        $crud->display_as('municipio_documentos_tipo_documento', 'Clasificación  del Documento');
        $crud->required_fields('municipio_documentos_nombre', 'municipio_documentos_archivo', 'municipio_documentos_tipo_documento');
        $crud->set_field_upload('municipio_documentos_archivo', 'uploads/municipio/documentos');
        $crud->unset_texteditor('municipio_documentos_descripcion');
        $crud->field_type('municipio_documentos_tipo_documento', 'dropdown', array('Agropecuario' => 'Agropecuario', 'Convivencia' => 'Convivencia', 'Cultura' => 'Cultura', 'Demografía' => 'Demografía', 'Economía' => 'Economía'
            , 'Educación' => 'Educación', 'Estudios Sociales' => 'Estudios Sociales', 'Familia' => 'Familia', 'Grupos Étnicos' => 'Grupos Étnicos', 'Indicadores' => 'Indicadores'
            , 'Institucional' => 'Institucional', 'Juventud' => 'Juventud', 'Medio Ambiente' => 'Medio Ambiente', 'Meteorología' => 'Meteorología', 'Minero' => 'Minero'
            , 'Mujeres' => 'Mujeres', 'Obras Públicas' => 'Obras Públicas', 'Recreacion y Deporte' => 'Recreacion y Deporte', 'Salud' => 'Salud', 'Servicios Públicos' => 'Servicios Públicos'
            , 'Tercera Edad' => 'Tercera Edad', 'Trabajo' => 'Trabajo', 'Tramites y Servicios' => 'Tramites y Servicios', 'Trasporte' => 'Trasporte', 'Vivienda' => 'Vivienda', 'Turismo' => 'Turismo', 'Otros' => 'Otros'));
        $ruta = 'Documentos del Municipio';
        $ruta_tab = 'Documentos del Municipio';
        $texto = 'Documentos sobre y correspondientes al Municipio  en diferentes áreas y temas.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 751, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
