<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Control extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));

        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function control_entes() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_entes');
        $crud->set_subject('ENTE DE CONTROL');
        $crud->display_as('control_entes_nombre', 'Nombre');
        $crud->display_as('control_entes_descripcion', 'Descripción ');
        $crud->display_as('control_entes_url', 'Sitio Web');
        $crud->display_as('control_entes_email_contacto', 'Correo Electrónico ');
        $crud->display_as('control_entes_telefono_contacto', 'Teléfono ');
        $crud->display_as('control_entes_fax_contacto', 'Fax');
        $crud->display_as('control_entes_direccion', 'Dirección');
        $crud->display_as('control_entes_ciudad', 'Ciudad');
        $crud->display_as('control_entes_tipo_control', 'Tipo de Control');
        $crud->required_fields('control_entes_nombre', 'control_entes_descripcion', 'control_entes_telefono_contacto', 'control_entes_email_contacto', 'control_entes_direccion', 'control_entes_ciudad', 'control_entes_tipo_control');
        $ruta = 'Entes de Control que Vigilan la Entidad ';
        $ruta_tab = 'Control  ';
        $texto = 'La entidad debe publicar la relación de todas las entidades que vigilan la entidad y los mecanismos de control que existen al interior y al exterior para hacer un seguimiento efectivo sobre la gestión de la misma. Para ello se debe indicar, como mínimo, el tipo de control que se ejecuta al interior y exterior (fiscal, social, político, etc.) y la forma como un particular puede comunicar una irregularidad ante la entidad responsable de hacer el control sobre ella (dirección, correo electrónico, teléfono o enlace al sistema de denuncias si existe).';
        $crud->set_rules('control_entes_email_contacto', 'Correo Electrónico', 'valid_email');
        $crud->unset_texteditor('control_entes_direccion', 'control_entes_descripcion');
        $crud->field_type('control_entes_tipo_control', 'dropdown', array('Fiscal' => 'Fiscal', 'Social' => 'Social', 'Político' => 'Político', 'Otro' => 'Otro'));
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 351, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_concejo() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_concejo');
        $crud->set_subject('INFORME CONCEJO/ASAMBLEA/CONGRESO');
        //  $crud->display_as('control_informe_concejo_nombre', 'Nombre');
        $crud->display_as('control_informe_concejo_archivo', 'Archivo');
        $crud->display_as('control_informe_concejo_periodo', 'Periodo');
        $crud->display_as('control_informe_concejo_nombre', 'Nombre');
        $crud->set_field_upload('control_informe_concejo_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_concejo_nombre', 'control_informe_concejo_archivo', 'control_informe_concejo_periodo');
        $crud->field_type('control_informe_concejo_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Informe al  Congreso/Asamblea/Concejo ';
        $ruta_tab = 'Control  ';
        $texto = 'La entidad publica, como mínimo el informe enviado al Congreso/Asamblea/Concejo, según aplique. Del periodo en vigencia y del inmediatamente anterior. Se debe publicar dentro del mismo mes de enviado.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 353, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_contraloria() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_contraloria');
        $crud->set_subject('ENLACE A LA CONTRALORÍA');
        $crud->display_as('control_informe_contraloria_embedded', 'Embebido');
        $crud->display_as('control_informe_contraloria_enlace', 'Enlace');
        $ruta = 'Enlace a la Contraloría  ';
        $crud->required_fields('control_informe_contraloria_embedded', 'control_informe_contraloria_enlace');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_texteditor('control_informe_contraloria_embedded');
        $ruta_tab = 'Control  ';
        $texto = 'La entidad publica el informe de rendición de la cuenta fiscal a la Contraloría Nacional, de acuerdo con la periodicidad definida. Se debe publicar dentro del mismo mes de enviado.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 364, $ruta, $ruta_tab, $texto);
    }

    public function control_auditorias() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_auditorias');
        $crud->set_subject('AUDITORIA');
        $crud->display_as('control_auditorias_nombre', 'Nombre');
        $crud->display_as('control_auditorias_archivo', 'Archivo');
        $crud->display_as('control_auditorias_periodo', 'Periodo');
        $crud->display_as('id_control_entes', 'Ente de Control');
        $crud->display_as('control_auditorias_fecha_auditoria', 'Fecha de Auditoria');
        $crud->display_as('control_auditorias_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_auditorias_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_auditorias_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_auditorias_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('control_auditorias_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->set_field_upload('control_auditorias_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_auditorias_fecha_auditoria', 'id_control_auditorias', 'control_auditorias_archivo', 'control_auditorias_periodo', 'control_auditorias_nombre');
        $crud->field_type('control_auditorias_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Auditorias';
        $crud->edit_fields('control_auditorias_nombre', 'control_auditorias_fecha_auditoria', 'id_control_entes', 'control_auditorias_fecha_actualizacion', 'control_auditorias_periodo', 'control_auditorias_archivo');
        $crud->set_relation('id_control_entes', 'control_entes', 'control_entes_nombre');
        $ruta_tab = 'Control  ';
        $texto = 'Auditorias.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 352, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_ciudadania() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_ciudadania');
        $crud->set_subject('INFORME A LA CIUDADANIA');
        $crud->display_as('control_informe_ciudadania_nombre', 'Nombre');
        $crud->display_as('control_informe_ciudadania_archivo', 'Archivo');
        $crud->display_as('control_informe_ciudadania_periodo', 'Periodo');
        $crud->display_as('control_informe_ciudadania_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informe_ciudadania_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informe_ciudadania_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informe_ciudadania_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informe_ciudadania_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_ciudadania_archivo', 'control_informe_ciudadania_periodo', 'control_informe_ciudadania_nombre');
        $crud->field_type('control_informe_ciudadania_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->edit_fields('control_informe_ciudadania_nombre', 'control_informe_ciudadania_archivo', 'control_informe_ciudadania_periodo');
        $ruta = 'Informe de Rendición de Cuenta a los Ciudadanos    ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica el informe de rendición de cuentas a los Ciudadanos, incluyendo la respuesta a las solicitudes realizadas por los ciudadanos, antes y durante el ejercicio de rendición. Se debe publicar dentro del mismo mes de realizado el evento.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 354, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_ciudadania_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_ciudadania_anexos');
        $crud->set_subject('ANEXOS DE INFORMES A LA CIUDADANIA');
        $crud->display_as('id_control_informe_ciudadania', 'Informe');
        $crud->display_as('control_informe_ciudadania_anexos_nombre', 'Nombre');
        $crud->display_as('control_informe_ciudadania_anexos_archivo', 'Archivo');
        $crud->display_as('control_informe_ciudadania_anexos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informe_ciudadania_anexos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informe_ciudadania_anexos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informe_ciudadania_anexos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informe_ciudadania_anexos_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_ciudadania_anexos_nombre', 'control_informe_ciudadania_anexos_archivo', 'id_control_informe_ciudadania');
        $crud->edit_fields('control_informe_ciudadania_anexos_nombre', 'control_informe_ciudadania_anexos_archivo', 'id_control_informe_ciudadania');
        $crud->set_relation('id_control_informe_ciudadania', 'control_informe_ciudadania', 'control_informe_ciudadania_nombre');
        $ruta = 'Anexos a los Informe de Rendición de Cuenta a los Ciudadanos';
        $ruta_tab = 'Control';
        $texto = 'Anexos a los Informe de Rendición de Cuenta a los Ciudadanos';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 368, $ruta, $ruta_tab, $texto);
    }

    public function control_informes_gestion_ciudadania() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informes_gestion_ciudadania');
        $crud->set_subject('INFORMES DE GESTIÓN A LA CIUDADANIA');
        $crud->display_as('id_control_informe_ciudadania', 'Informe');
        $crud->display_as('control_informes_gestion_ciudadania_nombre', 'Nombre');
        $crud->display_as('control_informes_gestion_ciudadania_archivo', 'Archivo');
        $crud->display_as('control_informes_gestion_ciudadania_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informes_gestion_ciudadania_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informes_gestion_ciudadania_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informes_gestion_ciudadania_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informes_gestion_ciudadania_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informes_gestion_ciudadania_nombre', 'control_informes_gestion_ciudadania_archivo', 'id_control_informe_ciudadania');
        $crud->edit_fields('control_informes_gestion_ciudadania_nombre', 'control_informes_gestion_ciudadania_archivo', 'id_control_informe_ciudadania');
        $crud->set_relation('id_control_informe_ciudadania', 'control_informe_ciudadania', 'control_informe_ciudadania_nombre');
        $ruta = 'Informe de Gestión a los Ciudadanos';
        $ruta_tab = 'Control';
        $texto = 'Informe de Gestión a los Ciudadanos';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 367, $ruta, $ruta_tab, $texto);
    }

    public function control_informes_cronograma() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informes_cronograma');
        $crud->set_subject('CRONOGRAMA DE RENDICION DE CUENTAS');
        $crud->display_as('control_informes_cronograma_nombre', 'Nombre');
        $crud->display_as('control_informes_cronograma_archivo', 'Archivo');
        $crud->display_as('control_informes_cronograma_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informes_cronograma_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informes_cronograma_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informes_cronograma_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informes_cronograma_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informes_cronograma_nombre', 'control_informes_cronograma_archivo');
        $crud->edit_fields('control_informes_cronograma_nombre', 'control_informes_cronograma_archivo');
        $ruta = 'Cronograma de Rendición de Cuentas';
        $ruta_tab = 'Control';
        $texto = 'Cronograma de Rendición de Cuentas';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 369, $ruta, $ruta_tab, $texto);
    }

    public function control_informes_ciudadania_evaluacion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('control_informes_ciudadania_evaluacion');
        $crud->set_subject('EVALUACION DE INFORMES A LA CIUDADANIA');
        $crud->display_as('id_control_informes_ciudadania_evaluacion', 'Informe');
        $crud->display_as('control_informes_ciudadania_evaluacion_nombre', 'Nombre');
        $crud->display_as('control_informes_ciudadania_evaluacion_archivo', 'Archivo');
        $crud->display_as('control_informes_ciudadania_evaluacion_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informes_ciudadania_evaluacion_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informes_ciudadania_evaluacion_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informes_ciudadania_evaluacion_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informes_ciudadania_evaluacion_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informes_ciudadania_evaluacion_nombre', 'control_informes_ciudadania_evaluacion_archivo', 'id_control_informes_ciudadania_evaluacion');
        $crud->edit_fields('control_informes_ciudadania_evaluacion_nombre', 'control_informes_ciudadania_evaluacion_archivo', 'id_control_informes_ciudadania_evaluacion');
        $crud->set_relation('id_control_informe_ciudadania', 'control_informe_ciudadania', 'control_informe_ciudadania_nombre');
        $ruta = 'Evaluación  de Informe a los Ciudadanos';
        $ruta_tab = 'Control';
        $texto = 'Evaluación  de Informe a los Ciudadanos';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 370, $ruta, $ruta_tab, $texto);
    }

    public function control_informes_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informes_anexos');
        $crud->set_subject('ANEXOS DE LAS EVALUACIONES A LOS INFORMES A LA CIUDADANIA');
        $crud->display_as('id_control_informes_ciudadania_evaluacion', 'Informe');
        $crud->display_as('control_informes_anexos_nombre', 'Nombre');
        $crud->display_as('control_informes_anexos_archivo', 'Archivo');
        $crud->display_as('control_informes_anexos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informes_anexos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informes_anexos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informes_anexos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_informes_anexos_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informes_anexos_nombre', 'control_informes_anexos_archivo', 'id_control_informes_ciudadania_evaluacion');
        $crud->edit_fields('control_informes_anexos_nombre', 'control_informes_anexos_archivo', 'id_control_informes_ciudadania_evaluacion');
        $crud->set_relation('id_control_informes_ciudadania_evaluacion', 'control_informes_ciudadania_evaluacion', 'control_informes_ciudadania_evaluacion_nombre');
        $ruta = 'Anexos a las evaluaciones de los  Informe de Rendición de Cuenta a los Ciudadanos';
        $ruta_tab = 'Control';
        $texto = 'Anexos a las evaluaciones de los  Informe de Rendición de Cuenta a los Ciudadanos';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 371, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_fiscal() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_fiscal');
        $crud->set_subject('INFORMES DE RENDICIÓN FISCAL');
        $crud->display_as('control_informe_fiscal_nombre', 'Nombre');
        $crud->display_as('control_informe_fiscal_archivo', 'Archivo');
        $crud->display_as('control_informe_fiscal_periodo', 'Periodo');
        $crud->display_as('control_informe_fiscal_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_informe_fiscal_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_informe_fiscal_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informe_fiscal_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informe_fiscal_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->set_field_upload('control_informe_fiscal_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_fiscal_nombre', 'control_informe_fiscal_archivo', 'control_informe_fiscal_periodo');
        $crud->field_type('control_auditorias_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Informes de Rendición Fiscal';
        $crud->edit_fields('control_informe_fiscal_nombre', 'control_informe_fiscal_archivo', 'control_informe_fiscal_periodo', 'control_informe_fiscal_fecha_actualizacion');
        $ruta_tab = 'Control  ';
        $texto = 'Informes de Rendición Fiscal .';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 366, $ruta, $ruta_tab, $texto);
    }

    public function control_planes_mejoramiento() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_planes_mejoramiento');
        $crud->set_subject('PLAN DE MEJORAMIENTO');
        $crud->display_as('control_planes_mejoramiento_nombre', 'Nombre');
        $crud->display_as('control_planes_mejoramiento_archivo', 'Archivo');
        $crud->display_as('control_planes_mejoramiento_periodo', 'Periodo');
        $crud->display_as('control_planes_mejoramiento_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_planes_mejoramiento_fecha_actualizacion', 'Fecha de Actualización');
        $crud->display_as('id_control_entes', 'Ente de Control');
        $crud->display_as('id_entidad_dependencia', 'Dependencia');
        $crud->set_field_upload('control_planes_mejoramiento_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_planes_mejoramiento_nombre', 'id_entidad_dependencia', 'control_planes_mejoramiento_archivo', 'id_control_entes', 'control_planes_mejoramiento_periodo');
        $crud->edit_fields('control_planes_mejoramiento_nombre', 'control_planes_mejoramiento_periodo', 'id_control_entes', 'id_entidad_dependencia', 'control_planes_mejoramiento_archivo', 'control_planes_mejoramiento_fecha_actualizacion');
        $crud->set_relation('id_control_entes', 'control_entes', 'control_entes_nombre');
        $crud->set_relation('id_entidad_dependencia', 'entidad_dependencia', 'entidad_dependencia_nombre');
        $crud->field_type('control_planes_mejoramiento_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_planes_mejoramiento_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('control_planes_mejoramiento_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $ruta = 'Planes de Mejoramiento';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica los Planes de Mejoramiento vigentes (exigidos por los entes de control internos o externos) Los planes de mejoramiento de acuerdo con los hallazgos de la Contraloría, se deben publicar de acuerdo con la periodicidad establecida por la ésta, dentro del mismo mes de su envío.<br>Se cuenta con un enlace al sitio web del organismo de control en donde se encuentren los informes que éste ha elaborado sobre la entidad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 355, $ruta, $ruta_tab, $texto);
    }

    public function control_reportes_control_interno() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('control_reportes_control_interno');
        $crud->set_subject('REPORTE DE CONTROL INTERNO');
        $crud->display_as('control_reportes_control_interno_nombre', 'Nombre');
        $crud->display_as('control_reportes_control_interno_archivo', 'Archivo');
        $crud->display_as('control_reportes_control_interno_periodo', 'Periodo');
        $crud->display_as('id_control_reportes_control_interno_tab', 'Tipo');
        $crud->display_as('control_reportes_control_interno_cuatrimestre', 'Periodicidad');
        $crud->set_field_upload('control_reportes_control_interno_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_reportes_control_interno_nombre', 'control_reportes_control_interno_archivo', 'control_reportes_control_interno_periodo', 'control_reportes_control_interno_cuatrimestre', 'id_control_reportes_control_interno_tab');
        $crud->field_type('control_reportes_control_interno_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5, date('Y') - 6 => date('Y') - 6, date('Y') - 7 => date('Y') - 7));
        $crud->field_type('control_reportes_control_interno_cuatrimestre', 'dropdown', periodicidad());
        $crud->set_relation('id_control_reportes_control_interno_tab', 'control_reportes_control_interno_tab', 'control_reportes_control_interno_tab_nombre');
        $ruta = 'Reportes de Control Interno';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica el informe pormenorizado del estado del control interno de acuerdo con lo señalado en el artículo 9 de la Ley 1474 de 2011 (Estatuto Anticorrupción).<br>Dicho informe se debe publicar cada cuatro meses según lo establecido por el Sistema Integrado de Gestión del Departamento Administrativo de la Función Pública.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 356, $ruta, $ruta_tab, $texto);
    }

    public function control_reportes_control_interno_tab() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('control_reportes_control_interno_tab');
        $crud->required_fields('control_reportes_control_interno_tab_nombre');
        $crud->set_subject('REPORTE DE CONTROL INTERNO');
        $crud->display_as('control_reportes_control_interno_tab_nombre', 'Nombre');
        $ruta = 'Tipos de Reporte de Control Interno';
        $ruta_tab = 'Control';
        $texto = 'Tipos de Reporte de Control Interno';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 372, $ruta, $ruta_tab, $texto);
    }

    public function control_poblacion_vulnerable() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_poblacion_vulnerable');
        $crud->set_subject('INFORMACIÓN PARA POBLACIÓN VULNERABLE');
        $crud->display_as('control_poblacion_vulnerable_descripcion', 'Descripción de Población Vulnerable ');
        $crud->display_as('control_poblacion_vulnerable_normas', 'Normas');
        $crud->display_as('control_poblacion_vulnerable_politicas', 'Políticas');
        //$crud->required_fields('control_poblacion_vulnerable_descripcion', 'control_poblacion_vulnerable_normas', 'control_poblacion_vulnerable_politicas');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Información para Población Vulnerable ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica las normas y la política dirigidos a población vulnerable (madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados), de acuerdo con su misión.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 357, $ruta, $ruta_tab, $texto);
    }

    public function control_poblacion_vulnerable_programas() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_poblacion_vulnerable_programas');
        $crud->set_subject('PROGRAMAS PARA LA POBLACIÓN VULNERABLE ');
        $crud->display_as('control_poblacion_vulnerable_programas_nombre', 'Nombre');
        $crud->display_as('control_poblacion_vulnerable_programas_archivo', 'Archivo');
        $crud->display_as('control_poblacion_vulnerable_programas_descripcion', 'Descripción');
        $crud->display_as('control_poblacion_vulnerable_programas_fecha', 'Fecha de Actualización');
        $crud->display_as('control_poblacion_vulnerable_programas_dirigido_a', 'Dirigido a');
        $crud->set_field_upload('control_poblacion_vulnerable_programas_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_poblacion_vulnerable_programas_nombre', 'control_poblacion_vulnerable_programas_descripcion', 'control_poblacion_vulnerable_programas_dirigido_a');
        $crud->field_type('control_poblacion_vulnerable_programas_fecha', 'hidden', date('Y-m-d'));
        $crud->unset_texteditor('control_poblacion_vulnerable_programas_descripcion');
        $crud->field_type('control_poblacion_vulnerable_programas_dirigido_a', 'multiselect', array("Madre Cabeza de Familia" => "Madre Cabeza de Familia", "Desplazados" => "Desplazados", "Personas en Condición de Discapacidad" => "Personas en Condición de Discapacidad", "Familias en Condición de Pobreza" => "Familias en Condición de Pobreza", "Niños y Niñas" => "Niños y Niñas", "Adulto Mayor" => "Adulto Mayor", "Etnias" => "Etnias", "Reinsertados" => "Reinsertados", "Otros" => "Otros"));
        $ruta = 'Programas para la Población Vulnerable ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica los programas dirigidos a población vulnerable (madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados), de acuerdo con su misión.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 358, $ruta, $ruta_tab, $texto);
    }

    public function control_poblacion_vulnerable_programas_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_poblacion_vulnerable_programas_avances');
        $crud->set_subject('AVANCES DE LOS PROGRAMAS PARA LA POBLACIÓN VULNERABLE ');
        $crud->display_as('control_poblacion_vulnerable_programas_avances_archivo', 'Archivo');
        $crud->display_as('control_poblacion_vulnerable_programas_avances_periodo', 'Periodo');
        $crud->display_as('control_poblacion_vulnerable_programas_avances_trimestre', 'Periodicidad');
        $crud->display_as('id_control_poblacion_vulnerable_programas', 'Programa');
        $crud->set_field_upload('control_poblacion_vulnerable_programas_avances_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_poblacion_vulnerable_programas_avances_archivo', 'control_poblacion_vulnerable_programas_avances_trimestre', 'control_poblacion_vulnerable_programas_avances_periodo', 'id_control_poblacion_vulnerable_programas');
        $crud->field_type('control_poblacion_vulnerable_programas_avances_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->field_type('control_poblacion_vulnerable_programas_avances_trimestre', 'dropdown', periodicidad());
        $crud->set_relation('id_control_poblacion_vulnerable_programas', 'control_poblacion_vulnerable_programas', 'control_poblacion_vulnerable_programas_nombre');
        $ruta = 'Avances de los Programas para la Población Vulnerable ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica los programas dirigidos a población vulnerable (madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados), de acuerdo con su misión.<br>Cuando se trate de programas o proyectos, debe publicar el avance de los mismos, cada 3 meses.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 359, $ruta, $ruta_tab, $texto);
    }

    public function control_poblacion_vulnerable_proyectos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_poblacion_vulnerable_proyectos');
        $crud->set_subject('PROYECTOS PARA LA POBLACIÓN VULNERABLE ');
        $crud->display_as('control_poblacion_vulnerable_proyectos_nombre', 'Nombre');
        $crud->display_as('control_poblacion_vulnerable_proyectos_archivo', 'Archivo');
        $crud->display_as('control_poblacion_vulnerable_proyectos_descripcion', 'Descripción');
        $crud->display_as('control_poblacion_vulnerable_proyectos_fecha', 'Fecha de Actualización');
        $crud->display_as('control_poblacion_vulnerable_proyectos_dirigido_a', 'Dirigido a');
        $crud->set_field_upload('control_poblacion_vulnerable_proyectos_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_poblacion_vulnerable_proyectos_nombre', 'control_poblacion_vulnerable_proyectos_descripcion', 'control_poblacion_vulnerable_proyectos_dirigido_a');
        $crud->field_type('control_poblacion_vulnerable_proyectos_fecha', 'hidden', date('Y-m-d'));
        $crud->unset_texteditor('control_poblacion_vulnerable_proyectos_descripcion');
        $crud->field_type('control_poblacion_vulnerable_proyectos_dirigido_a', 'multiselect', array("Madre Cabeza de Familia" => "Madre Cabeza de Familia", "Desplazados" => "Desplazados", "Personas en Condición de Discapacidad" => "Personas en Condición de Discapacidad", "Familias en Condición de Pobreza" => "Familias en Condición de Pobreza", "Niños y Niñas" => "Niños y Niñas", "Adulto Mayor" => "Adulto Mayor", "Etnias" => "Etnias", "Reinsertados" => "Reinsertados", "Otros" => "Otros"));
        $ruta = 'Proyectos para la Población Vulnerable ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica los proyectos dirigidos a población vulnerable (madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados), de acuerdo con su misión.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 360, $ruta, $ruta_tab, $texto);
    }

    public function control_poblacion_vulnerable_proyectos_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_poblacion_vulnerable_proyectos_avances');
        $crud->set_subject('AVANCES DE LOS PROYECTOS PARA LA POBLACIÓN VULNERABLE ');
        $crud->display_as('control_poblacion_vulnerable_proyectos_avances_archivo', 'Archivo');
        $crud->display_as('control_poblacion_vulnerable_proyectos_avances_periodo', 'Periodo');
        $crud->display_as('control_poblacion_vulnerable_proyectos_avances_trimestre', 'Periodicidad');
        $crud->display_as('id_control_poblacion_vulnerable_proyectos', 'Programa');
        $crud->set_field_upload('control_poblacion_vulnerable_proyectos_avances_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_poblacion_vulnerable_proyectos_avances_periodo', 'control_poblacion_vulnerable_proyectos_avances_trimestre', 'control_poblacion_vulnerable_proyectos_avances_archivo');
        $crud->field_type('control_poblacion_vulnerable_proyectos_avances_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->field_type('control_poblacion_vulnerable_proyectos_avances_trimestre', 'dropdown', periodicidad());
        $crud->set_relation('id_control_poblacion_vulnerable_proyectos', 'control_poblacion_vulnerable_proyectos', 'control_poblacion_vulnerable_proyectos_nombre');
        $ruta = 'Avances de los Proyectos para la Población Vulnerable ';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica los Proyectos dirigidos a población vulnerable (madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados), de acuerdo con su misión.<br>Cuando se trate de programas o proyectos, debe publicar el avance de los mismos, cada 3 meses.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 361, $ruta, $ruta_tab, $texto);
    }

    public function control_programas_sociales() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_programas_sociales');
        $crud->set_subject('LISTADO BENEFICIARIOS PROGRAMA SOCIAL');
        $crud->display_as('control_programas_sociales_archivo', 'Archivo');
        $crud->display_as('control_programas_sociales_programa', 'Programa');
        $crud->display_as('control_programas_sociales_descripcion', 'Descripción');
        $crud->display_as('control_programas_sociales_fecha', 'Fecha de Actualización');
        $crud->display_as('id_control_poblacion_vulnerable_proyectos', 'Programa');
        $crud->set_field_upload('control_programas_sociales_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_programas_sociales_archivo', 'control_programas_sociales_programa', 'control_programas_sociales_descripcion');
        $crud->field_type('control_programas_sociales_fecha', 'hidden', date('Y-m-d'));
        $crud->unset_texteditor('control_programas_sociales_descripcion');
        $ruta = 'Programas Sociales';
        $ruta_tab = 'Control';
        $texto = 'La entidad publica el listado de beneficiarios de los programas sociales, incluyendo, como mínimo: madre cabeza de familia, desplazado, personas en condición de discapacidad, familias en condición de pobreza, niños, adulto mayor, etnias, desplazados, reinsertados, de acuerdo con su misión.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 362, $ruta, $ruta_tab, $texto);
    }

    public function control_defensa_judicial() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_defensa_judicial');
        $crud->set_subject('INFORME DE DEFENSA JUDICIAL');
        $crud->display_as('control_defensa_judicial_archivo', 'Archivo');
        $crud->display_as('control_defensa_judicial_trimestre', 'Periodicidad');
        $crud->display_as('control_defensa_judicial_periodo', 'Periodo');
        $crud->set_field_upload('control_defensa_judicial_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_defensa_judicial_archivo', 'control_defensa_judicial_trimestre', 'control_defensa_judicial_periodo');
        $crud->field_type('control_defensa_judicial_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->field_type('control_defensa_judicial_trimestre', 'dropdown', periodicidad());
        $ruta = 'Defensa Judicial';
        $ruta_tab = 'Control';
        $texto = 'Las entidades deben publicar, trimestralmente un informe sobre las demandas contra la entidad, incluyendo:<ul><li>Número de demandas.</li><li>Estado en que se encuentra. </li><li>Pretensión o cuantía de la demanda. </li><li>Riesgo de pérdida. </li></ul>';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 363, $ruta, $ruta_tab, $texto);
    }

    public function control_otros_informes() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_otros_informes');
        $crud->set_subject('OTROS INFORMES');
        $crud->display_as('control_otros_informes_nombre', 'Nombre');
        $crud->display_as('control_otros_informes_archivo', 'Archivo');
        $crud->display_as('control_otros_informes_periodo', 'Periodo');
        $crud->display_as('control_otros_informes_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_otros_informes_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_otros_informes_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_otros_informes_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('control_otros_informes_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->set_field_upload('control_otros_informes_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_otros_informes_nombre', 'control_otros_informes_archivo', 'control_otros_informes_periodo');
        $ruta = 'Otros Informes';
        $crud->edit_fields('control_otros_informes_nombre', 'control_otros_informes_archivo', 'control_otros_informes_periodo', 'control_otros_informes_fecha_actualizacion');
        $ruta_tab = 'Control  ';
        $texto = 'Otros Informes.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 365, $ruta, $ruta_tab, $texto);
    }

    public function control_anuario_estadistico() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_anuario_estadistico');
        $crud->set_subject('ANUARIO ESTADÍSTICO');
        $crud->display_as('control_anuario_estadistico_nombre', 'Nombre');
        $crud->display_as('control_anuario_estadistico_archivo', 'Archivo');
        $crud->display_as('control_anuario_estadistico_periodo', 'Periodo');
        $crud->display_as('control_anuario_estadistico_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_anuario_estadistico_fecha_actualizacion', 'Fecha de Actualización ');
        $crud->field_type('control_anuario_estadistico_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_anuario_estadistico_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('control_anuario_estadistico_archivo', 'uploads/entidad/planeacion');
        $crud->required_fields('control_anuario_estadistico_nombre', 'control_anuario_estadistico_archivo', 'control_anuario_estadistico_periodo');
        $crud->field_type('control_anuario_estadistico_periodo', 'dropdown', periodos());
        $crud->edit_fields('control_anuario_estadistico_nombre', 'control_anuario_estadistico_archivo', 'control_anuario_estadistico_periodo', 'control_anuario_estadistico_fecha_actualizacion');
        $ruta = 'Anuario Estadístico ';
        $ruta_tab = 'Planeación';
        $texto = 'Anuario Estadístico';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 373, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
