<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_aseguramiento extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->model('App_model');
        $this->load->helper('url');
    }

    function importar_aseguramiento() {
        set_time_limit(0);
        ini_set('memory_limit', '2G');
        $this->load->model('App_model');
        $this->load->library('csvimport');
        $dato = $this->App_model->get_aseguramiento();
        if ($dato->aseguramiento_estado == 3) {
            $texto = "Iniciando el proceso de actualización de la base de datos de aseguramiento .... <br>";
            $texto .= "<br> Limpiando las tablas .... <br>";
            $this->App_model->limipiar_tablas();
            $texto .= "<br> Tablas limipadas correctamente .... <br>";
            $texto .= "<br> Cargando achivos CSV .... <br>";
            /* ======================ARCHIVO ASEGURAMIENTO PACIENTES=========================================== */
            $texto .= "<br> Cargando achivos  $dato->aseguramiento_aseguramiento.... <br>";
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_aseguramiento";
            $texto .= "<br>Leyendo el archivo:$dbName ....<br>";
            if (!file_exists($dbName)) {
                die("<br>NO se puede leer el archivo....<br>");
                $texto .= "<br>$dbName No encontrada<br>";
            } else {
                $texto .= "<br>Archivo cargado correctamente ....<br>";
                $file_data = $this->csvimport->get_array($dbName);
                $texto .= "<br>Importando archivo a la base de datos ....<br>";
                $x = 0;
                foreach ($file_data as $row) {
                    $data[] = array(
                        'aseguramiento_pacientes_identificacion' => $row["IDENTIFICACION"],
                        'aseguramiento_pacientes_tipo_identificacion' => $row["TPDOC"],
                        'aseguramiento_pacientes_nombres' => $row["NOMBRE1"] . " " . $row["NOMBRE2"],
                        'aseguramiento_pacientes_apellidos' => $row["APELLIDO1"] . " " . $row["APELLIDO2"],
                        'aseguramiento_pacientes_fecha_nacimiento' => $row["FECHANACIMIENTO"],
                        'aseguramiento_pacientes_sexo' => $row["SEXO"],
                        'aseguramiento_pacientes_codigosentidad' => $row["CODIGOSENTIDAD"],
                        'aseguramiento_pacientes_codactualiza' => $row["CODACTUALIZA"]
                    );
                    $x++;
                    if ($x == 8000) {
                        $x = 0;
                        $tabla = 'aseguramiento_pacientes';
                        $texto .= "<br>Guardando registros ....<br>";
                        $this->App_model->guardar_importacion($data, $tabla);
                        $data = NULL;
                        // hora actual
                        // echo date('h:i:s') . "\n";
                        sleep(2);
                        // echo date('h:i:s') . "\n";
                    }
                }
                $tabla = 'aseguramiento_pacientes';
                $texto .= "<br>Guardando registros ....<br>";
                $this->App_model->guardar_importacion($data, $tabla);
                $texto .= "<br>$tabla Actualizada....<br>";
            }
            /* =========================================ENTIDADES=========================================== */
            $texto .= "<br> Cargando achivos  $dato->aseguramiento_entidades.... <br>";
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_entidades";
            $texto .= "<br>Leyendo el archivo:$dbName ....<br>";
            if (!file_exists($dbName)) {
                die("<br>NO se puede leer el archivo....<br>");
            } else {
                $texto .= "<br>Archivo cargado correctamente ....<br>";
                $file_data = $this->csvimport->get_array($dbName);
                $texto .= "<br>Importando archivo a la base de datos ....<br>";
                foreach ($file_data as $row) {
                    $data2[] = array(
                        'entidades_codentidad' => $row["CODENTIDAD"],
                        'entidades_nomentidad' => $row["NOMENTIDAD"]
                    );
                }
                $tabla = 'entidades';
                $texto .= "<br>Guardando registros ....<br>";
                $this->App_model->guardar_importacion($data2, $tabla);
                $texto .= "<br>$tabla Actualizada....<br>";
            }
            /* ======================ARCHIVO ASEGURAMIENTO PACIENTES=========================================== */
            $texto .= "<br> Cargando achivos  $dato->aseguramiento_fec_actualiza.... <br>";
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_fec_actualiza";
            $texto .= "<br>Leyendo el archivo:$dbName ....<br>";
            if (!file_exists($dbName)) {
                die("<br>NO se puede leer el archivo....<br>");
            } else {
                $texto .= "<br>Archivo cargado correctamente ....<br>";
                $file_data = $this->csvimport->get_array($dbName);
                $texto .= "<br>Importando archivo a la base de datos ....<br>";
                foreach ($file_data as $row) {
                    $data3[] = array(
                        'fec_actualiza_codactualizacion' => $row["CODACTUALIZACION"],
                        'fec_actualiza_fechaultimaactualiza' => $row["FECHAULTIMAACTUALIZA"]
                    );
                }
                $tabla = 'fec_actualiza';
                $texto .= "<br>Guardando registros ....<br>";
                $this->App_model->guardar_importacion($data3, $tabla);
                $texto .= "<br>$tabla Actualizada....<br>";
            }
            /* ACTUALIZO EL LOG Y EL ESTADO DE ASEGURAMIENTO */
            echo $texto;
            $update = array(
                'aseguramiento_estado' => 1,
                'aseguramiento_log' => $texto
            );
            $this->App_model->update_aseguramiento($update);
        }
    }

    public function sincronizar() {
        $datos = array(
            'aseguramiento_estado' => 3,
        );
        $this->App_model->update_aseguramiento($datos);
        echo json_encode(true);
    }

}
