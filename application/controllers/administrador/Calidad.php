<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calidad extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('image_moo');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function entidad_calidad() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('entidad_calidad');
        $crud->set_subject('CALIDAD');
        $crud->display_as('entidad_calidad_descripcion', 'Descripción');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Calidad';
        $ruta_tab = 'Calidad';
        $texto = 'Sistema Integrado de Gestión SIGI .';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 501, $ruta, $ruta_tab, $texto);
    }

    public function entidad_caracterizacion_procesos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('entidad_caracterizacion_procesos');
        $crud->set_subject('CARACTERIZACION DE PROCESOS');
        $crud->display_as('entidad_caracterizacion_procesos_nombre', 'Nombre');
        $crud->display_as('entidad_caracterizacion_procesos_archivo', 'Archivo');
        $crud->display_as('entidad_caracterizacion_procesos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_caracterizacion_procesos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('entidad_caracterizacion_procesos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_caracterizacion_procesos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->edit_fields('entidad_caracterizacion_procesos_nombre', 'entidad_caracterizacion_procesos_archivo', 'entidad_caracterizacion_procesos_fecha_actualizacion');
          $crud->required_fields('entidad_caracterizacion_procesos_nombre', 'entidad_caracterizacion_procesos_archivo', 'entidad_caracterizacion_procesos_fecha_actualizacion');
        $crud->set_field_upload('entidad_caracterizacion_procesos_archivo', 'uploads/entidad/calidad');
        $ruta = 'Calidad';
        $ruta_tab = 'Calidad';
        $texto = 'Caracterización de procesos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 502, $ruta, $ruta_tab, $texto);
    }

    public function entidad_procedimientos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('entidad_procedimientos');
        $crud->set_subject('PROCEDIMIENTOS');
        $crud->display_as('entidad_procedimientos_nombre', 'Nombre');
        $crud->display_as('entidad_procedimientos_archivo', 'Archivo');
        $crud->display_as('entidad_procedimientos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_procedimientos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('entidad_procedimientos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_procedimientos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->edit_fields('entidad_procedimientos_nombre', 'entidad_procedimientos_archivo', 'entidad_procedimientos_fecha_actualizacion');
        $crud->required_fields('entidad_procedimientos_nombre', 'entidad_procedimientos_archivo', 'entidad_procedimientos_fecha_actualizacion');
        $crud->set_field_upload('entidad_procedimientos_archivo', 'uploads/entidad/calidad');
        $ruta = 'Calidad';
        $ruta_tab = 'Calidad';
        $texto = 'Procedimientos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 503, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
