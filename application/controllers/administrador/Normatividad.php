<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Normatividad extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function normatividad_leyes() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('normatividad_leyes');
        $crud->set_subject('LEYES/ORDENANZAS/ACUERDOS');
        $crud->display_as('normatividad_leyes_nombre', 'Nombre');
        $crud->display_as('normatividad_leyes_tipo', 'Tipo de Norma');
        $crud->display_as('normatividad_leyes_archivo', 'Archivo');
        $crud->display_as('normatividad_leyes_tematica', 'Temática');
        $crud->display_as('normatividad_leyes_descripcion', 'Descripción Corta');
        $crud->display_as('normatividad_leyes_fecha_ingreso', 'Fecha de Ingreso');
        $crud->field_type('normatividad_leyes_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->display_as('normatividad_leyes_fecha_expedicion', 'Fecha de Expedición');
        $crud->set_field_upload('normatividad_leyes_archivo', 'uploads/entidad/normatividad');
        $crud->required_fields('normatividad_leyes_descripcion', 'normatividad_leyes_nombre', 'normatividad_leyes_archivo', 'normatividad_leyes_tematica', 'normatividad_leyes_fecha_expedicion', 'normatividad_leyes_tipo');
        $crud->unset_texteditor('normatividad_leyes_tematica', 'normatividad_leyes_descripcion');
        $crud->field_type('normatividad_leyes_tipo', 'dropdown', array('Ley' => 'Ley', 'Ordenanza' => 'Ordenanza', 'Acuerdo' => 'Acuerdo'));
        $crud->edit_fields('normatividad_leyes_nombre', 'normatividad_leyes_fecha_expedicion', 'normatividad_leyes_tipo', 'normatividad_leyes_archivo', 'normatividad_leyes_tematica', 'normatividad_leyes_descripcion');
        $ruta = 'Ley/Ordenanza/Acuerdo';
        $ruta_tab = 'Normatividad';
        $texto = 'Se deben publicar la normatividad que rige a la entidad, la que determina su competencia y la que es aplicable a su actividad o producida por la misma. Esta información debe ser descargable y estar organizada por temática, tipo de norma y fecha de expedición de la más reciente hacia atrás.<br>Se deben publicar dentro de los siguientes 5 días de su expedición de acuerdo con los principios de oportunidad y publicidad.';
       // $crud->set_rules('normatividad_leyes_fecha_expedicion', 'Fecha de Finalización', 'callback_check_date_today|requiered');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 201, $ruta, $ruta_tab, $texto);
    }

    public function check_date_today($str) {


        $partes = explode('/', $this->input->post('normatividad_leyes_fecha_expedicion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($this->input->post('normatividad_leyes_fecha_expedicion') == NULL) {
            $this->form_validation->set_message('check_date_today', "El Campo fecha de expedición no puede estar vacío. ");
            return FALSE;
        }

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today', "La fecha de expedición  no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }

    public function normatividad_decretos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('normatividad_decretos');
        $crud->set_subject('DECRETOS');
        $crud->display_as('normatividad_decretos_nombre', 'Nombre');
        $crud->display_as('normatividad_decretos_archivo', 'Archivo');
        $crud->display_as('normatividad_decretos_tematica', 'Temática');
        $crud->display_as('normatividad_decretos_descripcion', 'Descripción Corta');
        $crud->display_as('normatividad_decretos_fecha_expedicion', 'Fecha de Expedición');
        $crud->display_as('normatividad_decretos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->field_type('normatividad_decretos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('normatividad_decretos_archivo', 'uploads/entidad/normatividad');
        $crud->required_fields('normatividad_decretos_nombre', 'normatividad_decretos_archivo', 'normatividad_decretos_tematica', 'normatividad_decretos_descripcion', 'normatividad_decretos_fecha_expedicion');
        $crud->unset_texteditor('normatividad_decretos_tematica', 'normatividad_decretos_descripcion');
        $crud->edit_fields('normatividad_decretos_nombre', 'normatividad_decretos_fecha_expedicion', 'normatividad_decretos_archivo', 'normatividad_decretos_tematica', 'normatividad_decretos_descripcion');
        $ruta = 'Decretos';
        $ruta_tab = 'Normatividad';
        $texto = 'Se deben publicar la normatividad que rige a la entidad, la que determina su competencia y la que es aplicable a su actividad o producida por la misma. Esta información debe ser descargable y estar organizada por temática, tipo de norma y fecha de expedición de la más reciente hacia atrás.<br>Se deben publicar dentro de los siguientes 5 días de su expedición de acuerdo con los principios de oportunidad y publicidad.';
        //  $crud->set_rules('normatividad_decretos_fecha_expedicion', 'Fecha de Finalización', 'callback_check_date_today_decretos');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 202, $ruta, $ruta_tab, $texto);
    }

    public function check_date_today_decretos($str) {


        $partes = explode('/', $this->input->post('normatividad_decretos_fecha_expedicion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($this->input->post('normatividad_decretos_fecha_expedicion') == NULL) {
            $this->form_validation->set_message('check_date_today_decretos', "El Campo fecha de expedición no puede estar vacío. ");
            return FALSE;
        }


        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_decretos', "La fecha de expedición  no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }

    public function normatividad_resoluciones() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('normatividad_resoluciones');
        $crud->set_subject('RESOLUCIÓN / CIRCULAR / OTRO');
        $crud->display_as('normatividad_resoluciones_nombre', 'Nombre');
        $crud->display_as('normatividad_resoluciones_tipo', 'Tipo de Norma');
        $crud->display_as('normatividad_resoluciones_archivo', 'Archivo');
        $crud->display_as('normatividad_resoluciones_tematica', 'Temática');
        $crud->display_as('normatividad_resoluciones_descripcion', 'Descripción Corta');
        $crud->display_as('normatividad_resoluciones_fecha_expedicion', 'Fecha de Expedición');
        $crud->display_as('normatividad_resoluciones_fecha_ingreso', 'Fecha de Ingreso');
        $crud->field_type('normatividad_resoluciones_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('normatividad_resoluciones_archivo', 'uploads/entidad/normatividad');
        $crud->required_fields('normatividad_resoluciones_nombre', 'normatividad_resoluciones_tipo', 'normatividad_resoluciones_archivo', 'normatividad_resoluciones_tematica', 'normatividad_resoluciones_descripcion', 'normatividad_resoluciones_fecha_expedicion');
        $crud->unset_texteditor('normatividad_resoluciones_tematica', 'normatividad_resoluciones_descripcion');
        $crud->field_type('normatividad_resoluciones_tipo', 'dropdown', array('Resolución' => 'Resolución', 'Circular' => 'Circular', 'Otro' => 'Otro'));
        $crud->edit_fields('normatividad_resoluciones_nombre', 'normatividad_resoluciones_fecha_expedicion', 'normatividad_resoluciones_tipo', 'normatividad_resoluciones_archivo', 'normatividad_resoluciones_tematica', 'normatividad_resoluciones_descripcion');
        $ruta = 'Resolución/Circular/Otro';
        $ruta_tab = 'Normatividad';
        $texto = 'Se deben publicar la normatividad que rige a la entidad, la que determina su competencia y la que es aplicable a su actividad o producida por la misma. Esta información debe ser descargable y estar organizada por temática, tipo de norma y fecha de expedición de la más reciente hacia atrás.<br>Se deben publicar dentro de los siguientes 5 días de su expedición de acuerdo con los principios de oportunidad y publicidad.';
        $crud->set_rules('normatividad_resoluciones_fecha_expedicion', 'Fecha de Finalización', 'callback_check_date_today_resoluciones');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 203, $ruta, $ruta_tab, $texto);
    }

    public function check_date_today_resoluciones($str) {


        $partes = explode('/', $this->input->post('normatividad_resoluciones_fecha_expedicion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($this->input->post('normatividad_resoluciones_fecha_expedicion') == NULL) {
            $this->form_validation->set_message('check_date_today_resoluciones', "El Campo fecha de expedición no puede estar vacío. ");
            return FALSE;
        }


        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_resoluciones', "La fecha de expedición  no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }



    public function check_date_today_politicas($str) {


        $partes = explode('/', $this->input->post('normatividad_politicas_fecha_expedicion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);


        if ($this->input->post('normatividad_politicas_fecha_expedicion') == NULL) {
            $this->form_validation->set_message('check_date_today_politicas', "El Campo fecha de expedición no puede estar vacío. ");
            return FALSE;
        }


        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_politicas', "La fecha de expedición  no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }

    public function normatividad_edictos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('normatividad_edictos');
        $crud->set_subject('EDICTOS');
        $crud->display_as('normatividad_edictos_nombre', 'Nombre');
        $crud->display_as('normatividad_edictos_archivo', 'Archivo');
        $crud->display_as('normatividad_edictos_tematica', 'Temática');
        $crud->display_as('normatividad_edictos_descripcion', 'Descripción Corta');
        $crud->display_as('normatividad_edictos_fecha_expedicion', 'Fecha de Expedición');
        $crud->display_as('normatividad_edictos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->field_type('normatividad_edictos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('normatividad_edictos_archivo', 'uploads/entidad/normatividad');
        $crud->required_fields('normatividad_edictos_descripcion', 'normatividad_edictos_nombre', 'normatividad_edictos_archivo', 'normatividad_edictos_tematica', 'normatividad_edictos_fecha_expedicion');
        $crud->unset_texteditor('normatividad_edictos_tematica', 'normatividad_edictos_descripcion');
        $crud->field_type('normatividad_edictos_tipo', 'dropdown', array('Políticas' => 'Políticas', 'Lineamientos' => 'Lineamientos', 'Manuales' => 'Manuales'));
        $crud->edit_fields('normatividad_edictos_nombre', 'normatividad_edictos_fecha_expedicion', 'normatividad_edictos_archivo', 'normatividad_edictos_tematica', 'normatividad_edictos_descripcion');
        $ruta = 'Edictos';
        $ruta_tab = 'Normatividad';
        $texto = 'Se deben publicar los Edictos que produzca la entidad. Esta información debe ser descargable y estar organizada por temática, tipo de norma y fecha de expedición de la más reciente hacia atrás. El listado deberá contener una breve descripción de los documentos.';
        $crud->set_rules('normatividad_edictos_fecha_expedicion', 'Fecha de Finalización', 'callback_check_date_today_edictos');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 205, $ruta, $ruta_tab, $texto);
    }

    public function check_date_today_edictos($str) {


        $partes = explode('/', $this->input->post('normatividad_edictos_fecha_expedicion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);


        if ($this->input->post('normatividad_edictos_fecha_expedicion') == NULL) {
            $this->form_validation->set_message('check_date_today_edictos', "El Campo fecha de expedición no puede estar vacío. ");
            return FALSE;
        }


        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_edictos', "La fecha de expedición  no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
