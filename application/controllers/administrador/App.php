<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->library('image_moo');
        $this->load->model('Referenciales_nodo_model');
        $this->load->model('App_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'aplicaciones';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function aseguramiento() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('aseguramiento');
        $crud->set_subject('ASEGURAMIENTO');
        $crud->display_as('aseguramiento_aseguramiento', 'CSV Tabla "ASEGURAMIENTO"');
        $crud->display_as('aseguramiento_entidades', 'CSV Tabla "ENTIDADES"');
        $crud->display_as('aseguramiento_fec_actualiza', 'CSV Tabla "FEC_ACTUALIZA"');
        $crud->display_as('aseguramiento_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('aseguramiento_fecha_actualizacion', 'hidden', date('Y-m-d H:i:s'));
        $crud->set_field_upload('aseguramiento_aseguramiento', 'uploads/app/aseguramiento');
        $crud->set_field_upload('aseguramiento_entidades', 'uploads/app/aseguramiento');
        $crud->set_field_upload('aseguramiento_fec_actualiza', 'uploads/app/aseguramiento');
        $crud->required_fields("aseguramiento_aseguramiento", 'aseguramiento_entidades', "aseguramiento_fec_actualiza");
        $ruta_tab = 'App ';
        $ruta = 'Aseguramiento';
        $texto = 'Aseguramiento ';
        $crud->unset_add();
        $crud->unset_delete();
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1101, $ruta, $ruta_tab, $texto);
    }

    public function _callback_envio($value, $row) {
        $dato = $this->App_model->get_aseguramiento();
        if ($dato->aseguramiento_estado != 1) {
            if ($dato->aseguramiento_estado == 3) {
                return '<button class="btn btn-block btn-sm btn-default" disabled><i class="fas fa-check-square"></i> Sincronizando</button>';
            } else {
                return "<a onclick='return false;' class='btn btn-primary btn-sm btn-block aseguramiento-sincronizar' href='#'><i class='fas fa-share-square'></i> Sincronizar</a>";
            }
        } else {
            return '<button class="btn btn-block btn-sm btn-default" disabled><i class="fas fa-check-square"></i> Sincronizado</button>';
        }
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

    public function pacientes() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('paciente');
        $crud->set_subject('PACIENTE');
        $crud->display_as('paciente_nombres', 'Nombres');
        $crud->display_as('paciente_apellidos', 'Apellidos');
        $crud->display_as('paciente_identificacion', 'Número de Identificación');
        $crud->display_as('paciente_direccion', 'Dirección');
        $crud->display_as('paciente_telefono', 'Teléfono de Contacto');
        $crud->display_as('paciente_celular', 'Número de Celular');
        $crud->display_as('paciente_email', 'Correo Electrónico');
        $crud->display_as('paciente_sexo', 'Sexo');
        $crud->display_as('paciente_fecha_nacimiento', 'Fecha de Nacimiento');
        $crud->display_as('paciente_password', 'Contraseña');
        $crud->display_as('id_ciudad', 'Ciudad');
        $crud->display_as('id_tipo_identificacion', 'Tipo de Identificación');
        $crud->display_as('id_estado', 'Estado');
        $crud->unset_texteditor('paciente_direccion');
        $crud->set_relation('id_estado', 'estado', 'estado_nombre');
        $crud->set_relation('id_ciudad', 'ciudad', '{ciudad_nombre} / {ciudad_departamento_nombre} / {ciudad_pais_nombre}');
        $crud->set_relation('id_tipo_identificacion', 'tipo_identificacion', 'tipo_identificacion_nombre');
        $crud->field_type('paciente_password', 'password');
        $state = $crud->getState();
        if ($state == 'add' || $state == 'clone' || $state == "insert_validation") {
            $crud->set_rules('paciente_identificacion', 'Número de Identificación', 'callback_check_unique|required|numeric');
        } else {
            $crud->set_rules('administrador_identificacion', 'Número de identificación', 'numeric|required');
        }
        $crud->set_rules('paciente_email', 'Correo Electrónico', 'valid_email|required');
        $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
        $crud->callback_before_update(array($this, 'encrypt_password_callback'));
        $crud->callback_edit_field('paciente_password', array($this, 'decrypt_password_callback'));
        $crud->columns('paciente_nombres', 'paciente_apellidos', 'id_tipo_identificacion', 'paciente_identificacion', 'paciente_fecha_nacimiento', 'paciente_sexo', 'id_ciudad', 'paciente_email', 'id_estado');
        $crud->required_fields("paciente_nombres", 'paciente_apellidos', "paciente_identificacion", "paciente_email", "id_tipo_identificacion", "id_estado");
        $ruta_tab = 'App ';
        $ruta = 'Pacientes';
        $texto = 'Pacientes ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1102, $ruta, $ruta_tab, $texto);
    }

    function check_unique($str) {
        if ($this->App_model->get_paciente($this->input->post('paciente_identificacion')) != FALSE) {
            $this->form_validation->set_message('check_unique', "El número de identificación ya se encuentra registrado.");
            return FALSE;
        } else {

            return TRUE;
        }
    }

    function encrypt_password_callback($post_array, $primary_key = null) {
        $this->load->library('encryption');
        $post_array['password'] = $this->encryption->encrypt($post_array['paciente_password']);
        return $post_array;
    }

    function decrypt_password_callback($value) {
        $this->load->library('encryption');
        $decrypted_password = $this->encryption->decrypt($value);
        return "<input type='password' name='password' value='$decrypted_password' />";
    }

    public function agendamiento() {

        $this->breadcrumbs->unshift_clear('Agendamiento', '#');
        $this->breadcrumbs->unshift_clear('App', '#');
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/agendamiento';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['menu'] = 'aplicaciones';
        $data['tab'] = 1103;
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $data['agendamiento'] = $this->App_model->get_setup();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function nuevo_agendamiento($id = NULL) {
        $this->breadcrumbs->unshift_clear('Nuevo Agendamiento', '#');
        $this->breadcrumbs->unshift_clear('Agendamiento', site_url("administrador/app/agendamiento"));
        $this->breadcrumbs->unshift_clear('App', '#');
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/nuevo_agendamiento';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['menu'] = 'aplicaciones';
        $data['tab'] = 1103;
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $data['servicios'] = $this->App_model->get_servicios();
        $data['sedes'] = $this->App_model->get_sedes();
        $data['agendamiento'] = $id;
        if ($id != NULL) {
            $data['agendamiento'] = $this->App_model->get_un_setup($id);
            $data['citas'] = $this->App_model->get_citas_setup($id);
        }
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    function eliminar_agendamiento($id) {
        $this->App_model->delete_setup($id);
        echo json_encode(TRUE);
    }

    function guardar_agendamiento() {
        $result = array();
        $datos = array(
            'setup_servicios_x_sedes_fecha_inicio' => $_POST['fecha_inicio'],
            'setup_servicios_x_sedes_fecha_final' => $_POST['fecha_fin'],
            'setup_servicios_x_sedes_cantidad' => $_POST['cantidad'],
            'setup_servicios_x_sedes_tiempo' => $_POST['tiempo'],
            'setup_servicios_x_sedes_lunes' => $_POST['r1'],
            'setup_servicios_x_sedes_martes' => $_POST['r2'],
            'setup_servicios_x_sedes_miercoles' => $_POST['r3'],
            'setup_servicios_x_sedes_jueves' => $_POST['r4'],
            'setup_servicios_x_sedes_viernes' => $_POST['r5'],
            'setup_servicios_x_sedes_sabados' => $_POST['r6'],
            'setup_servicios_x_sedes_domingos' => $_POST['r7'],
            'setup_servicios_x_sedes_lunes_hora_inicio' => $_POST['h1'],
            'setup_servicios_x_sedes_lunes_hora_fin' => $_POST['h11'],
            'setup_servicios_x_sedes_martes_hora_inicio' => $_POST['h2'],
            'setup_servicios_x_sedes_martes_hora_fin' => $_POST['h22'],
            'setup_servicios_x_sedes_miercoles_hora_inicio' => $_POST['h3'],
            'setup_servicios_x_sedes_miercoles_hora_fin' => $_POST['h33'],
            'setup_servicios_x_sedes_jueves_hora_inicio' => $_POST['h4'],
            'setup_servicios_x_sedes_jueves_hora_fin' => $_POST['h44'],
            'setup_servicios_x_sedes_viernes_hora_inicio' => $_POST['h5'],
            'setup_servicios_x_sedes_viernes_hora_fin' => $_POST['h55'],
            'setup_servicios_x_sedes_sabados_hora_inicio' => $_POST['h6'],
            'setup_servicios_x_sedes_sabados_hora_fin' => $_POST['h66'],
            'setup_servicios_x_sedes_domingos_hora_inicio' => $_POST['h7'],
            'setup_servicios_x_sedes_domingos_hora_fin' => $_POST['h77'],
            'setup_servicios_x_sedes_festivos' => $_POST['festivos'],
            'setup_servicios_x_sedes_almuerzo' => $_POST['almuerzo'],
            'id_servicios' => $_POST['servicio'],
            'id_sedes' => $_POST['sede']
        );
        $flag = FALSE;
        /* Valido la fecha inicial contr al afecha final */
        if ($_POST['fecha_inicio'] > $_POST['fecha_fin']) {
            $result = array('estado' => 2, 'texto' => 'La fecha de inicio no puede ser mayor a la fecha de finalización del agendamiento');
            $flag = TRUE;
        }
        /* Valido que las horas no esten solapdas */
        if ($_POST['r1'] == 'Si') {
            if ($_POST['h1'] >= $_POST['h11']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día lunes no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r2'] == 'Si') {
            if ($_POST['h2'] >= $_POST['h22']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día martes no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r3'] == 'Si') {
            if ($_POST['h3'] >= $_POST['h33']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día miercoles no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r4'] == 'Si') {
            if ($_POST['h4'] >= $_POST['h44']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día jueves no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r5'] == 'Si') {
            if ($_POST['h5'] >= $_POST['h55']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día viernes no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r6'] == 'Si') {
            if ($_POST['h6'] >= $_POST['h66']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día sabado no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        if ($_POST['r7'] == 'Si') {
            if ($_POST['h7'] >= $_POST['h77']) {
                $result = array('estado' => 2, 'texto' => 'La hora de inicio del día domingo no puede ser mayor o igual a la hora de finalización ');
                $flag = TRUE;
            }
        }
        /* Verifico que no haya una agenda que se solape */
        if ($_POST['id'] == NULL) {
            $query = $this->App_model->check_setup($_POST['servicio'], $_POST['sede'], $_POST['fecha_inicio'], $_POST['fecha_fin'], NULL);
        } else {
            $query = $this->App_model->check_setup($_POST['servicio'], $_POST['sede'], $_POST['fecha_inicio'], $_POST['fecha_fin'], $_POST['id']);
        }
        if ($query == TRUE) {
            $result = array('estado' => 2, 'texto' => 'Las fechas de inicio o finalización se solapan con otro agendamiento creado previamente, por favor verifique las fechas');
            $flag = TRUE;
        }

        if ($flag == FALSE) {
            /* Guardo el registro */
            if ($_POST['id'] == NULL) {
                $this->App_model->guardar_setup($datos);
                $result = array('estado' => 1, 'texto' => 'OK');
            } else {
                $this->App_model->update_setup($_POST['id'], $datos);
                $result = array('estado' => 1, 'texto' => 'OK');
            }
        }

        echo json_encode($result);
    }
    
    
       public function historico_citas() {

        $this->breadcrumbs->unshift_clear('Histórico Citas', '#');
        $this->breadcrumbs->unshift_clear('App', '#');
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/historico_citas';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['menu'] = 'aplicaciones';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $data['citas'] = $this->App_model->get_historico_citas();
        $data['tab'] = 1107;
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }
    

    public function citas() {

        $this->breadcrumbs->unshift_clear('Citas', '#');
        $this->breadcrumbs->unshift_clear('App', '#');
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/gestor_citas';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['menu'] = 'aplicaciones';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $data['citas'] = $this->App_model->get_citas();
        $data['tab'] = 1104;
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    function eliminar_cita() {
        $cita = $this->App_model->get_un_cita($_POST['id']);
        $this->App_model->delete_cita($_POST['id']);
        $setup = $this->App_model->get_un_setup($cita->id_setup_servicios_x_sedes);
        $query = $this->App_model->get_paciente_id($cita->id_paciente);
        $razon = $_POST['justificacion'];
        $email = array(
            'asunto' => 'Cancelación de agendamiento ',
            'texto' => "<p>Este correo es para indicarle que se eliminó una cita que previamente agendo con la siguiente información: </p>"
            . "<table cellspacing='2' cellpadding='2' border='1' style='text-align:left'>"
            . "<tr>"
            . "<th>Sede</th>"
            . "<td>$setup->sede</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Servicio</th>"
            . "<td>$setup->servicio</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Fecha</th>"
            . "<td>$cita->citas_dia</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Hora</th>"
            . "<td>$cita->citas_hora</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Razón</th>"
            . "<td>$razon</td>"
            . "</tr>"
            . "</table>"
            . "<br>"
        );
        $this->email_notificacion($query->paciente_email, $query->paciente_nombres, $query->paciente_apellidos, $email);
        echo json_encode(TRUE);
    }

    function confirmar_cita() {
        $cita = $this->App_model->get_un_cita($_POST['id']);
        $this->App_model->update_cita($_POST['id'], array('citas_confirmada' => 1));
        $setup = $this->App_model->get_un_setup($cita->id_setup_servicios_x_sedes);
        $query = $this->App_model->get_paciente_id($cita->id_paciente);
        $email = array(
            'asunto' => 'Confirmación de cita ',
            'texto' => "<p>Este correo es una confirmación de su agendamiento de citas mediante nuestro sitio web, los datos agendados son los siguientes: </p>"
            . "<table cellspacing='2' cellpadding='2' border='1' style='text-align:left'>"
            . "<tr>"
            . "<th>Sede</th>"
            . "<td>$setup->sede</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Servicio</th>"
            . "<td>$setup->servicio</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Fecha</th>"
            . "<td>$cita->citas_dia</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Hora</th>"
            . "<td>$cita->citas_hora</td>"
            . "</tr>"
            . "</table>"
            . "<br>"
            . "<p>Recuerde asistir con 20 minutos de anticipación a su cita y en caso de no poder asistir realice la cancelación de esta con al menos 2 horas de anticipación.</p>"
        );
        $this->email_notificacion($query->paciente_email, $query->paciente_nombres, $query->paciente_apellidos, $email);
        echo json_encode(TRUE);
    }

    function email_notificacion($to, $name, $surname, $datos) {
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $html = $name . ' ' . $surname . ","
                . "<br>"
                . "<br>";
        $html .= $datos['texto'];
        $html .= "<br><br>"
                . "Para información adicional por favor contáctanos  al teléfono +57 (4) 448 33 11  ";
        $this->email->initialize($config);
        $this->email->from($sender, "E.S.E Hospital del Sur");
        $this->email->to($to);
        $this->email->subject($datos['asunto']);
        $this->email->message($html);
        $this->email->send();
    }

    public function nueva_cita() {

        $this->breadcrumbs->unshift_clear('Citas', '#');
        $this->breadcrumbs->unshift_clear('App', '#');
        $this->breadcrumbs->unshift_clear('Aplicaciones', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/nueva_cita';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['menu'] = 'aplicaciones';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $data['servicios'] = $this->App_model->get_servicios();
        $data['sedes'] = $this->App_model->get_sedes();
        $data['tab'] = 1104;
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    function buscar_citas() {
        $aseguramiento = $this->verificar_aseguramiento($_POST['id']);
        if ($aseguramiento['estado'] == 1) {
            $paciente = $this->App_model->get_paciente($_POST['id']);
            if ($paciente != FALSE) {
                $citas = $this->App_model->get_citas_paciente($paciente->id_paciente, NULL, $_POST['servicio']);
                if ($citas != FALSE) {
                    $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                    $texto = 'El paciente ya cuenta con una cita asignada<br>
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Sede</th><th>Servicio </th><th>Fecha</th><th>Hora</th></tr></thead><tbody>
                    ';
                    foreach ($citas as $cita) {
                        $texto .= '<tr>'
                                . '<td>' . $cita->sede . '</td>'
                                . '<td>' . $cita->servicio . '</td>'
                                . '<td>' . $cita->citas_dia . '</td>'
                                . '<td>' . $cita->citas_hora . '</td>'
                                . '</tr>';
                    }
                    $texto .= ' </tbody>
                </table>
               </div>';
                    $result['mensaje'] = $texto;
                } else {
                    $result['objeto'] = $this->retornar_citas_disponibles($_POST['sede'], $_POST['servicio']);
                    if ($result['objeto'] != FALSE) {
                        $setup = $this->App_model->get_setup_by_sede_servicio($_POST['sede'], $_POST['servicio']);
                        $result['estado'] = 1;
                        $result['mensaje'] = "<div class='alert alert-info'><i class='fa fa-calendar'></i> Agendamiento desde el $setup->setup_servicios_x_sedes_fecha_inicio hasta el  $setup->setup_servicios_x_sedes_fecha_final</div>";
                    } else {
                        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = 'No se encontró agendamiento disponible para este servicio, por favor verifique el agendamiento';
                    }
                }
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = 'El paciente cuenta con aseguramiento, pero actualmente no está creado, por favor primero cree el paciente para poder asignar una cita ';
            }
        } else {
            $result = $aseguramiento;
        }
        echo json_encode($result);
    }

    function calendario() {
        $this->load->helper('string');
        $resultado = array();
        $objeto = $this->retornar_citas_disponibles($_POST['sede'], $_POST['servicio']);
        $colores = array(1 => '#00B69F', 2 => '#FF023E');
        $x = 0;
        foreach ($objeto as $obj) {
            $url = ($obj[3] == 1) ? site_url('app/guardar_cita/' . $obj[4] . '/' . $obj[0] . '/' . $obj[1]) : '';
            $datos = array(
                'id' => "$obj[4]",
                'title' => ($obj[3] == 1) ? 'Disponible' : 'No Disponible',
                'start' => $obj[0] . ' ' . $obj[1],
                'end' => $obj[0] . ' ' . $obj[2],
                'url' => "#",
                'color' => $colores[$obj[3]],
                'hora' => $obj[1],
                'dia' => $obj[0],
                'disponible' => $obj[3]
            );
            array_push($resultado, $datos);
            $x++;
        }
        echo json_encode($resultado);
    }

    function retornar_citas_disponibles($sede, $servicio) {
        /* Primero obntengo el Setup */
        $disponibilidad = array();
        $setup = $this->App_model->get_setup_by_sede_servicio($sede, $servicio);
        if ($setup == FALSE) {
            //   echo 'nada';
            return FALSE;
        } else {
            //  echo 'si encontro';
            /* Parametros */
            $tiempo_citas = $setup->setup_servicios_x_sedes_tiempo;
            $cantidad = $setup->setup_servicios_x_sedes_cantidad;
            /* Traigo las citas del setup */
            $this->App_model->get_citas_setup($setup->id_setup_servicios_x_sedes);
            $array_dias = array('domingos', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabados');
            // Start date
            $date = $setup->setup_servicios_x_sedes_fecha_inicio;
            // End date
            $end_date = $setup->setup_servicios_x_sedes_fecha_final;
            /* recorro los dias */
            while (strtotime($date) <= strtotime($end_date)) {
                $festivo = FALSE;
                if (date("Y-m-d", strtotime($date)) >= date('Y-m-d')) {
                    if ($setup->setup_servicios_x_sedes_festivos == 'Si') {
                        if ($this->check_dia_habil(date("Y-m-d", strtotime($date))) == TRUE) {
                            $festivo = TRUE;
                        }
                    }
                    // echo "$date\n <br> $festivo";
                    if ($festivo == FALSE) {
                        /* rocorreo las horas */
                        $start = 'setup_servicios_x_sedes_' . $array_dias[date("w", strtotime($date))] . '_hora_inicio';
                        $end = 'setup_servicios_x_sedes_' . $array_dias[date("w", strtotime($date))] . '_hora_fin';
                        $hora_inicio = $setup->$start;
                        $hora_fin = $setup->$end;
                        $open_time = strtotime($hora_inicio);
                        $close_time = strtotime($hora_fin);
                        if (date("Y-m-d", strtotime($date)) == date('Y-m-d')) {
                            if (date("H:i", strtotime($hora_inicio)) <= date('H:i')) {
                                $open_time = strtotime(date('H:00')) + 60 * 60;
                            }
                        }
                        $now = time();
                        for ($i = $open_time; $i < $close_time; $i += ($tiempo_citas * 60)) {
                            /* verifico si ya hay citas en este horario,dia */
                            $hay_citas = $this->App_model->check_cita_hora($setup->id_setup_servicios_x_sedes, $date, date("H:i", $i));
                            if ($cantidad > $hay_citas) {
                                //echo "<option>" . date("l - H:i", $i) . "</option>";
                                /* Verificao hora de almuerzo */
                                if ($setup->setup_servicios_x_sedes_almuerzo == 'Si') {
                                    if (date("H:i", $i) >= '12:00' && date("H:i", $i) <= '12:59') {
                                        array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 2, $setup->id_setup_servicios_x_sedes));
                                    } else {
                                        array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 1, $setup->id_setup_servicios_x_sedes));
                                    }
                                } else {
                                    array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 1, $setup->id_setup_servicios_x_sedes));
                                }
                            } else {
                                array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 2, $setup->id_setup_servicios_x_sedes));
                            }
                        }
                    }
                }
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
            //   echo '<pre>';
            //  print_r($disponibilidad);
            //  echo '</pre>';

            return $disponibilidad;
        }
    }

    function check_dia_habil($dia) {
        $this->load->library('festivos');
        $fest = new Festivos();
        return $fest->esFestivo(date("d", strtotime($dia)), date("m", strtotime($dia)), date("Y", strtotime($dia)));
    }

    public function verificar_aseguramiento($numero) {

        $this->load->library('csvimport');
        $result = array();
        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato"</div>';
        $dato = $this->App_model->get_aseguramiento();
        if ($dato != FALSE) {
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_aseguramiento";
            $dbEntidades = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_entidades";
            $dbActualizacion = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_fec_actualiza";
            if (file_exists($dbName)) {
                $texto = '
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Numero de Identificación</th><th>Nombres </th><th>Apellidos</th><th>Fecha de Nacimiento</th><th>Entidad</th><th>Fecha de Actualización</th></tr></thead><tbody>
                    ';
                try {
                    $flag = FALSE;
                    $aseguramiento = $this->csvimport->get_array($dbName);
                    $entidades = $this->csvimport->get_array($dbEntidades);
                    $actualizacion = $this->csvimport->get_array($dbActualizacion);
                    foreach ($aseguramiento as $row) {
                        if ($row["IDENTIFICACION"] == $numero) {
                            $texto .= '<tr>'
                                    . '<td>' . round($row['IDENTIFICACION']) . '</td>';
                            $texto .= '<td>' . $row['NOMBRE1'] . ' ' . $row['NOMBRE2'] . '</td>';
                            $texto .= '<td>' . $row['APELLIDO1'] . ' ' . $row['APELLIDO2'] . '</td>';
                            $texto .= '<td>' . $row['FECHANACIMIENTO'] . '</td>';
                            $nombreentidad = "N/A";
                            foreach ($entidades as $row2) {
                                if ($row2['CODENTIDAD'] == $row['CODIGOSENTIDAD']) {
                                    $nombreentidad = $row2['NOMENTIDAD'];
                                }
                            }
                            $texto .= '<td>' . $nombreentidad . '</td>';
                            $diaactualiza = "N/A";
                            foreach ($actualizacion as $row3) {
                                if ($row3['CODACTUALIZACION'] == $row['CODACTUALIZA']) {
                                    $diaactualiza = $row3['FECHAULTIMAACTUALIZA'];
                                }
                            }
                            $texto .= '<td>' . $diaactualiza . '</td>'
                                    . '</tr>';
                            $flag = TRUE;
                        }
                    }
                    $texto .= ' </tbody>
                </table>
               </div>';
                    if ($flag == TRUE) {
                        $result['estado'] = 1/* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = $texto;
                    } else {
                        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = 'El número de identificación ingresado no cuenta con aseguramiento, por favor verifíquelo ';
                    }
                } catch (Exception $e) {
                    echo 'No encontro registros' . $e;
                }
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = 'Servicio no disponible, intente mas tardes';
            }
        } else {
            $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
            $result['mensaje'] = 'Servicio no disponible, intente mas tarde';
        }

        return $result;
    }

    function guardar_cita() {
        $query = $this->App_model->get_paciente($_POST['paciente']);
        /* verifico que la hora todavia este disponible */
        $check = $this->App_model->check_cita_hora($_POST['id'], $_POST['dia'], $_POST['hora']);
        $setup = $this->App_model->get_un_setup($_POST['id']);
        if ($setup->setup_servicios_x_sedes_cantidad > $check) {
            /* Guardo la cita */
            $datos = array(
                'id_setup_servicios_x_sedes' => $setup->id_setup_servicios_x_sedes,
                'id_paciente' => $query->id_paciente,
                'citas_dia' => $_POST['dia'],
                'citas_hora' => $_POST['hora']
            );
            $dia = $_POST['dia'];
            $hora = $_POST['hora'];
            $this->App_model->guardar_cita($datos);
            $email = array(
                'asunto' => 'Confirmación agendamiento ',
                'texto' => "<p>Este correo es una confirmación de su agendamiento de citas mediante nuestro sitio web, los datos agendados son los siguientes: </p>"
                . "<table cellspacing='2' cellpadding='2' border='1' style='text-align:left'>"
                . "<tr>"
                . "<th>Sede</th>"
                . "<td>$setup->sede</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Servicio</th>"
                . "<td>$setup->servicio</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Fecha</th>"
                . "<td>$dia</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Hora</th>"
                . "<td>$hora</td>"
                . "</tr>"
                . "</table>"
                . "<br>"
                . "<p>Recuerde asistir con 20 minutos de anticipación a su cita y en caso de no poder asistir realice la cancelación de esta con al menos 2 horas de anticipación.</p>"
            );
            $this->email_notificacion($query->paciente_email, $query->paciente_nombres, $query->paciente_apellidos, $email);
            $mensaje = 1;
        } else {
            $mensaje = 2;
        }
        /* ENVIO EL CORREO ELECTRINICO */
        echo json_encode($mensaje);
    }

    public function sedes() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('sedes');
        $crud->set_subject('SEDES');
        $crud->display_as('sedes_nombre', 'Nombre');
        $crud->display_as('id_estado', 'Estado');
        $crud->set_relation('id_estado', 'estado', 'estado_nombre');
        $crud->required_fields("sedes_nombre", "id_estado");
        $ruta_tab = 'App ';
        $ruta = 'Sedes';
        $texto = 'Sedes ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1105, $ruta, $ruta_tab, $texto);
    }

    public function servicios() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('servicios');
        $crud->set_subject('SERVICIOS');
        $crud->display_as('servicios_nombre', 'Nombre');
        $crud->display_as('id_estado', 'Estado');
        $crud->set_relation('id_estado', 'estado', 'estado_nombre');
        $crud->required_fields("servicios_nombre", "id_estado");
        $ruta_tab = 'App ';
        $ruta = 'Servicios';
        $texto = 'Servicios ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1106, $ruta, $ruta_tab, $texto);
    }

}
