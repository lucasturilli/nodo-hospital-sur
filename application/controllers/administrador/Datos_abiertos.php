<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datos_abiertos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function catalogo_datos_abiertos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('catalago_datos_abiertos');
        $crud->set_subject('Catalogo');
        $crud->display_as('catalago_datos_abiertos_nombre', 'Nombre');
        $crud->display_as('catalago_datos_abiertos_url', 'URL');
        $crud->display_as('catalago_datos_abiertos_fecha_ingreso', 'Fecha de Ingreso ');
        $crud->required_fields('catalago_datos_abiertos_nombre', 'catalago_datos_abiertos_url', 'catalago_datos_abiertos_fecha_ingreso');
        $ruta = 'Catalogo de Datos Abiertos ';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'URL de enlace al sitio de catálogo de datos abiertos.';
        $crud->field_type('catalago_datos_abiertos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->edit_fields('catalago_datos_abiertos_nombre', 'catalago_datos_abiertos_url');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 551, $ruta, $ruta_tab, $texto);
    }

    public function archivos_datos_abiertos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('archivos_datos_abiertos');
        $crud->set_subject('Archivo');
        $crud->display_as('archivos_datos_abiertos_nombre', 'Nombre');
        $crud->display_as('archivos_datos_abiertos_archivo', 'Archivo');
        $crud->display_as('archivos_datos_abiertos_fecha_ingreso', 'Fecha de Ingreso ');
        $crud->required_fields('archivos_datos_abiertos_nombre', 'archivos_datos_abiertos_archivo', 'archivos_datos_abiertos_fecha_ingreso');
        $crud->set_field_upload('archivos_datos_abiertos_archivo', 'uploads/entidad/datos_abiertos');
        $ruta = 'Archivos de Datos Abiertos';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'Archivos que se publicaran en la sección de datos abiertos .';
        $crud->field_type('archivos_datos_abiertos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->edit_fields('archivos_datos_abiertos_nombre', 'archivos_datos_abiertos_archivo');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 552, $ruta, $ruta_tab, $texto);
    }
    
      public function mapas_datos_abiertos() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('mapas_datos_abiertos');
        $crud->set_subject('Mapas');
        $crud->display_as('mapas_datos_abiertos_nombre', 'Nombre');
        $crud->display_as('mapas_datos_abiertos_embebido', 'Embebido');
        $crud->display_as('mapas_datos_abiertos_fecha_ingreso', 'Fecha de Ingreso ');
        $crud->required_fields('mapas_datos_abiertos_nombre', 'mapas_datos_abiertos_embebido', 'mapas_datos_abiertos_fecha_ingreso');
        $ruta = 'Mapas de Datos Abiertos';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'Mapas que se publicaran en la sección de datos abiertos .';
        $crud->field_type('mapas_datos_abiertos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->edit_fields('mapas_datos_abiertos_nombre', 'mapas_datos_abiertos_embebido');
        $crud->unset_texteditor('mapas_datos_abiertos_embebido');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 556, $ruta, $ruta_tab, $texto);
    }

    public function registro_activos_informacion() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('registro_activos_informacion');
        $crud->set_subject('REGISTRO DE ACTIVOS DE INFORMACIÓN');
        $crud->display_as('registro_activos_informacion_nombre', 'Nombre');
        $crud->display_as('registro_activos_informacion_periodo', 'Periodo');
        $crud->display_as('registro_activos_informacion_archivo', 'Archivo');
        $crud->display_as('registro_activos_informacion_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('registro_activos_informacion_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('registro_activos_informacion_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('registro_activos_informacion_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('registro_activos_informacion_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->required_fields('registro_activos_informacion_nombre', 'registro_activos_informacion_periodo', 'registro_activos_informacion_archivo');
        $crud->edit_fields('registro_activos_informacion_nombre', 'registro_activos_informacion_periodo', 'registro_activos_informacion_archivo', 'registro_activos_informacion_fecha_actualizacion');
        $crud->set_field_upload('registro_activos_informacion_archivo', 'uploads/entidad/datos_abiertos');
        $ruta = 'Registro de Activos de Información';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'Registro de Activos de Información';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 553, $ruta, $ruta_tab, $texto);
    }

    public function informacion_clasificada_reservada() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('informacion_clasificada_reservada');
        $crud->set_subject('ÍNDICE DE INFORMACIÓN CLASIFICADA Y RESERVADA');
        $crud->display_as('informacion_clasificada_reservada_nombre', 'Nombre');
        $crud->display_as('informacion_clasificada_reservada_periodo', 'Periodo');
        $crud->display_as('informacion_clasificada_reservada_archivo', 'Archivo');
        $crud->display_as('informacion_clasificada_reservada_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('informacion_clasificada_reservada_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('informacion_clasificada_reservada_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('informacion_clasificada_reservada_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('informacion_clasificada_reservada_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->required_fields('informacion_clasificada_reservada_nombre', 'informacion_clasificada_reservada_periodo', 'informacion_clasificada_reservada_archivo');
        $crud->edit_fields('informacion_clasificada_reservada_nombre', 'informacion_clasificada_reservada_periodo', 'informacion_clasificada_reservada_archivo', 'informacion_clasificada_reservada_fecha_actualizacion');
        $crud->set_field_upload('informacion_clasificada_reservada_archivo', 'uploads/entidad/datos_abiertos');
        $ruta = 'Índice de Información Clasificada y Reservada';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'Índice de Información Clasificada y Reservada';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 554, $ruta, $ruta_tab, $texto);
    }

    public function esquema_publicacion() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('esquema_publicacion');
        $crud->set_subject('Archivo');
        $crud->display_as('esquema_publicacion_archivo', 'Archivo');
        $crud->display_as('esquema_publicacion_fecha_ingreso', 'Fecha de Ingreso ');
        $crud->display_as('esquema_publicacion_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('esquema_publicacion_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('esquema_publicacion_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('esquema_publicacion_archivo');
        $crud->edit_fields('esquema_publicacion_archivo', 'esquema_publicacion_fecha_actualizacion');
        $crud->set_field_upload('esquema_publicacion_archivo', 'uploads/entidad/datos_abiertos');
        $ruta = 'Esquema de Publicación ';
        $ruta_tab = 'Datos Abiertos ';
        $texto = 'Esquema de publicación  .';
        $crud->unset_add();
        $crud->unset_delete();
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 555, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
