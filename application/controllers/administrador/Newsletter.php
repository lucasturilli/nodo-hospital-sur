<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class 
 *
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Newsletter extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('encrypt');
    }

    function index($id) {

        //$codigo = $this->encrypt->decode($id);
        $this->Datos_entidad_model->delete_newsletter($id);
        echo '<script>alert("Su cuenta se desvinculo correctamente.");window.close();close();</script>';
    }

}

?>
