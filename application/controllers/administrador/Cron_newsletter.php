<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_newsletter extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->model('Datos_entidad_model');
        $this->load->helper('url');
    }

    public function index() {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        $envios = $this->Datos_entidad_model->get_cron_newsletter();
        if ($envios != FALSE) {
            $datos = array(
                'cron_newsletter_estado' => 1,
                'cron_newsletter_fecha_envio' => date('Y-m-d H:i:s')
            );
            $this->Datos_entidad_model->update_cron_newsletter($datos, $envio->cron_newsletter_alias);
            foreach ($envios as $envio) {
                $this->email_send($envio->cron_newsletter_alias);
            }
        }
    }

    public function email_send($alias = NULL) {
        $this->load->model('Datos_municipio_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->library('encrypt');
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = 'contacto@codweb.co';
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $this->email->initialize($config);
        $emails = $this->Datos_entidad_model->obtener_newsletter();
        $entidad = $this->Datos_entidad_model->obtener_datos_contacto();
        $entidad_logo = $this->Datos_entidad_model->obtener_entidad_logo();
        $datos = $this->Datos_entidad_model->obtener_noticia($alias);
        echo 'Enviando Emails<br/>';
        $array = array(
            "nombre" => $datos->entidad_noticias_titulo,
            "descripcion" => $datos->entidad_noticias_descripcion,
            "foto" => $datos->entidad_noticias_imagen,
            "tipo" => $datos->entidad_noticias_tipo,
            "alias" => $datos->entidad_noticias_alias
        );
   /*     if ($emails != FALSE) {

            $x = 1;
            switch ($array['tipo']) {
                case '1':
                    $tipo = 'noticias';
                    $vista = 'ver_noticia';
                    foreach ($emails as $email) {
                        echo "Enviando $x de " . count($emails) . ' - ' . $email->entidad_newsletter_email . '<br />';
                        $x++;
                        $this->email->clear();
                        $this->email->from($sender, 'Alcaldía de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
                        $this->email->to($email->entidad_newsletter_email);
                        $this->email->subject(substr($array['nombre'], 0, 250) . '...');
                        include 'template/newsletter.php';
                        $this->email->message($contenido);
                        $this->email->send();
                    }

                    break;
                case '2':
                    $tipo = 'noticias';
                    $vista = 'ver_notificacion';
                    foreach ($emails as $email) {
                        echo "Enviando $x de " . count($emails) . ' - ' . $email->entidad_newsletter_email . '<br />';
                        $x++;
                        $this->email->clear();
                        $this->email->from($sender, 'Alcaldía de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
                        $this->email->to($email->entidad_newsletter_email);
                        $this->email->subject(substr($array['nombre'], 0, 250) . '...');
                        include 'template/newsletter.php';
                        $this->email->message($contenido);
                        $this->email->send();
                    }

                    break;
            }
        }*/
        echo 'Envio Completo';
    }

    public function send($alias) {
        $datos = array(
            'cron_newsletter_fecha' => date('Y-m-d H:i:s'),
            'cron_newsletter_alias' => $alias,
            'cron_newsletter_estado' => 2
        );
        $this->Datos_entidad_model->insert_cron_newsletter($datos);
        $this->Datos_entidad_model->update_noticias($alias);
        echo json_encode(true);
    }

}
