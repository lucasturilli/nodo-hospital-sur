<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Planeacion extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));

        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function normatividad_politicas() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('normatividad_politicas');
        $crud->set_subject('POLÍTICAS/LINEAMIENTOS/MANUALES');
        $crud->display_as('normatividad_politicas_nombre', 'Nombre');
        $crud->display_as('normatividad_politicas_tipo', 'Tipo de Norma');
        $crud->display_as('normatividad_politicas_archivo', 'Archivo');
        $crud->display_as('normatividad_politicas_tematica', 'Temática');
        $crud->display_as('normatividad_politicas_descripcion', 'Descripción Corta');
        $crud->display_as('normatividad_politicas_fecha_expedicion', 'Fecha de Expedición');
        $crud->display_as('normatividad_politicas_fecha_ingreso', 'Fecha de Ingreso');
        $crud->field_type('normatividad_politicas_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->set_field_upload('normatividad_politicas_archivo', 'uploads/entidad/normatividad');
        $crud->required_fields('normatividad_politicas_descripcion', 'normatividad_politicas_nombre', 'normatividad_politicas_tipo', 'normatividad_politicas_archivo', 'normatividad_politicas_tematica', 'normatividad_politicas_fecha_expedicion');
        $crud->unset_texteditor('normatividad_politicas_tematica', 'normatividad_politicas_descripcion');
        $crud->field_type('normatividad_politicas_tipo', 'dropdown', array('Políticas' => 'Políticas', 'Lineamientos' => 'Lineamientos', 'Manuales' => 'Manuales'));
        $crud->edit_fields('normatividad_politicas_nombre', 'normatividad_politicas_fecha_expedicion', 'normatividad_politicas_tipo', 'normatividad_politicas_archivo', 'normatividad_politicas_tematica', 'normatividad_politicas_descripcion');
        $ruta = 'Políticas/Lineamientos/Manuales';
        $ruta_tab = 'Planeación';
        $texto = 'Se deben publicar las políticas, manuales o lineamientos que produzca la entidad. Esta información debe ser descargable y estar organizada por temática, tipo de norma y fecha de expedición de la más reciente hacia atrás. El listado deberá contener una breve descripción de los documentos.';
        // $crud->set_rules('normatividad_politicas_fecha_expedicion', 'Fecha de Finalización', 'callback_check_date_today_politicas');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 301, $ruta, $ruta_tab, $texto);
    }

    /* ANTICORRUPCON */

    public function anticorrupcion_definicion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('anticorrupcion_definicion');
        $crud->set_subject('PLAN ANTICORRUPCION');
        $crud->display_as('anticorrupcion_definicion_nombre', 'Nombre');
        $crud->display_as('id_anticorrupcion_definicion_tipo', 'Tipo');
        $crud->display_as('anticorrupcion_definicion_periodo', 'Periodo');
        $crud->display_as('anticorrupcion_definicion_archivo', 'Archivo');
        $crud->display_as('anticorrupcion_definicion_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('anticorrupcion_definicion_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('anticorrupcion_definicion_archivo', 'uploads/entidad/anticorrupcion');
        $crud->field_type('anticorrupcion_definicion_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('anticorrupcion_definicion_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('anticorrupcion_definicion_nombre', 'id_anticorrupcion_definicion_tipo', 'anticorrupcion_definicion_archivo', 'anticorrupcion_definicion_periodo');
        $crud->edit_fields('anticorrupcion_definicion_nombre', 'id_anticorrupcion_definicion_tipo', 'anticorrupcion_definicion_periodo', 'anticorrupcion_definicion_archivo', 'anticorrupcion_definicion_fecha_actualizacion');
        $crud->field_type('anticorrupcion_definicion_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $crud->set_relation('id_anticorrupcion_definicion_tipo', 'anticorrupcion_definicion_tipo', 'anticorrupcion_definicion_tipo_nombre');
        $ruta = 'Plan Anticorrupción';
        $ruta_tab = 'Planeación';
        $texto = 'Plan Anticorrupción ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 302, $ruta, $ruta_tab, $texto);
    }

    public function anticorrupcion_definicion_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('anticorrupcion_definicion_anexos');
        $crud->set_subject('ANEXO AL PLAN ANTICORRUPCION');
        $crud->display_as('anticorrupcion_definicion_anexos_nombre', 'Nombre');
        $crud->display_as('id_anticorrupcion_definicion', 'Plan');
        $crud->display_as('anticorrupcion_definicion_anexos_archivo', 'Archivo');
        $crud->display_as('anticorrupcion_definicion_anexos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('anticorrupcion_definicion_anexos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('anticorrupcion_definicion_anexos_archivo', 'uploads/entidad/anticorrupcion');
        $crud->field_type('anticorrupcion_definicion_anexos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('anticorrupcion_definicion_anexos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('anticorrupcion_definicion_anexos_nombre', 'id_anticorrupcion_definicion', 'anticorrupcion_definicion_anexos_archivo');
        $crud->edit_fields('anticorrupcion_definicion_anexos_nombre', 'id_anticorrupcion_definicion', 'anticorrupcion_definicion_anexos_archivo', 'anticorrupcion_definicion_anexos_fecha_actualizacion');
        $crud->set_relation('id_anticorrupcion_definicion', 'anticorrupcion_definicion', '{anticorrupcion_definicion_nombre} - {anticorrupcion_definicion_periodo}');
        $ruta = 'Anexos Plan Anticorrupción';
        $ruta_tab = 'Planeación';
        $texto = 'Anexos Plan Anticorrupción ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 321, $ruta, $ruta_tab, $texto);
    }

    public function anticorrupcion_definicion_tipo() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('anticorrupcion_definicion_tipo');
        $crud->set_subject('TIPO PLAN ANTICORRUPCION');
        $crud->display_as('anticorrupcion_definicion_tipo_nombre', 'Nombre');
        $crud->required_fields('anticorrupcion_definicion_tipo_nombre');
        $crud->edit_fields('anticorrupcion_definicion_tipo_nombre');
        $ruta = 'Tipo Plan Anticorrupción';
        $ruta_tab = 'Planeación';
        $texto = 'Tipo de Plan Anticorrupción ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 320, $ruta, $ruta_tab, $texto);
    }

    public function control_politicas() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_politicas');
        $crud->set_subject('POLÍTICAS, LINEAMIENTOS Y MANUALES');
        $crud->display_as('control_politicas_nombre', 'Nombre');
        $crud->display_as('control_politicas_tipo', 'Tipo');
        $crud->display_as('control_politicas_archivo', 'Archivo');
        $crud->display_as('control_politicas_periodo', 'Periodo');
        $crud->set_field_upload('control_politicas_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_politicas_nombre', 'control_politicas_tipo', 'control_politicas_archivo', 'control_politicas_periodo');
        $crud->field_type('control_politicas_tipo', 'dropdown', array('Políticas' => 'Políticas', 'Lineamientos' => 'Lineamientos', 'Manuales' => 'Manuales'));
        $crud->field_type('control_politicas_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $ruta = 'Políticas, Lineamientos y Manuales ';
        $ruta_tab = 'Planeación  ';
        $texto = ' El sujeto obligado debe publicar sus políticas, lineamientos y manuales, tales como:
a. Políticas y lineamientos sectoriales e institucionales según sea el caso.
b. Manuales según sea el caso.
c. Planes estratégicos, sectoriales e institucionales según sea el caso.
d. Plan de Rendición de cuentas para los sujetos obligados que les aplique.
e. Plan de Servicio al ciudadano para los sujetos obligados que les aplique.
f. Plan Anti trámites para los sujetos obligados que les aplique.
Si el sujeto obligado realiza un plan de acción unificado es válida la publicación de éste.
El sujeto obligado debe publicar el contenido de toda decisión y/o política que haya adoptado y afecte al público, junto con sus fundamentos y toda interpretación autorizada de ellas.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 303, $ruta, $ruta_tab, $texto);
    }

    public function control_plan_estrategico() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_plan_estrategico');
        $crud->set_subject('PLAN DE DESARROLLO');
        $crud->display_as('control_plan_estrategico_archivo', 'Archivo');
        $crud->display_as('control_plan_estrategico_periodo', 'Periodo');
        $crud->set_field_upload('control_plan_estrategico_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_plan_estrategico_archivo', 'control_plan_estrategico_periodo');
        $crud->field_type('control_plan_estrategico_periodo', 'dropdown', array('2004' => '2004 - 2007', '2008' => '2008 - 2011', '2012' => '2012 - 2015', '2016' => '2016 - 2019', '2020' => '2020 - 2023'));
        $ruta = 'Plan de desarrollo Vigente';
        $ruta_tab = 'Planeación';
        $texto = 'La entidad debe publicar como mínimo su plan estratégico vigente.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 304, $ruta, $ruta_tab, $texto);
    }

    public function control_plan_estrategico_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('control_plan_estrategico_avances');
        $crud->set_subject('ANEXOS AL PLAN DE DESARROLLO');
        $crud->display_as('control_plan_estrategico_avances_nombre', 'Nombre');
        $crud->display_as('control_plan_estrategico_avances_archivo', 'Archivo');
        $crud->display_as('control_plan_estrategico_avances_periodicidad', 'Periodocidad');
        $crud->display_as('id_control_plan_estrategico', 'Plan de desarrollo');
        $crud->display_as('control_plan_estrategico_avances_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('control_plan_estrategico_avances_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('control_plan_estrategico_avances_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('control_plan_estrategico_avances_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('control_plan_estrategico_avances_nombre','control_plan_estrategico_avances_archivo','control_plan_estrategico_avances_periodicidad', 'id_control_plan_estrategico');
        $crud->edit_fields('control_plan_estrategico_avances_nombre','control_plan_estrategico_avances_archivo','control_plan_estrategico_avances_periodicidad', 'id_control_plan_estrategico','control_plan_estrategico_avances_fecha_actualizacion');
        $crud->set_field_upload('control_plan_estrategico_avances_archivo', 'uploads/entidad/control');
        $crud->field_type('control_plan_estrategico_avances_periodicidad', 'dropdown', periodicidad());
        $crud->set_relation('id_control_plan_estrategico', 'control_plan_estrategico', 'control_plan_estrategico_periodo');
        $ruta = 'Anexos al Plan de desarrollo Vigente';
        $ruta_tab = 'Planeación';
        $texto = 'Anexos al Plan de desarrollo Vigente';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 322, $ruta, $ruta_tab, $texto);
    }

    public function control_plan_accion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_plan_accion');
        $crud->set_subject('PLAN DE ACCIÓN  ');
        $crud->display_as('id_entidad_dependencia', 'Dependencia');
        $crud->display_as('control_plan_accion_nombre', 'Nombre');
        $crud->display_as('control_plan_accion_archivo', 'Archivo');
        $crud->display_as('control_plan_accion_periodo', 'Periodo');
        $crud->set_field_upload('control_plan_accion_archivo', 'uploads/entidad/control');
        $crud->required_fields('id_entidad_dependencia', 'control_plan_accion_archivo', 'control_plan_accion_periodo');
        $crud->field_type('control_plan_accion_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->set_relation('id_entidad_dependencia', 'entidad_dependencia', 'entidad_dependencia_nombre');
//        $crud->change_field_type('control_plan_accion_nombre', 'hidden');
//        $crud->callback_before_insert(array($this, 'armar_plan_accion_nombre'));
//        $crud->callback_before_update(array($this, 'armar_plan_accion_nombre'));
        $ruta = 'Plan de Acción';
        $ruta_tab = 'Planeación  ';
        $texto = 'La entidad debe publicar el plan de acción en el cual se especifiquen los objetivos, las estrategias, los proyectos, las metas, los responsables, los planes generales de compras y la distribución presupuestal de sus proyectos de inversión junto a los indicadores de gestión, de acuerdo con lo establecido en el artículo 74 de la Ley 1474 de 2011 (Estatuto Anticorrupción).';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 305, $ruta, $ruta_tab, $texto);
    }

    function armar_plan_accion_nombre($post_array) {
        $this->load->model('Datos_entidad_model');
        $dependencia = $this->Datos_entidad_model->obtener_entidad_dependencia($post_array['id_entidad_dependencia']);
        $post_array['control_plan_accion_nombre'] = $dependencia->entidad_dependencia_nombre . ' - ' . $post_array['control_plan_accion_periodo'];

        return $post_array;
    }

    public function control_plan_accion_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_plan_accion_avances');
        $crud->set_subject('AVANCES DEL PLAN DE ACCIÓN  ');
        $crud->display_as('id_control_plan_accion', 'Plan de Acción');
        $crud->display_as('control_plan_accion_avances_archivo', 'Archivo');
        $crud->display_as('control_plan_accion_avances_trimestre', 'Periodicidad');
        $crud->set_field_upload('control_plan_accion_avances_archivo', 'uploads/entidad/control');
        $crud->required_fields('id_control_plan_accion', 'control_plan_accion_avances_periodo', 'control_plan_accion_avances_archivo', 'control_plan_accion_avances_trimestre');
        $crud->set_relation('id_control_plan_accion', 'control_plan_accion', 'control_plan_accion_nombre');
        $crud->field_type('control_plan_accion_avances_trimestre', 'dropdown', periodicidad());
        $ruta = 'Avances del Plan de Acción';
        $ruta_tab = 'Planeación  ';
        $texto = 'La entidad debe publicar los avances del plan de acción mínimo cada 3 meses.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 306, $ruta, $ruta_tab, $texto);
    }

    public function control_programas() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_programas');
        $crud->set_subject('PROGRAMAS O PROYECTOS EN EJECUCIÓN  ');
        $crud->display_as('control_programas_nombre', 'Nombre');
        $crud->display_as('control_programas_archivo', 'Archivo');
        $crud->display_as('control_programas_fecha_inscripcion', 'Fecha de Inscripción ');
        $crud->set_field_upload('control_programas_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_programas_nombre', 'control_programas_archivo', 'control_programas_fecha_inscripcion');
        $ruta = 'Programas y Proyectos en Ejecución ';
        $ruta_tab = 'Planeación  ';
        $texto = 'La entidad debe publicar, como mínimo, el plan operativo anual de inversiones o el instrumento donde se consignen los proyectos de inversión o programas que se ejecuten en cada vigencia. Los proyectos de inversión deben ordenarse según la fecha de inscripción en el Banco de Programas y Proyectos de Inversión nacional, según lo establecido en el artículo 77 de la Ley 1474 de 2011 (Estatuto Anticorrupción).';
        $crud->set_rules('control_programas_fecha_inscripcion', 'Fecha de Finalización', 'callback_check_date_today_programas');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 307, $ruta, $ruta_tab, $texto);
    }

    public function check_date_today_programas($str) {


        $partes = explode('/', $this->input->post('control_programas_fecha_inscripcion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_programas', "La fecha de inscripción   no puede ser mayor a la del día de hoy  .");
            return FALSE;
        }
    }

    public function control_programas_avances() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_programas_avances');
        $crud->set_subject('AVANCE DE PROGRAMA O PROYECTO EN EJECUCIÓN  ');
        // $crud->display_as('control_programas_avances_nombre', 'Nombre');
        $crud->display_as('control_programas_avances_archivo', 'Archivo');
        $crud->display_as('control_programas_avances_periodo', 'Periodo');
        $crud->display_as('control_programas_avances_trimestre', 'Periodicidad');
        $crud->display_as('id_control_programas', 'Programa o Proyecto en Ejecución');
        $crud->set_field_upload('control_programas_avances_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_programas_avances_periodo', 'control_programas_avances_trimestre', 'control_programas_avances_archivo', 'id_control_programas');
        $crud->set_relation('id_control_programas', 'control_programas', '{control_programas_nombre} inscrito el  {control_programas_fecha_inscripcion}');
        $crud->field_type('control_programas_avances_periodo', 'dropdown', periodos());
        $crud->field_type('control_programas_avances_trimestre', 'dropdown', periodicidad());
        $ruta = 'Avances de los Programas y Proyectos en Ejecución ';
        $ruta_tab = 'Planeación  ';
        $texto = 'Se debe publicar el avance de los programas y proyectos  en  ejecución, mínimo cada 3 meses.';
        $crud->set_rules('control_programas_fecha_inscripcion', 'Fecha de Finalización', 'callback_check_date_today_programas');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 308, $ruta, $ruta_tab, $texto);
    }

    public function control_indicadores_gestion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_indicadores_gestion');
        $crud->set_subject('METAS E INDICADORES DE GESTIÓN');
        $crud->display_as('control_indicadores_gestion_trimestre', 'Periodicidad');
        $crud->display_as('control_indicadores_gestion_archivo', 'Archivo');
        $crud->display_as('control_indicadores_gestion_periodo', 'Periodo');
        $crud->display_as('control_indicadores_gestion_nombre', 'Nombre');
        $crud->set_field_upload('control_indicadores_gestion_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_indicadores_gestion_nombre','control_indicadores_gestion_periodo', 'control_indicadores_gestion_archivo', 'control_indicadores_gestion_trimestre');
        $crud->field_type('control_indicadores_gestion_periodo', 'dropdown', periodos());
        $crud->field_type('control_indicadores_gestion_trimestre', 'dropdown', periodicidad());
        $ruta = 'Metas e Indicadores de Gestión';
//        $crud->unset_add();
//        $crud->unset_delete();
        $ruta_tab = 'Planeación';
        $texto = 'La entidad publica la información relacionada con metas, indicadores de gestión y/o desempeño, de acuerdo con su planeación estratégica. Se debe publicar su estado, mínimo cada 3 meses.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 309, $ruta, $ruta_tab, $texto);
    }

    public function control_planes_otros() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_planes_otros');
        $crud->set_subject('OTROS PLANES');
        $crud->display_as('control_planes_otros_nombre', 'Nombre');
        $crud->display_as('control_planes_otros_archivo', 'Archivo');
        $crud->display_as('control_planes_otros_tipo', 'Tipo de Plan');
        $crud->display_as('control_planes_otros_fecha', 'Fecha de Actualización');
        $crud->display_as('id_control_entes', 'Ente de Control');
        $crud->set_field_upload('control_planes_otros_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_planes_otros_nombre', 'control_planes_otros_archivo', 'control_planes_otros_tipo', 'control_planes_otros_fecha');
        $crud->field_type('control_planes_otros_fecha', 'hidden', date('Y-m-d'));
        $ruta = 'Otros Planes';
        $ruta_tab = 'Planeación';
        $texto = 'La entidad publica otros planes relacionados con temas anticorrupción, rendición de cuentas, servicio al ciudadano, antitrámites y los demás que solicite la normatividad vigente. Si la entidad realiza un plan de acción unificado es válido la publicación de éste.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 310, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_empalme() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_empalme');
        $crud->set_subject('INFORME DE EMPALME');
        $crud->display_as('control_informe_empalme_nombre_archivo', 'Nombre');
        $crud->display_as('control_informe_empalme_archivo', 'Archivo');
        $crud->display_as('control_informe_empalme_fecha', 'Fecha de Actualización');
        $crud->display_as('control_informe_empalme_periodo', 'Periodo');
        $crud->set_field_upload('control_informe_empalme_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_empalme_nombre_archivo', 'control_informe_empalme_archivo', 'control_informe_empalme_periodo');
        $crud->field_type('control_informe_empalme_fecha', 'hidden', date('Y-m-d'));
        $crud->field_type('control_informe_empalme_periodo', 'dropdown', array('2008-2011' => '2008-2011', '2012-2015' => '2012-2015', '2016-2019' => '2016-2019', '2020-2023' => '2020-2023'));
        $ruta = 'Informes de Empalme';
        $ruta_tab = 'Planeación';
        $texto = 'La entidad publica el informe de empalme del representante legal, cuando haya un cambio del mismo.<br>Se debe publicar cuando haya un cambio del representante legal y antes de desvincularse de la entidad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 311, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_empalme_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_empalme_anexos');
        $crud->set_subject('ANEXOS DE INFORME DE EMPALME');
        $crud->display_as('control_informe_empalme_anexos_nombre', 'Nombre');
        $crud->display_as('control_informe_empalme_anexos_archivo', 'Archivo');
        $crud->display_as('control_informe_empalme_anexos_fecha', 'Fecha de Actualización');
        $crud->display_as('id_control_informe_empalme', 'Informe');
        $crud->set_field_upload('control_informe_empalme_anexos_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_empalme_anexos_nombre', 'control_informe_empalme_anexos_archivo', 'control_informe_empalme_anexos_fecha');
        $crud->field_type('control_informe_empalme_anexos_fecha', 'hidden', date('Y-m-d'));
        $crud->set_relation('id_control_informe_empalme', 'control_informe_empalme', 'control_informe_empalme_periodo');
        $ruta = 'Anexos de Informe de Empalme';
        $ruta_tab = 'Planeación';
        $texto = 'La entidad publica el informe de empalme del representante legal, cuando haya un cambio del mismo.<br>Se debe publicar cuando haya un cambio del representante legal y antes de desvincularse de la entidad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 312, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_archivo() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_archivo');
        $crud->set_subject('INFORMES DE ARCHIVO');
        $crud->display_as('control_informe_archivo_nombre', 'Nombre');
        $crud->display_as('control_informe_archivo_archivo', 'Archivo');
        $crud->display_as('id_control_informe_archivo_tab', 'Pestaña');
        $crud->display_as('id_control_informe_archivo_categoria', 'Categoria');
        $crud->set_field_upload('control_informe_archivo_archivo', 'uploads/entidad/control');
        $crud->required_fields('control_informe_archivo_archivo', 'control_informe_archivo_nombre', 'id_control_informe_archivo_tab', 'id_control_informe_archivo_categoria');
        $crud->set_relation('id_control_informe_archivo_tab', 'control_informe_archivo_tab', 'control_informe_archivo_tab_nombre');
        $crud->set_relation('id_control_informe_archivo_categoria', 'control_informe_archivo_categoria', 'control_informe_archivo_categoria_nombre');
        $ruta = 'Informes de Archivo';
        $ruta_tab = 'Planeación';
        $texto = 'La entidad publica la Tabla de Retención Documental y el Programa de Gestión Documental.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 315, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_archivo_tab() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_archivo_tab');
        $crud->set_subject('TABS ARCHIVO');
        $crud->display_as('control_informe_archivo_tab_nombre', 'Nombre');
        $crud->required_fields('control_informe_archivo_tab_nombre');
        $ruta = 'Tabs de Archivo';
        $ruta_tab = 'Planeación';
        $texto = 'Pestañas para la seccion de informes de archivo.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 314, $ruta, $ruta_tab, $texto);
    }

    public function control_informe_archivo_categoria() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('control_informe_archivo_categoria');
        $crud->set_subject('CATEGORIAS ARCHIVO');
        $crud->display_as('control_informe_archivo_categoria_nombre', 'Nombre');
        $crud->required_fields('control_informe_archivo_categoria_nombre');
        $ruta = 'Categorias de Archivo';
        $ruta_tab = 'Planeación';
        $texto = 'Categorias para la seccion de informes de archivo.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 313, $ruta, $ruta_tab, $texto);
    }

    public function planeacion_plan_gasto_publico() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('planeacion_plan_gasto_publico');
        $crud->set_subject('PLAN DE GASTO PUBLICO');
        $crud->display_as('planeacion_plan_gasto_publico_nombre', 'Nombre');
        $crud->display_as('planeacion_plan_gasto_publico_periodo', 'Periodo');
        $crud->display_as('planeacion_plan_gasto_publico_archivo', 'Archivo');
        $crud->display_as('planeacion_plan_gasto_publico_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('planeacion_plan_gasto_publico_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('planeacion_plan_gasto_publico_archivo', 'uploads/entidad/planeacion');
        $crud->field_type('planeacion_plan_gasto_publico_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('planeacion_plan_gasto_publico_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('planeacion_plan_gasto_publico_nombre', 'planeacion_plan_gasto_publico_periodo', 'planeacion_plan_gasto_publico_archivo');
        $crud->edit_fields('planeacion_plan_gasto_publico_nombre', 'planeacion_plan_gasto_publico_periodo', 'planeacion_plan_gasto_publico_archivo', 'planeacion_plan_gasto_publico_fecha_actualizacion');
        $crud->field_type('planeacion_plan_gasto_publico_periodo', 'dropdown', array(date('Y') - 4 => date('Y') - 4, date('Y') - 3 => date('Y') - 3, date('Y') - 2 => date('Y') - 2, date('Y') - 1 => date('Y') - 1, date('Y') => date('Y')));
        $ruta = 'Plan de Gasto Publico';
        $ruta_tab = 'Planeación';
        $texto = 'Plan de Gasto Publico';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 316, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
