<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referenciales_entidad extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL) {
        if ($ruta != NULL) {
            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear('Administración ', 'Administración');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = FALSE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function entidad_redes_sociales_definicion() {

            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $crud->set_table('entidad_redes_sociales_definicion');
        $crud->set_subject('RED SOCIAL');
        $crud->display_as('entidad_redes_sociales_definicion_nombre', 'Nombre');
        $crud->display_as('entidad_redes_sociales_definicion_color', 'Color');
        $crud->display_as('entidad_redes_sociales_definicion_imagen', 'Logo CSS');
        $crud->display_as('entidad_redes_sociales_definicion_archivo', 'Logo');
        $crud->required_fields('entidad_redes_sociales_definicion_nombre', 'entidad_redes_sociales_definicion_imagen');
        $crud->set_field_upload('entidad_redes_sociales_definicion_archivo', 'uploads/redes');
        $ruta = 'Redes Sociales';
        $crud->unset_bootstrap();
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function entidad_seo() {
            $crud = new grocery_CRUD();  $crud->unset_bootstrap();
        
        $this->load->config('grocery_crud');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'ico');
        $crud->set_table('entidad_seo');
        $crud->set_subject('SEO');
        $crud->display_as('entidad_seo_favicon', 'Favicon');
        $crud->display_as('entidad_seo_palabras', 'Palabras Claves');
        $crud->display_as('entidad_seo_titulo', 'Titulo');
        $crud->display_as('entidad_seo_descripcion', 'Descripción');
        $crud->display_as('entidad_seo_google_analytics', 'Google Analytics');
        $crud->display_as('entidad_seo_twitter_vcard', 'Twitter Card');
        $crud->display_as('entidad_seo_ciudad', 'Ciudad');
        $crud->display_as('entidad_seo_region', 'Departamento');
        $crud->display_as('entidad_seo_country', 'Pais');
        $crud->set_field_upload('entidad_seo_favicon', 'uploads/entidad');
        $crud->unset_texteditor('entidad_seo_palabras', 'entidad_seo_descripcion', 'entidad_seo_google_analytics');
        $crud->unset_delete();
        $crud->unset_add();
        $crud->unset_bootstrap();
        $ruta = 'Configuración SEO';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
