<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * class Login
 *
 * Clase que me controla la autenticacion y elfomulario de ingreso al sistema
 *
 * @package    NODO
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->model('login_model');
        $this->load->model('Referenciales_nodo_model');
    }

    var $data = array();

    // Funcion que nos carga la vista del formulario de acceso al sistema
    function index($id = null, $pass = null, $mensaje = null) {

        $this->data['id'] = $id;
        $this->data['pass'] = $pass;
        $this->data['mensaje'] = $mensaje;
        $this->data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $this->load->view('login/formulario_de_acceso', $this->data); //Vista que se carga
    }

    // Funcion que valida que el usuario si exista en la base da datos
    function validar_usuario() {

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('numero', 'Numero de identificación', 'required');
        $this->form_validation->set_rules('password', 'Contraseña', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->index($this->input->post('numero'), $this->input->post('password'));
        } else {
            $this->load->model('login_model');
            $query = $this->login_model->validar($this->input->post('numero'));
            if ($query) {
                if ($this->encryption->decrypt($query->administrador_password) == $this->input->post('password')) {

                    if ($query->administrador_estado == 'INACTIVO') {
                        $this->data['mensaje'] = 'Su cuenta no está activa, póngase en contacto con el administrador. ';
                        $this->index($this->input->post('numero'), $this->input->post('password'), $this->data['mensaje']);
                    } else {
                        $data = $query;
                        $this->session->set_userdata('is_logged_in', TRUE);
                        $this->session->set_userdata('administrador_nombres', $data->administrador_nombres);
                        $this->session->set_userdata('administrador_apellidos', $data->administrador_apellidos);
                        $this->session->set_userdata('administrador_identificacion', $data->administrador_identificacion);
                        $this->session->set_userdata('administrador_email', $data->administrador_email);
                        $this->session->set_userdata('id_administrador', $data->id_administrador);
                        $this->session->set_userdata('id_administrador_rol', $data->id_administrador_rol);
                        $this->session->set_userdata('upload_image_file_manager', TRUE);
                        $this->login_model->update_log($query);
                        $this->Datos_entidad_model->eliminar_visitas_viejas();
                        redirect('administrador/principal/index');
                    }
                } else {
                    $this->data['mensaje'] = 'Contraseña Incorrecta';
                    $this->index($this->input->post('numero'), $this->input->post('password'), $this->data['mensaje']);
                }
            } else {
                $this->data['mensaje'] = 'El usuario no existe, verifique sus datos';
                $this->index($this->input->post('numero'), $this->input->post('password'), $this->data['mensaje']);
            }
        }
    }

    // Funcion que permite deslogear a un usuario del sistema
    function logout() {
        $this->session->sess_destroy();
        redirect('administrador/login/index', 'refresh');
    }

    function enviar_email() {
        $id = $_POST['id'];
        $email = $_POST['email'];
        $query = $this->login_model->validar($id);
        $ruta = site_url('administrador/login');
        $seo = $this->Datos_entidad_model->obtener_entidad_seo();
        if ($query != FALSE) {
            if ($query->administrador_email == $email) {
                $passw = $this->encryption->decrypt($query->administrador_password);
                $this->load->library('email');
                $smtp = $this->Referenciales_nodo_model->obtener_smtp();
                if ($smtp != FALSE) {
                    if ($smtp->smtp_host != NULL) {
                        $config['protocol'] = 'smtp';
                        $config['smtp_host'] = $smtp->smtp_host;
                        $config['smtp_port'] = $smtp->smtp_port;
                        $config['smtp_user'] = $smtp->smtp_user;
                        $config['smtp_pass'] = $smtp->smtp_pass;
                    }
                    $sender = $smtp->smtp_email;
                } else {
                    $sender = 'contacto@codweb.co';
                }
                $config['charset'] = 'utf-8';
                $config['newline'] = "\r\n";
                $config['mailtype'] = 'html'; // or html
                $this->email->initialize($config);
                $this->email->from($sender, "NODO");
                $this->email->reply_to('no-responder@codweb.co', 'No Responder');
                $this->email->to($email);
                $this->email->subject("Recuperación de Contraseña Software de Nodo");
                $this->email->message("<p>Los datos de ingreso al NODO de la $seo->entidad_seo_titulo son los siguientes:</p>"
                        . "<p><strong>Número de Identificación: </strong>$id</p>"
                        . "<p><strong>Correo Electrónico : </strong>$email</p>"
                        . "<p><strong>Contraseña : </strong>$passw</p>"
                        . "<p><strong>Ruta de Acceso : </strong><a target='_blank' href='$ruta'>Ingresar a Nodo</a></p>");
                $this->email->send(FALSE);
                echo json_encode(array('estado' => 1, 'error' => $email));
            } else {
                echo json_encode(array('estado' => 2, 'error' => 'El correo electrónico ingresado no coincide con el registrado '));
            }
        } else {
            echo json_encode(array('estado' => 2, 'error' => 'El número de identificación ingresado no existe '));
        }
    }

}
