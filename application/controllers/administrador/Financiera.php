<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Financiera extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function financiera_presupuesto() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('financiera_presupuesto');
        $crud->set_subject('PRESUPUESTO APROBADO EN EJERCICIO');
        $crud->display_as('financiera_presupuesto_ingresos', 'Ingresos');
        $crud->display_as('financiera_presupuesto_gastos', 'Gastos');
        $crud->display_as('financiera_presupuesto_periodo', 'Periodo');
        $crud->display_as('id_normatividad_leyes', 'Acuerdo');
        $crud->set_field_upload('financiera_presupuesto_ingresos', 'uploads/entidad/financiera');
        $crud->set_field_upload('financiera_presupuesto_gastos', 'uploads/entidad/financiera');
        $crud->field_type('financiera_presupuesto_periodo', 'dropdown', periodos());
        $crud->required_fields('financiera_presupuesto_periodo');
        $crud->set_relation('id_normatividad_leyes', 'normatividad_leyes', '{normatividad_leyes_nombre} - {normatividad_leyes_tipo}');
        $ruta = 'Presupuesto Aprobado en Ejercicio ';
        $ruta_tab = 'Presupuesto ';
        $texto = 'La entidad debe publicar el presupuesto en ejercicio detallado, que es el aprobado de acuerdo con las normas vigentes aplicables por tipo de entidad. Se deben publicar los presupuestos de ingresos y gastos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 251, $ruta, $ruta_tab, $texto);
    }

    public function financiera_presupuesto_ejecucion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('financiera_presupuesto_ejecucion');
        $crud->set_subject('EJECUCIÓN DE PRESUPUESTO APROBADO EN EJERCICIO');
        $crud->display_as('financiera_presupuesto_ejecucion_archivo', 'Archivo');
        $crud->display_as('financiera_presupuesto_ejecucion_trimestre', 'Periodicidad');
        $crud->display_as('id_financiera_presupuesto', 'Periodo');
        $crud->set_field_upload('financiera_presupuesto_ejecucion_archivo', 'uploads/entidad/financiera');
        $crud->required_fields('id_financiera_presupuesto', 'financiera_presupuesto_ejecucion_archivo', 'financiera_presupuesto_ejecucion_periodo', 'financiera_presupuesto_ejecucion_trimestre');
        $crud->field_type('financiera_presupuesto_ejecucion_trimestre', 'dropdown', periodicidad());
        $crud->set_relation('id_financiera_presupuesto', 'financiera_presupuesto', 'financiera_presupuesto_periodo');
        $ruta = 'Ejecución  de Presupuesto Aprobado en Ejercicio ';
        $ruta_tab = 'Presupuesto ';
        $texto = 'Ejecución de los  presupuestos de ingresos y gastos. <br>Se debe publicar igualmente, la ejecución, mínimo cada 3 meses.';
        $crud->set_rules('financiera_presupuesto_ejecucion_periodo_inicio', 'Fecha de Finalización', 'callback_check_date');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 252, $ruta, $ruta_tab, $texto);
    }

    public function check_date($str) {


        $partes = explode('/', $this->input->post('financiera_presupuesto_ejecucion_periodo_inicio'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', $this->input->post('financiera_presupuesto_ejecucion_periodo_fin'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date', "La fecha de inicio no puede ser posterior a la fecha de finalización.");
            return FALSE;
        }
    }

    public function financiera_historico_presupuesto() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('financiera_historico_presupuesto');
        $crud->set_subject('INFORMACIÓN HISTÓRICA DE PRESUPUESTOS');
        // $crud->display_as('financiera_historico_presupuesto_nombre', 'Nombre');
        $crud->display_as('financiera_historico_presupuesto_archivo', 'Archivo');
        $crud->display_as('financiera_historico_presupuesto_tipo', 'Tipo');
        $crud->field_type('financiera_historico_presupuesto_tipo', 'dropdown', array('Ingresos' => 'Ingresos', 'Gastos' => 'Gastos'));
        $crud->display_as('financiera_historico_presupuesto_periodo', 'Periodo ');
        $crud->field_type('financiera_historico_presupuesto_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->set_field_upload('financiera_historico_presupuesto_archivo', 'uploads/entidad/financiera');
        $crud->required_fields('financiera_historico_presupuesto_archivo', 'financiera_historico_presupuesto_tipo', 'financiera_historico_presupuesto_periodo');
        $ruta = 'Información Histórica de Presupuestos ';
        $ruta_tab = 'Presupuesto ';
        $texto = 'La entidad debe publicar la información detallada del presupuesto de ingresos y gastos, aprobado y ejecutado, de dos (2) años anteriores al año en ejercicio, con corte a diciembre del periodo respectivo.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 253, $ruta, $ruta_tab, $texto);
    }

    public function financiera_estados_financieros() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('financiera_estados_financieros');
        $crud->set_subject('ESTADOS FINANCIEROS ');
        $crud->display_as('financiera_estados_financieros_nombre', 'Nombre');
        $crud->display_as('financiera_estados_financieros_archivo', 'Archivo');
        $crud->display_as('financiera_estados_financieros_periodo', 'Periodo ');
        $crud->field_type('financiera_estados_financieros_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->set_field_upload('financiera_estados_financieros_archivo', 'uploads/entidad/financiera');
        $crud->required_fields('financiera_estados_financieros_archivo', 'financiera_estados_financieros_periodo', 'financiera_estados_financieros_nombre');
        $ruta = 'Estados Financieros ';
        $ruta_tab = 'Presupuesto ';
        $texto = 'La entidad debe publicar los estados financieros de las dos últimas vigencias, con corte a diciembre del año respectivo. ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 254, $ruta, $ruta_tab, $texto);
    }

    public function financiera_estatuto_tributario() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('financiera_estatuto_tributario');
        $crud->set_subject('ESTATUTOS TRIBUTARIOS ');
        $crud->display_as('financiera_estatuto_tributario_nombre', 'Nombre');
        $crud->display_as('financiera_estatuto_tributario_archivo', 'Archivo');
        $crud->display_as('financiera_estatuto_tributario_periodo', 'Periodo ');
        $crud->display_as('financiera_estatuto_tributario_descripcion', 'Descripción ');
        $crud->display_as('financiera_estatuto_tributario_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('financiera_estatuto_tributario_fecha_actualizacion', 'Fecha de Actualización');
        $crud->field_type('financiera_estatuto_tributario_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('financiera_estatuto_tributario_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('financiera_estatuto_tributario_periodo', 'dropdown', array(date('Y') => date('Y'), date('Y') - 1 => date('Y') - 1, date('Y') - 2 => date('Y') - 2, date('Y') - 3 => date('Y') - 3, date('Y') - 4 => date('Y') - 4, date('Y') - 5 => date('Y') - 5));
        $crud->set_field_upload('financiera_estatuto_tributario_archivo', 'uploads/entidad/financiera');
        $crud->required_fields('financiera_estatuto_tributario_nombre', 'financiera_estatuto_tributario_archivo', 'financiera_estatuto_tributario_periodo');
        $crud->edit_fields('financiera_estatuto_tributario_nombre', 'financiera_estatuto_tributario_archivo', 'financiera_estatuto_tributario_descripcion', 'financiera_estatuto_tributario_periodo', 'financiera_estatuto_tributario_fecha_actualizacion');
        $crud->unset_texteditor('financiera_estatuto_tributario_descripcion');
        $ruta = 'Estatutos Tributarios';
        $ruta_tab = 'Presupuesto ';
        $texto = 'Estatutos Tributarios';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 255, $ruta, $ruta_tab, $texto);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
