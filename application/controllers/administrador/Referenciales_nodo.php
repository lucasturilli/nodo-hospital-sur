<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referenciales_nodo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->load->library('encryption');
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL) {
        if ($ruta != NULL) {
            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear('Administración ', 'Administración');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = FALSE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
           $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function definicion_usuarios() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('administrador');
        $crud->set_subject('USUARIO DEL SISTEMA');
        $crud->set_relation('id_administrador_rol', 'administrador_rol', 'administrador_rol_nombre');
        $crud->set_relation_n_n('permisos', 'administrador_x_permisos', 'permisos', 'id_administrador', 'id_permisos', 'seccion');
        $crud->set_rules('administrador_password', 'Contraseña', 'min_length[6]|required');
        $crud->set_rules('administrador_email', 'Email', 'required');
        $state = $crud->getState();
        if ($state == 'add' || $state == 'clone' || $state == "insert_validation") {
            $crud->set_rules('administrador_identificacion', 'Número de identificación', 'callback_usuario_check|numeric|required');
        } else {
            $crud->set_rules('administrador_identificacion', 'Número de identificación', 'numeric|required');
        }
        $crud->display_as('id_administrador_rol', 'Rol');
        $crud->display_as('administrador_nombres', 'Nombre');
        $crud->display_as('administrador_password', 'Contraseña');
        $crud->display_as('administrador_apellidos', 'Apellidos');
        $crud->display_as('administrador_email', 'Correo electrónico ');
        $crud->display_as('administrador_identificacion', 'Número de identificación ');
        $crud->display_as('administrador_estado', 'Estado');
        $crud->change_field_type('administrador_password', 'password');
        $crud->required_fields('administrador_nombres', 'administrador_apellidos', 'administrador_email', 'administrador_identificacion', 'administrador_password', 'id_administrador_rol', 'administrador_estado');
        $crud->unset_columns('administrador_password');
        $crud->order_by('administrador_nombres');
        $crud->columns('administrador_nombres', 'administrador_apellidos', 'administrador_identificacion', 'administrador_email', 'id_administrador_rol', 'administrador_estado');
        if ($this->session->userdata('id_administrador_rol') != 1) {
            $crud->unset_add();
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->unset_read();
            $crud->columns('administrador_nombres', 'administrador_apellidos', 'administrador_identificacion', 'administrador_email');
        }
        $crud->callback_before_insert(array($this, 'antes_de_guardar'));
        $crud->callback_before_update(array($this, 'antes_de_guardar'));
        $crud->callback_edit_field('administrador_password', array($this, 'decrypt_password_callback'));
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $ruta = 'Definición de Usuarios';
        $this->_example_output($output, NULL, $ruta);
    }

    public function definicion_mi_usuario() {
        try {
            $crud = new grocery_CRUD();
            $crud->unset_bootstrap();

            $crud->set_table('administrador');
            $crud->set_subject('Definicion de Usuarios');
            $crud->set_relation('id_administrador_rol', 'administrador_rol', 'administrador_rol_nombre');
            $crud->set_rules('administrador_password', 'Contraseña', 'min_length[6]|required');
            $crud->set_rules('administrador_email', 'Email', 'valid_email|required');
            // $crud->set_rules('administrador_identificacion', 'Número de identificación', 'callback_usuario_check|numeric|required');
            $crud->display_as('id_administrador_rol', 'Rol');
            $crud->display_as('administrador_nombres', 'Nombres');
            $crud->display_as('administrador_password', 'Contraseña');
            $crud->display_as('administrador_apellidos', 'Apellidos');
            $crud->display_as('administrador_email', 'Correo electrónico ');
            $crud->display_as('administrador_identificacion', 'Número de identificación ');
            $crud->change_field_type('administrador_password', 'password');
            $crud->required_fields('administrador_nombres', 'administrador_apellidos', 'administrador_email', 'administrador_identificacion', 'administrador_password', 'id_administrador_rol');
            $crud->unset_columns('administrador_password');
            $crud->unset_read();
            if ($this->session->userdata('id_administrador_rol') != 1) {
                $crud->columns('administrador_nombres', 'administrador_apellidos', 'administrador_identificacion', 'administrador_email');
                $crud->edit_fields('administrador_nombres', 'administrador_apellidos', 'administrador_email', 'administrador_password');
            }
            $crud->callback_before_insert(array($this, 'antes_de_guardar'));
            $crud->callback_before_update(array($this, 'antes_de_guardar'));
            $crud->callback_edit_field('administrador_password', array($this, 'decrypt_password_callback'));
            $crud->where('administrador_identificacion', $this->session->userdata('administrador_identificacion'));
            $crud->unset_add();
            $crud->unset_delete();
            $crud->callback_after_insert(array($this, 'log_user'));
            $crud->callback_after_update(array($this, 'log_user'));
            $crud->callback_after_delete(array($this, 'log_user'));
            $output = $crud->render();
            $ruta = 'Mi Perfil';
            $this->_example_output($output, NULL, $ruta);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function usuario_check($str) {
        $id = $this->uri->segment(3);
        if ($this->Referenciales_nodo_model->verificar_administrador($str) == TRUE) {
            $this->form_validation->set_message('usuario_check', 'El Usuario ya se encuentra registrado');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function antes_de_guardar($post_array) {
        $post_array['administrador_password'] = $this->encryption->encrypt($post_array['administrador_password']);
        return $post_array;
    }

    function decrypt_password_callback($value) {
        $decrypted_password = $this->encryption->decrypt($value);
        return "<input type='password' style='box-sizing: initial;' name='administrador_password' value='$decrypted_password' />";
    }

    function log_user($post_array = NULL, $primary_key = NULL) {
        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);
        return true;
    }

    public function permisos() {
        try {
            $crud = new grocery_CRUD();
            $crud->unset_bootstrap();

            $crud->set_table('permisos');
            $crud->set_subject('Definicion de Permisos');
            $crud->required_fields('permisos');
            $ruta = 'Permisos';
            $output = $crud->render();
            $this->_example_output($output, NULL, $ruta);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function entidad_contador_visitas_dia() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_contador_visitas_dia');
        $crud->set_subject('VISITAS');
        $crud->display_as('entidad_contador_visitas_dia_fecha', 'Fecha');
        $crud->display_as('entidad_contador_visitas_dia_ip', 'IP');
        $crud->display_as('entidad_contador_visitas_dia_agente', 'Agente');
        $crud->display_as('entidad_redes_sociales_definicion_archivo', 'Logo');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->order_by('entidad_contador_visitas_dia_fecha', 'DESC');
        $ruta = 'Visitas';
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function audits() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('audit');
        $crud->set_subject('Registro de Publicaciones');
        $crud->display_as('audit_fecha', 'Fecha');
        $crud->display_as('audit_query', 'Query');
        $crud->display_as('audit_key_in_table', 'ID Tabla');
        $crud->display_as('audit_table', 'Tabla');
        $crud->display_as('audit_type', 'Tipo');
        $crud->display_as('audit_data', 'Datos');
        $crud->display_as('audit_id_usuario', 'Usuario');
        $crud->set_relation('audit_id_usuario', 'administrador', '{administrador_nombres} {administrador_apellidos}');
        $crud->order_by('audit_fecha', 'DESC');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $ruta = 'Registro de Publicaciones';
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function smtp() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('smtp');
        $crud->set_subject('SMTP');
        $crud->display_as('smtp_host', 'Host');
        $crud->display_as('smtp_user', 'User');
        $crud->display_as('smtp_pass', 'Password');
        $crud->display_as('smtp_port', 'Port');
        $crud->display_as('smtp_email', 'Email');
        $crud->set_rules('smtp_email', 'Email', 'valid_email');
        $crud->unset_add();
        $crud->unset_delete();
        // $crud->unset_edit();
        $ruta = 'SMTP';
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function audit_administrador() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('audit_administrador');
        $crud->set_subject('Logs');
        $crud->display_as('audit_administrador_nombre', 'Usuario');
        $crud->display_as('audit_administrador_fecha', 'Fecha');
        $crud->display_as('audit_administrador_ip', 'IP');
        $crud->display_as('audit_administrador_agente', 'Agente');
        $crud->order_by('audit_administrador_fecha', 'DESC');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $ruta = 'Logs Usuarios';
        $output = $crud->render();
        $this->_example_output($output, NULL, $ruta);
    }
    
        public function config_general() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('config_general');
        $crud->set_subject('Otras Configuraciones');
        $crud->display_as('config_general_portal_ninos', 'Enlace Portal Niños');
        $crud->display_as('config_general_url_chat', 'URL Chat');
        $crud->display_as('config_general_script_chat', 'Script Chat');
        $crud->display_as('config_general_url_pqrs', 'Enlace PQRS');
        $crud->display_as('config_general_pqrs_siglas', 'Sigla PQRS');
        $crud->display_as('config_general_video_ayuda', 'Ayudas Navegación ');
        $crud->unset_texteditor('config_general_url_chat', 'config_general_script_chat');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'SMTP';
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function recordatorios() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('alertas_definicion');
        $crud->set_subject('Alerta');
        $crud->display_as('alertas_definicion_tabla', 'Sección');
        $crud->display_as('alertas_definicion_email_notificacion', 'Correo Electrónico de Notificación');
        $crud->display_as('alertas_definicion_tipo_periocidad', 'Tipo de Periodicidad');
        $crud->display_as('alertas_definicion_periocidad', 'Cantidad de Periodicidad');
        $crud->display_as('alertas_definicion_usuario', 'Responsable de la Actuación ');
        $crud->display_as('alertas_definicion_last_email', 'Ultima Notificación');
        $crud->display_as('alertas_definicion_texto', 'Texto Personalizado de la Notificación ');
        $crud->field_type('alertas_definicion_tipo_periocidad', 'dropdown', array('1' => 'Días', '2' => 'Meses', '3' => 'Años'));
        $dias = array();
        for ($x = 1; $x <= 365; $x++) {
            $dias[$x] = $x;
        }
        $crud->field_type('alertas_definicion_periocidad', 'dropdown', $dias);
        $tablas = array(
            'anticorrupcion_definicion' => 'Plan Anticorrupción (7.2)',
            'archivos_datos_abiertos' => 'Archivos de Datos Abiertos (12.2)',
            'catalago_datos_abiertos' => 'Catalogo de Datos Abiertos (12.1)',
            'contratacion_avance_contractual' => 'Avances Contractuales (9.3.1)',
            'contratacion_contratos' => 'Procesos de Contratación (9.3)',
            'contratacion_convenios' => 'Convenios (9.4)',
            'contratacion_lineamientos' => 'Lineamientos de Adquisiciones y Compras (9.5)',
            'contratacion_plan_compras' => 'Plan de Compras (9.1)',
            'control_auditorias' => 'Auditorias (8.2)',
            'control_defensa_judicial' => 'Defensa Judicial (8.11)',
            'control_indicadores_gestion' => 'Metas e Indicadores de Gestión (7.6)',
            'control_informe_archivo' => 'Informes de Archivo (7.9.3)',
            'control_informe_ciudadania' => 'Informe de Rendición de Cuenta a los Ciudadanos (8.4)',
            'control_informe_concejo' => 'Informe al Congreso/Asamblea/Concejo (8.3)',
            'control_informe_empalme' => 'Informes de Empalme (7.8)',
            'control_informe_fiscal' => 'Informes de Rendición Fiscal (8.5)',
            'control_planes_mejoramiento' => 'Planes de Mejoramiento (8.6)',
            'control_plan_accion' => 'Plan de Acción (7.4)',
            'control_plan_accion_avances' => 'Avances del Plan de Acción (7.4.1)',
            'control_plan_estrategico' => 'Plan de desarrollo Vigente (7.3)',
            'control_poblacion_vulnerable_programas' => 'Programas para la Población Vulnerable (8.9)',
            'control_poblacion_vulnerable_programas_avances' => 'Avances de los Programas para la Población Vulnerable (8.9.1)',
            'control_poblacion_vulnerable_proyectos' => 'Proyectos para la Población Vulnerable (8.10)',
            'control_poblacion_vulnerable_proyectos_avances' => 'Avances de los Proyectos para la Población Vulnerable (8.10.1)',
            'control_reportes_control_interno' => 'Resportes de Control Interno (8.7)',
            'convocatorias' => 'Convocatorias (2.8)',
            'convocatorias_anexos' => 'Convocatorias Anexos (2.8.1)',
            'entidad_glosario' => 'Glosario (2.2)',
            'entidad_mipg_plan' => 'Plan de Acción MIPG (7.11)',
            'entidad_mipg_plan_avances' => 'Avances del Plan de Acción MIPG (7.11.1)',
            'entidad_mipg_planes' => 'Planes de MIPG (7.12)',
            'entidad_planta_personal' => 'Planta de Personal (3.4)',
            'entidad_politicas_datos' => 'Políticas y Protección de Datos (1.3)',
            'entidad_preguntas_frecuentes' => 'Preguntas y Respuestas Frecuentes (2.1)',
            'entidad_procedimientos' => 'Calidad - Procedimientos (11.3)',
            'estudios' => 'Estudios, investigaciones y otras publicaciones (2.9)',
            'financiera_estados_financieros' => 'Estados Financieros (6.2)',
            'financiera_presupuesto' => 'Presupuesto Aprobado en Ejercicio (6.1)',
            'financiera_presupuesto_ejecucion' => 'Ejecución de Presupuesto Aprobado en Ejercicio (6.1.1)',
            'informacion_adicional' => 'Información Adicional (2.10)',
            'informacion_clasificada_reservada' => 'Índice de Información Clasificada y Reservada (12.5)',
            'mapas_datos_abiertos' => 'Mapas de Datos Abiertos (12.3)',
            'planeacion_plan_gasto_publico' => 'Plan de Gasto Publico (7.10)',
            'registro_activos_informacion' => 'Registro de Activos de Información (12.4)',
            'servicios_formatos' => 'Formularios de trámites y servicios (10.3)',
            'servicios_pqrdf' => 'Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) (10.4)',
            'servicios_tramites' => 'Listado de Trámites y Servicios en línea (10.2)',
            'survey' => 'Encuesta'
        );
        asort($tablas);
        $crud->field_type('alertas_definicion_tabla', 'dropdown', $tablas);
        $crud->field_type('alertas_definicion_last_email', 'hidden');
        $crud->unset_texteditor('alertas_definicion_texto');
        $crud->required_fields('alertas_definicion_tabla', 'alertas_definicion_email_notificacion', 'alertas_definicion_tipo_periocidad', 'alertas_definicion_periocidad');
        $crud->set_rules('alertas_definicion_email_notificacion', 'Correo Electrónico de Notificación', 'valid_email|required');
        $crud->columns('alertas_definicion_tabla', 'alertas_definicion_email_notificacion', 'Periodicidad', 'Ultima Actualización', 'Diferencia', 'alertas_definicion_last_email');
        $crud->callback_column('Ultima Actualización', array($this, '_callback_last_check'));
        $crud->callback_column('Diferencia', array($this, '_callback_diferencia'));
        $crud->callback_column('Periodicidad', array($this, '_callback_periodicidad'));
        $ruta = 'Alertas y Recordatorios';
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta);
    }

    public function _callback_periodicidad($value, $row) {
        switch ($row->alertas_definicion_tipo_periocidad) {
            case 1:
                return "Cada $row->alertas_definicion_periocidad Días";
                break;
            case 2:
                return "Cada $row->alertas_definicion_periocidad Meses";
                break;
            case 3:
                return "Cada $row->alertas_definicion_periocidad Años";
                break;
        }
    }

    public function _callback_last_check($value, $row) {
        return $this->get_input_date($row->id_alertas_definicion);
    }

    public function _callback_diferencia($value, $row) {
        $input_date = $this->get_input_date($row->id_alertas_definicion);
        if ($input_date != "No Registra") {
            $datos_alerta = $this->Referenciales_nodo_model->get_datos_alerta($row->id_alertas_definicion);
            $datetime1 = new DateTime('now');
            $datetime2 = new DateTime($input_date);
            $interval = $datetime1->diff($datetime2);
            switch ($datos_alerta->alertas_definicion_tipo_periocidad) {
                case 1:
                    $diferencia = $datos_alerta->alertas_definicion_periocidad - $interval->format('%a');
                    break;
                case 2:
                    $diferencia = ($datos_alerta->alertas_definicion_periocidad * 30) - $interval->format('%a');
                    break;
                case 3:
                    $diferencia = ($datos_alerta->alertas_definicion_periocidad * 365) - $interval->format('%a');
                    break;
            }
            $clase = 'good';
            if ($diferencia < ($datos_alerta->alertas_definicion_periocidad / 2)) {
                $clase = 'wait';
            }
            if ($diferencia < 0) {
                $clase = 'bad';
            }
            return "<span class='$clase'>" . $interval->format('%y Años, %m Meses, %d Días') . '</span>';
        } else {
            return $input_date;
        }
    }

    public function get_input_date($id) {
        $datos = $this->Referenciales_nodo_model->get_last_check($id);
        $datos_alerta = $this->Referenciales_nodo_model->get_datos_alerta($id);
        $datos_tabla = $this->Referenciales_nodo_model->get_datos_tabla($datos_alerta->alertas_definicion_tabla);
        $return = "No Registra";
        /* Verifici en la tabla audit */
        if ($datos != FALSE) {
            $return = $datos->audit_fecha;
        } else {
            /* Verifico si en su propia tabla se definio */
//            $fiel = $datos_alerta->alertas_definicion_tabla . '_fecha_ingreso';
//            if (!isset($datos_tabla->$fiel)) {
//                $return = $datos_tabla->$fiel;
//            }
        }
        return $return;
    }

}
