<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_alertas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->model('Datos_entidad_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->helper('url');
    }

    public function index() {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        $alertas = $this->Referenciales_nodo_model->get_alertas_definicion();

        if ($alertas != FALSE) {
            foreach ($alertas as $row) {
                $resultado = $this->_callback_diferencia($row);
                if ($resultado[0] == 1) {
                    /* PARA LOS QUE YA SE VENCIERON */
                    if ($resultado[2] <= 0) {
                        //  echo '<br> menor ' . $resultado[2];

                        $datos = array(
                            'alertas_definicion_last_email' => date('Y-m-d H:i:s')
                        );
                        $this->Referenciales_nodo_model->update_alertas_definicion($datos, $row->id_alertas_definicion);
                        $this->email_send($row, $resultado);



                        /* LOS QUE ESTAN DENTRO DE OS ULTIMOS 10 DIAS */
                    } elseif ($resultado[2] > 0 && $resultado[2] <= 10) {
                        $datos = array(
                            'alertas_definicion_last_email' => date('Y-m-d H:i:s')
                        );
                        $this->Referenciales_nodo_model->update_alertas_definicion($datos, $row->id_alertas_definicion);
                        $this->email_send($row, $resultado);
                        //  echo '<br> mayor ' . $resultado[2];


                        /* LOS QUE ESTAN OK */
                    } else {
                        //  echo '<br> ok ' . $resultado[2];
                    }
                }
            }
        }

//        if ($envios != FALSE) {
//            $datos = array(
//                'cron_newsletter_estado' => 1,
//                'cron_newsletter_fecha_envio' => date('Y-m-d H:i:s')
//            );
//            $this->Datos_entidad_model->update_cron_newsletter($datos, $envio->cron_newsletter_alias);
//            foreach ($envios as $envio) {
//                $this->email_send($envio->cron_newsletter_alias);
//            }
//        }
    }

    public function email_send($row, $resultado) {
        $this->load->model('Datos_municipio_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->library('encrypt');
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = 'contacto@codweb.co';
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $this->email->initialize($config);
        $entidad = $this->Datos_entidad_model->obtener_datos_contacto();
        $entidad_logo = $this->Datos_entidad_model->obtener_entidad_logo();
        $nombre_tabla = $this->get_nombre_tabla($row->alertas_definicion_tabla);
        $ultimo_ingreso = $this->get_input_date($row->id_alertas_definicion);
        $periodicidad = $this->_callback_periodicidad($row);
        if ($resultado[2] <= 0) {
            $asunto = 'Alerta de publicación vencida - ' . $nombre_tabla;
        } else {
            $asunto = 'Alerta de publicación pronto a vencerse - ' . $nombre_tabla;
        }

        echo 'Enviando Email<br/>';

        $this->email->from($sender, 'Alcaldía de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
        $this->email->to($row->alertas_definicion_email_notificacion);
        $this->email->subject($asunto);
        include 'template/alertas.php';
        $this->email->message($contenido);
        $this->email->send();

        echo 'Envio Completo';
    }

    public function _callback_periodicidad($row) {
        switch ($row->alertas_definicion_tipo_periocidad) {
            case 1:
                return "Cada $row->alertas_definicion_periocidad Días";
                break;
            case 2:
                return "Cada $row->alertas_definicion_periocidad Meses";
                break;
            case 3:
                return "Cada $row->alertas_definicion_periocidad Años";
                break;
        }
    }

    public function _callback_diferencia($row) {
        $input_date = $this->get_input_date($row->id_alertas_definicion);
        if ($input_date != "No Registra") {
            $datos_alerta = $this->Referenciales_nodo_model->get_datos_alerta($row->id_alertas_definicion);
            $datetime1 = new DateTime('now');
            $datetime2 = new DateTime($input_date);
            $interval = $datetime1->diff($datetime2);
            switch ($datos_alerta->alertas_definicion_tipo_periocidad) {
                case 1:
                    $diferencia = $datos_alerta->alertas_definicion_periocidad - $interval->format('%a');
                    break;
                case 2:
                    $diferencia = ($datos_alerta->alertas_definicion_periocidad * 30) - $interval->format('%a');
                    break;
                case 3:
                    $diferencia = ($datos_alerta->alertas_definicion_periocidad * 365) - $interval->format('%a');
                    break;
            }
            $clase = 'good';
            if ($diferencia < ($datos_alerta->alertas_definicion_periocidad / 2)) {
                $clase = 'wait';
            }
            if ($diferencia < 0) {
                $clase = 'bad';
            }
            return array(1, $clase, $diferencia, $interval, "<span class='$clase'>" . $interval->format('%y Años, %m Meses, %d Días') . '</span>');
        } else {
            return array(2, $input_date);
        }
    }

    public function get_input_date($id) {
        $datos = $this->Referenciales_nodo_model->get_last_check($id);
        $datos_alerta = $this->Referenciales_nodo_model->get_datos_alerta($id);
        $datos_tabla = $this->Referenciales_nodo_model->get_datos_tabla($datos_alerta->alertas_definicion_tabla);
        $return = "No Registra";
        /* Verifici en la tabla audit */
        if ($datos != FALSE) {
            $return = $datos->audit_fecha;
        } else {
            /* Verifico si en su propia tabla se definio */
//            $fiel = $datos_alerta->alertas_definicion_tabla . '_fecha_ingreso';
//            if (!isset($datos->$fiel)) {
//                $return = $datos_tabla->$fiel;
//            }
        }
        return $return;
    }

    function get_nombre_tabla($key) {
        $tablas = array(
            'anticorrupcion_definicion' => 'Plan Anticorrupción (7.2)',
            'archivos_datos_abiertos' => 'Archivos de Datos Abiertos (12.2)',
            'catalago_datos_abiertos' => 'Catalogo de Datos Abiertos (12.1)',
            'contratacion_avance_contractual' => 'Avances Contractuales (9.3.1)',
            'contratacion_contratos' => 'Procesos de Contratación (9.3)',
            'contratacion_convenios' => 'Convenios (9.4)',
            'contratacion_lineamientos' => 'Lineamientos de Adquisiciones y Compras (9.5)',
            'contratacion_plan_compras' => 'Plan de Compras (9.1)',
            'control_auditorias' => 'Auditorias (8.2)',
            'control_defensa_judicial' => 'Defensa Judicial (8.11)',
            'control_indicadores_gestion' => 'Metas e Indicadores de Gestión (7.6)',
            'control_informe_archivo' => 'Informes de Archivo (7.9.3)',
            'control_informe_ciudadania' => 'Informe de Rendición de Cuenta a los Ciudadanos (8.4)',
            'control_informe_concejo' => 'Informe al Congreso/Asamblea/Concejo (8.3)',
            'control_informe_empalme' => 'Informes de Empalme (7.8)',
            'control_informe_fiscal' => 'Informes de Rendición Fiscal (8.5)',
            'control_planes_mejoramiento' => 'Planes de Mejoramiento (8.6)',
            'control_plan_accion' => 'Plan de Acción (7.4)',
            'control_plan_accion_avances' => 'Avances del Plan de Acción (7.4.1)',
            'control_plan_estrategico' => 'Plan de desarrollo Vigente (7.3)',
            'control_poblacion_vulnerable_programas' => 'Programas para la Población Vulnerable (8.9)',
            'control_poblacion_vulnerable_programas_avances' => 'Avances de los Programas para la Población Vulnerable (8.9.1)',
            'control_poblacion_vulnerable_proyectos' => 'Proyectos para la Población Vulnerable (8.10)',
            'control_poblacion_vulnerable_proyectos_avances' => 'Avances de los Proyectos para la Población Vulnerable (8.10.1)',
            'control_reportes_control_interno' => 'Resportes de Control Interno (8.7)',
            'convocatorias' => 'Convocatorias (2.8)',
            'convocatorias_anexos' => 'Convocatorias Anexos (2.8.1)',
            'entidad_glosario' => 'Glosario (2.2)',
            'entidad_mipg_plan' => 'Plan de Acción MIPG (7.11)',
            'entidad_mipg_plan_avances' => 'Avances del Plan de Acción MIPG (7.11.1)',
            'entidad_mipg_planes' => 'Planes de MIPG (7.12)',
            'entidad_planta_personal' => 'Planta de Personal (3.4)',
            'entidad_politicas_datos' => 'Políticas y Protección de Datos (1.3)',
            'entidad_preguntas_frecuentes' => 'Preguntas y Respuestas Frecuentes (2.1)',
            'entidad_procedimientos' => 'Calidad - Procedimientos (11.3)',
            'estudios' => 'Estudios, investigaciones y otras publicaciones (2.9)',
            'financiera_estados_financieros' => 'Estados Financieros (6.2)',
            'financiera_presupuesto' => 'Presupuesto Aprobado en Ejercicio (6.1)',
            'financiera_presupuesto_ejecucion' => 'Ejecución de Presupuesto Aprobado en Ejercicio (6.1.1)',
            'informacion_adicional' => 'Información Adicional (2.10)',
            'informacion_clasificada_reservada' => 'Índice de Información Clasificada y Reservada (12.5)',
            'mapas_datos_abiertos' => 'Mapas de Datos Abiertos (12.3)',
            'planeacion_plan_gasto_publico' => 'Plan de Gasto Publico (7.10)',
            'registro_activos_informacion' => 'Registro de Activos de Información (12.4)',
            'servicios_formatos' => 'Formularios de trámites y servicios (10.3)',
            'servicios_pqrdf' => 'Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) (10.4)',
            'servicios_tramites' => 'Listado de Trámites y Servicios en línea (10.2)',
            'survey' => 'Encuesta'
        );

        return $tablas[$key];
    }

}
