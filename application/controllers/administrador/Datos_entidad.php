<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datos_entidad extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('image_moo');
        $this->load->library('grocery_CRUD');
        $this->load->model('Referenciales_nodo_model');
        $this->load->model('Datos_entidad_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<span class="glyphicon glyphicon glyphicon-th-large"></span>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'entidad';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    /*     * **************************************************************INFORMACION GENERAL******************************************************************** */
    /*     * ****************************************************************************************************************************************************** */

    public function entidad_informacion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_informacion');
        $crud->set_subject('INFORMACIÓN GENERAL');
        $crud->display_as('entidad_informacion_nit', 'NIT');
        $crud->display_as('entidad_informacion_logo', 'Logo símbolo');
        $crud->display_as('entidad_informacion_mision', 'Misión');
        $crud->display_as('entidad_informacion_vision', 'Visión');
        $crud->display_as('entidad_informacion_presentacion', 'Presentación');
        $crud->display_as('entidad_informacion_funciones', 'Funciones');
        $crud->display_as('entidad_informacion_objetivos', 'Objetivos');
        $crud->display_as('entidad_informacion_organigrama', 'Organigrama');
        $crud->display_as('entidad_informacion_organigrama_acto', 'Organigrama Documento');
        $crud->display_as('entidad_informacion_protocolo_atencion', 'Protocolo de Atención al Ciudadano');
        $crud->display_as('entidad_informacion_costos_reproduccion', 'Costos de Reproducción de la Entidad ');
        $crud->display_as('entidad_informacion_carta_trato_digno', 'Carta de Trato Digno a los Ciudadanos ');
        $crud->set_field_upload('entidad_informacion_logo', 'uploads/entidad');
        $crud->set_field_upload('entidad_informacion_protocolo_atencion', 'uploads/entidad');
        $crud->set_field_upload('entidad_informacion_costos_reproduccion', 'uploads/entidad');
        $crud->set_field_upload('entidad_informacion_carta_trato_digno', 'uploads/entidad');
        $crud->set_field_upload('entidad_informacion_organigrama', 'uploads/entidad');
        $crud->required_fields('entidad_informacion_presentacion', 'entidad_informacion_nit', 'entidad_informacion_mision', 'entidad_informacion_vision', 'entidad_informacion_funciones', 'entidad_informacion_objetivos', 'entidad_informacion_organigrama');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Información General';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'Información general de la entidad pública que la identifica y hace referencia de sus funciones, objetivos y estructura organizacional.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1, $ruta, $ruta_tab, $texto);
    }

    public function entidad_contacto() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_contacto');
        $crud->set_subject('DATOS DE CONTACTO');
        $crud->display_as('entidad_contacto_email', 'Correo Electrónico');
        $crud->display_as('entidad_contacto_email_notificaciones_judiciales', 'Correo Electrónico Notificaciones Judiciales');
        $crud->display_as('entidad_contacto_telefono', 'Teléfono');
        $crud->display_as('entidad_contacto_fax', 'Fax');
        $crud->display_as('entidad_contacto_linea_gratuita', 'Línea Telefónica Gratuita');
        $crud->display_as('entidad_contacto_horarios_atencion', 'Horarios de Atención');
        $crud->display_as('entidad_contacto_direccion', 'Dirección');
        $crud->display_as('entidad_contacto_lineas', 'Líneas de Atencion');
        $crud->display_as('entidad_contacto_politicas_seguridad', 'Políticas de Seguridad');
        $crud->display_as('entidad_contacto_google_mapa', 'Google Mapa');
       // $crud->set_rules('entidad_contacto_email', 'Correo Electrónico', 'valid_email');
       // $crud->set_rules('entidad_contacto_email_notificaciones_judiciales', 'Correo Electrónico Notificaciones Judiciales', 'valid_email');
        $crud->unset_texteditor('entidad_contacto_google_mapa', 'entidad_contacto_horarios_atencion', 'entidad_contacto_direccion', 'entidad_contacto_lineas');
        $crud->required_fields('entidad_contacto_email', 'entidad_contacto_email_notificaciones_judiciales', 'entidad_contacto_telefono', 'entidad_contacto_fax', 'entidad_contacto_horarios_atencion', 'entidad_contacto_direccion', 'entidad_contacto_politicas_seguridad');
        $crud->unset_columns('entidad_contacto_google_mapa');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Datos de Contacto';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'Información de contacto y ubicación física de la entidad pública, incluyendo el correo electrónico para notificaciones judiciales, horarios de atención al público y políticas de seguridad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 2, $ruta, $ruta_tab, $texto);
    }

    public function entidad_politicas_datos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_politicas_datos');
        $crud->set_subject('POLÍTICAS Y PROTECCIÓN DE DATOS');
        $crud->display_as('entidad_politicas_datos_nombre', 'Nombre');
        $crud->display_as('entidad_politicas_datos_archivo', 'Archivo');
        $crud->required_fields('entidad_politicas_datos_archivo', 'entidad_politicas_datos_nombre');
        $crud->set_field_upload('entidad_politicas_datos_archivo', 'uploads/entidad/politicas');
        $ruta = 'Políticas y Protección de Datos';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'La entidad debe tener un enlace que dirija a las políticas de la seguridad de la información y protección de datos personales de la entidad, política de cero papel, políticas editorial y de actualización y condiciones de unos del sitio web.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 3, $ruta, $ruta_tab, $texto);
    }

    public function entidad_entidades() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_entidades');
        $crud->set_subject('ENTIDADES');
        $crud->display_as('entidad_entidades_nombre', 'Nombre de la Entidad');
        $crud->display_as('entidad_entidades_email', 'Correo Electrónico');
        $crud->display_as('entidad_entidades_telefono', 'Teléfono');
        $crud->display_as('entidad_entidades_fax', 'Fax');
        $crud->display_as('entidad_entidades_linea_gratuita', 'Línea Telefónica Gratuita');
        $crud->display_as('entidad_entidades_horarios_de_atencion', 'Horarios de Atención');
        $crud->display_as('entidad_entidades_direccion', 'Dirección');
        $crud->display_as('entidad_entidades_url', 'Sitio Web');
        $crud->set_rules('entidad_entidades_email', 'Correo Electrónico', 'valid_email');
        $crud->required_fields('entidad_entidades_nombre', 'entidad_entidades_email', 'entidad_entidades_telefono', 'entidad_entidades_direccion');
        $crud->unset_texteditor('entidad_entidades_horarios_de_atencion');
        $ruta = 'Directorio de Entidades';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'La entidad publica el listado de entidades que integran el mismo sector/rama/organismo, con enlace al sitio Web de cada una de éstas, en el caso de existir.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 7, $ruta, $ruta_tab, $texto);
    }

    public function entidad_sucursales() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_sucursales');
        $crud->set_subject('SUCURSALES O REGIONALES ');
        $crud->display_as('entidad_sucursales_nombre', 'Nombre de la Sucursal');
        $crud->display_as('entidad_sucursales_email', 'Correo Electrónico');
        $crud->display_as('entidad_sucursales_telefono', 'Teléfono');
        $crud->display_as('entidad_sucursales_fax', 'Fax');
        $crud->display_as('entidad_sucursales_linea_gratuita', 'Línea Telefónica Gratuita');
        $crud->display_as('entidad_sucursales_horarios_de_atencion', 'Horarios de Atención');
        $crud->display_as('entidad_sucursales_direccion', 'Dirección');
        $crud->set_rules('entidad_sucursales_email', 'Correo Electrónico', 'valid_email');
        $crud->required_fields('entidad_sucursales_nombre', 'entidad_sucursales_email', 'entidad_sucursales_telefono', 'entidad_sucursales_direccion');
        $crud->unset_texteditor('entidad_sucursales_horarios_de_atencion');
        $ruta = 'Sucursales o Regionales';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'Enlace a los datos de contacto de las sucursales o regionales que tenga la entidad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 6, $ruta, $ruta_tab, $texto);
    }

    public function entidad_agremiaciones() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_agremiaciones');
        $crud->set_subject('AGREMIACION');
        $crud->display_as('entidad_agremiaciones_nombre', 'Nombre Agremiación ');
        $crud->display_as('entidad_agremiaciones_clasificacion', 'Clasificación');
        $crud->display_as('entidad_agremiaciones_actividad', 'Actividad Principal ');
        $crud->display_as('entidad_agremiaciones_email', 'Correo Electrónico');
        $crud->display_as('entidad_agremiaciones_telefono', 'Teléfono');
        $crud->display_as('entidad_agremiaciones_fax', 'Fax');
        $crud->display_as('entidad_agremiaciones_url', 'Sitio Web');
        $crud->display_as('entidad_agremiaciones_direccion', 'Dirección');
        $crud->set_rules('entidad_agremiaciones_email', 'Correo Electrónico', 'valid_email');
        $crud->required_fields('entidad_agremiaciones_nombre', 'entidad_agremiaciones_clasificacion', 'entidad_agremiaciones_actividad');
        $ruta = 'Directorio de Agremiaciones, asociaciones y otros grupos de interés ';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'La entidad publica el listado de las principales agremiaciones o asociaciones relacionadas con la actividad propia de la entidad, con enlace al sitio Web de cada una de éstas. Así mismo publica los datos de contacto de los principales grupos de interés y/u organizaciones sociales o poblacionales.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 8, $ruta, $ruta_tab, $texto);
    }

    public function entidad_dependencia() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_dependencia');
        $crud->set_subject('DEPENDENCIA');
        $crud->display_as('entidad_dependencia_nombre', 'Nombre');
        $crud->display_as('entidad_dependencia_email', 'Correo Electrónico');
        $crud->display_as('entidad_dependencia_telefono', 'Teléfono');
        $crud->display_as('entidad_dependencia_fax', 'Fax');
        $crud->display_as('entidad_dependencia_horarios_de_atencion', 'Horarios de Atención');
        $crud->display_as('entidad_dependencia_objetivos', 'Objetivos');
        $crud->display_as('entidad_dependencia_funciones', 'Funciones');
        $crud->display_as('entidad_dependencia_informacion_adicional', 'Información Adicional ');
        $crud->display_as('id_entidad_funcionario', 'Funcionario Responsable');
        $crud->set_rules('entidad_dependencia_email', 'Correo Electrónico', 'valid_email|required');
        $crud->required_fields('entidad_dependencia_nombre', 'entidad_dependencia_email', 'entidad_dependencia_telefono');
        $crud->set_relation('id_entidad_funcionario', 'entidad_funcionario', '{entidad_funcionario_nombres} {entidad_funcionario_apellidos}');
        $crud->unset_texteditor('entidad_dependencia_direccion', 'entidad_dependencia_horarios_de_atencion');
        $ruta = 'Dependencias';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'Información general y de contacto de cada una de las dependencias, secretarias o departamentos con los que cuenta la entidad publica. ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 5, $ruta, $ruta_tab, $texto);
    }

    public function entidad_funcionario() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_funcionario');
        $crud->set_subject('FUNCIONARIO');
        $crud->display_as('entidad_funcionario_nombres', 'Nombres');
        $crud->display_as('entidad_funcionario_apellidos', 'Apellidos');
        $crud->display_as('entidad_funcionario_email', 'Correo Electrónico');
        $crud->display_as('entidad_funcionario_telefono', 'Teléfono');
        $crud->display_as('entidad_funcionario_foto', 'Fotografía');
        $crud->display_as('entidad_funcionario_profesion', 'Profesión');
        $crud->display_as('entidad_funcionario_cargo', 'Cargo');
        $crud->display_as('entidad_funcionario_perfil', 'Perfil');
        $crud->display_as('entidad_funcionario_tipo_vinculacion', 'Tipo de Vinculación');
        $crud->display_as('entidad_funcionario_manual_de_funciones', 'Manual de Funciones ');
        $crud->display_as('entidad_funcionario_evaluacion', 'Evaluación del Desempeño');
        $crud->display_as('id_entidad_dependencia', 'Dependencia en la que Labora');
        $crud->set_rules('entidad_funcionario_email', 'Correo Electrónico', 'valid_email');
        $crud->required_fields('entidad_funcionario_nombres', 'entidad_funcionario_apellidos', 'entidad_funcionario_email', 'entidad_funcionario_telefono', 'entidad_funcionario_profesion', 'entidad_funcionario_cargo', 'entidad_funcionario_perfil');
        $crud->set_relation('id_entidad_dependencia', 'entidad_dependencia', 'entidad_dependencia_nombre');
        $crud->set_field_upload('entidad_funcionario_evaluacion', 'uploads/entidad/funcionarios');
        $crud->set_field_upload('entidad_funcionario_manual_de_funciones', 'uploads/entidad/funcionarios');
        $crud->set_field_upload('entidad_funcionario_hoja_de_vida', 'uploads/entidad/funcionarios');
        $crud->set_field_upload('entidad_funcionario_foto', 'uploads/entidad/funcionarios');
        $crud->field_type('entidad_funcionario_tipo_vinculacion', 'dropdown', array('Vinculado' => 'Vinculado', 'Contratista' => 'Contratista', 'Asesor' => 'Asesor', 'Consultor' => 'Consultor', 'Otro' => 'Otro'));
        $crud->unset_texteditor('entidad_funcionario_perfil');
        $crud->callback_after_upload(array($this, 'redim_foto_funcionario'));
        $ruta = 'Funcionarios';
        $ruta_tab = 'Talento Humano ';
        $texto = 'La entidad publica el perfil y hoja de vida de sus servidores públicos principales, según su organigrama. La entidad debe actualizar esta información cada vez que ingresa un funcionario o cada vez que se desvincula.<br>Igualmente  publica los datos de contacto de toda la planta de personal, incluyendo contratistas. Los datos a publicar son: Nombre completo, profesión, cargo, correo electrónico, número de teléfono. Se actualiza cada vez que hay cambio de servidores públicos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 101, $ruta, $ruta_tab, $texto);
    }

    function redim_foto_funcionario($uploader_response, $field_info, $files_to_upload) {

        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize_crop(150, 200, FALSE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();

        return true;
    }

    public function entidad_manual_de_funciones() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_manual_de_funciones');
        $crud->set_subject('MANUAL DE FUNCIONES');
        $crud->display_as('entidad_manual_de_funciones_nombre', 'Nombre');
        $crud->display_as('entidad_manual_de_funciones_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_manual_de_funciones_fecha_actualizacion', 'Fecha de Actualización');
        $crud->display_as('entidad_manual_de_funciones_archivo', 'Archivo');
        $crud->display_as('id_entidad_definicion_manual_de_funciones', 'Tipo');
        $crud->set_field_upload('entidad_manual_de_funciones_archivo', 'uploads/entidad/funcionarios');
        $crud->field_type('entidad_manual_de_funciones_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_manual_de_funciones_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->set_relation('id_entidad_definicion_manual_de_funciones', 'entidad_definicion_manual_de_funciones', 'entidad_definicion_manual_de_funciones_nombre');
        $crud->required_fields('entidad_manual_de_funciones_nombre', 'entidad_manual_de_funciones_archivo', 'id_entidad_definicion_manual_de_funciones');
        $crud->edit_fields('entidad_manual_de_funciones_nombre', 'entidad_manual_de_funciones_archivo', 'id_entidad_definicion_manual_de_funciones', 'entidad_manual_de_funciones_fecha_actualizacion');
        $ruta = 'Manual de Funciones';
        $ruta_tab = 'Talento Humano';
        $texto = 'La entidad publica en su sitio web el manual de funciones y competencias. <br />La entidad publica estos documentos cada vez que se cambian o ajustan.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 102, $ruta, $ruta_tab, $texto);
    }

    public function entidad_definicion_manual_de_funciones() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_definicion_manual_de_funciones');
        $crud->set_subject('TIPOS MANUAL DE FUNCIONES');
        $crud->display_as('entidad_definicion_manual_de_funciones_nombre', 'Nombre');
        $crud->required_fields('entidad_definicion_manual_de_funciones_nombre');
        $ruta = 'Tipos Manual de Funciones';
        $ruta_tab = 'Talento Humano';
        $texto = 'La entidad publica en su sitio web el manual de funciones y competencias. <br />La entidad publica estos documentos cada vez que se cambian o ajustan.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 106, $ruta, $ruta_tab, $texto);
    }

    public function entidad_asignaciones_salarial() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_asignaciones_salarial');
        $crud->set_subject('ASIGNACIONES SALARIALES');
        $crud->display_as('entidad_asignaciones_salarial_escala_salarial', 'Escala Salarial');
        $crud->display_as('entidad_asignaciones_salarial_escala_viaticos', 'Escala de Viáticos');
        $crud->set_field_upload('entidad_asignaciones_salarial_escala_salarial', 'uploads/entidad/asignacion_salarial');
        $crud->set_field_upload('entidad_asignaciones_salarial_escala_viaticos', 'uploads/entidad/asignacion_salarial');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Asignaciones Salariales';
        $ruta_tab = 'Talento Humano';
        $texto = 'La entidad publica en su sitio web la escala salarial correspondiente a las categorías de todos los servidores que trabajan en la entidad, independientemente de su calidad de empleados, asesores, consultores o cualquier otra modalidad de contrato. Igualmente, se debe publicar la escala de viáticos Se actualiza cada vez que hay cambios en la escala salarial';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 103, $ruta, $ruta_tab, $texto);
    }

    public function entidad_planta_personal() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_planta_personal');
        $crud->set_subject('PLANTA PERSONAL');
        $crud->display_as('entidad_planta_personal_archivo', 'Embebido');
        $crud->unset_texteditor('entidad_planta_personal_archivo');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Planta de Personal';
        $ruta_tab = 'Talento Humano';
        $texto = 'Datos de contacto de toda la planta de personal, incluyendo contratistas. Los datos a publicar son: Nombre completo, profesión, cargo, correo electrónico, número de teléfono. Se actualiza cada vez que hay cambio de servidores públicos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 104, $ruta, $ruta_tab, $texto);
    }

    public function entidad_evaluacion_desempeno() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_evaluacion_desempeno');
        $crud->set_subject('EVALUACIÓN DE DESEMPEÑO GENERAL');
        $crud->display_as('entidad_evaluacion_desempeno_archivo', 'Archivo');
        $crud->set_field_upload('entidad_evaluacion_desempeno_archivo', 'uploads/entidad/funcionarios');
        $crud->unset_delete();
        $crud->unset_add();
        $ruta = 'Evaluación de Desempeño ';
        $ruta_tab = 'Talento Humano';
        $texto = 'Evaluación de desempeño  de toda la planta de personal, incluyendo contratistas.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 105, $ruta, $ruta_tab, $texto);
    }

    public function entidad_redes_sociales() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_redes_sociales');
        $crud->set_subject('RED SOCIAL');
        $crud->display_as('entidad_redes_sociales_link', 'Link de Enlace');
        $crud->display_as('id_entidad_redes_sociales_definicion', 'Red Social');
        $crud->required_fields('entidad_redes_sociales_link', 'id_entidad_redes_sociales_definicion');
        $crud->set_relation('id_entidad_redes_sociales_definicion', 'entidad_redes_sociales_definicion', 'entidad_redes_sociales_definicion_nombre');
        $ruta = 'Redes Sociales';
        $ruta_tab = 'Información General de la Entidad';
        $texto = 'Enlace a las redes sociales con los cuales cuenta la entidad publica. ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 9, $ruta, $ruta_tab, $texto);
    }

    public function entidad_preguntas_frecuentes() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_preguntas_frecuentes');
        $crud->set_subject('PREGUNTA Y RESPUESTA  FRECUENTE ');
        $crud->display_as('entidad_preguntas_frecuentes_pregunta', 'Pregunta');
        $crud->display_as('entidad_preguntas_frecuentes_respuesta', 'Respuesta');
        $crud->display_as('id_dependencia', 'Dependencia');
        $crud->required_fields('entidad_preguntas_frecuentes_pregunta', 'entidad_preguntas_frecuentes_respuesta');
        $crud->unset_texteditor('entidad_preguntas_frecuentes_pregunta', 'entidad_preguntas_frecuentes_respuesta');
        $crud->set_relation('id_dependencia', 'entidad_dependencia', 'entidad_dependencia_nombre');
        $ruta = 'Preguntas y Respuestas Frecuentes';
        $ruta_tab = 'Servicios de Información';
        $texto = 'La entidad ofrece una lista de respuestas a preguntas frecuentes relacionadas con la entidad, su gestión y los servicios y trámites que presta.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 51, $ruta, $ruta_tab, $texto);
    }

    public function entidad_glosario() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_glosario');
        $crud->set_subject('Palabra');
        $crud->display_as('entidad_glosario_palabra', 'Palabra');
        $crud->display_as('entidad_glosario_descripcion', 'Descripción ');
        $crud->required_fields('entidad_glosario_palabra', 'entidad_glosario_descripcion');
        $crud->unset_texteditor('entidad_glosario_descripcion');
        $ruta = 'Glosario';
        $ruta_tab = 'Servicios de Información';
        $texto = 'La entidad ofrece un glosario que contenga el conjunto de términos que usa la entidad o que tienen relación con su actividad.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 52, $ruta, $ruta_tab, $texto);
    }

    public function entidad_noticias() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_noticias');
        $crud->set_subject('NOTICIA');
        $crud->display_as('entidad_noticias_titulo', 'Titulo');
        $crud->display_as('entidad_noticias_alias', 'Alias');
        $crud->display_as('entidad_noticias_tipo', 'Tipo de Noticia');
        $crud->display_as('entidad_noticias_descripcion_corta', 'Descripción Corta ');
        $crud->display_as('entidad_noticias_descripcion', 'Descripción');
        $crud->display_as('entidad_noticias_imagen', 'Imagen Destacada ');
        $crud->display_as('entidad_noticias_rights_imagen', 'Texto Imagen');
        $crud->display_as('entidad_noticias_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('entidad_noticias_actualizacion', 'Última Actualización ');
        $crud->display_as('id_estado_de_publicacion', 'Estado de la Publicación ');
        $crud->display_as('entidad_noticias_enlace_imagen', 'Enlace Imagen Principal');
        $crud->display_as('entidad_noticias_enlace_tipo', 'Tipo de Enlace');
        $crud->display_as('entidad_noticias_tags', 'Tags');
        $crud->display_as('entidad_noticias_audio', 'Audio');
        $crud->field_type('entidad_noticias_enlace_tipo', 'dropdown', array('_self' => 'Misma Ventana', '_blank' => 'Nueva Ventana'));
        $crud->required_fields('entidad_noticias_alias', 'entidad_noticias_descripcion_corta', 'entidad_noticias_tipo', 'id_estado_de_publicacion', 'entidad_noticias_titulo', 'entidad_noticias_descripcion', 'entidad_noticias_fecha_ingreso', 'entidad_noticias_tags', 'entidad_noticias_audio','entidad_noticias_imagen');
        $crud->set_relation('id_estado_de_publicacion', 'estado_de_publicacion', 'estado_de_publicacion_estado');
        $crud->unset_texteditor('entidad_noticias_descripcion_corta');
        $crud->set_field_upload('entidad_noticias_imagen', 'uploads/noticias/');
        $crud->set_field_upload('entidad_noticias_audio', 'uploads/noticias/');
        // $crud->field_type('entidad_noticias_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_noticias_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_noticias_newsletter', 'hidden');
        $crud->field_type('entidad_noticias_tipo', 'hidden', 1);
        $crud->order_by('entidad_noticias_fecha_ingreso', 'desc');
        $crud->columns( 'entidad_noticias_titulo', 'entidad_noticias_descripcion_corta', 'entidad_noticias_imagen', 'entidad_noticias_fecha_ingreso', 'entidad_noticias_actualizacion', 'Enviar a Suscriptores');
        $crud->callback_after_upload(array($this, 'redim_slider'));
        //      $crud->callback_before_upload(array($this, 'check_image_size'));
        //    $crud->callback_before_delete(array($this, 'delete_imagen'));
        $crud->callback_before_update(array($this, 'check_image'));
        $crud->callback_before_insert(array($this, 'check_alias'));
        //  $crud->callback_after_insert(array($this, 'newsletter_noticias'));
        $crud->callback_column('Enviar a Suscriptores', array($this, '_callback_envio'));
        $crud->edit_fields('entidad_noticias_tipo', 'entidad_noticias_titulo', 'entidad_noticias_descripcion_corta', 'entidad_noticias_descripcion', 'entidad_noticias_imagen', 'entidad_noticias_rights_imagen', 'entidad_noticias_enlace_imagen', 'entidad_noticias_tags', 'entidad_noticias_enlace_tipo', 'entidad_noticias_fecha_ingreso', 'entidad_noticias_actualizacion', 'id_estado_de_publicacion', 'entidad_noticias_audio');
        $ruta = 'Noticias';
        $ruta_tab = 'Servicios de Información';
        $texto = 'Lugar donde se publicaran  las noticias más relevantes para la entidad y que estén relacionadas con su actividad.<br />'
                . 'Recomendaciones<br/>'
                . '•	Imagen panorámica, es decir que el ancho sea mayor que el alto, de lo contrario afectara el diseño del sitio.<br />
•	El tamaño mínimo en pixeles debe ser de 797 PX de ancho X 450 PX de alto.<br />
•	El tamaño máximo en peso debe ser de 1 MB, no se permiten fotos más pesadas pues afectara el rendimiento del sitio web.<br />
•	Buena resolución en la foto<br />
•	Que el foco de la foto se encuentre en el medio, pues si la foto no cumple con el tamaño especifico este la cortara de modo tal que se puede perder información.
';
        $crud->set_rules('entidad_noticias_alias', 'Alias', 'callback_check_alias_unique');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 53, $ruta, $ruta_tab, $texto);
    }

    public function _callback_envio($value, $row) {
        $noticia = $this->Datos_entidad_model->obtener_noticia($row->entidad_noticias_alias);
        if ($noticia != FALSE) {
            if ($noticia->entidad_noticias_newsletter == 1) {
                return '<button class="btn btn-block btn-sm btn-default" disabled><i class="fas fa-check-square"></i> Enviado</button>';
            } else {
                return "<a id='noticia_$noticia->entidad_noticias_alias' onclick='return false;' class='btn btn-primary btn-sm btn-block noticias-newsletter' href='#'><i class='fas fa-share-square'></i> Enviar</a>";
            }
        } else {
            return "";
        }
    }

    function check_image_size($files_to_upload, $field_info) {
        if ($field_info->field_name == 'entidad_noticias_imagen') {
            foreach ($files_to_upload as $file) {
                $data = getimagesize($file['tmp_name']);
            }
            if ($data[0] > $data [1]) {
                return TRUE;
            } else {
                return 'La imagen debe ser panorámica, el alto no puede ser mayor que el ancho ';
            }
        } else {
            return TRUE;
        }
    }

    function redim_slider($uploader_response, $field_info, $files_to_upload) {
        if ($field_info->field_name == 'entidad_noticias_imagen') {
            $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
            $file_uploaded_220x138 = $field_info->upload_path . '/220x138/' . $uploader_response[0]->name;
            $file_uploaded_notificaciones = $field_info->upload_path . '/notificaciones/' . $uploader_response[0]->name;
            $this->image_moo->load($file_uploaded)->resize_crop(277, 156)->save($file_uploaded_notificaciones, true);
            $this->image_moo->load($file_uploaded)->resize_crop(366, 206)->save($file_uploaded_220x138, true);
            $this->image_moo->load($file_uploaded)->resize_crop(961, 589, FALSE)->save($file_uploaded, true);
            $this->image_moo->clear_temp();
            return true;
        } else {
            return true;
        }
    }

    function delete_imagen($primary_key) {
        $image = $this->db->get_where('entidad_noticias', array('id_entidad_noticias' => $primary_key))->row();
        $url = 'uploads/noticias/carrousel/' . $image->entidad_noticias_imagen;
        $url_600x200 = 'uploads/noticias/220x138/' . $image->entidad_noticias_imagen;
        $thumb_url = 'uploads/noticias/' . $image->entidad_noticias_imagen;
        $notificacion = 'uploads/noticias/notificacion' . $image->entidad_noticias_imagen;
        unlink($url);
        unlink($url_600x200);
        unlink($thumb_url);
        unlink($notificacion);
        return true;
    }

    function check_image($post_array, $primary_key = NULL) {
        $image = $this->db->get_where('entidad_noticias', array('id_entidad_noticias' => $primary_key))->row();
        if ($post_array['entidad_noticias_imagen'] != $image->entidad_noticias_imagen) {
            $url = 'uploads/noticias/carrousel/' . $image->entidad_noticias_imagen;
            $url_600x200 = 'uploads/noticias/220x138/' . $image->entidad_noticias_imagen;
            $notificacion = 'uploads/noticias/notificacion' . $image->entidad_noticias_imagen;
            unlink($url);
            unlink($url_600x200);
            unlink($notificacion);
        }

        return true;
    }

    function check_alias($post_array, $primary_key = NULL) {

        $alias = $this->clear_alias($post_array['entidad_noticias_alias']);
        $post_array['entidad_noticias_alias'] = $alias;
        return $post_array;
    }

    function check_alias_unique($str) {

        $this->load->model('Datos_entidad_model');
        if ($this->Datos_entidad_model->check_alias_noticias($this->clear_alias($this->input->post('entidad_noticias_alias'))) == TRUE) {
            $this->form_validation->set_message('check_alias_unique', "El Alias ya existe.");
            return FALSE;
        } else {

            return TRUE;
        }
    }

    public function clear_alias($cadena) {
        $alias = str_replace(' ', '-', $cadena);
        $alias = preg_replace('/[^A-Za-z0-9 _\-\+\&]/', '', $alias);
        $alias = strtolower($alias);
        return $alias;
    }

    public function entidad_calendario() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_calendario');
        $crud->set_subject('ACTIVIDAD');
        $crud->display_as('entidad_calendario_nombre', 'Nombre');
        $crud->display_as('entidad_calendario_alias', 'Alias');
        $crud->display_as('entidad_calendario_descripcion', 'Descripción');
        $crud->display_as('entidad_calendario_imagen', 'Imagen Destacada ');
        $crud->display_as('entidad_calendario_fecha_inicio', 'Fecha de Inicio');
        $crud->display_as('entidad_calendario_fecha_fin', 'Fecha de Finalización');
        $crud->display_as('entidad_calendario_actualizacion', 'Última Actualización ');
        $crud->display_as('entidad_calendario_fecha_ingreso', 'Fecha de Ingreso ');
        $crud->display_as('entidad_calendario_dia', 'Todo el día');
        $crud->display_as('entidad_calendario_enlace_imagen', 'Enlace Imagen Principal');
        $crud->display_as('entidad_calendario_enlace_tipo', 'Tipo de Enlace');
        $crud->field_type('entidad_calendario_enlace_tipo', 'dropdown', array('_self' => 'Misma Ventana', '_blank' => 'Nueva Ventana'));
        $crud->display_as('id_estado_de_publicacion', 'Estado de la Publicación ');
        $crud->required_fields('entidad_calendario_alias', 'entidad_calendario_dia', 'entidad_calendario_nombre', 'entidad_calendario_descripcion', 'id_estado_de_publicacion');
        $crud->set_relation('id_estado_de_publicacion', 'estado_de_publicacion', 'estado_de_publicacion_estado');
        $crud->set_field_upload('entidad_calendario_imagen', 'uploads/eventos/');
        $crud->field_type('entidad_calendario_actualizacion', 'hidden', date('Y-m-d'));
        $crud->field_type('entidad_calendario_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->order_by('entidad_calendario_fecha_inicio', 'ASC');
        $crud->set_rules('entidad_calendario_fecha_inicio', 'Fecha de Finalización', 'callback_check_date');
        $crud->set_rules('entidad_calendario_alias', 'Alias', 'callback_check_alias_unique_calendario|required');
        $crud->callback_before_insert(array($this, 'todo_dia_callback'));
        $crud->callback_before_update(array($this, 'todo_dia_callback'));
        $crud->callback_before_insert(array($this, 'check_alias_calendario'));
        $crud->callback_after_upload(array($this, 'redim_calendario'));
        $crud->field_type('entidad_calendario_dia', 'dropdown', array('1' => 'Si', '2' => 'No'));
        $ruta = 'Calendario de Actividades';
        $ruta_tab = 'Servicios de Información';
        $texto = 'La entidad habilita un calendario de eventos y fechas clave relacionadas con los procesos misionales de la entidad. ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $crud->edit_fields('entidad_calendario_nombre', 'entidad_calendario_descripcion', 'entidad_calendario_imagen', 'entidad_calendario_enlace_imagen', 'entidad_calendario_enlace_tipo', 'entidad_calendario_actualizacion', 'entidad_calendario_fecha_inicio', 'entidad_calendario_fecha_fin', 'entidad_calendario_dia', 'id_estado_de_publicacion');
        $output = $crud->render();
        $this->_example_output($output, 54, $ruta, $ruta_tab, $texto);
    }

    function check_alias_calendario($post_array, $primary_key = NULL) {

        $alias = $this->clear_alias($post_array['entidad_calendario_alias']);
        $post_array['entidad_calendario_alias'] = $alias;
        return $post_array;
    }

    function check_alias_unique_calendario($str) {

        $this->load->model('Datos_entidad_model');
        if ($this->Datos_entidad_model->check_alias_calendario($this->clear_alias($this->input->post('entidad_calendario_alias'))) == TRUE) {
            $this->form_validation->set_message('check_alias_unique_calendario', "El Alias ya existe.");
            return FALSE;
        } else {

            return TRUE;
        }
    }

    function todo_dia_callback($post_array) {

        if ($post_array['entidad_calendario_dia'] == 1) {
            $post_array['entidad_calendario_fecha_inicio'] = date("Y-m-d") . ' 00:00:00';
            $post_array['entidad_calendario_fecha_fin'] = date("Y-m-d") . ' 23:59:59';
        } else {
            if ($post_array['entidad_calendario_fecha_inicio'] == NULL) {
                $this->form_validation->set_message('check_date', "La fecha de inicio no puede ser posterior a la fecha de finalización.");
                return FALSE;
            }
        };

        return $post_array;
    }

    public function check_date($str) {
        $partes = explode('/', $this->input->post('entidad_calendario_fecha_inicio'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', $this->input->post('entidad_calendario_fecha_fin'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date', "La fecha de inicio no puede ser posterior a la fecha de finalización.");
            return FALSE;
        }
    }

    function redim_calendario($uploader_response, $field_info, $files_to_upload) {
        if ($field_info->field_name == 'entidad_calendario_imagen') {
            $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
            $file_uploaded_notificaciones = $field_info->upload_path . '/notificaciones/' . $uploader_response[0]->name;
            $this->image_moo->load($file_uploaded)->resize_crop(317, 179)->save($file_uploaded_notificaciones, true);
            $this->image_moo->load($file_uploaded)->resize_crop(961, 589, FALSE)->save($file_uploaded, true);
            $this->image_moo->clear_temp();
            return true;
        } else {
            return true;
        }
    }

    public function entidad_ofertas_empleo() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_ofertas_empleo');
        $crud->set_subject('OFERTA DE EMPLEO');
        $crud->display_as('entidad_ofertas_empleo_url', 'Enlace al sitio');
        $crud->display_as('entidad_ofertas_empleo_target', 'Tipo de Enlace');
        $crud->field_type('entidad_ofertas_empleo_target', 'dropdown', array('_blank' => 'Nueva Pagina', '_self' => 'Misma Pagina'));
        $ruta = 'Ofertas de Empleo';
        $crud->unset_delete();
        $crud->unset_add();
        $ruta_tab = 'Servicios de Información';
        $texto = 'Enlace al sitio web donde se presta el servicio de ofertas de empleo .';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 55, $ruta, $ruta_tab, $texto);
    }

    public function entidad_boletines() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_boletines');
        $crud->set_subject('BOLETIN');
        $crud->display_as('entidad_boletines_nombre', 'Nombre');
        $crud->display_as('entidad_boletines_fecha', 'Fecha de Publicación');
        $crud->display_as('entidad_boletines_archivo', 'Archivo');
        $crud->set_field_upload('entidad_boletines_archivo', 'uploads/boletines');
        $crud->required_fields('entidad_boletines_nombre', 'entidad_boletines_archivo', 'entidad_boletines_fecha');
        $ruta = 'Boletines ';
        $ruta_tab = 'Servicios de Información';
        $texto = 'Boletines.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 56, $ruta, $ruta_tab, $texto);
    }

    public function entidad_periodico() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_periodico');
        $crud->set_subject('PERIÓDICO');
        $crud->display_as('entidad_periodico_nombre', 'Nombre');
        $crud->display_as('entidad_periodico_fecha', 'Fecha de Publicación');
        $crud->display_as('entidad_periodico_portada', 'Portada');
        $crud->display_as('entidad_periodico_archivo', 'Archivo');
        $crud->set_field_upload('entidad_periodico_archivo', 'uploads/periodico');
        $crud->set_field_upload('entidad_periodico_portada', 'uploads/periodico');
        $crud->required_fields('entidad_periodico_nombre', 'entidad_periodico_fecha', 'entidad_periodico_portada', 'entidad_periodico_archivo');
        $ruta = 'Periódicos  ';
        $ruta_tab = 'Servicios de Información';
        $texto = 'Boletines.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 57, $ruta, $ruta_tab, $texto);
    }

    public function entidad_tipo_documentos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_tipo_documentos');
        $crud->set_subject('TIPO DE DOCUMENTO');
        $crud->display_as('entidad_tipo_documentos_nombre', 'Tipo');
        $crud->required_fields('entidad_tipo_documentos_nombre');
        $crud->order_by('entidad_tipo_documentos_nombre', 'asc');
        $ruta = 'Tipos de Documento';
        $ruta_tab = 'Documentos de la entidad ';
        $texto = 'Definición de los tipos y clasificación de los documentos sobre el municipio.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 601, $ruta, $ruta_tab, $texto);
    }

    public function entidad_documentos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_documentos');
        $crud->set_subject('DOCUMENTO');
        $crud->display_as('entidad_documentos_nombre', 'Nombre');
        $crud->display_as('entidad_documentos_descripcion', 'Descripción');
        $crud->display_as('entidad_documentos_archivo', 'Archivo');
        $crud->display_as('id_estado_de_publicacion', 'Estado de la Publicación');
        $crud->display_as('id_entidad_tipo_documentos', 'Clasificación  del Documento');
        $crud->required_fields('entidad_documentos_nombre', 'entidad_documentos_archivo', 'id_entidad_tipo_documento', 'id_estado_de_publicacion');
        $crud->set_relation('id_estado_de_publicacion', 'estado_de_publicacion', 'estado_de_publicacion_estado');
        $crud->set_relation('id_entidad_tipo_documentos', 'entidad_tipo_documentos', 'entidad_tipo_documentos_nombre');
        $crud->set_field_upload('entidad_documentos_archivo', 'uploads/entidad/documentos');
        $crud->unset_texteditor('entidad_documentos_descripcion');
        $ruta = 'Documentos de la entidad';
        $ruta_tab = 'Documentos de la entidad';
        $texto = 'Documentos sobre y correspondientes a la entidad y sus funciones misionales  en diferentes áreas y temas.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 602, $ruta, $ruta_tab, $texto);
    }

    /* INFORMACION DE INTERES */

    public function convocatorias() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('convocatorias');
        $crud->set_subject('CONVOCATORIAS');
        $crud->display_as('convocatorias_nombre', 'Nombre');
        $crud->display_as('convocatorias_descripcion', 'Descripción');
        $crud->display_as('convocatorias_requisitos', 'Requisitos');
        $crud->display_as('convocatorias_objetivos', 'Objetivos');
        $crud->display_as('convocatorias_archivo', 'Archivo');
        $crud->display_as('convocatorias_fecha_inicio', 'Fecha de Inicio');
        $crud->display_as('convocatorias_fecha_fin', 'Fecha de Finalización');
        $crud->display_as('convocatorias_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('convocatorias_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('convocatorias_archivo', 'uploads/entidad/informacion_interes');
        $crud->field_type('convocatorias_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('convocatorias_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('convocatorias_archivo', 'convocatorias_nombre', 'convocatorias_descripcion', 'convocatorias_fecha_inicio', 'convocatorias_fecha_fin', 'convocatorias_requisitos', 'convocatorias_objetivos');
        $crud->edit_fields('convocatorias_archivo', 'convocatorias_nombre', 'convocatorias_descripcion', 'convocatorias_requisitos', 'convocatorias_objetivos', 'convocatorias_fecha_inicio', 'convocatorias_fecha_fin', 'convocatorias_fecha_actualizacion');
        $crud->unset_texteditor('convocatorias_descripcion', 'convocatorias_requisitos', 'convocatorias_objetivos');
        $ruta = 'Convocatorias';
        $ruta_tab = 'Servicios de Información';
        $texto = 'El sujeto obligado debe publicar convocatorias dirigidas a ciudadanos, usuarios y grupos de interés, especificando objetivos, requisitos y fechas de participación en dichos espacios.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 58, $ruta, $ruta_tab, $texto);
    }

    public function convocatorias_anexos() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('convocatorias_anexos');
        $crud->set_subject('CONVOCATORIAS ANEXOS');
        $crud->display_as('convocatorias_anexos_nombre', 'Nombre');
        $crud->display_as('convocatorias_anexos_archivo', 'Archivo');
        $crud->display_as('id_convocatorias', 'Convocatoria');
        $crud->display_as('convocatorias_anexos_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('convocatorias_anexos_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('convocatorias_anexos_archivo', 'uploads/entidad/informacion_interes');
        $crud->field_type('convocatorias_anexos_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('convocatorias_anexos_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('convocatorias_anexos_nombre', 'convocatorias_anexos_archivo');
        $crud->edit_fields('convocatorias_anexos_nombre', 'convocatorias_anexos_archivo', 'id_convocatorias', 'convocatorias_anexos_fecha_actualizacion');
        $crud->set_relation('id_convocatorias', 'convocatorias', 'convocatorias_nombre');
        $ruta = 'Convocatorias Anexos';
        $ruta_tab = 'Servicios de Información';
        $texto = 'El sujeto obligado debe publicar convocatorias dirigidas a ciudadanos, usuarios y grupos de interés, especificando objetivos, requisitos y fechas de participación en dichos espacios.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 61, $ruta, $ruta_tab, $texto);
    }

    public function estudios() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('estudios');
        $crud->set_subject('ESTUDIOS, INVESTIGACIONES U OTRAS PUBLICACIONES');
        $crud->display_as('estudios_nombre', 'Nombre');
        $crud->display_as('estudios_descripcion', 'Descripción');
        $crud->display_as('estudios_archivo', 'Archivo');
        $crud->display_as('estudios_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('estudios_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('estudios_archivo', 'uploads/entidad/informacion_interes');
        $crud->field_type('estudios_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('estudios_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('estudios_archivo', 'estudios_nombre', 'estudios_descripcion');
        $crud->edit_fields('estudios_archivo', 'estudios_nombre', 'estudios_descripcion', 'estudios_fecha_actualizacion');
        $crud->unset_texteditor('estudios_descripcion');
        $ruta = 'Estudios, investigaciones y otras publicaciones';
        $ruta_tab = 'Servicios de Información';
        $texto = 'El sujeto obligado debe publicar de manera organizada estudios, investigaciones y otro tipo de publicaciones de interés para ciudadanos, usuarios y grupos de interés, definiendo una periodicidad para estas publicaciones.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 59, $ruta, $ruta_tab, $texto);
    }

    public function informacion_adicional() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('informacion_adicional');
        $crud->set_subject('INFORMACIÓN ADICIONAL');
        $crud->display_as('informacion_adicional_nombre', 'Nombre');
        $crud->display_as('informacion_adicional_descripcion', 'Descripción');
        $crud->display_as('informacion_adicional_archivo', 'Archivo');
        $crud->display_as('informacion_adicional_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('informacion_adicional_fecha_actualizacion', 'Fecha de Actualización');
        $crud->set_field_upload('informacion_adicional_archivo', 'uploads/entidad/informacion_interes');
        $crud->field_type('informacion_adicional_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->field_type('informacion_adicional_fecha_actualizacion', 'hidden', date('Y-m-d'));
        $crud->required_fields('informacion_adicional_archivo', 'informacion_adicional_nombre', 'informacion_adicional_descripcion');
        $crud->edit_fields('informacion_adicional_archivo', 'informacion_adicional_nombre', 'informacion_adicional_descripcion', 'informacion_adicional_fecha_actualizacion');
        $crud->unset_texteditor('informacion_adicional_descripcion');
        $ruta = 'Información Adicional';
        $ruta_tab = 'Información Adicional';
        $texto = 'Los sujetos obligados en virtud del principio de la divulgación proactiva de la información, podrán publicar información general o adicional que resulte útil para los usuarios, ciudadanos o grupos de interés.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 60, $ruta, $ruta_tab, $texto);
    }

    /* ------------------------------------------------FALTANTES ----------------------------- */

    /* public function enviar_newsletter($array = NULL) {
      $this->load->model('Datos_entidad_model');
      $this->load->model('Datos_municipio_model');
      $this->load->library('encrypt');
      $emails = $this->Datos_entidad_model->obtener_newsletter();
      $entidad = $this->Datos_entidad_model->obtener_datos_contacto();
      $entidad_logo = $this->Datos_entidad_model->obtener_entidad_logo();

      if ($emails != FALSE) {
      $this->load->library('email');
      $config['protocol'] = 'smtp';
      $config['smtp_host'] = 'in-v3.mailjet.com';
      $config['smtp_port'] = '587';
      $config['smtp_user'] = 'e5705ade4b135f5405fcdbdee548e170';
      $config['smtp_pass'] = 'd23fd863a793376c3b3bf26863167b36';
      $config['charset'] = 'utf-8';
      $config['newline'] = "\r\n";
      $config['mailtype'] = 'html'; // or html
      $this->email->initialize($config);
      /* $this->email->from('comunicaciones@itagui.gov.co');
      $this->email->to('lucasturilli@gmail.com');
      $this->email->subject('hola');
      $this->email->message('test');
      $this->email->send(); *//*
      switch ($array['tipo']) {
      case '1':
      $tipo = 'noticias';
      $vista = 'ver_noticia';
      foreach ($emails as $email) {
      $this->email->clear();
      //  $this->email->from($entidad->entidad_contacto_email, 'Entidad de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
      $this->email->from('comunicaciones@itagui.gov.co', 'Entidad de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
      $this->email->to($email->entidad_newsletter_email);
      $this->email->subject($array['nombre']);
      include 'template/newsletter.php';
      $this->email->message($contenido);
      $this->email->send();
      }

      break;
      case '2':
      $tipo = 'noticias';
      $vista = 'ver_notificacion';
      foreach ($emails as $email) {
      $this->email->clear();
      // $this->email->from($entidad->entidad_contacto_email, 'Entidad de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
      $this->email->from('comunicaciones@itagui.gov.co', 'Entidad de ' . $this->Datos_municipio_model->obtener_nombre_escudo_municipio()->municipio_informacion_general_nombre);
      $this->email->to($email->entidad_newsletter_email);
      $this->email->subject($array['nombre']);
      include 'template/newsletter.php';
      $this->email->message($contenido);
      $this->email->send();
      }

      break;
      }
      }
      //print_r($contenido);
      // $this->session->set_userdata('contenido', $contenido);
      }
     */

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

}
