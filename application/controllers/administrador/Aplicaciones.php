<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aplicaciones extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->library('image_moo');
        $this->load->model('Referenciales_nodo_model');
        $this->is_logged_in();
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('is_logged_in');
        include ('template/error_login.php');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            echo $data;
            die();
        }
    }

    public function _example_output($output = null, $tab = NULL, $ruta = NULL, $ruta_tab = NULL, $texto = NULL) {
        if ($ruta != NULL) {

            $this->breadcrumbs->push($ruta, $ruta);
            $this->breadcrumbs->unshift_clear($ruta_tab, '#');
        }
        $this->breadcrumbs->unshift_clear('Información Entidad ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-tachometer-alt"></i>', site_url('administrador/principal'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'administrador/output_grocery_crud';
        $data['titulo'] = 'NODO - ADMINISTRADOR';
        $data['menu_lateral'] = TRUE;
        $data['permisos'] = $this->Referenciales_nodo_model->get_permisos_usuario($this->session->userdata('id_administrador'));
        $data['output'] = $output;
        $data['tab'] = $tab;
        $data['texto'] = $texto;
        $data['texto_titulo'] = $ruta;
        $data['menu'] = 'aplicaciones';
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('administrador/includes/template', $data); //Vista que se carga
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function entidad_widgets() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_widgets');
        $crud->set_subject('WIDGETS');
        $crud->display_as('entidad_widgets_facebook', 'Facebook Like Box');
        $crud->display_as('entidad_widgets_youtube_principal', 'Youtube Principal');
        $crud->display_as('entidad_widgets_youtube_sub1', 'Youtube Sub 1');
        $crud->display_as('entidad_widgets_youtube_sub2', 'Youtube Sub 2');
        $crud->display_as('entidad_widgets_youtube_sub3', 'Youtube Sub 3');
        $crud->display_as('entidad_widgets_instagram', 'Instagram');
        $crud->display_as('entidad_widgets_pinterest', 'Pinterest');
        $crud->display_as('entidad_widgets_twitter', 'Twitter');
        $crud->unset_texteditor('entidad_widgets_pinterest', 'entidad_widgets_instagram', 'entidad_widgets_youtube_sub1', 'entidad_widgets_youtube_sub2', 'entidad_widgets_youtube_sub3', 'entidad_widgets_twitter', 'entidad_widgets_youtube_principal', 'entidad_widgets_facebook');
        $crud->required_fields('entidad_widgets_twitter', 'entidad_widgets_facebook', 'entidad_widgets_youtube_principal');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Widgets';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Widgets que se mostraran en el sitio web, para generarlos ver los siguientes enlaces:
<ul>
<li><a href="https://developers.facebook.com/docs/plugins/like-box-for-pages"target="_blank">Facebook like box</a></li>
<li><a href="https://twitter.com/settings/widgets" target="_blank">Twitter</a></li>
<!-- <li><a href="https://developers.google.com/analytics/devguides/reporting/embed/v1/devguide" target="_blank">Google Analytics</a></li> -->
<li><a href="https://developers.google.com/youtube/youtube_subscribe_button" target="_blank">youtube</a></li></ul>
';

        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 801, $ruta, $ruta_tab, $texto);
    }

    public function entidad_banners_imagenes($banner) {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_banners_imagenes');
        $crud->set_subject('IMAGEN');
        $crud->display_as('entidad_banners_imagenes_banner', 'Banner');
        $crud->display_as('entidad_banners_imagenes_nombre', 'Nombre');
        $crud->display_as('entidad_banners_imagenes_imagen', 'Imagen');
        $crud->display_as('entidad_banners_imagenes_texto', 'Texto');
        $crud->display_as('entidad_banners_imagenes_url', 'Enlace');
        $crud->display_as('entidad_banners_imagenes_target', 'Tipo de Enclace');
        $crud->field_type('entidad_banners_imagenes_banner', 'hidden', $banner);
        $crud->field_type('entidad_banners_imagenes_target', 'dropdown', array('_blank' => 'Nueva Pagina', '_self' => 'Misma Pagina'));
        $crud->required_fields('entidad_banners_imagenes_nombre', 'entidad_banners_imagenes_banner', 'entidad_banners_imagenes_imagen');
        $crud->set_field_upload('entidad_banners_imagenes_imagen', 'uploads/banners');
        switch ($banner) {
            case 'principal':
                $numero_menu = 802;
                $crud->callback_after_upload(array($this, 'redim_slider_principal'));
                break;
            case 'lateral':
                $numero_menu = 803;
                $crud->field_type('entidad_banners_imagenes_texto', 'hidden');
                $crud->callback_after_upload(array($this, 'redim_slider_lateral'));
                $crud->columns('entidad_banners_imagenes_banner', 'entidad_banners_imagenes_nombre', 'entidad_banners_imagenes_imagen', 'entidad_banners_imagenes_url', 'entidad_banners_imagenes_target');
                break;
            case 'alcaldia':
                $numero_menu = 804;
                $crud->field_type('entidad_banners_imagenes_texto', 'hidden');
                $crud->field_type('entidad_banners_imagenes_url', 'hidden');
                $crud->field_type('entidad_banners_imagenes_target', 'hidden');
                $crud->callback_after_upload(array($this, 'redim_slider_interna'));
                $crud->columns('entidad_banners_imagenes_banner', 'entidad_banners_imagenes_nombre', 'entidad_banners_imagenes_imagen');
                break;
            case 'municipio':
                $numero_menu = 805;
                $crud->field_type('entidad_banners_imagenes_texto', 'hidden');
                $crud->field_type('entidad_banners_imagenes_url', 'hidden');
                $crud->field_type('entidad_banners_imagenes_target', 'hidden');
                $crud->callback_after_upload(array($this, 'redim_slider_interna'));
                break;
            case 'ninos':
                $numero_menu = 806;
                $crud->field_type('entidad_banners_imagenes_texto', 'hidden');
                $crud->field_type('entidad_banners_imagenes_url', 'hidden');
                $crud->field_type('entidad_banners_imagenes_target', 'hidden');
                $crud->callback_after_upload(array($this, 'redim_slider_interna'));
                break;
        }
        $crud->where('entidad_banners_imagenes_banner', $banner);
        $crud->callback_before_delete(array($this, 'delete_imagen'));
        $crud->callback_before_update(array($this, 'check_image'));
        $ruta = 'Banner ' . ucwords($banner);
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Imágenes que aparecerían en el banner ' . $banner . ', en el sitio web.<br />'
                . 'Recomendaciones<br />'
                . '•	Imagen panorámica, es decir que el ancho sea mayor que el alto, de lo contrario afectara el diseño del sitio.<br />
•	El tamaño máximo en peso debe ser de 1 MB, no se permiten fotos más pesadas pues afectara el rendimiento del sitio web.<br />
•	Buena resolución en la foto<br />
•	Que el foco de la foto se encuentre en el medio, pues si la foto no cumple con el tamaño especifico este la cortara de modo tal que se puede perder información.
';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, $numero_menu, $ruta, $ruta_tab, $texto);
    }

    function redim_slider_principal($uploader_response, $field_info, $files_to_upload) {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        if ($this->image_moo->load($file_uploaded)->width > $this->image_moo->load($file_uploaded)->height) {
            $this->image_moo->load($file_uploaded)->resize_crop(1920, 739, TRUE)->save($file_uploaded, true);
        } else {
            $this->image_moo->load($file_uploaded)->resize(1920, 739, TRUE)->save($file_uploaded, true);
        }
        $this->image_moo->clear_temp();
        return true;
    }

    function redim_slider_interna($uploader_response, $field_info, $files_to_upload) {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize_crop(1170, 300, TRUE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();
        return true;
    }

    function redim_slider_lateral($uploader_response, $field_info, $files_to_upload) {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize(310, 240)->save($file_uploaded, true);
        $this->image_moo->clear_temp();
        return true;
    }

    function delete_imagen($primary_key) {
        $image = $this->db->get_where('entidad_banners_imagenes', array('id_entidad_banners_imagenes' => $primary_key))->row();
        $url = 'uploads/banners/' . $image->entidad_banners_imagenes_imagen;
        unlink($url);
        return true;
    }

    function check_image($post_array, $primary_key) {
        $image = $this->db->get_where('entidad_banners_imagenes', array('id_entidad_banners_imagenes' => $primary_key))->row();
        if ($post_array['entidad_banners_imagenes_imagen'] != $image->entidad_banners_imagenes_imagen) {
            $url = 'uploads/banners/' . $image->entidad_banners_imagenes_imagen;
            unlink($url);
        }
        return true;
    }

    public function entidad_carrousel($tipo) {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_carrousel');
        $crud->set_subject('IMAGEN');
        $crud->display_as('entidad_carrousel_titulo', 'Titulo');
        $crud->display_as('entidad_carrousel_imagen', 'Imagen');
        $crud->display_as('entidad_carrousel_url', 'Enlace');
        $crud->required_fields('entidad_carrousel_titulo', 'entidad_carrousel_imagen', 'entidad_carrousel_url');
        $crud->set_field_upload('entidad_carrousel_imagen', 'uploads/carrousel');
        $crud->callback_after_upload(array($this, 'redim_image_carrousel'));
        $crud->field_type('entidad_carrousel_tipo', 'hidden', $tipo);
        $crud->columns('entidad_carrousel_titulo', 'entidad_carrousel_imagen', 'entidad_carrousel_url');
        $crud->where('entidad_carrousel_tipo', $tipo);
        $ruta = 'Carrusel';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Imágenes que aparecerían  en el carrusel del Footer del sitio web o en la seccion de ';
        switch ($tipo) {
            case 1:
                $numero_menu = 808;
                $texto = 'Imágenes que aparecerían  en el carrusel del Footer del sitio web.';
                break;
            case 2:
                $numero_menu = 809;
                $texto = 'Imágenes que aparecerían  en el carrusel de sitios de interés ';
                break;
        }
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, $numero_menu, $ruta, $ruta_tab, $texto);
    }

    function redim_image_carrousel($uploader_response, $field_info, $files_to_upload) {

        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize(240, 100, TRUE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();
        return true;
    }

    public function survey() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('survey');
        $crud->set_subject('ENCUESTA');
        $crud->display_as('survey_nombre', 'Nombre');
        $crud->display_as('survey_encuesta', 'Encabezado');
        $crud->display_as('survey_fecha_publicacion', 'Fecha de Publicación');
        $crud->display_as('survey_fecha_expiracion', 'Fecha de Expiración');
        $crud->display_as('id_estado_de_publicacion', 'Estado de la Publicación ');
        $crud->set_relation('id_estado_de_publicacion', 'estado_de_publicacion', 'estado_de_publicacion_estado');
        $crud->required_fields('survey_nombre', 'survey_encuesta', 'survey_fecha_publicacion', 'survey_fecha_expiracion', 'id_estado_de_publicacion');
        $crud->callback_column('Preguntas', array($this, '_callback_webpage_url'));
        $crud->unset_texteditor('survey_encuesta');
        $crud->columns('survey_nombre', 'survey_encuesta', 'survey_fecha_publicacion', 'survey_fecha_expiracion', 'id_estado_de_publicacion', 'Preguntas');
        $ruta = 'Encuesta';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Encuestas de interés público que aparecen en el sitio web.';
        $crud->set_rules('survey_fecha_expiracion', 'Fecha de Finalización', 'callback_check_date_today_encuesta');
        $crud->set_rules('survey_fecha_publicacion', 'Fecha de Finalización', 'callback_check_date_today_encuesta_begin');
        $crud->set_rules('survey_fecha_publicacion', 'Fecha de Finalización', 'callback_check_date');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 851, $ruta, $ruta_tab, $texto);
    }

    public function _callback_webpage_url($value, $row) {
        return "<a class='btn btn-sm btn-primary btn-block' href='" . site_url('administrador/aplicaciones/survey_preguntas/' . $row->id_survey) . "'>Añadir Preguntas</a>";
    }

    public function survey_preguntas($id) {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('survey_preguntas');
        $crud->set_subject('RESPUESTA');
        $crud->display_as('survey_preguntas_pregunta', 'Respuesta');
        $crud->display_as('survey_preguntas_respuestas', 'Votos');
        $crud->field_type('id_survey', 'hidden', $id);
        $crud->required_fields('survey_preguntas_respuesta');
        $crud->unset_columns('id_survey');
        $crud->field_type('survey_preguntas_respuestas', 'hidden', 0);
        $crud->edit_fields('survey_preguntas_pregunta');
        $crud->where('id_survey', $id);
        $ruta = 'Encuesta';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Encuestas de interés público que aparecen en el sitio web.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 115, $ruta, $ruta_tab, $texto);
    }

    public function check_date($str) {


        $partes = explode('/', $this->input->post('survey_fecha_publicacion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', $this->input->post('survey_fecha_expiracion'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date', "La fecha de inicio no puede ser posterior a la fecha de finalización.");
            return FALSE;
        }
    }

    public function check_date_today_encuesta_begin($str) {

        $partes = explode('/', $this->input->post('survey_fecha_publicacion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 < $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_encuesta_begin', "La fecha de publicacion debe ser mayor a la del día de hoy   .");
            return FALSE;
        }
    }

    public function check_date_today_encuesta($str) {

        $partes = explode('/', $this->input->post('survey_fecha_expiracion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', date('Y-m-d'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 < $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_today_encuesta', "La fecha de expiración debe ser mayor a la del día de hoy   .");
            return FALSE;
        }
    }

    public function newsletter() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_newsletter');
        $crud->set_subject('SUSCRIPTORES');
        $crud->display_as('entidad_newsletter_nombre', 'Nombre');
        $crud->display_as('entidad_newsletter_email', 'Correo electrónico');
        $crud->required_fields('entidad_newsletter_email');
        $crud->field_type('survey_preguntas_respuestas', 'hidden', 0);
        $ruta = 'Suscriptores';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Suscriptores inscritos por medio del sitio web a quienes le llegara información cada que se actualice el sitio web.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 901, $ruta, $ruta_tab, $texto);
    }

    public function entidad_banner_aviso() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_banner_aviso');
        $crud->set_subject('BANNER AVISO');
        $crud->display_as('entidad_banner_aviso_titulo', 'Título');
        $crud->display_as('entidad_banner_aviso_archivo', 'Archivo');
        $crud->display_as('entidad_banner_aviso_activo', 'Activo  ');
        $crud->display_as('entidad_banner_aviso_url', 'URL  ');
        $crud->display_as('entidad_banner_aviso_tipo_enlace', 'Tipo de Enlace  ');
        $crud->required_fields('entidad_newsletter_email');
        $crud->set_field_upload('entidad_banner_aviso_archivo', 'uploads/banners');
        $crud->field_type('entidad_banner_aviso_activo', 'dropdown', array('1' => 'Si', '2' => 'No'));
        $crud->field_type('entidad_banner_aviso_tipo_enlace', 'dropdown', array('_self' => 'Misma Ventana', '_blank' => 'Nueva Ventana'));
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Banner Aviso';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Banner principal que aparece cuando se ingresa al sitio web.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 807, $ruta, $ruta_tab, $texto);
    }

    public function entidad_iconos_home_tipo() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('entidad_iconos_home_tipo');
        $crud->set_subject('TIPOS');
        $crud->display_as('entidad_iconos_home_tipo_nombre', 'Nombre');
        $crud->display_as('entidad_iconos_home_tipo_orden', 'Orden');
        $crud->required_fields('entidad_iconos_home_tipo_nombre');
        $ruta = 'Iconos';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Pestañas de los iconos que aparecen en el Home del sitio web.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 814, $ruta, $ruta_tab, $texto);
    }

    public function entidad_iconos_home() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_iconos_home');
        $crud->set_subject('SERVICIO');
        $crud->display_as('entidad_iconos_home_nombre', 'Nombre');
        $crud->display_as('entidad_iconos_home_alias', 'Alias');
        $crud->display_as('entidad_iconos_home_imagen', 'Imagen');
        $crud->display_as('entidad_iconos_home_texto', 'Contenido');
        $crud->display_as('entidad_iconos_home_texto_corto', 'Contenido Corto');
        $crud->required_fields('entidad_iconos_home_nombre', 'entidad_iconos_home_url', 'entidad_iconos_home_imagen', 'entidad_iconos_home_texto', 'entidad_iconos_home_texto_corto');
        $crud->set_field_upload('entidad_iconos_home_imagen', 'uploads/iconos');
        $crud->field_type('entidad_iconos_home_alias', 'hidden');
        $ruta = 'Servicios';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Servicios que aparecen en el Home del sitio web.';
        $crud->unset_texteditor('entidad_iconos_home_texto_corto');
        $crud->callback_before_insert(array($this, 'check_alias_servicios'));
        $crud->callback_before_update(array($this, 'check_alias_servicios'));
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 810, $ruta, $ruta_tab, $texto);
    }

    function check_alias_servicios($post_array, $primary_key = NULL) {

        $alias = $this->clear_alias($post_array['entidad_iconos_home_nombre']);
        $post_array['entidad_iconos_home_alias'] = $alias;
        return $post_array;
    }

    function redim_imagen($uploader_response, $field_info, $files_to_upload) {

        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize_crop(150, 200, FALSE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();

        return true;
    }

    public function entidad_sub_menu() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_sub_menu');
        $crud->set_subject('SUB MENU');
        $crud->display_as('entidad_sub_menu_nombre', 'Nombre');
        $crud->display_as('entidad_sub_menu_enlace', 'Enlace');
        $crud->display_as('entidad_sub_menu_tipo_enlace', 'Tipo de Enlace');
        $crud->display_as('entidad_sub_menu_script', 'JAVASCRIPT');
        $crud->display_as('entidad_sub_menu_clase', 'Clase CSS');
        $crud->display_as('entidad_sub_menu_imagen', 'Imagen');
        $crud->set_field_upload('entidad_sub_menu_imagen', 'uploads/iconos');
        $crud->field_type('entidad_sub_menu_tipo_enlace', 'dropdown', array('_self' => 'Misma Ventana', '_blank' => 'Ventana Nueva'));
        $crud->required_fields('entidad_sub_menu_nombre', 'entidad_sub_menu_enlace', 'entidad_sub_menu_tipo_enlace', 'entidad_sub_menu_imagen');
        //$crud->callback_after_upload(array($this, 'redim_imagen'));
        $ruta = 'Sub Menu';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Menu lateral del home - Usar Fa Icons con la clase fa-3x';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 812, $ruta, $ruta_tab, $texto);
    }

    public function entidad_enlaces_footer() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_enlaces_footer');
        $crud->set_subject('ENLACE');
        $crud->display_as('entidad_enlaces_footer_nombre', 'Nombre');
        $crud->display_as('entidad_enlaces_footer_enlace', 'Enlace');
        $crud->display_as('entidad_enlaces_footer_tipo_enlace', 'Tipo de Enlace');
        $crud->field_type('entidad_enlaces_footer_tipo_enlace', 'dropdown', array('_self' => 'Misma Ventana', '_blank' => 'Ventana Nueva'));
        $crud->required_fields('entidad_enlaces_footer_nombre', 'entidad_enlaces_footer_enlace', 'entidad_enlaces_footer_tipo_enlace');
        $ruta = 'Enlaces Footer';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Enlaces del footer';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 811, $ruta, $ruta_tab, $texto);
    }

    public function entidad_paginas() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('entidad_paginas');
        $crud->set_subject('PAGINA');
        $crud->display_as('entidad_paginas_nombre', 'Nombre');
        $crud->display_as('entidad_paginas_alias', 'Alias');
        $crud->display_as('entidad_paginas_titulo', 'Titulo');
        $crud->display_as('entidad_paginas_contenido', 'Contenido');
        $crud->required_fields('entidad_paginas_alias', 'entidad_paginas_nombre', 'entidad_paginas_titulo', 'entidad_paginas_contenido');
        $crud->columns('entidad_paginas_nombre', 'entidad_paginas_titulo', 'Url');
        $crud->set_rules('entidad_paginas_alias', 'Alias', 'callback_check_alias_unique');
        $crud->callback_column('Url', array($this, '_callback_webpage_url_dos'));
        $crud->callback_before_insert(array($this, 'check_alias'));
        $crud->edit_fields('entidad_paginas_nombre', 'entidad_paginas_titulo', 'entidad_paginas_contenido');
        $ruta = 'Paginas';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Paginas internas ';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 951, $ruta, $ruta_tab, $texto);
    }

    public function _callback_webpage_url_dos($value, $row) {
        return site_url('sitio/pagina/' . $row->entidad_paginas_alias);
    }

    function check_alias($post_array, $primary_key = NULL) {

        $alias = $this->clear_alias($post_array['entidad_paginas_alias']);
        $post_array['entidad_paginas_alias'] = $alias;
        return $post_array;
    }

    function check_alias_unique($str) {

        $this->load->model('Aplicaciones_model');
        if ($this->Aplicaciones_model->check_alias($this->clear_alias($this->input->post('entidad_paginas_alias'))) == TRUE) {
            $this->form_validation->set_message('check_alias_unique', "El Alias ya existe.");
            return FALSE;
        } else {

            return TRUE;
        }
    }

    public function clear_alias($cadena) {
        $alias = str_replace(' ', '-', $cadena);
        $alias = preg_replace('/[^A-Za-z0-9 _\-\+\&]/', '', $alias);
        return strtolower($alias);
    }

    function log_user($post_array = NULL, $primary_key = NULL) {

        $this->load->model('Aplicaciones_model');
        $this->Aplicaciones_model->update_audit($post_array, $primary_key);

        return true;
    }

    public function embebido_home() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('embebido_home');
        $crud->set_subject('EMBEDDED HOME');
        $crud->display_as('embebido_home', 'Embedded');
        $crud->required_fields('entidad_newsletter_email');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_texteditor('embebido_home');
        $ruta = 'Embedded Home';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Embededd que reemplaza el silder principal.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 813, $ruta, $ruta_tab, $texto);
    }

    public function filemanager_folder() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('filemanager_folder');
        $crud->set_subject('CARPETA');
        $crud->display_as('filemanager_folder_nombre', 'Nombre Carpeta');
        $crud->required_fields('filemanager_folder_nombre');
        $ruta = 'Administrador de Archivos';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Administrador de Archivos / Carpetas.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 621, $ruta, $ruta_tab, $texto);
    }

    public function filemanager_files() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('filemanager_file');
        $crud->set_subject('ARCHIVOS');
        $crud->display_as('filemanager_file_nombre', 'Nombre Archivo');
        $crud->display_as('filemanager_file_archivo', 'Archivo');
        $crud->display_as('filemanager_file_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('id_administrador', 'Administrador');
        $crud->display_as('id_filemanager_folder', 'Carpeta');

        $crud->required_fields('filemanager_file_nombre', 'filemanager_file_archivo', 'filemanager_file_fecha_ingreso');
        $crud->edit_fields('filemanager_file_nombre', 'filemanager_file_archivo', 'id_administrador', 'id_filemanager_folder');
        $crud->set_field_upload('filemanager_file_archivo', 'uploads/filemanager');
        $crud->field_type('filemanager_file_fecha_ingreso', 'hidden', date('Y-m-d H:m:s'));
        $crud->field_type('id_administrador', 'hidden', $this->session->userdata('id_administrador'));
        $crud->columns('filemanager_file_nombre', 'id_filemanager_folder', 'filemanager_file_fecha_ingreso', 'URL');
        $crud->callback_column('URL', array($this, '_callback_url'));
        $ruta = 'Administrador de Archivos';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Administrador de Archivos / Archivos.';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 622, $ruta, $ruta_tab, $texto);
    }

    public function _callback_url($value, $row) {
        return "<a target='_blank' href='" . site_url('uploads/filemanager/' . $row->filemanager_file_archivo) . "'>Ver URL</a>";
    }

    public function notificaciones_electronicas_tipo() {

        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();

        $crud->set_table('notificaciones_electronicas_tipo');
        $crud->set_subject('TIPOS NOTIFICACIONES ELECTRONICAS');
        $crud->display_as('notificaciones_electronicas_tipo_nombre', 'Nombre');
        $crud->required_fields('notificaciones_electronicas_tipo_nombre');
        $ruta = 'Notificaciones Electrónicas';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Tipos de Notificaciones Electrónicas';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1001, $ruta, $ruta_tab, $texto);
    }

    public function notificaciones_electronicas() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('notificaciones_electronicas');
        $crud->set_subject('NOTIFICACIONES ELECTRONICAS');
        $crud->display_as('notificaciones_electronicas_identificacion', 'Identificación');
        $crud->display_as('notificaciones_electronicas_usuario', 'Notificado');
        $crud->display_as('notificaciones_electronicas_fecha_fijacion', 'Fecha de Fijación');
        $crud->display_as('notificaciones_electronicas_fecha_desfijacion', 'Fecha de Desfijación');
        $crud->display_as('notificaciones_electronicas_observaciones', 'Observaciones');
        $crud->display_as('notificaciones_electronicas_archivo', 'Archivo');
        $crud->display_as('id_notificaciones_electronicas_tipo', 'Tipo');
        $crud->set_relation('id_notificaciones_electronicas_tipo', 'notificaciones_electronicas_tipo', 'notificaciones_electronicas_tipo_nombre');
        $crud->set_field_upload('notificaciones_electronicas_archivo', 'uploads/notificaciones');
        $crud->set_rules('notificaciones_electronicas_fecha_desfijacion', 'Fecha de Desfijación', 'callback_check_date_notificacion');
        $crud->required_fields('notificaciones_electronicas_identificacion', 'notificaciones_electronicas_usuario', 'notificaciones_electronicas_fecha_fijacion', 'notificaciones_electronicas_fecha_desfijacion', 'notificaciones_electronicas_archivo');
        $ruta = 'Notificaciones Electrónicas';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Notificaciones Electrónicas';
        $crud->unset_texteditor('notificaciones_electronicas_observaciones');
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1002, $ruta, $ruta_tab, $texto);
    }

    public function check_date_notificacion($str) {


        $partes = explode('/', $this->input->post('notificaciones_electronicas_fecha_fijacion'));
        $fecha1 = join('-', $partes);

        $partes2 = explode('/', $this->input->post('notificaciones_electronicas_fecha_desfijacion'));
        $fecha2 = join('-', $partes2);

        if ($fecha2 >= $fecha1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_date_notificacion', "La fecha de Fijación no puede ser posterior a la fecha de Desfijación.");
            return FALSE;
        }
    }

    public function animales_config() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('animales_config');
        $crud->set_subject('DATOS DE CONTACTO');
        $crud->display_as('id_animales_adopcion', 'Id');
        $crud->display_as('animales_config_email', 'Correo Electrónico de Notificación');
        $crud->display_as('animales_config_telefono', 'Teléfono de Contacto');
        $crud->display_as('animales_config_direccion', 'Dirección');
        $crud->display_as('animales_config_texto', 'Descripción General');
        $crud->display_as('animales_config_tiempo_clasificados', 'Cantidad en Días de Publicación de los Clasificados');
        $crud->set_rules('animales_config_email', 'Correo Electrónico de Notificación', 'valid_email');
        $crud->set_rules('animales_config_tiempo_clasificados', 'Cantidad en Días de Publicación de los Clasificados', 'is_natural');
        $crud->unset_add();
        $crud->unset_delete();
        $ruta = 'Bienestar Animal';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Datos de Contacto';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1052, $ruta, $ruta_tab, $texto);
    }

    public function animales_adopcion() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('animales_adopcion');
        $crud->set_subject('ANIMAL EN ADOPCIÓN');
        $crud->display_as('id_animales_adopcion', 'Id');
        $crud->display_as('animales_adopcion_tipo', 'Tipo de Animal');
        $crud->display_as('animales_adopcion_foto', 'Foto');
        $crud->display_as('animales_adopcion_nombre', 'Nombre');
        $crud->display_as('animales_adopcion_edad', 'Edad');
        $crud->display_as('animales_adopcion_sexo', 'Sexo');
        $crud->display_as('animales_adopcion_tamano', 'Tamaño');
        $crud->display_as('animales_adopcion_raza', 'Raza');
        $crud->display_as('animales_adopcion_ficha', 'Ficha');
        $crud->display_as('animales_adopcion_microchip', 'Microchip');
        $crud->display_as('animales_adopcion_esterilizado', 'Esterilizado');
        $crud->display_as('animales_adopcion_descripcion', 'Descripción');
        $crud->display_as('animales_adopcion_fecha_ingreso', 'Fecha de Ingreso');
        $crud->display_as('animales_adopcion_estado', 'Estado');
        $crud->set_field_upload('animales_adopcion_ficha', 'uploads/adopcion');
        $crud->set_field_upload('animales_adopcion_foto', 'uploads/adopcion');
        $crud->columns('id_animales_adopcion', 'animales_adopcion_microchip', 'animales_adopcion_tipo', 'animales_adopcion_foto', 'animales_adopcion_nombre', 'animales_adopcion_edad', 'animales_adopcion_sexo', 'animales_adopcion_tamano', 'animales_adopcion_esterilizado', 'animales_adopcion_estado', 'animales_adopcion_fecha_ingreso');
        $crud->required_fields('animales_adopcion_tipo', 'animales_adopcion_foto', 'animales_adopcion_nombre', 'animales_adopcion_edad', 'animales_adopcion_sexo', 'animales_adopcion_tamano', 'animales_adopcion_raza', 'animales_adopcion_ficha', 'animales_adopcion_estado', 'animales_adopcion_esterilizado', 'animales_adopcion_microchip');
        $crud->edit_fields('animales_adopcion_tipo', 'animales_adopcion_foto', 'animales_adopcion_nombre', 'animales_adopcion_edad', 'animales_adopcion_sexo', 'animales_adopcion_tamano', 'animales_adopcion_raza', 'animales_adopcion_ficha', 'animales_adopcion_estado', 'animales_adopcion_esterilizado', 'animales_adopcion_microchip', 'animales_adopcion_descripcion');
        $crud->field_type('animales_adopcion_fecha_ingreso', 'hidden', date('Y-m-d'));
        $crud->unset_texteditor('animales_adopcion_descripcion');
        $crud->field_type('animales_adopcion_edad', 'dropdown', array(
            '1' => 'De 0 a 6 meses',
            '2' => 'De 6 meses a 1 a&ntilde;o',
            '3' => 'De 1 año a 3 a&ntilde;os',
            '4' => 'De 3 a 5 a&ntilde;os',
            '5' => 'De 5 a 8 a&ntilde;os',
            '6' => 'Mas de 8 a&ntilde;os'
        ));
        $crud->field_type('animales_adopcion_tamano', 'dropdown', array(
            '1' => 'Peque&ntilde;o',
            '2' => 'Mediano',
            '3' => 'Grande'
        ));
        $crud->field_type('animales_adopcion_tipo', 'dropdown', array(
            '1' => 'Canino',
            '2' => 'Felino'
        ));
        $crud->field_type('animales_adopcion_sexo', 'dropdown', array(
            '1' => 'Macho',
            '2' => 'Hembra'
        ));
        $crud->callback_after_upload(array($this, 'redim_animal'));
        $ruta = 'Bienestar Animal';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Animales en Adopción';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1051, $ruta, $ruta_tab, $texto);
    }

    function redim_animal($uploader_response, $field_info, $files_to_upload) {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize_crop(650, 500, TRUE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();
        return true;
    }

    public function animales_clasificados() {
        $crud = new grocery_CRUD();
        $crud->unset_bootstrap();
        $crud->set_table('animales_clasificados');
        $crud->set_subject('AVISOS CLASIFICADOS');
        $crud->display_as('animales_clasificados_fecha', 'Fecha de Ingreso');
        $crud->display_as('animales_clasificados_titulo', 'Título del Clasificado');
        $crud->display_as('animales_clasificados_descripcion', 'Contenido del Clasificado');
        $crud->display_as('animales_clasificados_foto', 'Foto del Animal');
        $crud->display_as('animales_clasificados_fecha_aprobacion', 'Fecha de Aprobación');
        $crud->display_as('animales_clasificados_solicitante', 'Nombres y Apellidos del Solcitante');
        $crud->display_as('animales_clasificados_solicitante_email', 'Correo Electrónico de Notificación');
        $crud->display_as('animales_clasificados_solicitante_contacto', 'Teléfono de Contacto');
        $crud->display_as('animales_clasificados_estado', 'Estado de Publicación');
        $crud->set_rules('animales_clasificados_solicitante_email', 'Correo Electrónico de Notificación', 'valid_email');
        $crud->set_rules('animales_clasificados_solicitante_contacto', 'Teléfono de Contacto', 'is_natural');
        $crud->required_fields('animales_clasificados_titulo', 'animales_clasificados_descripcion', 'animales_clasificados_foto', 'animales_clasificados_solicitante', 'animales_clasificados_solicitante_email', 'animales_clasificados_solicitante_contacto', 'animales_clasificados_estado');
        $crud->edit_fields('animales_clasificados_titulo', 'animales_clasificados_descripcion', 'animales_clasificados_foto', 'animales_clasificados_solicitante', 'animales_clasificados_solicitante_email', 'animales_clasificados_solicitante_contacto', 'animales_clasificados_estado', 'animales_clasificados_fecha_aprobacion');
        $crud->field_type('animales_clasificados_fecha', 'hidden', date('Y-m-d'));
        $crud->field_type('animales_clasificados_fecha_aprobacion', 'hidden', date('Y-m-d'));
        $crud->unset_texteditor('animales_clasificados_descripcion');
        $crud->field_type('animales_clasificados_estado', 'dropdown', array('1' => 'Aprobado', '2' => 'Pendiente', '3' => 'Rechazado', '4' => 'Archivado'));
        $crud->set_field_upload('animales_clasificados_foto', 'uploads/adopcion');
        $crud->callback_before_update(array($this, 'fecha_actualizacion'));
        $crud->callback_after_upload(array($this, 'redim_foto'));
        $crud->callback_after_update(array($this, 'editar_clasificado'));

        $ruta = 'Bienestar Animal';
        $ruta_tab = 'Aplicaciones ';
        $texto = 'Avisos Clasificados';
        $crud->callback_after_insert(array($this, 'log_user'));
        $crud->callback_after_update(array($this, 'log_user'));
        $crud->callback_after_delete(array($this, 'log_user'));
        $output = $crud->render();
        $this->_example_output($output, 1053, $ruta, $ruta_tab, $texto);
    }

    function redim_foto($uploader_response, $field_info, $files_to_upload) {
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize_crop(512, 512, TRUE)->save($file_uploaded, true);
        $this->image_moo->clear_temp();
        return true;
    }

    function editar_clasificado($post_array, $primary_key = NULL) {

        switch ($post_array['animales_clasificados_estado']) {
            case 3:
                $estado = 'rechazado';
                break;
            case 4:
                $estado = 'archivado';
                break;
            case 1:
                $estado = 'aprovado';
                break;
        }

        $this->load->model('Aplicaciones_model');
        $this->load->model('Datos_entidad_model');
        /* Envio el correo electronico de notificacion */
        $seo = $this->Datos_entidad_model->obtener_entidad_seo();
        $animales_config = $this->Aplicaciones_model->obtener_animales_config();
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['sendmail'] = 'smtp';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $this->email->initialize($config);
        $this->email->from($sender, $seo->entidad_seo_titulo);
        $this->email->to($post_array['animales_clasificados_solicitante_email']);
        $this->email->subject('Aviso Clasificado');
        $this->email->message("Este mensaje es generado automáticamente por el sitio web de la $seo->entidad_seo_titulo y tiene como propósito informarte que el aviso clasificado publicado con el título de “" . $post_array['animales_clasificados_titulo'] . " ” fue $estado. <br> <br>
                        Si requieres continuar con la publicación por favor ingresa un nuevo aviso clasificado.");
        $this->email->send();

        return true;
    }

    function fecha_actualizacion($post_array, $primary_key = NULL) {
        $post_array['animales_clasificados_fecha_aprobacion'] = date('Y-m-d');
        return $post_array;
    }

}
