<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->helper('cookie');
        $this->load->helper('email');
        $this->load->model('App_model');
        $this->load->library('encryption');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    function aseguramiento() {
        $this->breadcrumbs->unshift('Validación de Derechos', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/aseguramiento';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function aseguramiento_ajax($numero) {

        $this->load->library('csvimport');
        $result = array();
        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato con el número (' . $numero . '), por favor verifíquelo e intente de nuevo "</div>';
        $dato = $this->App_model->get_aseguramiento();
        if ($dato != FALSE) {
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_aseguramiento";
            $dbEntidades = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_entidades";
            $dbActualizacion = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_fec_actualiza";
            if (file_exists($dbName)) {
                $texto = '
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Numero de Identificación</th><th>Nombres </th><th>Apellidos</th><th>Fecha de Nacimiento</th><th>Entidad</th><th>Fecha de Actualización</th></tr></thead><tbody>
                    ';
                try {
                    $flag = FALSE;
                    $aseguramiento = $this->csvimport->get_array($dbName);
                    $entidades = $this->csvimport->get_array($dbEntidades);
                    $actualizacion = $this->csvimport->get_array($dbActualizacion);
                    foreach ($aseguramiento as $row) {
                        if ($row["IDENTIFICACION"] == $numero) {
                            $texto .= '<tr>'
                                    . '<td>' . round($row['IDENTIFICACION']) . '</td>';
                            $texto .= '<td>' . $row['NOMBRE1'] . ' ' . $row['NOMBRE2'] . '</td>';
                            $texto .= '<td>' . $row['APELLIDO1'] . ' ' . $row['APELLIDO2'] . '</td>';
                            $texto .= '<td>' . $row['FECHANACIMIENTO'] . '</td>';
                            $nombreentidad = "N/A";
                            foreach ($entidades as $row2) {
                                if ($row2['CODENTIDAD'] == $row['CODIGOSENTIDAD']) {
                                    $nombreentidad = $row2['NOMENTIDAD'];
                                }
                            }
                            $texto .= '<td>' . $nombreentidad . '</td>';
                            $diaactualiza = "N/A";
                            foreach ($actualizacion as $row3) {
                                if ($row3['CODACTUALIZACION'] == $row['CODACTUALIZA']) {
                                    $diaactualiza = $row3['FECHAULTIMAACTUALIZA'];
                                }
                            }
                            $texto .= '<td>' . $diaactualiza . '</td>'
                                    . '</tr>';
                            $flag = TRUE;
                        }
                    }
                    $texto .= ' </tbody>
                </table>
               </div>';
                    if ($flag == TRUE) {
                        $result['estado'] = 1/* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = $texto;
                    } else {
                        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato con el número (' . $numero . '), por favor verifíquelo e intente de nuevo "</div>';
                    }
                } catch (Exception $e) {
                    echo 'No encontro registros' . $e;
                }
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 2</div>';
            }
        } else {
            $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
            $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 1</div>';
        }

        echo json_encode($result);
    }

    function citas() {
        if ($this->session->userdata('paciente_is_logged_in') == TRUE) {
            redirect(site_url('app/paciente'), 'refresh');
        }
        $this->breadcrumbs->unshift('Solicitud de Citas ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/citas';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function validar_usuario() {

        $id = $_POST['id'];
        $passw = $_POST['passw'];
        $query = $this->App_model->get_paciente($id);
        if ($query == FALSE) {
            $result = array('respuesta' => '2', 'texto' => 'El número de identificación no se encuentra registrado por favor verifique los datos o cree una nueva cuenta ');
        } else {
            $password_db = $this->encryption->decrypt($query->paciente_password);
            if ($password_db != $passw) {
                $result = array('respuesta' => '2', 'texto' => 'La contraseña ingresada no coincide, por favor verifique los datos ');
            } else {
                if ($query->id_estado == 2) {
                    $result = array('respuesta' => '2', 'texto' => 'La cuenta esta inactiva, por favor comunícate con nosotros para solucionar el problema ');
                } else {
                    $result = array('respuesta' => '1', 'texto' => NULL);
                    $this->session->set_userdata('paciente_is_logged_in', TRUE);
                    $this->session->set_userdata('paciente_id', $id);
                    /* Traigo los datos de validadcion de derechos */
                    $aseguramiento = $this->verificar_aseguramiento($id);
                    $this->session->set_userdata('aseguramiento', $aseguramiento);
                }
            }
        }
        echo json_encode($result);
    }

    function crear_cuenta() {

        $this->breadcrumbs->unshift('Crear Nueva Cuenta', '#');
        $this->breadcrumbs->unshift('Solicitud de Citas ', site_url("app/citas"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['tipo_identificacion'] = $this->App_model->get_tipo_identificacion();
        $data['paises'] = $this->App_model->obtener_paises();
        $departamentos = $this->App_model->departamentos();
        $data['departamentos'] = $departamentos->result_array();
        $data['captcha'] = $this->Datos_entidad_model->obtener_entidad_config()->config_general_captcha_public;
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/crear_cuenta';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function obtenerdepartamentos($id) {

        $data['departamentos'] = $this->App_model->departamentos_pais($id)->result_array();
        echo json_encode($data['departamentos']);
    }

    function obtenerciudades($id) {

        $data['ciudad'] = $this->App_model->municipios($id)->result_array();
        echo json_encode($data['ciudad']);
    }

    function check_paciente() {

        $paciente = $this->App_model->get_paciente($_POST['id']);
        if ($paciente == FALSE) {
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }

    function guardar_paciente() {

        $this->load->helper('string');
        $passw = random_string('alnum', 6);
        $datos = array(
            'paciente_nombres' => $this->input->post('nombres'),
            'paciente_apellidos' => $this->input->post('apellidos'),
            'paciente_identificacion' => $this->input->post('id'),
            'paciente_direccion' => $this->input->post('direccion'),
            'paciente_telefono' => $this->input->post('telefono'),
            'paciente_celular' => $this->input->post('celular'),
            'paciente_email' => $this->input->post('email'),
            'paciente_sexo' => $this->input->post('sexo'),
            'paciente_fecha_nacimiento' => $this->input->post('fecha'),
            'paciente_password' => $this->encryption->encrypt($passw),
            'id_ciudad' => ($this->input->post('ciudad') != '') ? $this->input->post('ciudad') : NULL,
            'id_tipo_identificacion' => $this->input->post('tipo_identificacion'),
            'id_estado' => 1
        );
        $paciente = $this->App_model->get_paciente($this->input->post('id'));
        if ($paciente == FALSE) {
            $this->App_model->guardar_paciente($datos);
            $this->email_paciente($this->input->post('email'), $this->input->post('nombres'), $this->input->post('apellidos'), $this->input->post('id'), $passw);
        }
        /* Envio el email para la verificacion */
        $this->confirmacion_cuenta($datos);
    }

    function recuperar_datos() {
        $this->breadcrumbs->unshift('Recuperar Datos de Acceso ', '#');
        $this->breadcrumbs->unshift('Solicitud de Citas ', site_url("app/citas"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['captcha'] = $this->Datos_entidad_model->obtener_entidad_config()->config_general_captcha_public;
        $data['main_content'] = 'app/recuperar_datos';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function restablecer_cuenta() {


        $this->load->helper('string');
        $id = $_POST['id'];
        $email = $_POST['email'];
        $query = $this->App_model->get_paciente($id);
        if ($query == FALSE) {
            $result = array('respuesta' => '2', 'texto' => 'El número de identificación ingresado no se encuentra registrado , por favor verifique los datos ');
        } elseif ($query->paciente_email != $email) {
            $result = array('respuesta' => '2', 'texto' => 'El correo electrónico ingresado no coincide con el registrado previamente, por favor verifique los datos ');
        } else {
            $result = array('respuesta' => '1', 'texto' => 'ok');
            $passw = random_string('alnum', 6);
            $datos = array(
                'paciente_password' => $this->encryption->encrypt($passw)
            );
            $this->App_model->update_paciente($query->id_paciente, $datos);
            $this->email_paciente($email, $query->paciente_nombres, $query->paciente_apellidos, $id, $passw);
        }
        echo json_encode($result);
    }

    function email_paciente($to, $name, $surname, $id, $passw) {
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $html = $name . ' ' . $surname . ", a continuación, podrás encontrar los datos de acceso para la solicitud de citas en línea "
                . "<br>"
                . "<br>"
                . "<ul>"
                . "<li>Número de Identificación</li>"
                . "<li><b>$id</b></li>"
                . "<li>Contraseña</li>"
                . "<li><b>$passw</b></li>"
                . "<li>Ruta de Acceso</li>"
                . "<li><b><a href='" . site_url("app/citas") . "'>" . site_url("app/citas") . "</a></b></li>"
                . "<br><br>"
                . "Para información adicional por favor contáctanos  al teléfono +57 (4) 444 57 55  ";
        $this->email->initialize($config);
        $this->email->from($sender, "E.S.E Hospital del Sur");
        $this->email->to($to);
        $this->email->subject('Datos de acceso a solicitud de citas en línea');
        $this->email->message($html);
        $this->email->send();
    }

    function email_notificacion($to, $name, $surname, $datos) {
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $html = $name . ' ' . $surname . ","
                . "<br>"
                . "<br>";
        $html .= $datos['texto'];
        $html .= "<br><br>"
                . "Para información adicional por favor contáctanos  al teléfono +57 (4) 448 33 11 ";
        $this->email->initialize($config);
        $this->email->from($sender, "E.S.E Hospital del Sur");
        $this->email->to($to);
        $this->email->subject($datos['asunto']);
        $this->email->message($html);
        $this->email->send();
    }

    function confirmacion_cuenta($datos) {
        $this->breadcrumbs->unshift('Confirmación de creación de cuenta ', '#');
        $this->breadcrumbs->unshift('Solicitud de Citas ', site_url("app/citas"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'app/confirmacion_cuenta';
        $data['datos'] = $datos;
        $this->load->view('includes/template', ($data + $this->add));
    }

    function paciente($tab = NULL, $mensaje = NULL) {
        if ($this->session->userdata('paciente_is_logged_in') != TRUE) {
            $this->session->set_userdata('paciente_is_logged_in', FALSE);
            $this->session->set_userdata('paciente_id', NULL);
            $this->session->set_userdata('aseguramiento', NULL);
            redirect(site_url('app/citas'), 'refresh');
        }
        $query = $this->App_model->get_paciente($this->session->userdata('paciente_id'));
        $this->breadcrumbs->unshift("$query->paciente_nombres $query->paciente_apellidos", '#');
        $this->breadcrumbs->unshift('Solicitud de Citas ', site_url("app/citas"));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('app/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['captcha'] = $this->Datos_entidad_model->obtener_entidad_config()->config_general_captcha_public;
        $data['main_content'] = 'app/paciente';
        $data['paciente'] = $query;
        $data['tab'] = $tab;
        $data['mensaje'] = $mensaje;
        $data['tipo_identificacion'] = $this->App_model->get_tipo_identificacion();
        $data['info_ciudad'] = $this->App_model->get_ciudad($query->id_ciudad);
        $data['paises'] = $this->App_model->obtener_paises();
        if ($data['info_ciudad'] != FALSE) {
            $data['departamentos'] = $this->App_model->get_departamentos($data['info_ciudad']->ciudad_id_pais);
            $data['ciudades'] = $this->App_model->get_ciudades($data['info_ciudad']->ciudad_id_departamento);
        }
        $data['servicios'] = $this->App_model->get_servicios();
        $data['sedes'] = $this->App_model->get_sedes();
        $data['citas'] = $this->App_model->get_citas_paciente($query->id_paciente);
        $this->load->view('includes/template', ($data + $this->add));
    }

    function update_paciente() {
        $datos = array(
            'paciente_nombres' => $this->input->post('nombres'),
            'paciente_apellidos' => $this->input->post('apellidos'),
            'paciente_direccion' => $this->input->post('direccion'),
            'paciente_telefono' => $this->input->post('telefono'),
            'paciente_celular' => $this->input->post('celular'),
            'paciente_email' => $this->input->post('email'),
            'paciente_sexo' => $this->input->post('sexo'),
            'paciente_fecha_nacimiento' => $this->input->post('fecha'),
            'id_ciudad' => ($this->input->post('ciudad') != '') ? $this->input->post('ciudad') : NULL,
            'id_tipo_identificacion' => $this->input->post('tipo_identificacion'),
        );
        $this->App_model->update_paciente($this->input->post('id'), $datos);
        echo json_encode(TRUE);
    }

    function update_paciente_passw() {
        $datos = array(
            'paciente_password' => $this->encryption->encrypt($this->input->post('passw'))
        );
        $this->App_model->update_paciente($this->input->post('id'), $datos);
        echo json_encode(TRUE);
    }

    function check_passw() {

        $paciente = $this->App_model->get_paciente($_POST['id_paciente']);
        $password_db = $this->encryption->decrypt($paciente->paciente_password);
        if ($password_db == $_POST['id']) {
            echo json_encode(TRUE);
        } else {
            echo json_encode(FALSE);
        }
    }

    function paciente_sing_out() {
        $this->session->set_userdata('paciente_is_logged_in', FALSE);
        $this->session->set_userdata('paciente_id', NULL);
        $this->session->set_userdata('aseguramiento', NULL);
        redirect(site_url('app/citas'), 'refresh');
    }

    public function verificar_aseguramiento($numero) {

        $this->load->library('csvimport');
        $result = array();
        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato"</div>';
        $dato = $this->App_model->get_aseguramiento();
        if ($dato != FALSE) {
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_aseguramiento";
            $dbEntidades = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_entidades";
            $dbActualizacion = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_fec_actualiza";
            if (file_exists($dbName)) {
                $texto = '
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Numero de Identificación</th><th>Nombres </th><th>Apellidos</th><th>Fecha de Nacimiento</th><th>Entidad</th><th>Fecha de Actualización</th></tr></thead><tbody>
                    ';
                try {
                    $flag = FALSE;
                    $aseguramiento = $this->csvimport->get_array($dbName);
                    $entidades = $this->csvimport->get_array($dbEntidades);
                    $actualizacion = $this->csvimport->get_array($dbActualizacion);
                    foreach ($aseguramiento as $row) {
                        if ($row["IDENTIFICACION"] == $numero) {
                            $texto .= '<tr>'
                                    . '<td>' . round($row['IDENTIFICACION']) . '</td>';
                            $texto .= '<td>' . $row['NOMBRE1'] . ' ' . $row['NOMBRE2'] . '</td>';
                            $texto .= '<td>' . $row['APELLIDO1'] . ' ' . $row['APELLIDO2'] . '</td>';
                            $texto .= '<td>' . $row['FECHANACIMIENTO'] . '</td>';
                            $nombreentidad = "N/A";
                            foreach ($entidades as $row2) {
                                if ($row2['CODENTIDAD'] == $row['CODIGOSENTIDAD']) {
                                    $nombreentidad = $row2['NOMENTIDAD'];
                                }
                            }
                            $texto .= '<td>' . $nombreentidad . '</td>';
                            $diaactualiza = "N/A";
                            foreach ($actualizacion as $row3) {
                                if ($row3['CODACTUALIZACION'] == $row['CODACTUALIZA']) {
                                    $diaactualiza = $row3['FECHAULTIMAACTUALIZA'];
                                }
                            }
                            $texto .= '<td>' . $diaactualiza . '</td>'
                                    . '</tr>';
                            $flag = TRUE;
                        }
                    }
                    $texto .= ' </tbody>
                </table>
               </div>';
                    if ($flag == TRUE) {
                        $result['estado'] = 1/* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = $texto;
                    } else {
                        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = '<div class="alert alert-danger" role="alert"><i class="fa fa-info-circle"></i> Actualmente no cuentas con cobertura en nuestra entidad por lo cual no puede realizar el registro de solicitud de citas en línea. <br><br>Para cualquier inquietud o información adicional lo invitamos a que se ponga en contacto en nuestras líneas de atención. </div>';
                    }
                } catch (Exception $e) {
                    echo 'No encontro registros' . $e;
                }
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 2</div>';
            }
        } else {
            $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
            $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 1</div>';
        }

        return $result;
    }

    function buscar_citas() {
        $paciente = $this->App_model->get_paciente($_POST['id']);
        $citas = $this->App_model->get_citas_paciente($paciente->id_paciente, NULL, $_POST['servicio']);
        if ($citas != FALSE) {
            $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
            $texto = '<div class="alert alert-danger"><i class="fa fa-info-circle"></i> Usted ya cuenta con una cita previamente ingresada para este servicio, si requiere reprogramar por favor cancélela y vuelva a solicitar una nueva cita. </div>
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Sede</th><th>Servicio </th><th>Fecha</th><th>Hora</th></tr></thead><tbody>
                    ';
            foreach ($citas as $cita) {
                $texto .= '<tr>'
                        . '<td>' . $cita->sede . '</td>'
                        . '<td>' . $cita->servicio . '</td>'
                        . '<td>' . $cita->citas_dia . '</td>'
                        . '<td>' . $cita->citas_hora . '</td>'
                        . '</tr>';
            }
            $texto .= ' </tbody>
                </table>
               </div>';
            $result['mensaje'] = $texto;
        } else {

            $result['objeto'] = $this->retornar_citas_disponibles($_POST['sede'], $_POST['servicio']);
            if ($result['objeto'] != FALSE) {
                $setup = $this->App_model->get_setup_by_sede_servicio($_POST['sede'], $_POST['servicio']);
                $result['estado'] = 1;
                $result['mensaje'] = "<div class='alert alert-info'><i class='fa fa-calendar'></i> Agendamiento desde el $setup->setup_servicios_x_sedes_fecha_inicio hasta el  $setup->setup_servicios_x_sedes_fecha_final</div>";
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = '<div class="alert alert-danger"><i class="fa fa-info-circle"></i> No se encontró agendamiento disponible para este servicio, por favor intente luego o cambie la sede para verificar disponibilidad. </div>';
            }
        }
        echo json_encode($result);
    }

    function calendario() {
        $this->load->helper('string');
        $resultado = array();
        $objeto = $this->retornar_citas_disponibles($_POST['sede'], $_POST['servicio']);
        $colores = array(1 => '#00B69F', 2 => '#FF023E');
        $x = 0;
        foreach ($objeto as $obj) {
            $url = ($obj[3] == 1) ? site_url('app/guardar_cita/' . $obj[4] . '/' . $obj[0] . '/' . $obj[1]) : '';
            $datos = array(
                'id' => "$obj[4]",
                'title' => ($obj[3] == 1) ? 'Disponible' : 'No Disponible',
                'start' => $obj[0] . ' ' . $obj[1],
                'end' => $obj[0] . ' ' . $obj[2],
                'url' => "#",
                'color' => $colores[$obj[3]],
                'hora' => $obj[1],
                'dia' => $obj[0],
                'disponible' => $obj[3]
            );
            array_push($resultado, $datos);
            $x++;
        }
        echo json_encode($resultado);
    }

    function guardar_cita() {
        if ($this->session->userdata('paciente_is_logged_in') != TRUE) {
            $this->session->set_userdata('paciente_is_logged_in', FALSE);
            $this->session->set_userdata('paciente_id', NULL);
            $this->session->set_userdata('aseguramiento', NULL);
            redirect(site_url('app/citas'), 'refresh');
        }
        $query = $this->App_model->get_paciente($this->session->userdata('paciente_id'));
        /* verifico que la hora todavia este disponible */
        $check = $this->App_model->check_cita_hora($_POST['id'], $_POST['dia'], $_POST['hora']);
        $setup = $this->App_model->get_un_setup($_POST['id']);
        if ($setup->setup_servicios_x_sedes_cantidad > $check) {
            /* Guardo la cita */
            $datos = array(
                'id_setup_servicios_x_sedes' => $setup->id_setup_servicios_x_sedes,
                'id_paciente' => $query->id_paciente,
                'citas_dia' => $_POST['dia'],
                'citas_hora' => $_POST['hora']
            );
            $dia = $_POST['dia'];
            $hora = $_POST['hora'];
            $this->App_model->guardar_cita($datos);
            $email = array(
                'asunto' => 'Solicitud de cita ',
                'texto' => "<p>Este correo es una confirmación de su solicitud de cita realizada mediante nuestro sitio web. Recuerde que esta se encuentra en proceso de validación por parte de nuestro personal y en las próximas 6 horas usted recibirá un correo electrónico con la aprobación o negación de su cita, los datos agendados son los siguientes: </p>"
                . "<table cellspacing='2' cellpadding='2' border='1' style='text-align:left'>"
                . "<tr>"
                . "<th>Sede</th>"
                . "<td>$setup->sede</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Servicio</th>"
                . "<td>$setup->servicio</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Fecha</th>"
                . "<td>$dia</td>"
                . "</tr>"
                . "<tr>"
                . "<th>Hora</th>"
                . "<td>$hora</td>"
                . "</tr>"
                . "</table>"
                . "<br>"
            );
            $this->email_notificacion($query->paciente_email, $query->paciente_nombres, $query->paciente_apellidos, $email);
            $mensaje = 1;
        } else {
            $mensaje = 2;
        }
        /* ENVIO EL CORREO ELECTRINICO */
        echo json_encode($mensaje);
    }

    function eliminar_cita($id) {
        $cita = $this->App_model->get_un_cita($id);
        $this->App_model->delete_cita($id);
        $setup = $this->App_model->get_un_setup($cita->id_setup_servicios_x_sedes);
        $query = $this->App_model->get_paciente_id($cita->id_paciente);
        $email = array(
            'asunto' => 'Cancelación de agendamiento ',
            'texto' => "<p>Este correo es una confirmación de la cancelación de la cita generada previamente, los datos de la cita son los siguientes: </p>"
            . "<table cellspacing='2' cellpadding='2' border='1' style='text-align:left'>"
            . "<tr>"
            . "<th>Sede</th>"
            . "<td>$setup->sede</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Servicio</th>"
            . "<td>$setup->servicio</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Fecha</th>"
            . "<td>$cita->citas_dia</td>"
            . "</tr>"
            . "<tr>"
            . "<th>Hora</th>"
            . "<td>$cita->citas_hora</td>"
            . "</tr>"
            . "</table>"
            . "<br>"
        );
        $this->email_notificacion($query->paciente_email, $query->paciente_nombres, $query->paciente_apellidos, $email);
        echo json_encode(TRUE);
    }

    function retornar_citas_disponibles($sede, $servicio) {
        /* Primero obntengo el Setup */
        $disponibilidad = array();
        $setup = $this->App_model->get_setup_by_sede_servicio($sede, $servicio);
        if ($setup == FALSE) {
            //   echo 'nada';
            return FALSE;
        } else {
            //  echo 'si encontro';
            /* Parametros */
            $tiempo_citas = $setup->setup_servicios_x_sedes_tiempo;
            $cantidad = $setup->setup_servicios_x_sedes_cantidad;
            /* Traigo las citas del setup */
            $this->App_model->get_citas_setup($setup->id_setup_servicios_x_sedes);
            $array_dias = array('domingos', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabados');
            // Start date
            $date = $setup->setup_servicios_x_sedes_fecha_inicio;
            // End date
            $end_date = $setup->setup_servicios_x_sedes_fecha_final;
            /* recorro los dias */
            while (strtotime($date) <= strtotime($end_date)) {
                $festivo = FALSE;
                if (date("Y-m-d", strtotime($date)) >= date('Y-m-d')) {
                    if ($setup->setup_servicios_x_sedes_festivos == 'Si') {
                        if ($this->check_dia_habil(date("Y-m-d", strtotime($date))) == TRUE) {
                            $festivo = TRUE;
                        }
                    }
                    // echo "$date\n <br> $festivo";
                    if ($festivo == FALSE) {
                        /* rocorreo las horas */
                        $start = 'setup_servicios_x_sedes_' . $array_dias[date("w", strtotime($date))] . '_hora_inicio';
                        $end = 'setup_servicios_x_sedes_' . $array_dias[date("w", strtotime($date))] . '_hora_fin';
                        $hora_inicio = $setup->$start;
                        $hora_fin = $setup->$end;
                        $open_time = strtotime($hora_inicio);
                        $close_time = strtotime($hora_fin);
                        if (date("Y-m-d", strtotime($date)) == date('Y-m-d')) {
                            if (date("H:i", strtotime($hora_inicio)) <= date('H:i')) {
                                $open_time = strtotime(date('H:00')) + 60 * 60;
                            }
                        }
                        $now = time();
                        for ($i = $open_time; $i < $close_time; $i += ($tiempo_citas * 60)) {
                            /* verifico si ya hay citas en este horario,dia */
                            $hay_citas = $this->App_model->check_cita_hora($setup->id_setup_servicios_x_sedes, $date, date("H:i", $i));
                            if ($cantidad > $hay_citas) {
                                //echo "<option>" . date("l - H:i", $i) . "</option>";
                                /* Verificao hora de almuerzo */
                                if ($setup->setup_servicios_x_sedes_almuerzo == 'Si') {
                                    if (date("H:i", $i) >= '12:00' && date("H:i", $i) <= '12:59') {
                                        array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 2, $setup->id_setup_servicios_x_sedes));
                                    } else {
                                        array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 1, $setup->id_setup_servicios_x_sedes));
                                    }
                                } else {
                                    array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 1, $setup->id_setup_servicios_x_sedes));
                                }
                            } else {
                                array_push($disponibilidad, array($date, date("H:i", $i), date("H:i", ($i + ($tiempo_citas * 60))), 2, $setup->id_setup_servicios_x_sedes));
                            }
                        }
                    }
                }
                $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
            }
            //   echo '<pre>';
            //  print_r($disponibilidad);
            //  echo '</pre>';

            return $disponibilidad;
        }
    }

    function check_dia_habil($dia) {
        $this->load->library('festivos');
        $fest = new Festivos();
        return $fest->esFestivo(date("d", strtotime($dia)), date("m", strtotime($dia)), date("Y", strtotime($dia)));
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    