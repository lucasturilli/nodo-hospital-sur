<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coming_soon extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
    }

    public function index() {

        $data['logo'] = $this->Datos_entidad_model->obtener_entidad_logo();
        $data['entidad_contacto'] = $this->Datos_entidad_model->obtener_datos_contacto();
        $data['social'] = $this->Datos_entidad_model->obtener_entidad_redes_sociales();
        $data['seo'] = $this->Datos_entidad_model->obtener_entidad_seo();
        $this->load->view('coming_soon/page', $data);
    }

}
