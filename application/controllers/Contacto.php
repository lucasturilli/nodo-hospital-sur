<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contacto extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->helper('cookie');
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index($mensaje = NULL) {

        $this->breadcrumbs->unshift('Contacto', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'contacto';
        $data['mensaje'] = $mensaje;
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function enviar_email() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('email', 'Correo electrónico ', 'required|valid_email');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required');
        $seo = $this->Datos_entidad_model->obtener_entidad_seo();
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $this->load->library('email');
            $smtp = $this->Referenciales_nodo_model->obtener_smtp();
            if ($smtp != FALSE) {
                if ($smtp->smtp_host != NULL) {
                    $config['protocol'] = 'smtp';
                    $config['smtp_host'] = $smtp->smtp_host;
                    $config['smtp_port'] = $smtp->smtp_port;
                    $config['smtp_user'] = $smtp->smtp_user;
                    $config['smtp_pass'] = $smtp->smtp_pass;
                }
                $sender = $smtp->smtp_email;
            } else {
                $sender = $entidad_contacto->entidad_contacto_email;
            }
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $this->email->initialize($config);
            $this->email->from($sender, "E.S.E Hospital del Sur");
            $this->email->to($this->Datos_entidad_model->obtener_datos_contacto()->entidad_contacto_email);
            $this->email->cc($this->input->post('email'));
            $this->email->subject('Nuevo mensaje generado desde el sitio web');
            $this->email->message($this->input->post('mensaje') . '<br /><br /><br />Enviado por<br />' . $this->input->post('nombre') . '<br />Correo electrónico<br />' . $this->input->post('email') . '<br />Teléfono de contacto <br />' . $this->input->post('telefono'));
            $this->email->send();
            $this->index(TRUE);
        }
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    