<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bienestar extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->helper('cookie');
        $this->load->library('image_moo');
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index() {
        $this->breadcrumbs->unshift('Bienestar Animal', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'bienestar/home';
        $data['animales_config'] = $this->Aplicaciones_model->obtener_animales_config();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function adopcion($tipo = 0, $sexo = 0, $tamano = 0, $edad = 0) {
        $data['animales'] = $this->Aplicaciones_model->obtener_animales($tipo, $sexo, $tamano, $edad);
        $data['par1'] = $tipo;
        $data['par2'] = $sexo;
        $data['par3'] = $tamano;
        $data['par4'] = $edad;
        $this->breadcrumbs->unshift_clear('Animales en Adopción', '#');
        $this->breadcrumbs->unshift('Bienestar Animal', site_url('bienestar'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'bienestar/adopcion';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function adopcion_ajax($id) {
        $tipo = array(
            '' => 'Seleccione el tipo',
            '1' => 'Canino',
            '2' => 'Felino');

        $sexo = array(
            '' => 'Seleccione el sexo',
            '1' => 'Macho',
            '2' => 'Hembra');

        $tamano = array(
            '' => 'Seleccione el Tamaño',
            '1' => 'Peque&ntilde;o',
            '2' => 'Mediano',
            '3' => 'Grande');
        $edad = array(
            '' => 'Seleccione el rango de edad',
            '1' => 'De 0 a 6 meses',
            '2' => 'De 6 meses a 1 a&ntilde;o',
            '3' => 'De 1 año a 3 a&ntilde;os',
            '4' => 'De 3 a 5 a&ntilde;os',
            '5' => 'De 5 a 8 a&ntilde;os',
            '6' => 'Mas de 8 a&ntilde;os');

        $dato = $this->Aplicaciones_model->obtener_una_animales($id);
        $texto = '   <div class="modal-content ">
    <div class="modal-header bg-secondary">
        <h4 class="modal-title " id="myModalLabel">' . $dato->animales_adopcion_nombre . '</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-5 text-center">
                <img src="' . site_url() . 'uploads/adopcion/' . $dato->animales_adopcion_ficha . '" class="img-thumbnail img-fluid center-block" alt="Responsive image">
            </div>
            <div class="col-md-7">
                <table class="table  table-condensed  table-striped table-sm">
                    <tr>
                        <td>Tipo de Animal</td>
                        <td>' . $tipo[$dato->animales_adopcion_tipo] . '</td>
                    </tr>
                    <tr>
                        <td>Microchip</td>
                        <td>' . $dato->animales_adopcion_microchip . '</td>
                    </tr>
                    <tr>
                        <td>Sexo</td>
                        <td>' . $sexo[$dato->animales_adopcion_sexo] . '</td>
                    </tr>
                    <tr>
                        <td>Esterilizado</td>
                        <td>' . $dato->animales_adopcion_esterilizado . '</td>
                    </tr>
                    <tr>
                        <td>Tamaño</td>
                        <td>' . $tamano[$dato->animales_adopcion_tamano] . '</td>
                    </tr>
                    <tr>
                        <td>Rango de Edad</td>
                        <td>' . $edad[$dato->animales_adopcion_edad] . '</td>
                    </tr>
                    <tr>
                        <td>Raza</td>
                        <td>' . $dato->animales_adopcion_raza . '</td>
                    </tr>
                    <tr>
                        <td>Descripción</td>
                        <td>' . $dato->animales_adopcion_descripcion . '</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
    </div>
</div>';
        echo json_encode($texto);
    }

    public function video_ajax($id) {
        $dato = $this->Aplicaciones_model->obtener_una_animales($id);
        $texto = '        <div class="modal-content ">
            <div class="modal-header bg-secondary">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <h4 class="modal-title " id="myModalLabel">' . $dato->animales_adopcion_nombre . '</h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="' . $dato->animales_adopcion_video . '"></iframe>
</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function formulario_ajax() {
        $nombre = $_POST['nombre'];
        $email = $_POST['email'];
        $telefono = $_POST['telefono'];
        $id = $_POST['id'];
        $dato = $this->Aplicaciones_model->obtener_una_animales($id);
        $animales_config = $this->Aplicaciones_model->obtener_animales_config();
        $seo = $this->Datos_entidad_model->obtener_entidad_seo();
        $texto = ' <div class="modal-content">
            <div class="modal-header alert alert-secondary">
                <h4 class="modal-title " id="myModalLabel">Listo</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
            </div>
            <div class="modal-body">
                <p><b>Muchas gracias ' . $nombre . '</b><br><br>
En el menor tiempo posible un funcionario de la entidad se pondrá en contacto contigo para seguir el proceso de adopción.<br><br>
Muchas gracias
</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="location.reload();" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $this->email->initialize($config);
        $this->email->from($sender, $seo->entidad_seo_titulo);
        $this->email->to(($animales_config->animales_config_email != NULL) ? "$animales_config->animales_config_email" : "$entidad_contacto->entidad_contacto_email");
        $this->email->cc($email);
        $this->email->subject('Nueva adopción registrada desde el sitio web ');
        $this->email->message('Mensaje generado desde el sitio web para la adopción de animales' . '<br /><br /><br />Nombre y apellidos del solicitante: ' . $nombre . '<br />Correo electrónico: ' . $email . '<br />Teléfono de contacto: ' . $telefono . '<br />Id Animal: ' . $id . '<br />Nombre: ' . $dato->animales_adopcion_nombre);
        $this->email->attach(site_url() . 'uploads/adopcion/' . $dato->animales_adopcion_ficha);
        $this->email->send();
        echo json_encode($texto);
    }

    public function clasificados() {
        $this->breadcrumbs->unshift_clear('Avisos Clasificados', '#');
        $this->breadcrumbs->unshift('Bienestar Animal', site_url('bienestar'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'bienestar/clasificados';
        $data['animales_config'] = $this->Aplicaciones_model->obtener_animales_config();
        $data['clasificados'] = $this->Aplicaciones_model->get_animales_clasificados();
        /* Archivo los que requieran archivar */
        $animales_config = $this->Aplicaciones_model->obtener_animales_config();
        if ($data['clasificados'] != NULL) {
            foreach ($data['clasificados'] as $clasificado) {
                $diff = strtotime(date('Y-m-d')) - strtotime($clasificado->animales_clasificados_fecha);
                $diff = $diff / 60 / 60 / 24;
                if ($diff > $animales_config->animales_config_tiempo_clasificados) {
                    $this->Aplicaciones_model->update_animales_clasificado($clasificado->id_animales_clasificados, 4);
                    /* Envio el correo electronico de notificacion */
                    $seo = $this->Datos_entidad_model->obtener_entidad_seo();
                    $this->load->library('email');
                    $smtp = $this->Referenciales_nodo_model->obtener_smtp();
                    if ($smtp != FALSE) {
                        if ($smtp->smtp_host != NULL) {
                            $config['protocol'] = 'smtp';
                            $config['smtp_host'] = $smtp->smtp_host;
                            $config['smtp_port'] = $smtp->smtp_port;
                            $config['smtp_user'] = $smtp->smtp_user;
                            $config['smtp_pass'] = $smtp->smtp_pass;
                        }
                        $sender = $smtp->smtp_email;
                    } else {
                        $sender = $entidad_contacto->entidad_contacto_email;
                    }
                    $config['charset'] = 'utf-8';
                    $config['newline'] = "\r\n";
                    $config['mailtype'] = 'html'; // or html
                    $this->email->initialize($config);
                    $this->email->from($sender, $seo->entidad_seo_titulo);
                    $this->email->to("$clasificado->animales_clasificados_solicitante_email");
                    $this->email->subject('Aviso Clasificado Archivado');
                    $this->email->message("Este mensaje es generado automáticamente por el sitio web de la $seo->entidad_seo_titulo y tiene como propósito informarte que el aviso clasificado publicado con el título de “ $clasificado->animales_clasificados_titulo ” ya sobrepaso el periodo establecido y fue archivado automáticamente por el sistema. <br> <br>
                        Si requieres continuar con la publicación por favor ingresa un nuevo aviso clasificado.");
                    $this->email->send();
                }
            }
        }
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function clasificados_ajax($id) {
        $dato = $this->Aplicaciones_model->get_un_animales_clasificados($id);
        $texto = '        <div class="modal-content">
            <div class="modal-header bg-secondary">
                <h4 class="modal-title " id="myModalLabel">' . $dato->animales_clasificados_titulo . '</h4>
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-condensed table-sm">
                    <tr>
                        <th>Fecha de Ingreso</th>
                        <td>' . $dato->animales_clasificados_fecha . '</td></tr><tr>                        
                        <th>Contacto</th>
                        <td>' . $dato->animales_clasificados_solicitante . '</td></tr><tr>
                        <th>Correo Electrónico</th>
                        <td>' . $dato->animales_clasificados_solicitante_email . '</td></tr><tr>
                        <th>Teléfono de Contacto</th>
                        <td>' . $dato->animales_clasificados_solicitante_contacto . '</td></tr><tr>
                        <th>Contenido del Clasificado</th>
                        <td>' . $dato->animales_clasificados_descripcion . '</td></tr><tr>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button"  class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>';
        echo json_encode($texto);
    }

    public function ingresar_clasificados() {
        $this->breadcrumbs->unshift_clear('Nuevo Aviso Clasificado', '#');
        $this->breadcrumbs->unshift('Avisos Clasificados', site_url('bienestar/clasificados'));
        $this->breadcrumbs->unshift('Bienestar Animal', site_url('bienestar'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'bienestar/add_clasificados';
        $data['animales_config'] = $this->Aplicaciones_model->obtener_animales_config();
        $data['configuracion'] = $this->Datos_entidad_model->obtener_entidad_config();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function upload_file() {
        if (!empty($_FILES)) {
            $config['upload_path'] = "./uploads/tmp/";
            $config['allowed_types'] = 'png|PNG|jpg|JPG|jpeg|JPEG|gif|GIF';
            $config['max_size'] = ((int) (ini_get('upload_max_filesize')) * 1024);

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload("file")) {
                $error = array('estado' => 1, 'error' => $this->upload->display_errors(), 'data' => '');
                echo json_encode($error);
            } else {
                $file = $_FILES["file"]['tmp_name'];
                list($width, $height) = getimagesize($file);
                if ($width < "512" || $height < "512") { // You can add your logic
                    $error = array('estado' => 1, 'error' => "Error : La imagen está muy pequeña, debe ser de mínimo 512 px de alto por 512 px de ancho ", 'data' => '');
                    echo json_encode($error);
                } else {


                    $data = array('estado' => 2, 'error' => '', 'upload_data' => $this->upload->data());
                    echo json_encode($data);
                }
            }
        }
    }

    public function eliminar_archivo($id) {
        $file = array(
            'productos_foto' => NULL
        );
        $this->Crud_productos_model->update_productos($file, $id);
        echo json_encode(TRUE);
    }

    public function guardar_clasificado() {
        $foto = $_POST['id'];
        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $email = $_POST['email'];
        $titulo = $_POST['titulo'];
        $mensaje = $_POST['mensaje'];
        $datos = array(
            'animales_clasificados_fecha' => date('Y-m-d'),
            'animales_clasificados_titulo' => $titulo,
            'animales_clasificados_descripcion' => $mensaje,
            'animales_clasificados_foto' => $foto,
            'animales_clasificados_solicitante' => $nombre,
            'animales_clasificados_solicitante_email' => $email,
            'animales_clasificados_solicitante_contacto' => $telefono,
            'animales_clasificados_estado' => 2
        );
        $this->Aplicaciones_model->guardar_clasificado($datos);
        /* Muevo el archivo */
        $file = "uploads/tmp/$foto";
        $output = "uploads/adopcion/$foto";
        rename($file, $output);
        /* REDIMENSIONO LA IMAGEN */
        $this->redim_foto($output);
        /* Envio el correo electronico de notificacion */
        $animales_config = $this->Aplicaciones_model->obtener_animales_config();
        $seo = $this->Datos_entidad_model->obtener_entidad_seo();
        $this->load->library('email');
        $smtp = $this->Referenciales_nodo_model->obtener_smtp();
        if ($smtp != FALSE) {
            if ($smtp->smtp_host != NULL) {
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = $smtp->smtp_host;
                $config['smtp_port'] = $smtp->smtp_port;
                $config['smtp_user'] = $smtp->smtp_user;
                $config['smtp_pass'] = $smtp->smtp_pass;
            }
            $sender = $smtp->smtp_email;
        } else {
            $sender = $entidad_contacto->entidad_contacto_email;
        }
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $this->email->initialize($config);
        $this->email->from($sender, $seo->entidad_seo_titulo);
        $this->email->to(($animales_config->animales_config_email != NULL) ? "$animales_config->animales_config_email" : "$entidad_contacto->entidad_contacto_email");
        $this->email->cc($email);
        $this->email->subject('Nuevo aviso clasificado en la sección de bienestar animal del sitio web ');
        $this->email->message('Mensaje generado desde el sitio web para la publicación de avisos clasificados ' . '<br /><br /><br />Nombre y apellidos del solicitante: ' . $nombre . '<br />Correo electrónico: ' . $email . '<br />Teléfono de contacto: ' . $telefono . '<br />Título del Clasificado: ' . $titulo . '<br />Contenido: ' . $mensaje);
        $this->email->attach(site_url() . 'uploads/adopcion/' . $foto);
        $this->email->send();
        echo json_encode(TRUE);
    }

    function redim_foto($file) {
        $this->image_moo->load($file)->resize_crop(512, 512, TRUE)->save($file, true);
        $this->image_moo->clear_temp();
        return true;
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    