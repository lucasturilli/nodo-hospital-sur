<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sitio extends CI_Controller {

    public $add = array();
    public $header = array();
    public $footer = array();
    public $dias = array('dias' => array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado"));
    public $meses = array('meses' => array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"));

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("America/Bogota");
        setlocale(LC_TIME, 'es_ES');
        $this->load->model('Datos_entidad_model');
        $this->load->model('Datos_municipio_model');
        $this->load->model('Aplicaciones_model');
        $this->load->model('Referenciales_nodo_model');
        $this->load->helper('cookie');
        $this->load->helper('email');
        $this->header = array(
            'actualizacion' => $this->Datos_entidad_model->obtener_actualizacion(),
            'entidad_redes_sociales' => $this->Datos_entidad_model->obtener_entidad_redes_sociales(),
            'seo' => $this->Datos_entidad_model->obtener_entidad_seo(),
            'tab_menu' => NULL,
            'aviso' => $this->Datos_entidad_model->obtener_banner_aviso(),
            'empleo_link' => $this->Datos_entidad_model->obtener_entidad_ofertas_empleo(),
            'entidad_logo' => $this->Datos_entidad_model->obtener_entidad_logo(),
            'configuracion' => $this->Datos_entidad_model->obtener_entidad_config()
        );
        $this->footer = array(
            'carrousel' => $this->Aplicaciones_model->obtener_carrousel(),
            'entidad_contacto' => $this->Datos_entidad_model->obtener_datos_contacto(),
            'entidad_politicas_datos' => $this->Datos_entidad_model->obtener_politicas_datos(),
            'script_facebook_twitter' => TRUE,
            'widgets' => $this->Aplicaciones_model->obtener_widgets(),
            'visitas' => $this->Datos_entidad_model->obtener_visitas(),
            'enlaces_footer' => $this->Aplicaciones_model->obtener_enlaces_footer(),
            'informacion_general' => $this->Datos_entidad_model->obtener_entidad_informacion()
        );
        $this->barra_lateral = array('barra_lateral' => $this->Aplicaciones_model->obtener_enlaces_sub_menu());
        $this->add = $this->header + $this->footer + $this->dias + $this->meses + $this->barra_lateral;
        if ($this->session->userdata('visita') == FALSE) {
            $this->session->set_userdata('visita', TRUE);
            if ($this->Datos_entidad_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']) == FALSE) {
                $this->Datos_entidad_model->visitas(array($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT']));
            }
        }
    }

    public function index() {
        $data['survey_ip'] = $this->Aplicaciones_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']);
        $data['noticias'] = $this->Datos_entidad_model->obtener_noticias();
        $data['notificaciones'] = $this->Datos_entidad_model->obtener_notificaciones();
        $data['banner_principal'] = $this->Aplicaciones_model->obtener_banner('principal');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['calendario'] = $this->Datos_entidad_model->obtener_eventos_mes();
        $data['survey'] = $this->Aplicaciones_model->obtener_survey();
        if ($data['survey'] != FALSE) {
            $data['survey_preguntas'] = $this->Aplicaciones_model->obtener_survey_preguntas($data['survey']->id_survey);
            $total = $this->Aplicaciones_model->obtenet_votaciones($data['survey']->id_survey);
            $x = 1;
            $estilos = array('success ', 'info ', 'warning ', 'danger ');
            $texto = '<ul class="list-unstyled">';
            if ($total[0] != NULL) {
                foreach ($total[0] as $dato) {
                    if ($total[1] == 0) {
                        $total[1] = 1;
                    }
                    $valor = round(($dato->total_pregunta / $total[1]) * 100, 0);
                    $color = $estilos[fmod($x, 4)];
                    $texto .= '
                            <li>' . $dato->survey_preguntas_pregunta . ' - ' . $dato->total_pregunta . ' Votos (' . round(($dato->total_pregunta / $total[1]) * 100, 0) . '%)</li>
                            <li>
                            <div class="progress survey_progress">
                                    <div class="progress-bar progress-bar-' . $color . ' progress-bar-striped"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ' . $valor . '%">
                                    </div>
                                </div>
                            </li>';
                    $x++;
                }
                $texto .= '</ul><p>Total de Votos ' . $total[1] . '</p><p class="text-center">Tu voto ya fue registrado !</p>'
                        . '                       <div class="buttons">
                            <a href="' . site_url('sitio/encuestas') . '" data-toggle="tooltip" title="Ver Todas las Encuestas" class="btn button-u gray">Ver Todas</a>
                        </div>';
                $data['survey_texto'] = $texto;
            }
        }
        $data['main_content'] = 'home';
        $data['iconos'] = $this->Aplicaciones_model->obtener_iconos();
        $data['embebido_home'] = $this->Aplicaciones_model->obtener_embebido_home();
        $data['enlaces_sub_menu'] = $this->Aplicaciones_model->obtener_enlaces_sub_menu();
        $data['enlaces'] = $this->Aplicaciones_model->obtener_enlaces();
        $this->load->view('includes/template', ($data + $this->add));
    }

    function newsletter() {
        $email = $_POST['email'];
        if (valid_email($email)) {
            $estado = $this->Datos_entidad_model->verificar_newsletter($email);
            if ($estado == FALSE) {
                //YA ESTA INSCRITO
                echo json_encode(2);
            } else {
                //SE INCRIBIO
                echo json_encode(1);
            }
        } else {
            //CORREO MALO
            echo json_encode(3);
        }
    }

    function survey($survey, $session, $response) {
        if ($session == $this->session->all_userdata()['__ci_last_regenerate']) {
            $ip = $this->Aplicaciones_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']);
            if ($ip == FALSE) {
                $this->Aplicaciones_model->ingresar_votacion($response);
                $this->Aplicaciones_model->ingresar_votacion_ip($_SERVER['REMOTE_ADDR'], $response);
            }
            $cookie = array(
                'name' => 'voto',
                'value' => TRUE,
                'expire' => 86400
            );
            $this->input->set_cookie($cookie);
            $this->session->sess_destroy();
        }
        $total = $this->Aplicaciones_model->obtenet_votaciones($survey);
        $x = 1;
        $estilos = array('success ', 'info ', 'warning ', 'danger ');
        $texto = '<ul class="list-unstyled">';
        if ($total[0] != NULL) {
            foreach ($total[0] as $dato) {
                if ($total[1] == 0) {
                    $total[1] = 1;
                }
                $valor = round(($dato->total_pregunta / $total[1]) * 100, 0);
                $color = $estilos[fmod($x, 4)];
                $texto .= '
                            <li>' . $dato->survey_preguntas_pregunta . ' - ' . $dato->total_pregunta . ' Votos (' . round(($dato->total_pregunta / $total[1]) * 100, 0) . '%)</li>
                            <li>
                            <div class="progress survey_progress">
                                    <div class="progress-bar  progress-bar-' . $color . ' progress-bar-striped"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ' . $valor . '%">
                                    </div>
                                </div>
                            </li>';
                $x++;
            }
            $texto .= '</ul><p>Total de Votos ' . $total[1] . '</p><p class="text-center">Tu voto ya fue registrado !</p>';
        }
        echo json_encode($texto);
    }

    function noticias($paginas = 0) {
        $this->load->library('pagination');
        $this->breadcrumbs->unshift('Noticias', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        //  $data['titulo'] = 'Noticias';
        $data['main_content'] = 'noticias';
        $data['noticias'] = $this->Datos_entidad_model->obtener_noticias();
        $config['base_url'] = site_url('sitio/noticias');
        $config['total_rows'] = count($data['noticias']);
        $config['per_page'] = 12;
        $config['full_tag_open'] = '<hr><div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        $this->pagination->initialize($config);
        $data['paginacion'] = $this->pagination->create_links();
        $data['paginas'] = $paginas;
        $this->load->view('includes/template', ($data + $this->add));
    }

    function ver_noticia($alias) {
        $this->breadcrumbs->unshift('Noticia', '#');
        $this->breadcrumbs->unshift('Noticias', site_url('sitio/noticias'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'ver_noticia';
        $data['noticia'] = $this->Datos_entidad_model->obtener_noticia($alias);
        $data['noticias'] = $this->Datos_entidad_model->obtener_noticias_relacionadas($data['noticia']->entidad_noticias_tags, $data['noticia']->id_entidad_noticias);
        $this->load->view('includes/template', ($data + $this->add));
    }

    function notificaciones($paginas = 0) {
        $this->load->library('pagination');
        $this->breadcrumbs->unshift('Notificaciones - Anuncios', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        //  $data['titulo'] = 'Notificaciones - Anuncios';
        $data['main_content'] = 'notificaciones';
        $data['noticias'] = $this->Datos_entidad_model->obtener_notificaciones();
        $config['base_url'] = site_url('sitio/notificaciones');
        $config['total_rows'] = count($data['noticias']);
        $config['per_page'] = 12;
        $config['full_tag_open'] = '<ul class="pagination paginacion">';
        $config['full_tag_close'] = '</ul>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data['paginacion'] = $this->pagination->create_links();
        $data['paginas'] = $paginas;
        $this->load->view('includes/template', ($data + $this->add));
    }

    function ver_notificacion($alias) {

        $this->breadcrumbs->unshift('Notificación - Anuncio', '#');
        $this->breadcrumbs->unshift('Notificaciones y Anuncios', site_url('sitio/notificaciones'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        //   $data['titulo'] = 'Notificacion - Anuncio';
        $data['main_content'] = 'ver_notificacion';
        $data['noticia'] = $this->Datos_entidad_model->obtener_noticia($alias);
        $data['noticias'] = $this->Datos_entidad_model->obtener_notificaciones_relacionadas($data['noticia']->entidad_noticias_tags, $data['noticia']->id_entidad_noticias);
        $this->load->view('includes/template', ($data + $this->add));
    }

    function calendario() {
        $this->breadcrumbs->unshift('Calendario de Eventos', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'calendario';
        $data['calendario'] = $this->Datos_entidad_model->obtener_eventos();
        $this->load->view('includes/template', ($data + $this->add));
    }

    function calendario_evento($alias) {
        $this->breadcrumbs->unshift('Evento ', '#');
        $this->breadcrumbs->unshift('Calendario de Eventos', site_url('sitio/calendario') . '/' . date('Y') . '/' . date('m'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        //  $data['titulo'] = 'Evento';
        $data['main_content'] = 'ver_evento';
        $data['evento'] = $this->Datos_entidad_model->obtener_evento($alias);
        $data['calendario'] = $this->Datos_entidad_model->obtener_eventos_mes();
        $this->load->view('includes/template', ($data + $this->add));
    }

    function encuestas($encuesta = NULL) {
        $this->breadcrumbs->unshift('Encuestas', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'encuestas';
        $data['survey'] = $this->Aplicaciones_model->obtener_survey_id($encuesta);
        if ($data['survey'] != FALSE) {
            $data['survey_preguntas'] = $this->Aplicaciones_model->obtener_survey_preguntas($data['survey']->id_survey);
            $data['total'] = $this->Aplicaciones_model->obtenet_votaciones($data['survey']->id_survey);
        }
        $data['all_survey'] = $this->Aplicaciones_model->obtener_survey_todos();
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function encuesta() {
        $this->breadcrumbs->unshift('Encuesta', '#');
        $this->breadcrumbs->unshift('Encuestas', site_url('sitio/encuestas'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['survey_ip'] = $this->Aplicaciones_model->get_ultima_visita_ip($_SERVER['REMOTE_ADDR']);
        $data['survey'] = $this->Aplicaciones_model->obtener_survey();
        if ($data['survey'] != FALSE) {
            $data['survey_preguntas'] = $this->Aplicaciones_model->obtener_survey_preguntas($data['survey']->id_survey);
            $total = $this->Aplicaciones_model->obtenet_votaciones($data['survey']->id_survey);
            $x = 1;
            $estilos = array('bg-success ', 'bg-info ', 'bg-warning ', 'bg-danger ');
            $texto = '<dl>';
            foreach ($total[0] as $dato) {
                if ($total[1] == 0) {
                    $total[1] = 1;
                }
                $valor = round(($dato->total_pregunta / $total[1]) * 100, 0);
                $color = $estilos[fmod($x, 4)];
                $texto .= '
                            <dt>' . $dato->survey_preguntas_pregunta . '</dt>
                            <dd><div class="progress survey_progress">
                                    <div class="progress-bar active ' . $color . ' progress-bar-striped"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ' . $valor . '%">
                                    </div>
                                </div>
                                <span class="">' . $dato->total_pregunta . ' Votos (' . round(($dato->total_pregunta / $total[1]) * 100, 0) . '%)</span>
                            </dd>';
                $x++;
            }
            $texto .= '</dl><span class="">Total de Votos ' . $total[1] . '</span><p style="text-align:center; margin-bottom:0px; padding-bottom: 0px; ">Tu voto ya fue registrado !</p>';
            $data['survey_texto'] = $texto;
        }
        $data['main_content'] = 'encuesta';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function not_found() {

        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'not_found';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function pagina($alias) {
        $data['pagina'] = $this->Aplicaciones_model->obtener_pagina($alias);
        if ($data['pagina'] == FALSE) {
            $this->breadcrumbs->unshift_clear('Error 404', '#');
            $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
            $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
            $data['breadcrumbs'] = $this->breadcrumbs->show();
            $data['main_content'] = 'not_found';
            $this->load->view('includes/template', ($data + $this->add));
        } else {
            if ($data['pagina'] != FALSE) {
                $this->breadcrumbs->unshift($data['pagina']->entidad_paginas_nombre, '#');
            }
            $this->breadcrumbs->unshift_clear('Página', '#');
            $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
            $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
            $data['breadcrumbs'] = $this->breadcrumbs->show();
            $data['main_content'] = 'pagina';
            $this->load->view('includes/template', ($data + $this->add));
        }
    }

    public function servicio($alias) {
        $data['pagina'] = $this->Aplicaciones_model->obtener_un_iconos($alias);
        $this->breadcrumbs->unshift($data['pagina']->entidad_iconos_home_nombre, '#');
        $this->breadcrumbs->unshift_clear('Servicio', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'servicio';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function mapa_del_sitio() {

        $this->breadcrumbs->unshift_clear('Mapa del Sitio', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'mapa_del_sitio';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function ventanilla() {
        $this->breadcrumbs->unshift('Ventanilla Única de Atención al Ciudadano ', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'ventanilla';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function tramites_servicios($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $data['search'] = $search;
        $this->breadcrumbs->unshift_clear('Trámites en Línea', '#');
        $this->breadcrumbs->unshift('Ventanilla Única de Atención al Ciudadano ', site_url('sitio/ventanilla'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->load->model('Datos_servicios_model');
        $data['tramites_en_linea'] = $this->Datos_servicios_model->obtener_tramites_en_linea(1);
        $data['main_content'] = 'tramites_servicios';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function notificaciones_en_linea($search = NULL) {
        if ($search != NULL) {
            $search = str_replace('%20', ' ', $search);
        }
        $this->load->model('Datos_servicios_model');
        $this->breadcrumbs->unshift_clear('Servicios y Notificaciones en Línea', '#');
        $this->breadcrumbs->unshift('Ventanilla Única de Atención al Ciudadano ', site_url('sitio/ventanilla'));
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['tramites_en_linea'] = $this->Datos_servicios_model->obtener_tramites_en_linea(2);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'notificaciones_en_linea';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function portal_transaccional() {
        $this->breadcrumbs->unshift('Portal Transaccional', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'portal_transaccional';
        $this->load->view('includes/template', ($data + $this->add));
    }

    function notificaciones_electronicas() {
        $this->breadcrumbs->unshift('Notificación por Aviso', '#');
        $this->breadcrumbs->unshift('<i class="fas fa-home"></i> Inicio', site_url('sitio/index'));
        $this->breadcrumbs->back('<i class="fas fa-arrow-circle-left"></i> Atras', NULL);
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        $data['main_content'] = 'notificaciones_electronicas';
        $this->load->view('includes/template', ($data + $this->add));
    }

    public function notificaciones_electronicas_ajax($palabra) {
        $palabra = str_replace('%20', ' ', $palabra);
        $this->load->model('Aplicaciones_model');
        $dato = $this->Aplicaciones_model->buscar_notificaciones_electronicas($palabra);
        if ($dato != FALSE) {
            $texto = '
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered full_table table-responsive">
                <thead><tr><th>Tipo de Notificación</th><th>Identificación </th><th>Nombres y Apellidos</th><th>Fecha de Fijación</th><th>Fecha de Desfijación</th><th>Observaciones</th><th>Archivo</th></tr></thead><tbody>
                    ';
            foreach ($dato as $row) {
                $texto .= '<tr><td>' . $row->tipo . '</td>';
                $texto .= '<td>' . $row->notificaciones_electronicas_identificacion . '</td>';
                $texto .= '<td>' . $row->notificaciones_electronicas_usuario . '</td>';
                $texto .= '<td>' . $row->notificaciones_electronicas_fecha_fijacion . '</td>';
                $texto .= '<td>' . $row->notificaciones_electronicas_fecha_desfijacion . '</td>';
                $texto .= '<td>' . $row->notificaciones_electronicas_observaciones . '</td>';
                $texto .= '<td><a target="_blank"  href="' . site_url('uploads/notificaciones') . '/' . $row->notificaciones_electronicas_archivo . '" class="btn btn-primary btn-xs" ><i class="fa fa-file-pdf"> </i> Ver Archivo</a></td></tr>';
            }
            $texto .= ' </tbody>
                </table>
               </div>
';
        } else {
            $texto = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se encontró ningún registro con "' . $palabra . '"</div>';
        }
        echo json_encode($texto);
    }
}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    