<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /* =======================================================================================================================================
     * ======================================================================================================================================
     */

    function aseguramiento_ajax2($numero) {
        $this->load->model('App_model');
        $result = array();
        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato con el número (' . $numero . '), por favor verifíquelo e intente de nuevo "</div>';
        $dato = $this->App_model->get_aseguramiento();
        if ($dato != FALSE) {
            $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_archivo";
            if (file_exists($dbName)) {
                $sql = "SELECT ASEGURAMIENTO.*, ENTIDADES.CODENTIDAD, ENTIDADES.NOMENTIDAD, FEC_ACTUALIZA.*
  FROM ( ASEGURAMIENTO
    LEFT JOIN ENTIDADES ON ASEGURAMIENTO.CODIGOSENTIDAD = ENTIDADES.CODENTIDAD )
    LEFT JOIN FEC_ACTUALIZA ON ASEGURAMIENTO.CODACTUALIZA = FEC_ACTUALIZA.CODACTUALIZACION
 WHERE ( ASEGURAMIENTO.IDENTIFICACION =$numero)";
                try {
                    $flag = FALSE;
                    $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");
                    $rs = $db->prepare($sql);
                    $rs->execute();
                    $texto = '
               <div class="table-responsive">
                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                <thead><tr><th>Numero de Identificación</th><th>Nombres </th><th>Apellidos</th><th>Fecha de Nacimiento</th><th>Entidad</th><th>Fecha de Actualización</th></tr></thead><tbody>
                    ';
                    while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
                        $texto .= '<tr>'
                                . '<td>' . round($row['IDENTIFICACION']) . '</td>';
                        $texto .= '<td>' . $row['NOMBRE1'] . ' ' . $row['NOMBRE2'] . '</td>';
                        $texto .= '<td>' . $row['APELLIDO1'] . ' ' . $row['APELLIDO2'] . '</td>';
                        $texto .= '<td>' . $row['FECHANACIMIENTO'] . '</td>';
                        $texto .= '<td>' . $row['NOMENTIDAD'] . '</td>';
                        $texto .= '<td>' . $row['FECHAULTIMAACTUALIZA'] . '</td>';
                        $flag = TRUE;
                    }
                    $texto .= ' </tbody>
                </table>
               </div>';
                    if ($flag == TRUE) {
                        $result['estado'] = 1/* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = $texto;
                    } else {
                        $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                        $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se registra ningún dato con el número (' . $numero . '), por favor verifíquelo e intente de nuevo "</div>';
                    }
                } catch (Exception $e) {
                    echo 'No encontro registros' . $e;
                }
            } else {
                $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
                $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 2</div>';
            }
        } else {
            $result['estado'] = 0 /* 0 para error 1 para servicio ok */;
            $result['mensaje'] = '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> El servicio no se encuentra disponible en el momento, por favor intente más tarde. CÓD. 1</div>';
        }

        echo json_encode($result);
    }


    function test() {
        $this->load->model('App_model');
        $dato = $this->App_model->get_aseguramiento();
        echo "File Route:<br>";
        echo FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_archivo" . '<br>';
        echo 'Read the File:<br>';
        $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_archivo";
        if (!file_exists($dbName)) {
            die("Could not find database file.");
        }
        echo 'Done ! , File Exist<br>';
        try {
            $db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$dbName; Uid=; Pwd=;");

            $sql = "SELECT * FROM ENTIDADES";
            $rs = $db->prepare($sql);
            $rs->execute();
            while ($result = $rs->fetch(PDO::FETCH_ASSOC)) {
                echo "<pre>";
                echo print_r($result);
                echo "</pre>";
            }
        } catch (Exception $e) {
            echo $e;
        }
    }

    function test2() {
        $this->load->model('App_model');
        $this->load->library('csvimport');
        $dbName = FCPATH . "uploads/app/aseguramiento/ASEGURAMIENTO_data.csv";
        $file_data = $this->csvimport->get_array($dbName);
        echo "<pre>";
        echo print_r($file_data);
        echo "</pre>";
        foreach ($file_data as $row) {
            $data[] = array(
                'first_name' => $row["First Name"],
                'last_name' => $row["Last Name"],
                'phone' => $row["Phone"],
                'email' => $row["Email"]
            );
        }
        // $this->csv_import_model->insert($data);
    }

    function test3() {
        $this->load->model('App_model');
        $this->load->library('csvimport');
        $dato = $this->App_model->get_aseguramiento();
        $dbName = FCPATH . "uploads/app/aseguramiento/$dato->aseguramiento_entidades";
        $file_data = $this->csvimport->get_array($dbName);
        /* foreach ($file_data as $row) {
          if ($row["IDENTIFICACION"] == 1011411057) {
          echo "<pre>";
          echo print_r($row);
          echo "</pre>";
          }
          } */
        echo "<pre>";
        echo print_r($file_data);
        echo "</pre>";
    }
