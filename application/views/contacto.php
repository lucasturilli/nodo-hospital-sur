<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Contáctenos</a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <iframe src="<?php echo $entidad_contacto->entidad_contacto_google_mapa; ?>" width="100%" height="300" frameborder="0" style="border:0"></iframe>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <h4><strong>Redes sociales</strong></h4>
                    <div class="share-area footer-social  clearfix">
                        <?php
                        if ($entidad_redes_sociales != FALSE) {
                            foreach ($entidad_redes_sociales as $dato) {
                                echo '<a target="_blank" href="' . $dato->entidad_redes_sociales_link . '" data-toggle="tooltip" data-placement="bottom" title="Ver información en la red social ' . $dato->entidad_redes_sociales_definicion_nombre . '" class="text-capitalize"><i class="fab ' . $dato->entidad_redes_sociales_definicion_imagen . '"></i></a>';
                            }
                        }
                        ?>
                    </div>
                    <h4><strong>Información de contacto</strong></h4>
                    <table class="table table-borderless">
                        <tr>
                            <td><i class="fas fa-location-arrow "></i></td>
                            <td><?php echo $seo->entidad_seo_ciudad; ?>, <?php echo $seo->entidad_seo_region; ?> - Colombia<br> <?php echo $entidad_contacto->entidad_contacto_direccion; ?></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-phone "></i></td>
                            <td><?php echo $entidad_contacto->entidad_contacto_telefono; ?></td>
                        </tr>
                        <tr>
                            <td><i class="fas  fa-envelope "></i></td>
                            <td><?php echo $entidad_contacto->entidad_contacto_email; ?></td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-print "></i></td>
                            <td><?php echo $entidad_contacto->entidad_contacto_fax; ?></td>
                        </tr>
                    </table>
                    <h4><strong>Correo electrónico para notificaciones judiciales</strong></h4>
                    <table class="table table-borderless">
                        <tr>
                            <td><i class="fas  fa-envelope "></i></td>
                            <td><?php echo $entidad_contacto->entidad_contacto_email_notificaciones_judiciales; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <?php if ($mensaje != TRUE) { ?>
                        <h4><strong>Ponte en contacto con nosotros </strong></h4>
                        <p><small>Escríbenos todos tus comentarios, recuerda que los campos con (*) son obligatorios.</small></p>
                        <?php
                        $attributes = array('id' => 'contact_form');
                        echo form_open('contacto/enviar_email', $attributes);
                        //echo form_input('nombre', set_value('nombre'));
                        ?>
                        <div class="form-group <?php echo (form_error('nombre') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="nombre">Nombre*</label>
                            <input name="nombre" type="text" class="form-control required" value="<?php set_value('nombre') ?>" required="" placeholder="Ingrese su nombre y apellidos">
                        </div>
                        <div class="form-group <?php echo (form_error('email') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="email">Correo electrónico*</label>
                            <input type="email" class="form-control required" name="email" value="<?php set_value('email') ?>" required="" placeholder="Ingrese su correo electrónico">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Teléfono de contacto</label>
                            <input type="tel" name="telefono" class="form-control" value="<?php set_value('telefono') ?>" placeholder="Ingrese un número de contacto">
                        </div>
                        <div class="form-group <?php echo (form_error('mensaje') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="mensaje">Mensaje*</label>
                            <textarea class="form-control required" name="mensaje" value="<?php set_value('mensaje') ?>" required="" rows="2"></textarea>
                        </div>
                        <button id="submit_contact_form" type="submit" class="btn button-u">Enviar mensaje <i class="fas fa-envelope"></i></button>
                        <?php
                        echo form_close();
                    } else {
                        ?>
                        <div class="alert alert-success" role="alert"><strong>Su correo fue recibido exitosamente</strong><br/>
                            Próximamente le estaremos comunicando la respectiva respuesta a su mensaje. Muchas gracias por escribirnos. 
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </article>
</div>

