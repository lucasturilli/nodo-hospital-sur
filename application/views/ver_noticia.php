<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link href="<?php echo site_url(); ?>css/social-share-kit.css" rel="stylesheet">
<script src="<?php echo site_url(); ?>js/social-share-kit.min.js"></script>



<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#"><?php echo $noticia->entidad_noticias_titulo; ?></a></h3>
        </div>
        <div class="single-post">
            <div class="post_time hidden-xs">
                <ul class="list-unstyled">
                    <li><i class="fa fa-calendar" aria-hidden="true"></i>Fecha de Ingreso: <?php echo date('d', strtotime($noticia->entidad_noticias_fecha_ingreso)) . " de " . $meses[date('n', strtotime($noticia->entidad_noticias_fecha_ingreso)) - 1] . " del " . date('Y', strtotime($noticia->entidad_noticias_fecha_ingreso)); ?></li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i>Última actualización: <?php echo date('d', strtotime($noticia->entidad_noticias_actualizacion)) . " de " . $meses[date('n', strtotime($noticia->entidad_noticias_actualizacion)) - 1] . " del " . date('Y', strtotime($noticia->entidad_noticias_actualizacion)); ?></li>
                </ul>
            </div>
            <?php if ($noticia->entidad_noticias_imagen != NULL) { ?>
                <?php if ($noticia->entidad_noticias_enlace_imagen == NULL) { ?>
                    <img class="img-responsive center-block"  src="<?php echo site_url('uploads/noticias') . '/' . $noticia->entidad_noticias_imagen ?>">
                <?php } else { ?>
                    <a href="<?php echo prep_url($noticia->entidad_noticias_enlace_imagen); ?>" target="<?php echo $noticia->entidad_noticias_enlace_tipo; ?>"><img class="img-responsive center-block"  src="<?php echo site_url('uploads/noticias') . '/' . $noticia->entidad_noticias_imagen ?>"></a>
                <?php } ?>
                <?php if ($noticia->entidad_noticias_rights_imagen != NULL) { ?>
                    <blockquote class="blockquote blockquote-reverse">
                        <footer class="blockquote-footer"><?php echo $noticia->entidad_noticias_rights_imagen; ?></footer>
                    </blockquote>
                <?php } else { ?>
                    <br>
                <?php } ?>

            <?php } ?>
            <?php if ($noticia->entidad_noticias_audio != NULL) { ?>
                <audio controls >
                    <source src="<?php echo site_url('uploads/noticias') . '/' . $noticia->entidad_noticias_audio; ?>" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio>
            <?php } ?>
            <div class="post_content">
                <?php echo $noticia->entidad_noticias_descripcion; ?>
            </div>
            <div class="tag-area">
                <div class="tag-icon">
                    <img src="<?php echo site_url(); ?>/images/tag_icon.png" alt="image">
                </div>
                <div class="tag-item">
                    <?php
                    $tags = explode(',', $noticia->entidad_noticias_tags);
                    if ($tags != FALSE) {
                        foreach ($tags as $tag) {
                            ?>
                            <span class="badge badge-pill badge-dark"><?php echo $tag ?></span>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="text-right">
                <div class="ssk-group ssk-count  ssk-round">
                    <a href="" class="ssk ssk-facebook"></a>
                    <a href="" class="ssk ssk-twitter"></a>
                    <a href="" class="ssk ssk-google-plus"></a>
                    <a href="" class="ssk ssk-pinterest"></a>
                    <a href="" class="ssk ssk-vk"></a>
                    <a href="" class="ssk ssk-linkedin"></a>
                    <a href="" class="ssk ssk-buffer"></a>
                    <a href="" class="ssk ssk-email"></a>
                </div>
            </div>
        </div>
    </article>
    <article class="blog-item">
        <div class="section-title">
            <h2><i class="fas fa-circle"></i> Noticias Relacionadas</h2>
        </div>
        <?php
        if ($noticias != FALSE) {
            $cantidad = count($noticias);
            $r = 1;
            $j = 1;
            $variables = array(rand(1, $cantidad), rand(1, $cantidad), rand(1, $cantidad), rand(1, $cantidad));
            foreach ($noticias as $dato) {
                if ($r <= 3 && in_array($j, $variables)) {
                    ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="notice-content">
                            <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><img src="<?php echo site_url('uploads/noticias') . '/' . $dato->entidad_noticias_imagen; ?>" alt="" data-toggle="tooltip" data-placement="top" title="Ampliar Información"></a>
                            <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>" class="title2" href="#"><?php echo $dato->entidad_noticias_titulo; ?></a>
                            <div class="social flex">
                                <div class="share">
                                    <a data-toggle="tooltip" title="Compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><i class="fab fa-facebook-f"></i></a>
                                    <a data-toggle="tooltip" title="Compartir" href="https://twitter.com/intent/tweet?text=E.S.E Hospital del Sur <?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?> vía <?php echo $seo->entidad_seo_twitter_vcard; ?>"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $r++;
                }
                $j++;
            }
        }
        ?>
        <div class="col-xs-12">
            <a href="<?php echo site_url('sitio/noticias'); ?>" class="more" data-toggle="tooltip" title="Ver Todas las Noticias">+ Ver Todas las Noticias</a>
        </div>
    </article>
</div>



<script type="text/javascript">
    SocialShareKit.init();
</script>