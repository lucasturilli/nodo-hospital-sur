<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/moment.min.js"></script>
<!-- Full Calendar -->
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/locale/es.js"></script>


<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Calendario de Eventos</a></h3>
        </div>
        <div class="col-md-12 col-lg-12 bg-calendar">
            <div id="calendar"></div>
        </div>  
    </article>
</div>

<script>
    $(document).ready(function () {
    /* initialize the calendar
     -----------------------------------------------------------------*/
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $('#calendar').fullCalendar({
    header: {
    left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
    },
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            locale: 'es',
            height: 600,
            eventColor: '#00B69F',
            events: [
<?php if ($calendario != FALSE) { ?>
    <?php foreach ($calendario as $evento) { ?>
                    {
                    title: '<?php echo $evento->entidad_calendario_nombre; ?>',
                            start: new Date(<?php echo date('Y', strtotime($evento->entidad_calendario_fecha_inicio)) ?>, <?php echo date('m', strtotime($evento->entidad_calendario_fecha_inicio)) - 1 ?>, <?php echo date('d', strtotime($evento->entidad_calendario_fecha_inicio)) ?>,<?php echo date('H', strtotime($evento->entidad_calendario_fecha_inicio)) ?>,<?php echo date('i', strtotime($evento->entidad_calendario_fecha_inicio)) ?>),
                            end: new Date(<?php echo date('Y', strtotime($evento->entidad_calendario_fecha_fin)) ?>, <?php echo date('m', strtotime($evento->entidad_calendario_fecha_fin)) - 1 ?>, <?php echo date('d', strtotime($evento->entidad_calendario_fecha_fin)) ?>,<?php echo date('H', strtotime($evento->entidad_calendario_fecha_fin)) ?>,<?php echo date('i', strtotime($evento->entidad_calendario_fecha_fin)) ?>),
                            allDay: <?php echo ($evento->entidad_calendario_dia == 1) ? 'true' : 'false'; ?>,
                            url: '<?php echo site_url('sitio/calendario_evento') . '/' . $evento->entidad_calendario_alias; ?>'
                    },
    <?php } ?>
<?php } ?>
            {
            title: 'Click for Google',
                    start: new Date(2000, m, 28),
                    end: new Date(2000, m, 29),
                    url: 'http://google.com/'
            }

            ]
    });
    });
</script>
