<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="noticias-post-title color">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="post_title">
                    <h2>Mapa del sitio</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="noticias-post-area color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo site_url(); ?>images/logo.png" class="mx-auto center-block" alt="logo">
                <hr>
            </div>
            <div class=" col-md-4">
                <div class="card card-mapa-del-sitio">
                    <div class="card-header">
                        Nuestra Institución
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Información General
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url("entidad/index/mision_vision"); ?>">Misión y Visión</a>
                                    <a class="dropdown-item" href="<?php echo site_url("entidad/index/objetivos_funciones"); ?>">Objetivos y Funciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url("entidad/index/organigrama"); ?>">Organigrama</a>
                                    <a class="dropdown-item" href="<?php echo site_url("entidad/index/dependencias"); ?>">Dependencias</a>
                                    <a class="dropdown-item" href="<?php echo site_url("entidad/directorio_sucursales"); ?>">Directorio de Sucursales</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Servicios de Información
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item"href="<?php echo site_url('entidad/preguntas_frecuentes') ?>" >Preguntas y Respuestas Frecuentes</a>
                                    <a class="dropdown-item"   href="<?php echo site_url('entidad/glosario') ?>" >Glosario</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/noticias') ?>" >Noticias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/notificaciones') ?>" >Anuncios</a>
                                    <a class="dropdown-item"  href="<?php echo site_url('entidad/boletines') ?>" >Boletines</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/calendario') . '/' . date('Y') . '/' . date('m') ?>" >Calendario de Eventos</a>
                                    <a class="dropdown-item"  href="<?php echo site_url('sitio/encuestas') ?>" >Encuestas</a>
                                    <a class="dropdown-item"  target="_blank" href="<?php echo $configuracion->config_general_portal_ninos; ?>" >Portal para Niños y Niñas</a>
                                    <a class="dropdown-item" target="<?php echo $empleo_link->entidad_ofertas_empleo_target; ?>" href="<?php echo prep_url($empleo_link->entidad_ofertas_empleo_url); ?>" >Ofertas de Empleo</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Talento Humano
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/funcionarios') ?>" >Funcionarios Principales</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/listado_planta') ?>" >Listado de Personal</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/manual_funciones') ?>" >Manual de Funciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/evaluacion_desempeno') ?>" >Evaluación de Desempeño</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/asignaciones_salariales') ?>" >Asignaciones Salariales</a> 
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Normatividad
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/leyes') ?>" >Leyes / Ordenanzas / Acuerdos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/decretos') ?>" >Decretos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/circulares') ?>" >Circulares y/u otros actos administrativos de carácter general</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/resoluciones') ?>" >Resoluciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/edictos') ?>" >Edictos y Avisos</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Presupuesto
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/presupuesto') ?>" >Presupuesto Aprobado en Ejercicio</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/presupuesto_historico') ?>" >Información Histórica de Presupuestos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/estados_financieros') ?>" >Estados Financieros</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Planeación
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/politicas') ?>" >Políticas / Lineamientos / Manuales</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/plan_anticorrupcion') ?>" >Plan Anticorrupción</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/plan_estrategico') ?>" >Plan de Desarrollo</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/plan_accion') ?>" >Plan de Acción</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/planeacion_plan_gasto_publico') ?>" >Plan de Gasto Público</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/programas_proyectos') ?>" >Programas y Proyectos en Ejecución</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/indicadores_gestion') ?>" >Metas e Indicadores de Gestión</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/planes_otros') ?>" >Otros Planes</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/informe_empalme') ?>" >Informes de Empalme</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/control_archivo') ?>" >Gestión Documental</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Control
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/entes') ?>" >Entes de Control que Vigilan la Entidad</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/auditorias') ?>" >Auditorías  </a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/informe_concejo') ?>" >Informes al Concejo</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/informe_pqrs') ?>" >Informe de Peticiones, Quejas, Reclamos, Denuncias y Sugerencias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/informe_fiscal') ?>" >Informes de Rendición de Cuenta Fiscal </a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/informe_ciudadania') ?>" >Informes de Rendición de Cuenta a los Ciudadanos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/planes_mejoramiento') ?>" >Planes de Mejoramiento</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/reportes_control_interno') ?>" >Reportes de Control Interno</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/poblacion_vulnerable') ?>" >Información para Población Vulnerable</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/programas_sociales') ?>" >Programas Sociales</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/defensa_judicial') ?>" >Defensa Judicial</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/otros_informes') ?>" >Otros Informes</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Contratación
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/plan_compras') ?>" >Plan de Adquisiciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/secop') ?>" >Información SECOP</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/contratos') ?>" >Procesos de Contratación</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/avance_contractual') ?>" >Avances Contractuales</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/contratacion_lineamientos') ?>" >Lineamientos de Adquisiciones  y Compras</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Trámites y Servicios
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/tramites') ?>" >Listado de Trámites y Servicios</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/tramites_en_linea') ?>" >Trámites en Línea</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/formularios_tramites') ?>" >Formularios de Trámites</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Datos Abiertos
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/datos_abiertos') ?>" >Datos Abiertos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/catalogo_abiertos') ?>" >Catálogo de Datos Abiertos</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Calidad
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/index/calidad') ?>" >Sistema de Calidad</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/caracterizacion_procesos') ?>" >Caracterización de Procesos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/procedimientos') ?>" >Procedimientos</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <a class="btn btn-link" href="<?php echo site_url('entidad/documentos') ?>">Documentos</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=" col-md-4">
                <div class="card card-mapa-del-sitio">
                    <div class="card-header">
                        Atención al Ciudadano
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Información de Interés
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <!--                                    <a class="dropdown-item" data-toggle="modal" data-target="#ayudas_navegacion_sitio" href="#"> Ayuda para navegar el sitio</a>-->
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/calendario'); ?>"> Calendario de eventos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/convocatorias') ?>"> Convocatorias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/estudios'); ?>"> Estudio, investigaciones y otras publicaciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/glosario'); ?>"> Glosario</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/informacion_adicional'); ?>"> Información adicional</a>
                                    <a class="dropdown-item" href="<?php echo $configuracion->config_general_portal_ninos; ?>" target="_blank"> Información para niños y jóvenes</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/noticias'); ?>"> Noticias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/preguntas_frecuentes'); ?>"> Preguntas y respuestas frecuentes</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/datos_abiertos'); ?>"> Publicación de datos abiertos</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Información de Interés
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" data-toggle="modal" data-target="#ayudas_navegacion_sitio" href="#"> Ayuda para navegar el sitio</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/calendario'); ?>"> Calendario de eventos</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/convocatorias') ?>"> Convocatorias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/estudios'); ?>"> Estudio, investigaciones y otras publicaciones</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/glosario'); ?>"> Glosario</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/informacion_adicional'); ?>"> Información adicional</a>
                                    <a class="dropdown-item" href="<?php echo $configuracion->config_general_portal_ninos; ?>" target="_blank"> Información para niños y jóvenes</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/noticias'); ?>"> Noticias</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/preguntas_frecuentes'); ?>"> Preguntas y respuestas frecuentes</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/datos_abiertos'); ?>"> Publicación de datos abiertos</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle btn-responsive" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Ventanilla Única de Atención al Ciudadano
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" target="_blank" href="<?php echo $configuracion->config_general_url_pqrs; ?>"> PQRSF</a>
                                    <a class="dropdown-item" href="<?php echo site_url('entidad/tramites_en_linea'); ?>"> Trámites en Línea</a>
                                    <a class="dropdown-item" href="<?php echo site_url('sitio/notificaciones_electronicas') ?>"> Servicios y Notificaciones en Línea</a>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="dropdown">
                                <button class="btn btn-link dropdown-item-mapa dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Mecanismos de Contacto
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#mecanismos_atencion"> Mecanismos para la atención al ciudadano</a>
                                    <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#atencion_linea"> Atención en Línea</a>
                                    <a class="dropdown-item" href="<?php echo site_url('contacto'); ?>"> Buzón de Contacto</a>
                                    <a class="dropdown-item" href="<?php echo $configuracion->config_general_url_pqrs; ?>" target="_blank"> Solicitud y verificación de peticiones, quejas y reclamos</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#informacion_publica"> Solicitudes de información pública</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#localizacion"> Localización física y días de atención al público</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#notificaciones"> Correo electrónico para notificaciones judiciales</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#politicas"> Políticas de seguridad de la información y protección de datos personales</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=" col-md-4">
                <div class="card card-mapa-del-sitio">
                    <div class="card-header">
                        Transparencia
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Calidad </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Contratación</a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Control </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Datos Abiertos</a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Estados Financieros y Presupuesto </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Estructura Orgánica y Talento Humano </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Información de Interés </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Instrumentos de Gestión de Información Pública </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Mecanismo de Contacto </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Normatividad </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Planeación </a></li>
                        <li class="list-group-item"><a class="btn btn-link" href="<?php echo site_url('entidad/transparencia'); ?>">Trámites y Servicios </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
