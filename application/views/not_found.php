<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Error 404</a></h3>
        </div>
        <h3>La página a la que tratas de ingresar no existe</h3><p>Verifica nuevamente la dirección que ingresaste o puedes dar clic <strong><a href="<?php echo site_url(); ?>">aquí</a></strong> para regresar a la página inicial. </p>
        <img class="img-responsive center-block" src="<?php echo site_url('images/error-404.png'); ?>" />
    </article>
</div>