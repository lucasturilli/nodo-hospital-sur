<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Solicitud de Citas</a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <h4><strong>Por favor ingrese para poder realizar el proceso de solicitud de citas en línea </strong></h4>
            <hr>
            <?php
            $attributes = array('id' => 'form');
            echo form_open('app/validar_usuario', $attributes);
            ?>
            <div class="form-group <?php echo (form_error('id') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Número de Identificación *</label>
                <input name="id" type="text" class="form-control required" value="<?php set_value('id') ?>" required="" placeholder="Ingrese su número de identificación">
            </div>
            <div class="form-group <?php echo (form_error('passw') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="passw">Contraseña *</label>
                <input type="password" class="form-control required" name="passw" value="<?php set_value('passw') ?>" required="" placeholder="Ingrese su contraseña">
            </div>
            <button id="" type="submit" class="btn button-u">Ingresar <i class="fas fa-sign-in"></i></button>
            <?php
            echo form_close();
            ?>
            <hr>
            <p>
                <a href="<?php echo site_url("app/crear_cuenta"); ?>"><i class="fas fa-user"></i> Crear nueva cuenta</a><br>
                <a href="<?php echo site_url("app/recuperar_datos"); ?>"><i class="fas fa-key"></i> Recuperar datos de acceso</a>
            </p>
        </div>
    </article>
</div>

<script>
    $("#form").validate({
        rules: {
            id: {
                required: true,
                number: true
            },
            password: {
                required: true
            }
        },
        submitHandler: function (form) {
            var url = '<?php echo site_url("app/validar_usuario") ?>';
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'JSON',
                cache: false,
                beforeSend: function () {
                    $.blockUI(
                            {
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }, message: '<p>Buscando, por favor espere...</p>'
                            }
                    );
                },
                complete: function () {
                    $.unblockUI();
                },
                data: {
                    id: $('input[name=id]').val(),
                    passw: $('input[name=passw]').val()

                },
                success: function (msg) {
                    if (msg.respuesta == 2) {
                        swal({
                            allowOutsideClick: false,
                            title: "Error!",
                            text: msg.texto,
                            type: "error",
                            confirmButtonText: "Continuar"
                        });
                    } else {
                        window.location.href = "<?php echo site_url("app/paciente/") ?>";
                    }
                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
            return false; // block the default submit action
        }
    });

</script>
