<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($tipo_identificacion != false) {
    $tipo_identifiaciones = array('' => 'Seleccione el tipo de identificación');
    foreach ($tipo_identificacion as $dato) {

        $tipo_identifiaciones[$dato->id_tipo_identificacion] = $dato->tipo_identificacion_nombre;
    }
}
$ciud = array('' => 'seleccione la Ciudad');
$dept = array('' => 'seleccione el Departamento');
$pais = array('' => 'seleccione el País');
foreach ($paises as $record) {
    $pais[$record['ciudad_id_pais']] = $record['ciudad_pais_nombre'];
}
$sexo = array('' => 'seleccione el Sexo', 'M' => 'Masculino', 'F' => 'Femenino');
?>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/datetime/build/css/bootstrap-datetimepicker.min.css">
<script src="<?php echo site_url(); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo site_url(); ?>bower_components/datetime/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script src="<?php echo site_url(); ?>bower_components/moment/locale/es.js"></script>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Crear nueva cuenta</a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <h4><strong>Por favor diligencie completamente el siguiente formulario para la creación de su cuenta </strong></h4>
            <p><small>Tenga en cuenta que los campos marcados con (*) son obligatorios.</small></p>
            <hr>
            <?php
            $attributes = array('id' => 'form');
            echo form_open('app/guardar_paciente', $attributes);
            ?>
            <div class="form-group <?php echo (form_error('nombres') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="nombres">Nombres *</label>
                <input name="nombres" type="text" class="form-control " value="<?php echo set_value('nombres') ?>"  placeholder="Ingrese sus nombres">
            </div>
            <div class="form-group <?php echo (form_error('apellidos') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="apellidos">Apellidos *</label>
                <input name="apellidos" type="text" class="form-control " value="<?php echo set_value('apellidos') ?>"  placeholder="Ingrese sus apellidos">
            </div>
            <div class="form-group <?php echo (form_error('id') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Número de Identificación *</label>
                <input name="id" type="text" class="form-control " value="<?php echo set_value('id') ?>"  placeholder="Ingrese su número de identificación">
            </div>
            <div class="form-group <?php echo (form_error('tipo_identificacion') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="tipo_identificacion">Tipo de Identificación *</label>
                <?php echo form_dropdown('tipo_identificacion', $tipo_identifiaciones, set_value('tipo_identificacion'), 'class="form-control registration-input registration-select"'); ?>
            </div>
            <div class="form-group <?php echo (form_error('fecha') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Fecha de Nacimiento *</label>
                <input name="fecha" type="text" class="form-control  datetimepicker" value="<?php echo set_value('fecha') ?>"  placeholder="Ingrese su fecha de nacimiento">
            </div>
            <div class="form-group <?php echo (form_error('sexo') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="tipo_identificacion">Sexo *</label>
                <?php echo form_dropdown('sexo', $sexo, set_value('sexo'), 'class="form-control registration-input registration-select"'); ?>
            </div>
            <div class="form-group <?php echo (form_error('email') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Correo Electrónico *</label>
                <input name="email" type="text" class="form-control " value="<?php echo set_value('email') ?>"  placeholder="Ingrese su correo electrónico">
            </div>
            <div class="form-group <?php echo (form_error('telefono') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Teléfono de Contacto</label>
                <input name="telefono" type="text" class="form-control " value="<?php echo set_value('telefono') ?>"  placeholder="Ingrese su teléfono de contacto">
            </div>
            <div class="form-group <?php echo (form_error('celular') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Número de Celular</label>
                <input name="celular" type="text" class="form-control " value="<?php echo set_value('celular') ?>"  placeholder="Ingrese su número de celular">
            </div>
            <div class="form-group <?php echo (form_error('direccion') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Dirección</label>
                <input name="direccion" type="text" class="form-control " value="<?php echo set_value('direccion') ?>"  placeholder="Ingrese su dirección">
            </div>
            <div class="form-group <?php echo (form_error('pais') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="pais">País</label>
                <?php echo form_dropdown('pais', $pais, set_value('pais'), 'class="form-control registration-input registration-select"'); ?>
            </div>
            <div class="form-group <?php echo (form_error('departamento') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="pais">Departamento</label>
                <?php echo form_dropdown('departamento', $dept, set_value('departamento'), 'class="form-control registration-input registration-select"'); ?>
            </div>
            <div class="form-group <?php echo (form_error('ciudad') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="pais">Ciudad</label>
                <?php echo form_dropdown('ciudad', $ciud, set_value('ciudad'), 'class="form-control registration-input registration-select"'); ?>
            </div>
            <hr>
            <div class="row ">
                <?php
                if ($captcha != FALSE) {
                    ?>
                    <div class="col-md-6">
                        <div class="registration-form-group">
                            <label class="registration-label" for="inlineFormInputGroup"> (*) Casilla de verificación</label>

                            <div class="g-recaptcha" data-sitekey="<?php echo $captcha ?>"></div>
                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>

                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-6">
                    <div class="registration-form-group">
                        <label class="registration-label" for="terms">(*) Autorización de datos personales</label>
                        <div class="registrtion-checkbox-group">
                            <input type="checkbox" name="terms" id="terms" class="registration-checkbox" required>
                            <label for="terms" class="terms-text"> <span class="checkbox-btn"></span> Autorizo para tratar mis datos personales con el fin de comunicarme cualquier situación o evento relacionado con mi solicitud; en cumplimiento de la Ley 1581 de 2012 y el Decreto 1377 de 2013 de la República de Colombia.</label>
                        </div>

                    </div>
                </div>
            </div>
            <hr>
            <button id="" type="submit" class="btn button-u">Guardar <i class="fas fa-save"></i></button>
                <?php
                echo form_close();
                ?>
        </div>
    </article>
</div>

<script>

    $(function () {
        $('.datetimepicker').datetimepicker({
            locale: 'es',
            format: 'Y-MM-DD',
            maxDate: new Date()
        });
    });

    $("#form").validate({
        rules: {
            nombres: {
                required: true
            },
            apellidos: {
                required: true
            },
            id: {
                required: true,
                number: true,
                remote: {
                    url: "<?php echo site_url("app/check_paciente"); ?>",
                    type: "post",
                    data: {
                        id: function () {
                            return $("input[name=id]").val();
                        }
                    }
                }
            },
            tipo_identificacion: {
                required: true
            },
            fecha: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
         messages:
             {
                 id:
                 {
                    remote: jQuery.validator.format("{0} El número de identificación ya se encuentra registrado ")
                 }

             },
        submitHandler: function (form) {
            if (grecaptcha.getResponse()) {
                $.blockUI();
                form.submit();
            } else {
                swal.fire({
                    title: "Alerta",
                    text: "Por favor verifica que no eres un robot para poder continuar",
                    type: "warning",
                    confirmButtonText: "Cerrar"
                })
            }
        }
    });


    $('select[name=pais]').change(function () {
        var id = $(this).val();
        var url = "<?php echo site_url('app/obtenerdepartamentos/'); ?>" + "/" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('select[name=departamento]')[0].options.length = 0;
                for (var i in msg) {
                    var a = msg[i]['ciudad_id_departamento'];
                    var b = msg[i]['ciudad_departamento_nombre'];
                    $('select[name=departamento]').append(
                            $('<option></option>').val('' + a).html('' + b));
                }
                actualizar_ciudad2();
            }
        });
    });


    $('select[name=departamento]').change(function () {
        var id = $(this).val();
        $('input[name=value_departamento]').val(id);
        var url = "<?php echo site_url('app/obtenerciudades/'); ?>" + "/" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('select[name=ciudad]')[0].options.length = 0;
                for (var i in msg) {
                    var a = msg[i]['id_ciudad'];
                    var b = msg[i]['ciudad_nombre'];
                    $('select[name=ciudad]').append(
                            $('<option></option>').val('' + a).html('' + b));
                }
            }

        });
    });

    function actualizar_ciudad2() {
        var id = $('select[name=departamento]').val();
        if (!empty(id)) {
            var url = "<?php echo site_url('app/obtenerciudades/'); ?>" + "/" + id;
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                success: function (msg) {
                    $('select[name=ciudad]')[0].options.length = 0;
                    for (var i in msg) {
                        var a = msg[i]['id_ciudad'];
                        var b = msg[i]['ciudad_nombre'];
                        $('select[name=ciudad]').append(
                                $('<option></option>').val('' + a).html('' + b));
                    }
                }

            });
        }
    }

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default :
                return false;
        }
    }

</script>
