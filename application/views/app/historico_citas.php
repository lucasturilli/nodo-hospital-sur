<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/css/autoFill.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/css/buttons.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/ColReorder-1.3.2/css/colReorder.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/FixedColumns-3.2.2/css/fixedColumns.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/FixedHeader-3.1.2/css/fixedHeader.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/KeyTable-2.1.2/css/keyTable.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/RowReorder-1.1.2/css/rowReorder.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Scroller-1.4.2/css/scroller.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Select-1.2.0/css/select.bootstrap.min.css"/>

<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/JSZip-2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/pdfmake-0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/pdfmake-0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/js/dataTables.autoFill.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/js/autoFill.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/ColReorder-1.3.2/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/FixedColumns-3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/FixedHeader-3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/KeyTable-2.1.2/js/dataTables.keyTable.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/RowReorder-1.1.2/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Scroller-1.4.2/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Select-1.2.0/js/dataTables.select.min.js"></script>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <h3>
                Histórico Citas 
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class = "table-responsive">
                <table  class = "table table-striped table-hover table-bordered full_table fullwidth">
                    <thead>
                        <tr>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por estado">Estado</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por sede">Sede</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por servicio">Servicio</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha">Fecha</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por hora">Hora</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por paciente">Paciente</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por Identificación">Identificación</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por sexo">Sexo</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha nacimiento">Fecha Nacimiento</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($citas != FALSE) { ?>
                            <?php foreach ($citas as $row) { ?>
                                <tr>
                                    <td class="<?php echo ($row->citas_confirmada == 1)?'success':'danger'?>"><?php echo ($row->citas_confirmada == 1) ? 'Confirmada' : 'Pendiente'; ?></td>
                                    <td><?php echo $row->sede; ?></td>
                                    <td><?php echo $row->servicio; ?></td>
                                    <td><?php echo $row->citas_dia; ?></td>
                                    <td><?php echo $row->citas_hora; ?></td>
                                    <td><?php echo $row->paciente_nombres . ' ' . $row->paciente_apellidos; ?></td>
                                    <td><?php echo $row->paciente_identificacion; ?></td>
                                    <td><?php echo $row->paciente_sexo; ?></td>
                                    <td><?php echo $row->paciente_fecha_nacimiento; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<script>


   
    $(function () {
        $('.full_table').dataTable({
            responsive: false,
            dom: "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'l><'col-sm-7'p><'col-sm-12'i>>",
            buttons: [
                {
                    extend: 'copy',
                    text: '<i class="fa fa-files-o"></i> Copiar'
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel'
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text-o"></i> Csv'
                },
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o"></i> Pdf'
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i> Imprimir'
                }

            ],
            "tableTools": {
                "sSwfPath": "<?php echo site_url(); ?>js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "pagingType": "full_numbers",
            "aLengthMenu": [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, "TODOS"]],
            "bPaginate": true,
            "bLengthChange": true,
            "responsive": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "oLanguage": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0",
                "sInfoFiltered": "(filtrado de un total de _MAX_ )",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "order": [[3, "desc"]],
            "columnDefs": [

            ]
        });
    });


</script>