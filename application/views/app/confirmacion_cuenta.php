<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Confirmación de creación de cuenta </a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <div class="alert alert-success">
                <h4>Apreciado ciudadano, su cuenta fue creada exitosamente. 
                        Al correo electrónico <?php echo $datos['paciente_email']; ?> se enviaron los datos de acceso al sistema de solicitud de citas en línea. <br ><br>Si por algún motivo este correo electrónico no llega por favor comuníquese con nuestra líneas de atención para realizar la respectiva verificación.  </h4>
            </div>
            <hr>
            <a href="<?php echo site_url("app/citas"); ?>" id="" type="button" class="btn button-u">Ingresar <i class="fas fa-sign-in"></i></a>
            <hr>
        </div>
    </article>
</div>

