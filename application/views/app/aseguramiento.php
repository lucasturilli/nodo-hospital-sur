<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Validación de Derechos</a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <form class="form">
                <div class="form-group mb-2">
                    <label for="exampleInputName2">Ingrese el número de identificación para realizar la consulta de validación de derechos </label>
                </div>
                <div class="form-group mb-2">
                    <input type="number" class="form-control" id="texto" placeholder="Ingrese el número de identificación ">
                </div>
                <button type="submit" id="buscar_notificacion" onclick="return false;" class="btn button-u">Buscar</button>
            </form>
            <hr>
            <div id="resultados">
            </div>
        </div>
    </article>
</div>

<script>
    $('#buscar_notificacion').click(function () {
        var texto = $('#texto').val();
        if (texto === "") {
            swal.fire({
                title: "",
                text: "Debe ingresar el numero de identificación para buscar",
                confirmButtonText: "Continuar",
                type: "info"
            });
            return false;
        } else {
            if (texto.length < 4) {
                swal.fire({
                    title: "",
                    text: "Debes ingresar al menos 4 dígitos para poder realizar una búsqueda efectiva",
                    confirmButtonText: "Continuar",
                    type: "info"
                });
            } else {
                var url = "<?php echo site_url('app/aseguramiento_ajax') . '/' ?>" + texto;
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    cache: false,
                    beforeSend: function () {
                        $.blockUI(
                                {
                                    css: {
                                        border: 'none',
                                        padding: '15px',
                                        backgroundColor: '#000',
                                        '-webkit-border-radius': '10px',
                                        '-moz-border-radius': '10px',
                                        opacity: .5,
                                        color: '#fff'
                                    }, message: '<p>Buscando, por favor espere...</p>'
                                }
                        );
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (msg) {
                        $('#resultados').empty();
                        $('#resultados').append(msg.mensaje);
                    },
                    error: function (jqXHR, exception) {

                        if (jqXHR.status === 0) {
                            alert('No Conecta.\n Verifique su conexión a internet.');
                        } else if (jqXHR.status == 404) {
                            alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                        } else if (jqXHR.status == 500) {
                            alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'parsererror') {
                            alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'timeout') {
                            alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'abort') {
                            alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                        }

                    }

                });
            }
        }
    }
    );

</script>
