<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$sede = array('' => 'Seleccione un valor');
if ($sedes != FALSE) {
    foreach ($sedes as $dato) {

        $sede[$dato->id_sedes] = $dato->sedes_nombre;
    }
}

$servicio = array('' => 'Seleccione un valor');
if ($servicios != FALSE) {
    foreach ($servicios as $dato) {

        $servicio[$dato->id_servicios] = $dato->servicios_nombre;
    }
}
$fest = array('Si' => 'Si', 'No' => 'No');
?>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/datetime/build/css/bootstrap-datetimepicker.min.css">
<script src="<?php echo site_url(); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo site_url(); ?>bower_components/datetime/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script src="<?php echo site_url(); ?>bower_components/moment/locale/es.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>plugins/iCheck/all.css">
<script src="<?php echo site_url(); ?>plugins/iCheck/icheck.min.js"></script>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <h3>
                Nuevo Agendamiento 
            </h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $attributes = array('id' => 'form', 'class' => 'form-horizontal');
            echo form_open('#', $attributes);
            if ($agendamiento != NULL) {
                echo form_hidden('id', $agendamiento->id_setup_servicios_x_sedes);
            } else {
                echo form_hidden('id', NULL);
            }
            $disableb = ($agendamiento != NULL) ? ($citas != FALSE) ? 'disabled=disabled' : '' : '';
            ?>
            <div class="form-group"><label class="col-sm-2 control-label">Sede *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('sede', $sede, set_value('sede', ($agendamiento != NULL) ? $agendamiento->id_sedes : ''), 'class="form-control m-b" ' . $disableb); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Servicio *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('servicio', $servicio, set_value('servicio', ($agendamiento != NULL) ? $agendamiento->id_servicios : ''), 'class="form-control m-b" ' . $disableb); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Fecha de Inicio *</label>
                <div class="col-sm-10"><input name="fecha_inicio" value="<?php echo set_value('fecha_inicio', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_fecha_inicio : ''); ?>" type="text" class="form-control datetimepicker_begin" <?php echo ($agendamiento != NULL) ? ($citas != FALSE) ? 'readonly' : '' : '' ?>>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Fecha de Finalización *</label>
                <div class="col-sm-10"><input name="fecha_fin" value="<?php echo set_value('fecha_fin', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_fecha_final : ''); ?>" type="text" class="form-control datetimepicker_end" <?php echo ($agendamiento != NULL) ? ($citas != FALSE) ? 'readonly' : '' : '' ?>>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Cantidad de Citas Simultaneas *</label>
                <div class="col-sm-10"><input name="cantidad" value="<?php echo set_value('cantidad', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_cantidad : ''); ?>" type="text" class="form-control" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Tiempo por Cita (En Minutos)*</label>
                <div class="col-sm-10"><input name="tiempo" value="<?php echo set_value('tiempo', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_tiempo : ''); ?>" type="text" class="form-control" <?php echo ($agendamiento != NULL) ? ($citas != FALSE) ? 'readonly' : '' : '' ?>>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Omitir Días Festivos *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('festivos', $fest, set_value('festivos', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_festivos : ''), 'class="form-control m-b" ' . $disableb); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Excluir Hora de almuerzo (12:00 a 13:00) *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('almuerzo', $fest, set_value('almuerzo', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_almuerzo : ''), 'class="form-control m-b" ' . $disableb); ?>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Lunes
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r1" id="r11" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_lunes == 'Si') ? 'checked' : '') : 'checked'; ?>  >
                    </label>
                    <label>No
                        <input type="radio" name="r1" id="r12" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_lunes == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h1" value="<?php echo set_value('h1', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_lunes_hora_inicio : ''); ?>" type="text" class="form-control datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h11" value="<?php echo set_value('h11', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_lunes_hora_fin : ''); ?>" type="text" class="form-control datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Martes
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r2" id="r21" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_martes == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r2" id="r22" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_martes == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h2" value="<?php echo set_value('h2', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_martes_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h22" value="<?php echo set_value('h22', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_martes_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Miercoles
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r3" id="r31" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_miercoles == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r3" id="r32" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_miercoles == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h3" value="<?php echo set_value('h3', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_miercoles_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h33" value="<?php echo set_value('h33', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_miercoles_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Jueves
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r4" id="r41" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_jueves == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r4" id="r42" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_jueves == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h4" value="<?php echo set_value('h4', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_jueves_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h44" value="<?php echo set_value('h44', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_jueves_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Viernes
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r5" id="r51" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_viernes == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r5" id="r52" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_viernes == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h5" value="<?php echo set_value('h5', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_viernes_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h55" value="<?php echo set_value('h55', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_viernes_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Sabado
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r6" id="r61" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_sabados == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r6" id="r62" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_sabados == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h6" value="<?php echo set_value('h6', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_sabados_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h66" value="<?php echo set_value('h66', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_sabados_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">
                    Domingo
                </label>
                <div class="col-sm-10">
                    <label>Si
                        <input type="radio" name="r7" id="r71" value="Si" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_domingos == 'Si') ? 'checked' : '') : 'checked'; ?>>
                    </label>
                    <label>No
                        <input type="radio" name="r7" id="r72" value="No" class="flat-red rr" <?php echo ($agendamiento != NULL) ? (($agendamiento->setup_servicios_x_sedes_domingos == 'No') ? 'checked' : '') : ''; ?>>
                    </label>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Inicio</label>
                <div class="col-sm-10"><input name="h7" value="<?php echo set_value('h7', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_domingos_hora_inicio : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Hora de Finalización</label>
                <div class="col-sm-10"><input name="h77" value="<?php echo set_value('h77', ($agendamiento != NULL) ? $agendamiento->setup_servicios_x_sedes_domingos_hora_fin : ''); ?>" type="text" class="form-control  datetimepicker_hora" >
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button id="" type="submit" class="btn btn-primary">Guardar <i class="fas fa-save"></i></button>
                    <a href="<?php echo site_url('administrador/app/agendamiento') ?>" type="button" class="btn btn-default">Cancelar <i class="fas fa-backward"></i></a>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('input[type="checkbox"], input[type="radio"]').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        $('.datetimepicker_begin').datetimepicker({
            locale: 'es',
            format: 'Y-MM-DD'
        });
        $('.datetimepicker_end').datetimepicker({
            locale: 'es',
            format: 'Y-MM-DD',
            minDate: new Date()
        });
        $('.datetimepicker_hora').datetimepicker({
            locale: 'es',
            format: 'HH:mm',
            stepping: 10
        });

        $("#form").validate({
            rules: {
                sede: {
                    required: true
                },
                servicio: {
                    required: true
                },
                fecha_inicio: {
                    required: true
                },
                fecha_fin: {
                    required: true
                },
                cantidad: {
                    required: true,
                    number: true,
                    min: 1
                },
                tiempo: {
                    required: true,
                    number: true
                },
                h1: {
                    required: function (element) {
                        return   $("input[name='r1']:checked").val() == 'Si';
                    }
                },
                h11: {
                    required: function (element) {
                        return   $("input[name='r1']:checked").val() == 'Si';
                    }

                },
                h2: {
                    required: function (element) {
                        return   $("input[name='r2']:checked").val() == 'Si';
                    }
                },
                h22: {
                    required: function (element) {
                        return   $("input[name='r2']:checked").val() == 'Si';
                    }

                },
                h3: {
                    required: function (element) {
                        return   $("input[name='r3']:checked").val() == 'Si';
                    }
                },
                h33: {
                    required: function (element) {
                        return   $("input[name='r3']:checked").val() == 'Si';
                    }

                },
                h4: {
                    required: function (element) {
                        return   $("input[name='r4']:checked").val() == 'Si';
                    }
                },
                h44: {
                    required: function (element) {
                        return   $("input[name='r4']:checked").val() == 'Si';
                    }

                },
                h5: {
                    required: function (element) {
                        return   $("input[name='r5']:checked").val() == 'Si';
                    }
                },
                h55: {
                    required: function (element) {
                        return   $("input[name='r5']:checked").val() == 'Si';
                    }

                },
                h6: {
                    required: function (element) {
                        return   $("input[name='r6']:checked").val() == 'Si';
                    }
                },
                h66: {
                    required: function (element) {
                        return   $("input[name='r6']:checked").val() == 'Si';
                    }

                },
                h7: {
                    required: function (element) {
                        return   $("input[name='r7']:checked").val() == 'Si';
                    }
                },
                h77: {
                    required: function (element) {
                        return   $("input[name='r7']:checked").val() == 'Si';
                    }

                }
            },
            submitHandler: function (form) {
                var url = '<?php echo site_url("administrador/app/guardar_agendamiento") ?>';
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    data: {
                        fecha_inicio: $('input[name=fecha_inicio]').val(),
                        fecha_fin: $('input[name=fecha_fin]').val(),
                        cantidad: $('input[name=cantidad]').val(),
                        tiempo: $('input[name=tiempo]').val(),
                        r1: $('input[name=r1]:checked').val(),
                        r2: $('input[name=r2]:checked').val(),
                        r3: $('input[name=r3]:checked').val(),
                        r4: $('input[name=r4]:checked').val(),
                        r5: $('input[name=r5]:checked').val(),
                        r6: $('input[name=r6]:checked').val(),
                        r7: $('input[name=r7]:checked').val(),
                        h1: $('input[name=h1]').val(),
                        h11: $('input[name=h11]').val(),
                        h2: $('input[name=h2]').val(),
                        h22: $('input[name=h22]').val(),
                        h3: $('input[name=h3]').val(),
                        h33: $('input[name=h33]').val(),
                        h4: $('input[name=h4]').val(),
                        h44: $('input[name=h44]').val(),
                        h5: $('input[name=h5]').val(),
                        h55: $('input[name=h55]').val(),
                        h6: $('input[name=h6]').val(),
                        h66: $('input[name=h66]').val(),
                        h7: $('input[name=h7]').val(),
                        h77: $('input[name=h77]').val(),
                        festivos: $('select[name=festivos]').val(),
                        almuerzo: $('select[name=almuerzo]').val(),
                        servicio: $('select[name=servicio]').val(),
                        sede: $('select[name=sede]').val(),
                        id: $('input[name=id]').val()
                    },
                    success: function (msg) {
                        if (msg.estado === 1) {
                            swal({
                                allowOutsideClick: false,
                                title: "Listo!",
                                text: "El registro se realizó correctamente ",
                                type: "success",
                                confirmButtonText: "Continuar"
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = "<?php echo site_url("administrador/app/agendamiento/") ?>";
                                }
                            });
                        } else {
                            swal({
                                allowOutsideClick: false,
                                title: "Error!",
                                text: msg.texto,
                                type: "error",
                                confirmButtonText: "Continuar"
                            });
                        }
                    },
                    error: function (jqXHR, exception) {

                        if (jqXHR.status === 0) {
                            alert('No Conecta.\n Verifique su conexión a internet.');
                        } else if (jqXHR.status === 404) {
                            alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                        } else if (jqXHR.status === 500) {
                            alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'parsererror') {
                            alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'timeout') {
                            alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'abort') {
                            alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                        }

                    }

                });
                return false; // block the default submit action
            }
        });





    });
</script>