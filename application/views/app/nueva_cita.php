<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$sede = array('' => 'Seleccione un valor');
if ($sedes != FALSE) {
    foreach ($sedes as $dato) {

        $sede[$dato->id_sedes] = $dato->sedes_nombre;
    }
}

$servicio = array('' => 'Seleccione un valor');
if ($servicios != FALSE) {
    foreach ($servicios as $dato) {

        $servicio[$dato->id_servicios] = $dato->servicios_nombre;
    }
}
?>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/datetime/build/css/bootstrap-datetimepicker.min.css">
<script src="<?php echo site_url(); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo site_url(); ?>bower_components/datetime/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script src="<?php echo site_url(); ?>bower_components/moment/locale/es.js"></script>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <h3>
                Nueva Cita 
            </h3>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            $attributes = array('id' => 'form', 'class' => 'form-horizontal');
            echo form_open('#', $attributes);
            ?>
            <div class="form-group"><label class="col-sm-2 control-label">Sede *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('sede', $sede, set_value('sede'), 'class="form-control m-b" '); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Servicio *</label>
                <div class="col-sm-10">
                    <?php echo form_dropdown('servicio', $servicio, set_value('servicio'), 'class="form-control m-b" '); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-sm-2 control-label">Numero de Identificación *</label>
                <div class="col-sm-10"><input name="identificacion" value="<?php echo set_value('identificacion'); ?>" type="text" class="form-control " >
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <button id="" type="submit" class="btn btn-primary">Buscar Agendamiento <i class="fas fa-search"></i></button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
            <div id="resultados">
            </div>
        </div>
        <div class="col-md-12">
            <div id="calendar" class=" hidden"></div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        $("#form").validate({
            rules: {
                sede: {
                    required: true
                },
                servicio: {
                    required: true
                },
                identificacion: {
                    required: true,
                    number: true
                }

            },
            submitHandler: function (form) {
                var url = '<?php echo site_url("administrador/app/buscar_citas") ?>';
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    data: {
                        servicio: $('select[name=servicio]').val(),
                        sede: $('select[name=sede]').val(),
                        id: $('input[name=identificacion]').val()
                    },
                    success: function (msg) {
                        if (msg.estado === 1) {
                            $('#resultados').empty();
                            $('#resultados').append("<hr>" + msg.mensaje);
                            $('#calendar').removeClass('hidden');
                            $('#calendar').fullCalendar('destroy');
                            var date = new Date();
                            var d = date.getDate();
                            var m = date.getMonth();
                            var y = date.getFullYear();
                            $('#calendar').fullCalendar({
                                header: {
                                    left: 'prev,next today',
                                    center: 'title',
                                    right: 'month,agendaWeek,agendaDay'
                                },
                                editable: false,
                                droppable: false, // this allows things to be dropped onto the calendar
                                locale: 'es',
                                height: 600,
                                defaultView: 'month',
                                minTime: '05:00', // a start time (10am in this example)
                                maxTime: '22:00', // an end time (6pm in this example)
                                slotDuration: '00:15:00',
                                eventSources: [
                                    // your event source
                                    {
                                        url: '<?php echo site_url('administrador/app/calendario'); ?>', // use the `url` property
                                        type: 'POST',
                                        data: {
                                            sede: $('select[name=sede]').val(),
                                            servicio: $('select[name=servicio]').val()
                                        }
                                    }

                                    // any other sources...

                                ],
                                eventClick: function (event) {
                                    if (event.disponible == 1) {
                                        swal({
                                            allowOutsideClick: false,
                                            title: "Confirmación!",
                                            text: "¿ Está seguro de tomar la cita el día " + Unix_timestamp(event.start) + " a las " + event.hora + " ?",
                                            type: "warning",
                                            confirmButtonText: "Confirmar",
                                            showCancelButton: true,
                                            cancelButtonText: "Cancelar"
                                        }).then((result) => {
                                            if (result.value) {
                                                var url = '<?php echo site_url("administrador/app/guardar_cita") ?>';
                                                $.ajax({
                                                    url: url,
                                                    type: 'post',
                                                    dataType: 'JSON',
                                                    cache: false,
                                                    beforeSend: function () {
                                                        $.blockUI();
                                                    },
                                                    complete: function () {
                                                        $.unblockUI();
                                                    },
                                                    data: {
                                                        id: event.id,
                                                        dia: event.dia,
                                                        hora: event.hora,
                                                        paciente: $('input[name=identificacion]').val()
                                                    },
                                                    success: function (msg) {
                                                        if (msg === 1) {
                                                            swal({
                                                                allowOutsideClick: false,
                                                                title: "Listo!",
                                                                text: "La cita quedo asignada correctamente",
                                                                type: "success",
                                                                confirmButtonText: "Continuar"
                                                            }).then((result) => {
                                                                if (result.value) {
                                                                    window.location.href = "<?php echo site_url("administrador/app/citas/") ?>";
                                                                }
                                                            });
                                                        } else {
                                                            swal({
                                                                allowOutsideClick: false,
                                                                title: "Alerta",
                                                                text: "La hora especificada ya no estaba disponible, por favor intente de nuevo ",
                                                                type: "warning",
                                                                confirmButtonText: "Continuar"
                                                            }).then((result) => {
                                                                if (result.value) {
                                                                    location.reload();
                                                                }
                                                            });
                                                        }

                                                    }

                                                });
                                            }
                                        });
                                    } else {
                                        swal.fire({
                                            title: "Alerta",
                                            text: "La hora especificada  no estaba disponible, por favor seleccione otra hora",
                                            type: "warning",
                                            confirmButtonText: "Cerrar"
                                        });
                                    }

                                }
                            });
                            $('#calendar').fullCalendar('refresh');
                        } else {
                            $('#resultados').empty();
                            $('#calendar').addClass('hidden');
                            $('#calendar').empty();
                            swal({
                                allowOutsideClick: false,
                                title: "Error!",
                                html: msg.mensaje,
                                type: "error",
                                confirmButtonText: "Continuar"
                            });
                        }
                    },
                    error: function (jqXHR, exception) {

                        if (jqXHR.status === 0) {
                            alert('No Conecta.\n Verifique su conexión a internet.');
                        } else if (jqXHR.status === 404) {
                            alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                        } else if (jqXHR.status === 500) {
                            alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'parsererror') {
                            alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'timeout') {
                            alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'abort') {
                            alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                        }

                    }

                });
                return false; // block the default submit action
            }
        });

    });

    function Unix_timestamp(t)
    {
        var date = new Date(t);
// Hours part from the timestamp
        var hours = date.getHours();
// Minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
// Seconds part from the timestamp
        var seconds = "0" + date.getSeconds();

// Will display time in 10:30:23 format
        var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', timeZone: 'America/Bogota'};
        return formattedTime = date.toLocaleString('es-CO', options);
    }
</script>