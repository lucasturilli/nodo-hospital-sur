<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/datetime/build/css/bootstrap-datetimepicker.min.css">
<script src="<?php echo site_url(); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo site_url(); ?>bower_components/datetime/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script src="<?php echo site_url(); ?>bower_components/moment/locale/es.js"></script>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Recuperar datos de acceso </a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
            <h4><strong>Para recuperar sus datos de acceso al sistema para la solicitud de citas en línea, por favor diligencie el siguiente formulario </strong></h4>
            <p><small>Tenga en cuenta que los campos marcados con (*) son obligatorios.</small></p>
            <hr>
            <?php
            $attributes = array('id' => 'form');
            echo form_open('#', $attributes);
            ?>
            <div class="form-group <?php echo (form_error('id') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Número de Identificación *</label>
                <input name="id" type="text" class="form-control " value="<?php set_value('id') ?>"  placeholder="Ingrese su número de identificación">
            </div>
            <div class="form-group <?php echo (form_error('email') != NULL) ? 'has-error' : '' ?>">
                <label class="control-label" for="id">Correo Electrónico *</label>
                <input name="email" type="text" class="form-control " value="<?php set_value('email') ?>"  placeholder="Ingrese su correo electrónico">
            </div>
            <hr>
            <div class="row ">
                <?php
                if ($captcha != FALSE) {
                    ?>
                    <div class="col-md-12">
                        <div class="registration-form-group">
                            <label class="registration-label" for="inlineFormInputGroup"> (*) Casilla de verificación</label>

                            <div class="g-recaptcha" data-sitekey="<?php echo $captcha ?>"></div>
                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>

                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <hr>
            <button id="" type="submit" class="btn button-u">Recuperar Datos <i class="fas fa-key"></i></button>
                <?php
                echo form_close();
                ?>
        </div>
    </article>
</div>

<script>


    $("#form").validate({
        rules: {
            id: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        },
        submitHandler: function (form) {
            if (grecaptcha.getResponse()) {
                var url = '<?php echo site_url("app/restablecer_cuenta") ?>';
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    data: {
                        id: $('input[name=id]').val(),
                        email: $('input[name=email]').val()
                    },
                    success: function (msg) {
                        if (msg.respuesta == 2) {
                            swal({
                                allowOutsideClick: false,
                                title: "Error!",
                                text: msg.texto,
                                type: "error",
                                confirmButtonText: "Continuar"
                            });
                        } else {
                            swal({
                                allowOutsideClick: false,
                                title: "Listo!",
                                text: "Su cuenta fue restablecida, por favor verifique su correo electrónico al cual se enviaron los nuevos datos de acceso. ",
                                type: "success",
                                confirmButtonText: "Continuar"
                            }).then((result) => {
                                if (result.value) {
                                    window.location.href = "<?php echo site_url("app/citas/") ?>";
                                }
                            });
                        }
                    },
                    error: function (jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            alert('No Conecta.\n Verifique su conexión a internet.');
                        } else if (jqXHR.status === 404) {
                            alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                        } else if (jqXHR.status === 500) {
                            alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'parsererror') {
                            alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'timeout') {
                            alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                        } else if (exception === 'abort') {
                            alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                        }
                    }
                });
            } else {
                swal.fire({
                    title: "Alerta",
                    text: "Por favor verifica que no eres un robot para poder continuar",
                    type: "warning",
                    confirmButtonText: "Cerrar"
                })
            }
        }
    });





    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default :
                return false;
        }
    }

</script>
