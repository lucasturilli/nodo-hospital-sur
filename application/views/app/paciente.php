<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/moment.min.js"></script>
<!-- Full Calendar -->
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo site_url(); ?>js/plugins/fullcalendar/locale/es.js"></script>
<script src="<?php echo site_url(); ?>js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/datetime/build/css/bootstrap-datetimepicker.min.css">
<script src="<?php echo site_url(); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo site_url(); ?>bower_components/datetime/build/js/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script src="<?php echo site_url(); ?>bower_components/moment/locale/es.js"></script>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3>Solicitud de citas en línea  <span class="float-right boton-salir"><a class="boton-salir" data-toggle="tooltip" title="Cerrar Sesión" href="<?php echo site_url('app/paciente_sing_out') ?>"><i class="fas fa-sign-out-alt boton-salir"></i> Salir</a></span></h3>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3 info-paciente">
                <div class="list-group lista-paciente">
                    <a href="<?php echo site_url('app/paciente'); ?>" class="list-group-item <?php echo ($tab == NULL) ? 'active' : ''; ?> <?php echo ($tab == 'update_info') ? 'active' : ''; ?>">Información General</a>
                    <a href="<?php echo site_url('app/paciente/passw'); ?>" class="list-group-item <?php echo ($tab == 'passw') ? 'active' : ''; ?>">Contraseña</a>
                    <a href="<?php echo site_url('app/paciente/citas'); ?>" class="list-group-item <?php echo ($tab == 'citas') ? 'active' : ''; ?>">Solicitar Citas</a>
                    <a href="<?php echo site_url('app/paciente/cancelar'); ?>" class="list-group-item <?php echo ($tab == 'cancelar') ? 'active' : ''; ?>">Consultar y/o Cancelar</a>
                </div>
            </div>
            <div class="col-md-9 info-paciente">
                <?php
                switch ($tab) {
                    case 'update_info':
                        if ($tipo_identificacion != false) {
                            $tipo_identifiaciones = array('' => 'Seleccione el tipo de identificación');
                            foreach ($tipo_identificacion as $dato) {

                                $tipo_identifiaciones[$dato->id_tipo_identificacion] = $dato->tipo_identificacion_nombre;
                            }
                        }
                        $ciud = array('' => 'seleccione la Ciudad');
                        $dept = array('' => 'seleccione el Departamento');
                        $pais = array('' => 'seleccione el País');
                        foreach ($paises as $record) {
                            $pais[$record['ciudad_id_pais']] = $record['ciudad_pais_nombre'];
                        }
                        if ($info_ciudad != FALSE) {
                            foreach ($departamentos as $record) {
                                $dept[$record->ciudad_id_departamento] = $record->ciudad_departamento_nombre;
                            }
                            foreach ($ciudades as $record) {
                                $ciud[$record->id_ciudad] = $record->ciudad_nombre;
                            }
                        }
                        $sexo = array('' => 'seleccione el Sexo', 'M' => 'Masculino', 'F' => 'Femenino');
                        ?>
                        <h3>Actualización de Datos </h3>
                        <p><small>Tenga en cuenta que los campos marcados con (*) son obligatorios.</small></p>
                        <?php
                        $attributes = array('id' => 'form');
                        echo form_open('app/guardar_paciente', $attributes);
                        ?>
                        <div class="form-group <?php echo (form_error('nombres') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="nombres">Nombres *</label>
                            <input name="nombres" type="text" class="form-control " value="<?php echo set_value('nombres', $paciente->paciente_nombres) ?>"  placeholder="Ingrese sus nombres">
                        </div>
                        <div class="form-group <?php echo (form_error('apellidos') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="apellidos">Apellidos *</label>
                            <input name="apellidos" type="text" class="form-control " value="<?php echo set_value('apellidos', $paciente->paciente_apellidos) ?>"  placeholder="Ingrese sus apellidos">
                        </div>
                        <div class="form-group <?php echo (form_error('id') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Número de Identificación *</label>
                            <input name="id" type="text" class="form-control " value="<?php echo set_value('id', $paciente->paciente_identificacion) ?>"  placeholder="Ingrese su número de identificación" readonly>
                        </div>
                        <div class="form-group <?php echo (form_error('tipo_identificacion') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="tipo_identificacion">Tipo de Identificación *</label>
                            <?php echo form_dropdown('tipo_identificacion', $tipo_identifiaciones, set_value('tipo_identificacion', $paciente->id_tipo_identificacion), 'class="form-control registration-input registration-select"'); ?>
                        </div>
                        <div class="form-group <?php echo (form_error('fecha') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Fecha de Nacimiento *</label>
                            <input name="fecha" type="text" class="form-control  datetimepicker" value="<?php echo set_value('fecha', $paciente->paciente_fecha_nacimiento) ?>"  placeholder="Ingrese su fecha de nacimiento">
                        </div>
                        <div class="form-group <?php echo (form_error('sexo') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="tipo_identificacion">Sexo *</label>
                            <?php echo form_dropdown('sexo', $sexo, set_value('sexo', $paciente->paciente_sexo), 'class="form-control registration-input registration-select"'); ?>
                        </div>
                        <div class="form-group <?php echo (form_error('email') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Correo Electrónico *</label>
                            <input name="email" type="text" class="form-control " value="<?php echo set_value('email', $paciente->paciente_email) ?>"  placeholder="Ingrese su correo electrónico">
                        </div>
                        <div class="form-group <?php echo (form_error('telefono') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Teléfono de Contacto</label>
                            <input name="telefono" type="text" class="form-control " value="<?php echo set_value('telefono', $paciente->paciente_telefono) ?>"  placeholder="Ingrese su teléfono de contacto">
                        </div>
                        <div class="form-group <?php echo (form_error('celular') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Número de Celular</label>
                            <input name="celular" type="text" class="form-control " value="<?php echo set_value('celular', $paciente->paciente_celular) ?>"  placeholder="Ingrese su número de celular">
                        </div>
                        <div class="form-group <?php echo (form_error('direccion') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Dirección</label>
                            <input name="direccion" type="text" class="form-control " value="<?php echo set_value('direccion', $paciente->paciente_direccion) ?>"  placeholder="Ingrese su dirección">
                        </div>
                        <div class="form-group <?php echo (form_error('pais') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="pais">País</label>
                            <?php echo form_dropdown('pais', $pais, set_value('pais', ($info_ciudad != FALSE) ? $info_ciudad->ciudad_id_pais : ''), 'class="form-control registration-input registration-select"'); ?>
                        </div>
                        <div class="form-group <?php echo (form_error('departamento') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="pais">Departamento</label>
                            <?php echo form_dropdown('departamento', $dept, set_value('departamento', ($info_ciudad != FALSE) ? $info_ciudad->ciudad_id_departamento : ''), 'class="form-control registration-input registration-select"'); ?>
                        </div>
                        <div class="form-group <?php echo (form_error('ciudad') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="pais">Ciudad</label>
                            <?php echo form_dropdown('ciudad', $ciud, set_value('ciudad', ($info_ciudad != FALSE) ? $info_ciudad->id_ciudad : ''), 'class="form-control registration-input registration-select"'); ?>
                        </div>
                        <button id="" type="submit" class="btn button-u">Guardar <i class="fas fa-save"></i></button>
                        <?php
                        echo form_close();
                        ?>
                        <?php
                        break;
                    case 'passw':
                        ?>
                        <h3>Cambio de Contraseña </h3>
                        <?php
                        $attributes = array('id' => 'form_passw');
                        echo form_open('#', $attributes);
                        ?>
                        <div class="form-group <?php echo (form_error('password') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="nombres">Contraseña Actual *</label>
                            <input name="password" type="password" class="form-control " value="<?php echo set_value('password') ?>"  placeholder="Ingrese su contraseña actual">
                        </div>
                        <div class="form-group <?php echo (form_error('newpassw') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="apellidos">Nueva Contraseña *</label>
                            <input name="newpassw" id="newpassw" type="password" class="form-control " value="<?php echo set_value('newpassw') ?>"  placeholder="Ingrese una nueva contraseña">
                        </div>
                        <div class="form-group <?php echo (form_error('repassw') != NULL) ? 'has-error' : '' ?>">
                            <label class="control-label" for="id">Repita Contraseña *</label>
                            <input name="repassw" type="password" class="form-control " value="<?php echo set_value('repassw') ?>"  placeholder="Ingrese nuevamente la contraseña" >
                        </div>
                        <button id="" type="submit" class="btn button-u">Guardar <i class="fas fa-save"></i></button>
                        <?php
                        echo form_close();
                        ?>
                        <?php
                        break;
                    case 'citas':
                        ?>
                        <h3>Solicitar Citas </h3>
                        <?php if ($this->session->userdata('aseguramiento')['estado'] == 0) { ?>
                            <?php echo $this->session->userdata('aseguramiento')['mensaje']; ?>
                        <?php } else { ?>
                            <?php
                            $sede = array('' => 'Seleccione un valor');
                            if ($sedes != FALSE) {
                                foreach ($sedes as $dato) {

                                    $sede[$dato->id_sedes] = $dato->sedes_nombre;
                                }
                            }
                            $servicio = array('' => 'Seleccione un valor');
                            if ($servicios != FALSE) {
                                foreach ($servicios as $dato) {

                                    $servicio[$dato->id_servicios] = $dato->servicios_nombre;
                                }
                            }
                            $attributes = array('id' => 'form_buscar');
                            echo form_open('#', $attributes);
                            ?>
                            <div class="form-group <?php echo (form_error('sede') != NULL) ? 'has-error' : '' ?>">
                                <label class="control-label" for="pais">Sede *</label>
                                <?php echo form_dropdown('sede', $sede, set_value('sede'), 'class="form-control registration-input registration-select"'); ?>
                            </div>
                            <div class="form-group <?php echo (form_error('servicio') != NULL) ? 'has-error' : '' ?>">
                                <label class="control-label" for="pais">Servicio *</label>
                                <?php echo form_dropdown('servicio', $servicio, set_value('servicio'), 'class="form-control registration-input registration-select"'); ?>
                            </div>
                            <button id="" type="submit" class="btn button-u">Buscar Disponibilidad <i class="fas fa-search"></i></button>
                            <?php
                            echo form_close();
                            ?>
                            <div id="resultados">
                            </div>

                            <?php
                        }
                        ?>
                        <?php
                        break;
                    case 'cancelar':
                        ?>
                        <h3>Consultar y/o Cancelar Citas </h3>
                        <?php if ($citas == FALSE) { ?>
                            <div class="alert alert-info" role="alert"><?php echo $paciente->paciente_nombres ?>, actualmente no cuentas con ninguna cita asignada</div>
                        <?php } else { ?>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped table-hover table-bordered  table-responsive">
                                    <thead>
                                        <tr><th>Sede</th><th>Servicio </th><th>Fecha</th><th>Hora</th><th>Estado</th><th>Acciones</th></tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($citas as $cita) { ?>
                                            <tr>
                                                <td><?php echo $cita->sede; ?></td>   
                                                <td><?php echo $cita->servicio; ?></td>   
                                                <td><?php echo $cita->citas_dia; ?></td>   
                                                <td><?php echo $cita->citas_hora; ?></td> 
                                                <td><?php echo ($cita->citas_confirmada== 1)?'Confirmada':'Pendiente de Aprobación '; ?></td> 
                                                <?php if ($cita->citas_confirmada == 2) { ?>
                                                    <td><a class="btn btn-xs btn-danger delete_modal" data-toggle="tooltip" data-placement="top"  title="Eliminar" alt="Eliminar" id="row_<?php echo $cita->id_citas; ?>"  href="#"><i class="fa fa-trash"></i></a></td>  
                                                <?php } else { ?>
                                                    <td><a class="btn btn-xs btn-danger no_delete" data-toggle="tooltip" data-placement="top"  title="Eliminar" alt="Eliminar" href="#"><i class="fa fa-trash"></i></a></td>  
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- DELETE MODAL -->
                            <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content ">
                                        <div class="modal-header alert-danger">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Alerta !</h4>
                                        </div>
                                        <div class="modal-body contenido_modal_delete">
                                            ¿ Esta seguro de eliminar la cita ?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
                                            <button type="button" id="delete_submit"  class="btn btn-danger">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        break;
                    default :
                        ?>
                        <h3>Estado de Aseguramiento</h3>
                        <?php echo $this->session->userdata('aseguramiento')['mensaje']; ?>
                        <h3>Datos del Paciente</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover table-striped">
                                <tr>
                                    <th>Nombres y Apellidos</th>
                                    <td><?php echo $paciente->paciente_nombres . ' ' . $paciente->paciente_apellidos; ?></td>
                                    <th>Número de Identificación</th>
                                    <td><?php echo $paciente->paciente_identificacion; ?></td>
                                </tr>
                                <tr>
                                    <th>Tipo de Identificación</th>
                                    <td><?php echo $paciente->tipo_identificacion; ?></td>
                                    <th>Fecha de Nacimiento</th>
                                    <td><?php echo $paciente->paciente_fecha_nacimiento; ?></td>
                                </tr>
                                <tr>
                                    <th>Sexo</th>
                                    <td><?php echo $paciente->paciente_sexo; ?></td>
                                    <th>Correo Electrónico</th>
                                    <td><?php echo $paciente->paciente_email; ?></td>
                                </tr>
                                <tr>
                                    <th>Teléfono de Contacto</th>
                                    <td><?php echo $paciente->paciente_telefono; ?></td>
                                    <th>Número de Celular</th>
                                    <td><?php echo $paciente->paciente_celular; ?></td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td><?php echo $paciente->paciente_direccion; ?></td>
                                    <th>Ciudad</th>
                                    <td><?php echo $paciente->ciudad; ?></td>
                                </tr>
                            </table>
                        </div>
                        <p>
                            <a class="btn button-u" href="<?php echo site_url('app/paciente/update_info'); ?>">Actualizar Información »</a>
                        </p>
                        <?php
                        break;
                }
                ?>

            </div>
            <div class="col-md-12">
                <div id="calendar" class=" hidden"></div>
            </div>
        </div>

    </article>
</div>

<script>



    $(function () {
        $('.datetimepicker').datetimepicker({
            locale: 'es',
            format: 'Y-MM-DD',
            maxDate: new Date()
        });
    });
    $("#form").validate({
        rules: {
            nombres: {
                required: true
            },
            apellidos: {
                required: true
            },
            tipo_identificacion: {
                required: true
            },
            fecha: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            sexo: {
                required: true
            }
        },
        submitHandler: function (form) {

            var url = '<?php echo site_url("app/update_paciente") ?>';
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'JSON',
                cache: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                data: {
                    nombres: $('input[name=nombres]').val(),
                    apellidos: $('input[name=apellidos]').val(),
                    tipo_identificacion: $('select[name=tipo_identificacion]').val(),
                    fecha: $('input[name=fecha]').val(),
                    email: $('input[name=email]').val(),
                    sexo: $('select[name=sexo]').val(),
                    telefono: $('input[name=telefono]').val(),
                    celular: $('input[name=celular]').val(),
                    direccion: $('input[name=direccion]').val(),
                    ciudad: $('select[name=ciudad]').val(),
                    id: <?php echo $paciente->id_paciente; ?>
                },
                success: function (msg) {
                    swal({
                        allowOutsideClick: false,
                        title: "Listo!",
                        text: "El registro se realizó correctamente ",
                        type: "success",
                        confirmButtonText: "Continuar"
                    }).then((result) => {
                        if (result.value) {
                            window.location.href = "<?php echo site_url("app/paciente/") ?>";
                        }
                    });
                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
            return false; // block the default submit action
        }
    });
    $("#form_passw").validate({
        rules: {
            password: {
                required: true,
                remote: {
                    url: "<?php echo site_url("app/check_passw"); ?>",
                    type: "post",
                    data: {
                        id: function () {
                            return $("input[name=password]").val();
                        },
                        id_paciente: <?php echo $paciente->paciente_identificacion; ?>
                    }
                }
            },
            newpassw: {
                required: true,
                minlength: 6
            },
            repassw: {
                required: true,
                equalTo: "#newpassw"
            }
        }, messages: {
            'repassw':
                    {
                        equalTo: "Las contraseñas no coinciden"
                    },
            'password':
                    {
                        remote: jQuery.validator.format("La contraseña no coincide con la registrada ")
                    }
        },
        submitHandler: function (form) {

            var url = '<?php echo site_url("app/update_paciente_passw") ?>';
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'JSON',
                cache: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                data: {
                    passw: $('input[name=newpassw]').val(),
                    id: <?php echo $paciente->id_paciente; ?>
                },
                success: function (msg) {
                    swal({
                        allowOutsideClick: false,
                        title: "Listo!",
                        text: "El registro se realizó correctamente ",
                        type: "success",
                        confirmButtonText: "Continuar"
                    }).then((result) => {
                        if (result.value) {
                            window.location.href = "<?php echo site_url("app/paciente/") ?>";
                        }
                    });
                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
            return false; // block the default submit action
        }
    });
    $('select[name=pais]').change(function () {
        var id = $(this).val();
        var url = "<?php echo site_url('app/obtenerdepartamentos/'); ?>" + "/" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('select[name=departamento]')[0].options.length = 0;
                for (var i in msg) {
                    var a = msg[i]['ciudad_id_departamento'];
                    var b = msg[i]['ciudad_departamento_nombre'];
                    $('select[name=departamento]').append(
                            $('<option></option>').val('' + a).html('' + b));
                }
                actualizar_ciudad2();
            }
        });
    });
    $('select[name=departamento]').change(function () {
        var id = $(this).val();
        $('input[name=value_departamento]').val(id);
        var url = "<?php echo site_url('app/obtenerciudades/'); ?>" + "/" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('select[name=ciudad]')[0].options.length = 0;
                for (var i in msg) {
                    var a = msg[i]['id_ciudad'];
                    var b = msg[i]['ciudad_nombre'];
                    $('select[name=ciudad]').append(
                            $('<option></option>').val('' + a).html('' + b));
                }
            }

        });
    });
    function actualizar_ciudad2() {
        var id = $('select[name=departamento]').val();
        if (!empty(id)) {
            var url = "<?php echo site_url('app/obtenerciudades/'); ?>" + "/" + id;
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                success: function (msg) {
                    $('select[name=ciudad]')[0].options.length = 0;
                    for (var i in msg) {
                        var a = msg[i]['id_ciudad'];
                        var b = msg[i]['ciudad_nombre'];
                        $('select[name=ciudad]').append(
                                $('<option></option>').val('' + a).html('' + b));
                    }
                }

            });
        }
    }

    function empty(e) {
        switch (e) {
            case "":
            case 0:
            case "0":
            case null:
            case false:
            case typeof this == "undefined":
                return true;
            default :
                return false;
        }
    }



    $("#form_buscar").validate({
        rules: {
            sede: {
                required: true
            },
            servicio: {
                required: true
            }
        },
        submitHandler: function (form) {

            var url = '<?php echo site_url("app/buscar_citas") ?>';
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'JSON',
                cache: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                data: {
                    sede: $('select[name=sede]').val(),
                    servicio: $('select[name=servicio]').val(),
                    id: <?php echo $paciente->paciente_identificacion; ?>
                },
                success: function (msg) {
                    if (msg.estado === 0) {
                        $('#resultados').empty();
                        $('#calendar').addClass('hidden');
                        $('#resultados').append("<hr>" + msg.mensaje);
                        $('#calendar').empty();
                    } else {
                        $('#resultados').empty();
                        $('#resultados').append("<hr>" + msg.mensaje);
                        $('#calendar').removeClass('hidden');
                        $('#calendar').fullCalendar('destroy');
                        var date = new Date();
                        var d = date.getDate();
                        var m = date.getMonth();
                        var y = date.getFullYear();
                        $('#calendar').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,agendaWeek,agendaDay'
                            },
                            editable: false,
                            droppable: false, // this allows things to be dropped onto the calendar
                            locale: 'es',
                            height: 600,
                            defaultView: 'month',
                            minTime: '05:00', // a start time (10am in this example)
                            maxTime: '22:00', // an end time (6pm in this example)
                            slotDuration: '00:15:00',
                            eventSources: [
                                // your event source
                                {
                                    url: '<?php echo site_url('app/calendario'); ?>', // use the `url` property
                                    type: 'POST',
                                    data: {
                                        sede: $('select[name=sede]').val(),
                                        servicio: $('select[name=servicio]').val()
                                    }
                                }

                                // any other sources...

                            ],
                            eventClick: function (event) {
                                if (event.disponible == 1) {
                                    swal({
                                        allowOutsideClick: false,
                                        title: "Confirmación!",
                                        text: "¿ Está seguro de tomar la cita el día " + Unix_timestamp(event.start) + " a las " + event.hora + " ?",
                                        type: "warning",
                                        confirmButtonText: "Confirmar",
                                        showCancelButton: true,
                                        cancelButtonText: "Cancelar"
                                    }).then((result) => {
                                        if (result.value) {
                                            var url = '<?php echo site_url("app/guardar_cita") ?>';
                                            $.ajax({
                                                url: url,
                                                type: 'post',
                                                dataType: 'JSON',
                                                cache: false,
                                                beforeSend: function () {
                                                    $.blockUI();
                                                },
                                                complete: function () {
                                                    $.unblockUI();
                                                },
                                                data: {
                                                    id: event.id,
                                                    dia: event.dia,
                                                    hora: event.hora
                                                },
                                                success: function (msg) {
                                                    if (msg === 1) {
                                                        swal({
                                                            allowOutsideClick: false,
                                                            title: "Listo!",
                                                            text: "Su cita ha sido solicitada y está en proceso de validación, En las próximas 6 horas usted recibirá un correo electrónico de la aprobación o negación de su cita.",
                                                            type: "success",
                                                            confirmButtonText: "Continuar"
                                                        }).then((result) => {
                                                            if (result.value) {
                                                                window.location.href = "<?php echo site_url("app/paciente/") ?>";
                                                            }
                                                        });
                                                    } else {
                                                        swal({
                                                            allowOutsideClick: false,
                                                            title: "Alerta",
                                                            text: "La hora especificada ya no estaba disponible, por favor intente de nuevo ",
                                                            type: "warning",
                                                            confirmButtonText: "Continuar"
                                                        }).then((result) => {
                                                            if (result.value) {
                                                                location.reload();
                                                            }
                                                        });
                                                    }

                                                }

                                            });
                                        }
                                    });
                                } else {
                                    swal.fire({
                                        title: "Alerta",
                                        text: "La hora especificada no está disponible, por favor seleccione otra hora ",
                                        type: "warning",
                                        confirmButtonText: "Cerrar"
                                    });
                                }

                            }
                        });
                        $('#calendar').fullCalendar('refresh');
                    }

                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
            return false; // block the default submit action
        }
    });
    $('.delete_modal').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(4);
        $('#delete').modal('show');
        $('#delete_submit').click(function (e) {
            e.stopImmediatePropagation();
            var url = '<?php echo site_url("app/eliminar_cita") . '/' ?>' + id;
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (msg) {
                    if (msg === true) {
                        $('#delete').modal('hide');
                        swal({
                            allowOutsideClick: false,
                            title: "Listo!",
                            text: "El cita se elimino correctamente ",
                            type: "success",
                            confirmButtonText: "Continuar"
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        });
                    } else {
                        if (msg['code'] === 1451 || msg['code'] === 1452) {
                            $('#delete').modal('hide');
                            alert("No se puede elimnar el registro, pues te ya tiene registros asociados");
                        }
                    }
                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
        });
    });


    $('.no_delete').click(function () {
        swal.fire({
            title: "Alerta",
            text: "Para poder cancelar una cita que ya esté en estado de “confirmada”, debe hacer llamando a la linea telefónica +(57) (4) 448 33 11 con al menos 2 hora de antelación a la hora programada.  ",
            type: "warning",
            confirmButtonText: "Cerrar"
        });
    });
    function Unix_timestamp(t)
    {
        var date = new Date(t);
// Hours part from the timestamp
        var hours = date.getHours();
// Minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
// Seconds part from the timestamp
        var seconds = "0" + date.getSeconds();
// Will display time in 10:30:23 format
        var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', timeZone: 'America/Bogota'};
        return formattedTime = date.toLocaleString('es-CO', options);
    }

</script>