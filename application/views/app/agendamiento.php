<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/css/autoFill.bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/css/buttons.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/ColReorder-1.3.2/css/colReorder.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/FixedColumns-3.2.2/css/fixedColumns.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/FixedHeader-3.1.2/css/fixedHeader.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/KeyTable-2.1.2/css/keyTable.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/RowReorder-1.1.2/css/rowReorder.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Scroller-1.4.2/css/scroller.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('plugins/datatables2'); ?>/Select-1.2.0/css/select.bootstrap.min.css"/>

<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/JSZip-2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/pdfmake-0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/pdfmake-0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/DataTables-1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/js/dataTables.autoFill.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/AutoFill-2.1.2/js/autoFill.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Buttons-1.2.0/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/ColReorder-1.3.2/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/FixedColumns-3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/FixedHeader-3.1.2/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/KeyTable-2.1.2/js/dataTables.keyTable.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Responsive-2.1.0/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/RowReorder-1.1.2/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Scroller-1.4.2/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('plugins/datatables2'); ?>/Select-1.2.0/js/dataTables.select.min.js"></script>
<div class="col-md-9">
    <div class="row">
        <div class="col-md-12">
            <h3>
                Agendamiento 
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a href="<?php echo site_url("administrador/app/nuevo_agendamiento"); ?>" type="button" class="btn btn-success">
                <i class="fa fa-plus"></i> Nuevo Agendamiento 
            </a>
            <hr>
            <div class = "table-responsive">
                <table  class = "table table-striped table-hover table-bordered full_table fullwidth">
                    <thead>
                        <tr>
                            <th ><span >Acciones</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por sede">Sede</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por servicio">Servicio</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha">Fecha Inicio</span></th>
                            <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha">Fecha de Finalización</span></th>
                            <th ><span  >Lunes</span></th>
                            <th ><span  >Martes</span></th>
                            <th ><span  >Miercoles</span></th>
                            <th ><span  >Jueves</span></th>
                            <th ><span  >Viernes</span></th>
                            <th ><span  >Sabado</span></th>
                            <th ><span  >Domingo</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($agendamiento != FALSE) { ?>
                            <?php foreach ($agendamiento as $row) { ?>
                                <tr>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="...">
                                            <a class="btn btn-xs btn-primary dim" title="Editar" data-toggle="tooltip" data-placement="top"  alt="Editar" href="<?php echo site_url("administrador/app/nuevo_agendamiento") . '/' . $row->id_setup_servicios_x_sedes; ?>"><i class="fa fa-pencil-alt"></i></a>
                                            <a class="btn btn-xs btn-danger delete_modal" data-toggle="tooltip" data-placement="top"  title="Eliminar" alt="Eliminar" id="row_<?php echo $row->id_setup_servicios_x_sedes; ?>"  href="#"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <td><?php echo $row->sede; ?></td>
                                    <td><?php echo $row->servicio; ?></td>
                                    <td><?php echo $row->setup_servicios_x_sedes_fecha_inicio; ?></td>
                                    <td><?php echo $row->setup_servicios_x_sedes_fecha_final; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_lunes == 'Si') ? "$row->setup_servicios_x_sedes_lunes_hora_inicio - $row->setup_servicios_x_sedes_lunes_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_martes == 'Si') ? "$row->setup_servicios_x_sedes_martes_hora_inicio - $row->setup_servicios_x_sedes_martes_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_miercoles == 'Si') ? "$row->setup_servicios_x_sedes_miercoles_hora_inicio - $row->setup_servicios_x_sedes_miercoles_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_jueves == 'Si') ? "$row->setup_servicios_x_sedes_jueves_hora_inicio - $row->setup_servicios_x_sedes_jueves_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_viernes == 'Si') ? "$row->setup_servicios_x_sedes_viernes_hora_inicio - $row->setup_servicios_x_sedes_viernes_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_sabados == 'Si') ? "$row->setup_servicios_x_sedes_sabados_hora_inicio - $row->setup_servicios_x_sedes_sabados_hora_fin" : ' - '; ?></td>
                                    <td><?php echo ($row->setup_servicios_x_sedes_domingos == 'Si') ? "$row->setup_servicios_x_sedes_domingos_hora_inicio - $row->setup_servicios_x_sedes_domingos_hora_fin" : ' - '; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <div id="calendar"></div>
        </div>
    </div>
</div>


<!-- DELETE MODAL -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Alerta !</h4>
            </div>
            <div class="modal-body contenido_modal_delete">
                ¿ Esta seguro de eliminar el agendamiento, tenga en cuenta que, si este cuenta con citas programadas, estas serán eliminadas igualmente ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
                <button type="button" id="delete_submit"  class="btn btn-danger">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<script>


    $('.delete_modal').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(4);
        $('#delete').modal('show');
        $('#delete_submit').click(function (e) {
            e.stopImmediatePropagation();
            var url = '<?php echo site_url("administrador/app/eliminar_agendamiento") . '/' ?>' + id;
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                success: function (msg) {
                    if (msg === true) {
                        $('#delete').modal('hide');
                        location.reload();
                    } else {
                        if (msg['code'] === 1451 || msg['code'] === 1452) {
                            $('#delete').modal('hide');
                            alert("No se puede elimnar el registro, pues te ya tiene registros asociados");
                        }
                    }
                },
                error: function (jqXHR, exception) {

                    if (jqXHR.status === 0) {
                        alert('No Conecta.\n Verifique su conexión a internet.');
                    } else if (jqXHR.status === 404) {
                        alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                    } else if (jqXHR.status === 500) {
                        alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'parsererror') {
                        alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'timeout') {
                        alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                    } else if (exception === 'abort') {
                        alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                    }

                }

            });
        });
    });
    $(function () {
        $('.full_table').dataTable({
            responsive: false,
            dom: "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'l><'col-sm-7'p><'col-sm-12'i>>",
            buttons: [
                {
                    extend: 'copy',
                    text: '<i class="fa fa-files-o"></i> Copiar'
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-excel-o"></i> Excel'
                },
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-text-o"></i> Csv'
                },
                {
                    extend: 'pdf',
                    text: '<i class="fa fa-file-pdf-o"></i> Pdf'
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i> Imprimir'
                }

            ],
            "tableTools": {
                "sSwfPath": "<?php echo site_url(); ?>js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "pagingType": "full_numbers",
            "aLengthMenu": [[10, 25, 50, 100, 250, 500, -1], [10, 25, 50, 100, 250, 500, "TODOS"]],
            "bPaginate": true,
            "bLengthChange": true,
            "responsive": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "oLanguage": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando del _START_ al _END_ de un total de _TOTAL_",
                "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0",
                "sInfoFiltered": "(filtrado de un total de _MAX_ )",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "order": [[3, "desc"]],
            "columnDefs": [
                {"orderable": false, "targets": 5},
                {"orderable": false, "targets": 6},
                {"orderable": false, "targets": 7},
                {"orderable": false, "targets": 8},
                {"orderable": false, "targets": 9},
                {"orderable": false, "targets": 10},
                {"orderable": false, "targets": 11},
                {"orderable": false, "targets": 0}
            ]
        });
    });
    $(document).ready(function () {
        /* initialize the calendar
         -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next ',
                center: 'title',
                right: 'month'
            },
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            locale: 'es',
            height: 600,
            eventColor: '#00B69F',
            events: [
                <?php
if ($agendamiento != FALSE) {
    $colores = array('purple', 'green', 'blue', 'red', '#00B69F', 'teal');
    $x = 0;
    ?>
    <?php foreach ($agendamiento as $evento) { ?>
                    {
                    title: '<?php echo $evento->sede . ' / ' . $evento->servicio; ?>',
                            start: new Date(<?php echo date('Y', strtotime($evento->setup_servicios_x_sedes_fecha_inicio)) ?>, <?php echo date('m', strtotime($evento->setup_servicios_x_sedes_fecha_inicio)) - 1 ?>, <?php echo date('d', strtotime($evento->setup_servicios_x_sedes_fecha_inicio)) ?>,<?php echo date('H', strtotime($evento->setup_servicios_x_sedes_fecha_inicio)) ?>,<?php echo date('i', strtotime($evento->setup_servicios_x_sedes_fecha_inicio)) ?>),
                            end: new Date(<?php echo date('Y', strtotime($evento->setup_servicios_x_sedes_fecha_final)) ?>, <?php echo date('m', strtotime($evento->setup_servicios_x_sedes_fecha_final)) - 1 ?>, <?php echo date('d', strtotime($evento->setup_servicios_x_sedes_fecha_final)) ?>,<?php echo date('H', strtotime($evento->setup_servicios_x_sedes_fecha_final)) ?>,<?php echo date('i', strtotime($evento->setup_servicios_x_sedes_fecha_final)) ?>),
                            allDay: true,
                            url: '<?php echo site_url('sitio/calendario_evento') . '/'; ?>',
                            color: '<?php echo $colores[$x] ?>'
                    },
        <?php
        $x++;
        if ($x == 6) {
            $x = 0;
        }
    }
    ?>
<?php } ?>

                {
                    title: 'Click for Google',
                    start: new Date(2000, m, 28),
                    end: new Date(2000, m, 29),
                    url: 'http://google.com/'
                }

            ]
        });
    });

</script>