<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Presupuesto Aprobado en Ejercicio</spam></h2>
                </div>
                <p>Presupuesto en ejercicio detallado, que es el aprobado de acuerdo con las normas vigentes aplicables para la entidad.</p>
                <hr>
                <div class=" table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Ingresos</th>
                            <td><?php
                                if ($presupuesto != NULL) {
                                    if ($presupuesto->financiera_presupuesto_ingresos != NULL) {
                                        ?><a target="_blank" href="<?php echo site_url('uploads/entidad/financiera') . '/' . $presupuesto->financiera_presupuesto_ingresos; ?>"  class="btn btn-primary "><i class="fas fa-file-<?php echo obtenerFielType($presupuesto->financiera_presupuesto_ingresos); ?>"></i> Ver Documento</a></p>
                                        <?php
                                    } else {
                                        echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                    }
                                } else {
                                    echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th>Egresos</th>
                            <td><?php
                                if ($presupuesto != NULL) {
                                    if ($presupuesto->financiera_presupuesto_gastos != NULL) {
                                        ?><a target="_blank" href="<?php echo site_url('uploads/entidad/financiera') . '/' . $presupuesto->financiera_presupuesto_gastos; ?>"  class="btn btn-primary "><i class="fas fa-file-<?php echo obtenerFielType($presupuesto->financiera_presupuesto_gastos); ?>"></i> Ver Documento</a></p>
                                        <?php
                                    } else {
                                        echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                    }
                                } else {
                                    echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th>Ejecución del Presupuesto</th>
                            <td><a id="ejecucion" href="#" onclick="return false;"  class="btn btn-primary ">Ver Ejecuciones</i></a></p> </td>
                        </tr>
                        <tr>
                            <th>Acuerdo</th>
                            <td><?php
                                if ($presupuesto != NULL) {
                                    if ($presupuesto->id_normatividad_leyes != NULL) {
                                        ?><a target="_blank" href="<?php echo site_url('uploads/entidad/normatividad') . '/' . $presupuesto->normatividad_leyes_archivo; ?>"  class="btn btn-primary "><i class="fas fa-file-pdf"> </i> Ver Documento</a></p>
                                        <?php
                                    } else {
                                        echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                    }
                                } else {
                                    echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                                }
                                ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('#ejecucion').click(function () {
        var url = "<?php echo site_url('entidad/presupuesto_ajax') ?>";
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>