<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Convenios</spam></h2>
                </div>
                <?php if ($convenios != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por numero de proceso">Numero de Proceso</span></th>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por tipo de objeto">Objeto</span></th>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha de publicacion">Fecha  de Publicación</span></th>
                                    <th >Archivo</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 0;
                                foreach ($convenios as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->contratacion_convenios_numero_proceso; ?></td>
                                        <td ><?php echo $dato->contratacion_convenios_objeto; ?></td>
                                        <td ><?php echo $dato->contratacion_convenios_fecha_publicacion; ?></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/contratacion') . '/' . $dato->contratacion_convenios_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->contratacion_convenios_archivo); ?>"> </i> Ver Archivo</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
