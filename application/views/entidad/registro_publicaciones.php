<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-12 col-lg-12">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Registro de Publicaciones</spam></h2>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered  fullwidth" id="data" >
                        <thead>
                            <tr>
                                <?php foreach ($columns as $column) { ?>
                                    <th><?php echo $column; ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <?php foreach ($columns as $column) { ?>
                                    <th><?php echo $column; ?></th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        //datatables
        table = $('#data').DataTable({
            responsive: true,
            "dom": '<"clear">lfrtip',
            "pagingType": "full_numbers",
            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('entidad/registro_publicaciones_ajax_list') ?>",
                "type": "POST",
            },
            "oLanguage": {
                "sProcessing": "<img src='<?php echo site_url(); ?>images/ring.gif'>",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "tableTools": {
                "sSwfPath": "<?php echo site_url(); ?>js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            },
            "columnDefs": [
                {
                    "targets": [4], //first column / numbering column
                    "orderable": false, //set not orderable
                }
            ]
        });
    });
</script>