s<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Planes de Mejoramiento</spam></h2>
                </div>
                <p>Planes de Mejoramiento vigentes (exigidos por los entes de control internos o externos) y enlace al sitio web del organismo de control en donde se encuentren los informes que éste ha elaborado sobre la entidad. </p>
                <hr>
                <?php if ($planes_mejoramiento != FALSE) { ?>
                    <ul class="nav nav-pills nav-fill" role="tablist" id="myTab">
                        <?php
                        $j = 0;
                        foreach ($planes_mejoramiento_periodo as $periodo_plan) {
                            ?>
                            <li class="nav-item" role="tab_<?php echo $periodo_plan->control_planes_mejoramiento_periodo; ?>" >
                                <a class="nav-link  <?php echo($j == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $periodo_plan->control_planes_mejoramiento_periodo; ?>" aria-controls="tab_<?php echo $periodo_plan->control_planes_mejoramiento_periodo; ?>" role="tab" data-toggle="tab"><?php echo $periodo_plan->control_planes_mejoramiento_periodo; ?>
                                </a></li>
                            <?php
                            $j++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <div class="tab-content">
                        <?php
                        $x = 0;
                        foreach ($planes_mejoramiento_periodo as $periodo_plan) {
                            ?>
                            <div role="tabpanel" class="tab-pane  <?php echo ($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo_plan->control_planes_mejoramiento_periodo; ?>">
                                <div class="table-responsive">
                                    <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                            <tr>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por ente">Ente de Control</span></th>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por dependencia">Dependencia</span></th>
                                                <th ><span>Archivo</span></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($planes_mejoramiento as $dato) {
                                                if ($dato->control_planes_mejoramiento_periodo == $periodo_plan->control_planes_mejoramiento_periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato->control_planes_mejoramiento_nombre; ?></td>
                                                        <td ><?php echo $dato->ente; ?></td>
                                                        <td ><?php echo $dato->dependencia; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_planes_mejoramiento_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_planes_mejoramiento_archivo); ?>"> </i> Ver Archivo</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $x++;
                        }
                        ?>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
            <?php } ?>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>