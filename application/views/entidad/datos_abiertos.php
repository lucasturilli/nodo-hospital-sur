<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Datos Abiertos</spam></h2>
                </div>
                <?php if ($archivos != FALSE) { ?>
                    <div class="table-responsive">
                        <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Fecha">Fecha de Ingreso</span></th>
                                    <th ><span>Archivo</span></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($archivos as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->archivos_datos_abiertos_nombre; ?></td>
                                        <td ><?php echo $dato->archivos_datos_abiertos_fecha_ingreso; ?></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/datos_abiertos') . '/' . $dato->archivos_datos_abiertos_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->archivos_datos_abiertos_archivo); ?>"> </i> Ver Archivo</a></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>


