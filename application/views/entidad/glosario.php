<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Glosario</spam></h2>
                </div>
                <?php if ($glosario != FALSE) { ?>
                    <div class="table-responsive">
                        <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por palabra">Palabra</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por significado ">Significado </span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($glosario as $dato) {
                                    ?>
                                    <tr>
                                        <th ><?php echo $dato->entidad_glosario_palabra; ?></th>
                                        <td ><?php echo $dato->entidad_glosario_descripcion; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No se encontraron palabras que empiecen por la letra “<?php echo $letra; ?>”</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>