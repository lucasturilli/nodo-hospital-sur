<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Información Histórica de Presupuestos</spam></h2>
                </div>
                <p>Información detallada del presupuesto histórico de ingresos y gastos, aprobado y ejecutado, con corte a diciembre del periodo respectivo.</p>
                <hr>
                <?php if ($presupuesto != FALSE) { ?>
                    <div class = "table-responsive">
                        <table  class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por periodo">Periodo</span></th>
                                    <th >Ingresos</th>
                                    <th >Gastos</th>
                                    <th >Acuerdo</th>
                                    <th >Ejecuciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 0;
                                foreach ($presupuesto as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->financiera_presupuesto_periodo; ?></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/financiera') . '/' . $dato->financiera_presupuesto_ingresos; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato->financiera_presupuesto_ingresos); ?>"></i> Ver Archivo</a></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/financiera') . '/' . $dato->financiera_presupuesto_gastos; ?>" class="btn btn-warning btn-xs"> <i class="fas fa-file-<?php echo obtenerFielType($dato->financiera_presupuesto_gastos); ?>"></i> Ver Archivo</a></td>
                                        <td ><?php if ($dato->id_normatividad_leyes != NULL) {
                                        ?><a target="_blank" href="<?php echo site_url('uploads/entidad/normatividad') . '/' . $dato->normatividad_leyes_archivo; ?>" class="btn btn-danger btn-xs"> <i class="fas fa-file-<?php echo obtenerFielType($dato->financiera_presupuesto_gastos); ?>"></i> Ver Archivo</a><?php
                                            } else {
                                                echo 'No Disponible';
                                            }
                                            ?>
                                        </td>
                                        <td ><a id="norma_<?php echo $dato->financiera_presupuesto_periodo; ?>" href="#" onclick="return false;" class="btn btn-info btn-xs">Ver Ejecuciones</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                <?php } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('td a').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(6)
        var url = "<?php echo site_url('entidad/presupuesto_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>