<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Procedimientos</spam></h2>
                </div>
                <?php if ($contratos != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth ">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por nombre">Numero de Proceso</span></th>
                                    <th >Archivo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 0;
                                foreach ($contratos as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->entidad_procedimientos_nombre; ?></td>
                                        <td ><a href="<?php echo site_url('uploads/entidad/calidad') . '/' . $dato->entidad_procedimientos_archivo; ?>" target="_blank" class="btn btn-primary btn-xs "><i class="fas fa-file-<?php echo obtenerFielType($dato->entidad_procedimientos_archivo); ?>"> </i> Ver Archivo</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>