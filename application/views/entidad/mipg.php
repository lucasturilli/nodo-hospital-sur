<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Modelo Integrado de Planeación y Gestión “MIPG”</spam></h2>
                </div>
                <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                    <li class="nav-item" role="tab1"><a class="nav-link active" aria-controls="tab1"  data-toggle="tab" href="#tab1" role="tab">Plan de Acción</a></li>
                    <li class="nav-item" role="tab2"><a class="nav-link " aria-controls="tab2" data-toggle="tab" href="#tab2" role="tab">Planes</a></li>
                </ul>
                <hr>
                <div class="tab-content">
                    <div class="tab-pane   active" id="tab1" role="tabpanel">
                        <?php if ($plan != FALSE) { ?>
                            <div class="table-responsive">
                                <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                            <th ><span>Archivo</span></th>
                                            <th ><span>Avances del Plan</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($plan as $dato) {
                                            ?>
                                            <tr>
                                                <td ><?php echo $dato->entidad_mipg_plan_nombre; ?></td>
                                                <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/adicional') . '/' . $dato->entidad_mipg_plan_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->entidad_mipg_plan_archivo); ?>"> </i> Ver Archivo</a></td>
                                                <td ><a id="programa_<?php echo $dato->id_entidad_mipg_plan; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_avances" ><i class="fas fa-plus-square"> </i> Ver Avances</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?> 
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane " id="tab2" role="tabpanel">
                        <?php if ($planes != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th ><span>Archivo</span></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($planes as $dato) {
                                            ?>
                                            <tr>
                                                <td ><?php echo $dato->entidad_mipg_planes_nombre; ?></td>
                                                <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/adicional') . '/' . $dato->entidad_mipg_planes_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->entidad_mipg_planes_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>

    $('.ver_avances').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/mipg_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>


