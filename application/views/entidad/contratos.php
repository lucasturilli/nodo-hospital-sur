<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Procesos de Contratación</spam></h2>
                </div>
                <?php if ($contratos != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por numero de proceso">Numero de Proceso</span></th>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por tipo">Tipo</span></th>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por tipo de objeto">Objeto</span></th>
                                    <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por fecha de publicacion">Fecha  de Publicación</span></th>
                                    <th >Enlace a SECOP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 0;
                                foreach ($contratos as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->contratacion_contratos_numero_proceso; ?></td>
                                        <td ><?php echo $dato->contratacion_contratos_tipo; ?></td>
                                        <td ><?php echo $dato->contratacion_contratos_objeto; ?></td>
                                        <td ><?php echo $dato->contratacion_contratos_fecha_publicacion; ?></td>
                                        <td ><a data-toggle = "tooltip" data-placement = "top"   title="Ver en Secop" alt="Ver en Secop" target="_blank" href="<?php echo prep_url($dato->contratacion_contratos_enlace); ?>" class="btn btn-secondary btn-sm" ><i class="fas fa-external-link-alt"></i></a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>