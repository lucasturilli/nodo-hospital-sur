<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Mapas de Datos Abiertos</spam></h2>
                </div>
                <?php if ($mapas != FALSE) { ?>
                    <div class="table-responsive">
                        <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                    <th ><span>Enlace</span></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($mapas as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->mapas_datos_abiertos_nombre; ?></td>
                                        <td ><a onclick="mostrar_mapa('<?php echo $dato->id_mapas_datos_abiertos; ?>');"  href="#" class="btn btn-primary btn-xs" > Ver Mapa</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    </div>
</div>
<script>
    function mostrar_mapa(id) {
        var url = "<?php echo site_url('entidad/mapas_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });

    }
<?php if ($tab == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show')
        });
<?php } else { ?>
        $(function () {
            $('#myTab li:eq(' +<?php echo $tab; ?> + ') a').tab('show')
        });
<?php } ?>
</script>

