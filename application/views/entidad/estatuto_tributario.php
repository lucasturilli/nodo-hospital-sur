<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Estatuto Tributario</spam></h2>
                </div>
                <?php
                $x = 0;
                if ($presupuesto != FALSE) {
                    ?>
                    <ul class="nav nav-pills nav-fill" id="myTab"  role="tablist">
                        <?php foreach ($periodos as $perido) { ?>
                            <li class="nav-item" role="tab_<?php echo $perido->financiera_estatuto_tributario_periodo; ?>" ><a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $perido->financiera_estatuto_tributario_periodo; ?>" aria-controls="tab_<?php echo $perido->financiera_estatuto_tributario_periodo; ?>" role="tab" data-toggle="tab"><?php echo $perido->financiera_estatuto_tributario_periodo; ?></a></li>
                            <?php
                            $x++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <div class="tab-content">
                        <?php
                        $x = 0;
                        foreach ($periodos as $perido) {
                            ?>
                            <div role="tabpanel" class="tab-pane  <?php echo($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $perido->financiera_estatuto_tributario_periodo; ?>">
                                <!-- Table -->
                                <div class = "table-responsive">
                                    <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                            <tr>
                                                <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por nombre">Nombre</span></th>
                                                 <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por descripción">Descripción</span></th>
                                                <th >Archivo</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($presupuesto as $dato) {
                                                if ($dato->financiera_estatuto_tributario_periodo == $perido->financiera_estatuto_tributario_periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato->financiera_estatuto_tributario_nombre; ?></td>
                                                        <td ><?php echo $dato->financiera_estatuto_tributario_descripcion; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/financiera') . '/' . $dato->financiera_estatuto_tributario_archivo; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato->financiera_estatuto_tributario_archivo); ?>"></i> Ver Archivo</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $x++;
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>

