<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-12 col-lg-12">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Transparencia y acceso a información pública</spam></h2>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-check"></i>
                        <h3>1. Calidad  </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/caracterizacion_procesos'); ?>">Caracterización de Procesos</a></li>
                            <li><a href="<?php echo site_url('entidad/procedimientos'); ?>">Procedimientos</a></li>
                            <li><a href="<?php echo site_url('entidad/index/calidad'); ?>">Sistema de Calidad</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-id-badge"></i>
                        <h3>2. Contratación  </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/avance_contractual'); ?>">Avances Contractuales</a></li>
        <!--                    <li><a href="<?php //echo site_url('entidad/convenios');   ?>">Convenios </a></li>-->
                            <li><a href="<?php echo site_url('entidad/index/secop'); ?>">Información SECOP</a></li>
                            <li><a href="<?php echo site_url('entidad/contratacion_lineamientos'); ?>">Lineamientos de Adquisiciones y Compras </a></li>
                            <li><a href="<?php echo site_url('entidad/plan_compras'); ?>">Plan de Adquisiciones</a></li>
                            <li><a href="<?php echo site_url('entidad/contratos'); ?>">Procesos de Contratación </a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-clipboard"></i>
                        <h3>3. Control </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/auditorias'); ?>">Auditorias</a></li>
                            <li><a href="<?php echo site_url('entidad/index/defensa_judicial'); ?>">Defensa Judicial</a></li>
                            <li><a href="<?php echo site_url('entidad/entes'); ?>">Entes de Control que Vigilan la Entidad</a></li>
                            <li><a href="<?php echo site_url('entidad/index/poblacion_vulnerable'); ?>">Información para Población Vulnerable</a></li>
                            <li><a href="<?php echo site_url('entidad/index/informe_concejo'); ?>">Informes al Concejo</a></li>
                            <li><a href="<?php echo site_url('entidad/informe_pqrs'); ?>">Informe de Peticiones, Quejas, Reclamos, Denuncias y Solicitudes de Acceso a la Información</a></li>
                            <li><a href="<?php echo site_url('entidad/informe_ciudadania'); ?>">Informes de Rendición de Cuenta a los Ciudadanos</a></li>
                            <li><a href="<?php echo site_url('entidad/index/informe_fiscal'); ?>">Informes de Rendición de Cuenta Fiscal</a></li>
                            <li><a href="<?php echo site_url('entidad/planes_mejoramiento'); ?>">Planes de Mejoramiento</a></li>
                            <li><a href="<?php echo site_url('entidad/programas_poblacion_vulnerable'); ?>">Programas para la Población Vulnerable</a></li>
                            <li><a href="<?php echo site_url('entidad/proyectos_poblacion_vulnerable'); ?>">Proyectos para la Población Vulnerable </a></li>
                            <li><a href="<?php echo site_url('entidad/reportes_control_interno'); ?>">Reportes de Control Interno</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-unlock-alt"></i>
                        <h3>4. Datos Abiertos</h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/datos_abiertos'); ?>">Archivos de Datos Abiertos</a></li>
                            <li><a href="<?php echo site_url('entidad/catalogo_abiertos'); ?>">Catálogo de Datos Abiertos</a></li>
        <!--                    <li><a href="<?php //echo site_url('entidad/mapas_abiertos');   ?>">Mapas de Datos Abiertos</a></li>-->
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-money-bill-alt"></i>
                        <h3>5. Estados Financieros y Presupuesto </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/estados_financieros'); ?>">Estados Financieros</a></li>
                            <li><a href="<?php echo site_url('entidad/presupuesto_historico'); ?>">Información Histórica de Presupuestos</a></li>
                            <li><a href="<?php echo site_url('entidad/presupuesto'); ?>">Presupuesto Aprobado en Ejercicio</a></li>
        <!--                    <li><a href="<?php //echo site_url('entidad/presupuesto_estatuto_tributario');   ?>">Estatuto Tributario</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-users"></i>
                        <h3>6. Estructura Orgánica y Talento Humano </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/index/asignaciones_salariales'); ?>">Asignaciones Salariales</a></li>
                            <li><a href="<?php echo site_url('entidad/index/dependencias'); ?>">Dependencias</a></li>
                            <li><a href="<?php echo site_url('entidad/directorio_agremiaciones'); ?>">Directorio de agremiaciones, asociaciones y otros grupos de interés</a></li>
                            <li><a href="<?php echo site_url('entidad/directorio_sucursales'); ?>">Directorio de Sucursales o Regionales</a></li>
                            <li><a href="<?php echo site_url('entidad/directorio_entidades'); ?>">Directorio de Entidades</a></li>
                            <li><a href="<?php echo site_url('entidad/index/evaluacion_desempeno'); ?>">Evaluación de Desempeño</a></li>
                            <li><a href="<?php echo site_url('entidad/index/funcionarios'); ?>">Funcionarios</a></li>
                            <li><a href="<?php echo site_url('entidad/index/listado_planta'); ?>">Listado de Personal</a></li>
                            <li><a href="<?php echo site_url('entidad/manual_funciones'); ?>">Manual de funciones y competencias</a></li>
                            <li><a href="<?php echo site_url('entidad/index/mision_vision'); ?>">Misión y Visión</a></li>
                            <li><a href="<?php echo site_url('entidad/index/objetivos_funciones'); ?>">Objetivos y Funciones</a></li>
                            <li><a href="<?php echo $empleo_link->entidad_ofertas_empleo_url; ?>" target="<?php echo $empleo_link->entidad_ofertas_empleo_target; ?>">Ofertas de Empleo</a></li>
                            <li><a href="<?php echo site_url('entidad/index/organigrama'); ?>">Organigrama</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-podcast"></i>
                        <h3>7. Información de Interés </h3>
                        <ul>
                            <!--                        <li><a  id="ayudas_navegacion2" href="#" onclick="return false;" > Ayuda para navegar el sitio</a></li>-->
                            <li><a  href="<?php echo site_url('sitio/calendario'); ?>" > Calendario de actividades</a></li>
                            <li><a  href="<?php echo site_url('entidad/convocatorias') ?>" > Convocatorias</a></li>
                            <li><a  href="<?php echo site_url('entidad/estudios'); ?>" >Estudio, investigaciones y otras publicaciones </a></li>
                            <li><a  href="<?php echo site_url('entidad/glosario'); ?>" > Glosario</a></li>
                            <li><a  href="<?php echo site_url('entidad/informacion_adicional'); ?>" > Información adicional </a></li>
                            <li><a  href="<?php echo $configuracion->config_general_portal_ninos; ?>" target="_blank" > Información para niños y jóvenes</a></li>
                            <li><a  href="<?php echo site_url('sitio/noticias'); ?>" > Noticias</a></li>
                            <li><a  href="<?php echo site_url('entidad/preguntas_frecuentes'); ?>" > Preguntas y respuestas frecuentes</a></li>
                            <li><a  href="<?php echo site_url('entidad/datos_abiertos'); ?>" > Publicación de datos abiertos</a></li>

                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-university"></i>
                        <h3>8. Instrumentos de Gestión de Información Pública </h3>
                        <ul>
                            <ul>
                                <li><a target="_blank" href="<?php echo site_url("uploads/entidad/$informacion_general->entidad_informacion_costos_reproduccion"); ?>">Costos de Reproducción de Información</a></li>
                                <li><a href="<?php echo site_url('entidad/esquema_publicacion'); ?>">Esquema de Publicación</a></li>
                                <li><a href="<?php echo site_url('entidad/informacion_clasificada_reservada'); ?>">Índice de Información Clasificada y Reservada</a></li>
                                <li><a href="<?php echo site_url('entidad/informe_pqrs'); ?>">Informe de Peticiones, Quejas, Reclamos, Denuncias y Solicitudes de Acceso a la Información</a></li>
                                <li><a target="_blank" href="<?php echo $configuracion->config_general_url_pqrs; ?>">Presentar Peticiones, Quejas o Reclamos</a></li>
                                <li><a href="<?php echo site_url('entidad/control_archivo'); ?>">Programa de Gestión Documental</a></li>
                                <li><a href="<?php echo site_url('entidad/registro_activos_informacion'); ?>">Registro de Activos de Información</a></li>
                                <li><a href="<?php echo site_url('entidad/registro_publicaciones'); ?>">Registro de Publicaciones</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">

                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-handshake"></i>
                        <h3>9. Mecanismo de Contacto </h3>
                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#mecanismos_atencion">Mecanismos para la atención al ciudadano</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#atencion_linea">Atención en línea</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#buzon_contacto">Buzón de Contacto</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#pqrs">Solicitud y verificación de peticiones, quejas y reclamos </a></li>
                            <li><a href="#" data-toggle="modal" data-target="#informacion_publica"><span></span> Solicitudes de información pública</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#localizacion">Localización física y días de atención al público </a></li>
                            <li><a href="#" data-toggle="modal" data-target="#notificaciones">Correo electrónico para notificaciones judiciales </a></li>
                            <li><a href="#" data-toggle="modal" data-target="#politicas">Políticas de seguridad de la información del sitio web y protección de datos personales</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#trato_digno">Derechos de los ciudadanos - Carta de trato digno</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-file"></i>
                        <h3>10. Normatividad </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/circulares'); ?>">Circulares y/u Otros Actos Administrativos de Carácter General</a></li>
                            <li><a href="<?php echo site_url('entidad/decretos'); ?>">Decretos</a></li>
                            <li><a href="<?php echo site_url('entidad/edictos'); ?>">Edictos y Avisos</a></li>
                            <li><a href="<?php echo site_url('entidad/leyes'); ?>">Leyes / Ordenanzas / Acuerdos</a></li>
                            <li><a href="<?php echo site_url('entidad/resoluciones'); ?>">Resoluciones</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-signal"></i>
                        <h3>11. Planeación </h3>
                        <ul>
                            <li><a target="_blank" href="<?php echo site_url('foro'); ?>">Participación en Formulación de Políticas</a></li>
                            <li><a href="<?php echo site_url('entidad/control_archivo'); ?>">Gestión Documental</a></li>
                            <li><a href="<?php echo site_url('entidad/informe_empalme'); ?>">Informes de Empalme</a></li>
                            <li><a href="<?php echo site_url('entidad/index/indicadores_gestion'); ?>">Metas e Indicadores de Gestión</a></li>
        <!--                    <li><a href="<?php //echo site_url('entidad/mipg');   ?>">Modelo Integrado de Planeación y Gestión “MIPG”</a></li>-->
                            <li><a href="<?php echo site_url('entidad/index/planes_otros'); ?>">Otros Planes</a></li>
                            <li><a href="<?php echo site_url('entidad/plan_anticorrupcion'); ?>">Plan Anticorrupción</a></li>
                            <li><a href="<?php echo site_url('entidad/plan_accion'); ?>">Plan de Acción</a></li>
                            <li><a href="<?php echo site_url('entidad/plan_estrategico'); ?>">Plan de Desarrollo</a></li>
                            <li><a href="<?php echo site_url('entidad/planeacion_plan_gasto_publico'); ?>">Plan de Gasto Público</a></li>
                            <li><a href="<?php echo site_url('entidad/politicas'); ?>">Políticas / Lineamientos / Manuales</a></li>
                            <li><a href="<?php echo site_url('entidad/programas_proyectos'); ?>">Programas y Proyectos en Ejecución</a></li>
                        </ul>
                    </div>
                    <div class="poll poll-transparencia">
                        <i class="float-right fas fa-list-ol"></i>
                        <h3>12. Trámites y Servicios </h3>
                        <ul>
                            <li><a href="<?php echo site_url('entidad/index/formularios_tramites'); ?>">Formularios de Trámites</a></li>
                            <li><a href="<?php echo site_url('entidad/tramites'); ?>">Listado de Trámites y Servicios</a></li>
                            <li><a href="<?php echo site_url('entidad/tramites_en_linea'); ?>">Trámites en Línea</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
</main>






<!--==========================================================================
                                                        Start macanisoms and informations   area
==============================================r==============================-->

<!--==========================================================================
                                                        End macanisoms and informations   area
============================================================================-->






<div class="modal fade" id="buzon_contacto">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Buzón de Contacto</h4>
            </div>
            <div class="modal-body ">
                El buzón de contáctenos es un formulario para el ingreso de solicitudes de información para resolver sus dudas puntuales<br><br> Use el siguiente enlace para ingresar al formulario de registro<hr>
                <a class="btn btn-primary" href="<?php echo site_url('contacto'); ?>">Contáctenos</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="pqrs">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Solicitud y verificación de peticiones, quejas y reclamos</h4>
            </div>
            <div class="modal-body ">
                <p>De clic en el siguiente botón para ingresar al sistema de recepción de peticiones, quejas, reclamos, denuncias y sugerencias </p><hr>
                <a href="<?php echo $configuracion->config_general_url_pqrs; ?>" target="_blank" class="btn btn-primary">Clic Aquí</a>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="trato_digno">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header alert alert-info">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Derechos de los ciudadanos - Carta de trato digno</h4>
            </div>
            <div class="modal-body ">
                <p>De clic en el botón para ver el documento</p><hr>
                <a class="btn btn-primary" target="_blank" href="<?php echo site_url("uploads/entidad/$informacion_general->entidad_informacion_carta_trato_digno"); ?>">Ver Documento</a>
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


<script>
    $('#ayudas_navegacion2').click(function () {
        $('#ayudas_navegacion_sitio').modal();
    });
</script>