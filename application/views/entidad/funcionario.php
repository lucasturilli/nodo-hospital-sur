<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo"><?php echo $funcionario->entidad_funcionario_nombres . ' ' . $funcionario->entidad_funcionario_apellidos; ?></spam></h2>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped  ">
                        <tr>
                            <td >
                                <img class="img-thumbnail"  title="<?php echo $funcionario->entidad_funcionario_nombres . ' ' . $funcionario->entidad_funcionario_apellidos; ?>" alt="<?php echo $funcionario->entidad_funcionario_nombres . ' ' . $funcionario->entidad_funcionario_apellidos; ?>" src="<?php echo ($funcionario->entidad_funcionario_foto != NULL) ? site_url('uploads/entidad/funcionarios') . '/' . $funcionario->entidad_funcionario_foto : site_url('uploads/entidad/funcionarios') . '/no_user.png'; ?>"></td>
                            <td><h4 class="text-uppercase"><?php echo $funcionario->entidad_funcionario_nombres . ' ' . $funcionario->entidad_funcionario_apellidos; ?><br/><small><?php echo $funcionario->entidad_funcionario_cargo; ?></small></h4></td>
                        </tr>
                        <tr>
                            <th>Profesión</th>
                            <td><?php echo $funcionario->entidad_funcionario_profesion; ?></td>
                        </tr>
                        <tr>
                            <th>Perfil</th>
                            <td><?php echo $funcionario->entidad_funcionario_perfil; ?></td>
                        </tr>
                        <tr>
                            <th>Correo Electrónico</th>
                            <td><?php echo $funcionario->entidad_funcionario_email; ?></td>
                        </tr>
                        <tr>
                            <th>Teléfono</th>
                            <td><?php echo $funcionario->entidad_funcionario_telefono; ?></td>
                        </tr>
                        <tr>
                            <th>Tipo de Vinculación</th>
                            <td><?php echo ($funcionario->entidad_funcionario_tipo_vinculacion != NULL) ? $funcionario->entidad_funcionario_tipo_vinculacion : 'Información no disponible'; ?></td>
                        </tr>
                        <tr>
                            <th>Manual de Funciones</th>
                            <td><?php if ($funcionario->entidad_funcionario_manual_de_funciones != NULL) { ?><a target="_blank"  href="<?php echo site_url('uploads/entidad/funcionarios') . '/' . $funcionario->entidad_funcionario_manual_de_funciones; ?>"  class="btn btn-primary btn-sm"><i class="fas fa-file-<?php echo obtenerFielType($funcionario->entidad_funcionario_manual_de_funciones); ?>"> </i> Ver Documento</a>
                                    <?php
                                } else {
                                    echo 'Información no disponible';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Evaluación del Desempeño</th>
                            <td><?php if ($funcionario->entidad_funcionario_evaluacion != NULL) { ?><a target="_blank"  href="<?php echo site_url('uploads/entidad/funcionarios') . '/' . $funcionario->entidad_funcionario_evaluacion; ?>"  class="btn btn-primary btn-sm"><i class="fas fa-file-<?php echo obtenerFielType($funcionario->entidad_funcionario_evaluacion); ?>"> </i> Ver Documento</a>
                                    <?php
                                } else {
                                    echo 'Información no disponible';
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Dependencia en la que Labora</th>
                            <td ><?php if ($funcionario->id_dependencia != NULL) { ?><a href="<?php echo site_url('entidad/dependencia') . '/' . $funcionario->id_dependencia; ?>"  data-toggle="tooltip" data-placement="top" title="Ver información completa del la dependencia"><?php echo $funcionario->dependencia; ?></a>
                                    <?php
                                } else {
                                    echo 'Información no disponible';
                                }
                                ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>