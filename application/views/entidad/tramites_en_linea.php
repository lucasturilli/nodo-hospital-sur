<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Trámites y Servicios en Línea</spam></h2>
                </div>
                <p>Listado de trámites y servicios en línea clasificados por temática y área.</p>
                <?php
                if ($tramites_en_linea != FALSE) {
                    $x = 0;
                    ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($dependencias as $dep) {
                            $flag = FALSE;
                            foreach ($tramites_en_linea as $dato) {
                                if ($dato->id_entidad_dependencia === $dep->id_entidad_dependencia) {
                                    if ($flag === FALSE) {
                                        $flag = TRUE;
                                        ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading<?php echo $x; ?>">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $dep->id_entidad_dependencia; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $dep->id_entidad_dependencia; ?>">
                                                        <?php echo ($dato->dependencia != '') ? $dato->dependencia : 'Otros'; ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_<?php echo $dep->id_entidad_dependencia; ?>" class="panel-collapse collapse  <?php echo ($tab_list != NULL) ? ($dep->id_entidad_dependencia == $tab_list) ? 'show' : '' : ''; ?>" role="tabpanel" aria-labelledby="heading<?php echo $x; ?>" data-parent="#accordion">
                                                <div class="panel-body">
                                                    <table id = "" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                                        <thead>
                                                            <tr>
                                                                <th ><span data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                                <th ><span data-toggle="tooltip" data-placement="top" title="Clic para ordenar por descripción">Temática </span></th>
                                                                <th ><span>Descripción</span></th>
                                                                <th ><span>Enlace</span></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            foreach ($tramites_en_linea as $dato) {
                                                                if ($dato->id_entidad_dependencia === $dep->id_entidad_dependencia) {
                                                                    ?>
                                                                    <tr>
                                                                        <td ><?php echo $dato->servicios_tramites_en_linea_nombre; ?></td>
                                                                        <td ><?php echo $dato->servicios_tramites_en_linea_tematica; ?></td>
                                                                        <td >
                                                                            <button type="button" id="certificado_<?php echo $dato->id_servicios_tramites_en_linea; ?>" style="color:#fff;" class="btn btn-primary btn-xs tramite" >Descripción y Requisitos</button>
                                                                        </td>
                                                                        <td >
                                                                            <a href="<?php echo $dato->servicios_tramites_en_linea_enlace; ?>" target="_blank"  id="certificado_<?php echo $dato->id_servicios_tramites_en_linea; ?>"  class="btn btn-warning btn-xs" >Realizar Tramite</a>
                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                            ?> 
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                $x++;
                            }
                        }
                        ?>
                    </div>
                    <!-- Table -->
                </div>
                <?php
            } else {

                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
            }
            ?>
        </div>
    </div>
</main>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('.tramite').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(12)
        var url = "<?php echo site_url('entidad/tramites_en_linea_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>

