<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Informes de Rendición de Cuenta a los Ciudadanos</spam></h2>
                </div>
                <?php if ($informe_ciudadania != FALSE) { ?>
                    <ul class="nav nav-pills nav-fill" role="tablist" id="myTab">
                        <li class="nav-item" role="tab_1" ><a class="nav-link active" href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">Informes</a></li>
                        <li class="nav-item"role="tab_2" ><a class="nav-link " href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">Informes de Gestión</a></li>
                        <li class="nav-item"role="tab_3" ><a class="nav-link " href="#tab_3" aria-controls="tab_3" role="tab" data-toggle="tab">Cronograma</a></li>
                        <li class="nav-item" role="tab_4" ><a class="nav-link " href="#tab_4" aria-controls="tab_4" role="tab" data-toggle="tab">Evaluaciones</a></li>
                    </ul>
                <hr>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active " id="tab_1">
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th ><span>Archivo</span></th>
                                            <th ><span>Anexos</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($informe_ciudadania as $dato) {
                                            ?>
                                            <tr>
                                                <td ><?php echo $dato->control_informe_ciudadania_nombre; ?></td>
                                                <td ><?php echo $dato->control_informe_ciudadania_periodo; ?></td>
                                                <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informe_ciudadania_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informe_ciudadania_archivo); ?>"> </i> Ver Archivo</a></td>
                                                <td ><a id="programa_<?php echo $dato->id_control_informe_ciudadania; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_anexos" ><i class="fas fa-plus-square"> </i> Ver Anexos</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="tab_2">
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Informe">Informe</span></th>
                                            <th ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($informe_gestion != FALSE) {
                                            foreach ($informe_gestion as $dato) {
                                                ?>
                                                <tr>
                                                    <td ><?php echo $dato->control_informes_gestion_ciudadania_nombre; ?></td>
                                                    <td ><?php echo $dato->periodo; ?></td>
                                                    <td ><?php echo $dato->informe; ?></td>
                                                    <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informes_gestion_ciudadania_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informes_gestion_ciudadania_archivo); ?>"> </i> Ver Archivo</a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane " id="tab_3">
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th ><span>Archivo</span></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($informe_cronograma != FALSE) {
                                            foreach ($informe_cronograma as $dato) {
                                                ?>
                                                <tr>
                                                    <td ><?php echo $dato->control_informes_cronograma_nombre; ?></td>
                                                    <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informes_cronograma_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informes_cronograma_archivo); ?>"> </i> Ver Archivo</a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab_4">
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Informe</span></th>
                                            <th ><span>Archivo</span></th>
                                            <th ><span>Anexos</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($informe_evaluacion != FALSE) {
                                            foreach ($informe_evaluacion as $dato) {
                                                ?>
                                                <tr>
                                                    <td ><?php echo $dato->control_informes_ciudadania_evaluacion_nombre; ?></td>
                                                    <td ><?php echo $dato->periodo; ?></td>
                                                    <td ><?php echo $dato->informe; ?></td>
                                                    <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informes_ciudadania_evaluacion_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informes_ciudadania_evaluacion_archivo); ?>"> </i> Ver Archivo</a></td>
                                                    <td ><a id="evaluacion_<?php echo $dato->id_control_informes_ciudadania_evaluacion; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_anexos_evaluacion" ><i class="fas fa-plus-square"> </i> Ver Anexos</a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
    $('.ver_anexos').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/informe_ciudadania_anexos_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });

    $('.ver_anexos_evaluacion').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(11)
        var url = "<?php echo site_url('entidad/informe_ciudadania_evaluacion_anexos_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>

