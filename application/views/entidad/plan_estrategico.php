<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Plan de Desarrollo</spam></h2>
                </div>
                <?php if ($plan_estrategico_vigente != NULL) { ?>
                    <p>Plan de desarrollo vigente para el periodo <strong><?php echo $plan_estrategico_vigente->control_plan_estrategico_periodo . ' - ' . ($plan_estrategico_vigente->control_plan_estrategico_periodo + 3); ?></strong></p>
                    <hr>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Ver plan de desarrollo</th>
                            <td><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $plan_estrategico_vigente->control_plan_estrategico_archivo; ?>" class="btn btn-primary btn-sm "><i class="fas fa-file-<?php echo obtenerFielType($plan_estrategico_vigente->control_plan_estrategico_archivo); ?>"> </i> Ver Archivo</a></td>
                        </tr>
                        <tr>
                            <th>Ver los avances y anexos </th>
                            <td><a id="programa_<?php echo $plan_estrategico_vigente->id_control_plan_estrategico; ?>" href="#" onclick="return false;" class="btn btn-warning btn-sm ver_anexos" ><i class="fa fa-plus-square"> </i> Ver Anexos</a></td>
                        </tr>
                    </table>
                <?php } else { ?>
                    <div class=" alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible <br><br></div>
                <?php }
                ?>
                <hr>
                <h3>Histórico</h3>
                <hr>
                <?php if ($plan_estrategico != FALSE) { ?>
                    <div class="table-responsive">
                        <table id="myTable"  class="table table-striped table-hover table-bordered full_table">
                            <thead>
                                <tr>
                                    <th ><span data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                    <th ><span>Archivo</span></th>
                                    <th ><span>Anexos</span></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($plan_estrategico as $dato) {
                                    ?>
                                    <tr>
                                        <td   ><?php echo $dato->control_plan_estrategico_periodo . ' - ' . ($dato->control_plan_estrategico_periodo + 3); ?></td>
                                        <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_plan_estrategico_archivo; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato->control_plan_estrategico_archivo); ?>"> </i> Ver Archivo</a></td>
                                        <td ><a id="programa_<?php echo $dato->id_control_plan_estrategico; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_anexos" ><i class="fa fa-plus-square"> </i> Ver Anexos</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>

    $('.ver_anexos').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/plan_estrategico_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>