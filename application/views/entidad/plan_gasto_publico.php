<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Plan de Gasto Público</spam></h2>
                </div>
                <?php if ($planes != FALSE) { ?>
                    <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                        <?php
                        $x = 0;
                        foreach ($periodos as $perido) {
                            ?>
                            <li class="nav-item" role="tab_<?php echo $perido->planeacion_plan_gasto_publico_periodo; ?>" >
                                <a class="nav-link <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $perido->planeacion_plan_gasto_publico_periodo; ?>" aria-controls="tab_<?php echo $perido->planeacion_plan_gasto_publico_periodo; ?>" role="tab" data-toggle="tab"><?php echo $perido->planeacion_plan_gasto_publico_periodo; ?>
                                </a></li>
                            <?php
                            $x++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $x = 0;
                        foreach ($periodos as $perido) {
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo ($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $perido->planeacion_plan_gasto_publico_periodo; ?>">
                                <!-- Table -->
                                <div class = "table-responsive">
                                    <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                            <tr>
                                                <th ><span data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por nombre">Nombre</span></th>
                                                <th >Archivo</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($planes as $dato) {
                                                if ($dato->planeacion_plan_gasto_publico_periodo == $perido->planeacion_plan_gasto_publico_periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato->planeacion_plan_gasto_publico_nombre; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/planeacion') . '/' . $dato->planeacion_plan_gasto_publico_archivo; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato->planeacion_plan_gasto_publico_archivo); ?>"> </i> Ver Archivo</a></td>

                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $x++;
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>