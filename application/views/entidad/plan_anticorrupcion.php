<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Plan Anticorrupción</spam></h2>
                </div>
                <?php
                $x = 0;
                if ($anticorrupcion != FALSE) {
                    ?>
                    <ul class="nav nav-pills nav-fill" id="myTab"  role="tablist">
                        <?php foreach ($periodos as $perido) { ?>
                            <li class="nav-item" role="tab_<?php echo $perido->anticorrupcion_definicion_periodo; ?>" ><a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $perido->anticorrupcion_definicion_periodo; ?>" aria-controls="tab_<?php echo $perido->anticorrupcion_definicion_periodo; ?>" role="tab" data-toggle="tab"><?php echo $perido->anticorrupcion_definicion_periodo; ?></a></li>
                            <?php
                            $x++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <div class="tab-content">
                        <?php
                        $x = 0;
                        foreach ($periodos as $perido) {
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $perido->anticorrupcion_definicion_periodo; ?>">
                                <!-- Table -->
                                <div class = "table-responsive">
                                    <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                            <tr>
                                                <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por nombre">Nombre</span></th>
                                                <th ><span  data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por tipo de norma">Tipo Archivo</span></th>
                                                <th >Archivo</th>
                                                <th   ><span>Anexos</span></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($anticorrupcion as $dato) {
                                                if ($dato->anticorrupcion_definicion_periodo == $perido->anticorrupcion_definicion_periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato->anticorrupcion_definicion_nombre; ?></td>
                                                        <td ><?php echo $dato->anticorrupcion_definicion_tipo_nombre; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/anticorrupcion') . '/' . $dato->anticorrupcion_definicion_archivo; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato->anticorrupcion_definicion_archivo); ?>"> </i> Ver Archivo</a></td>
                                                        <td ><a id="programa_<?php echo $dato->id_anticorrupcion_definicion; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_anexos" ><i class="fa fa-plus-square"> </i> Ver Anexos</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $x++;
                        }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('.ver_anexos').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/plan_anticorrupcion_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>