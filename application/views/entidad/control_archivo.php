<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Gestión Documental</spam></h2>
                </div>
                <?php
                if ($archivos != FALSE) {
                    $x = 0;
                    $j = 0;
                    $xx = 0;
                    ?>
                    <div role="tabpanel">
                        <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                            <?php foreach ($tabs as $periodo) { ?>
                                <li class="nav-item" role="tab_<?php echo $periodo->id_control_informe_archivo_tab; ?>">
                                    <a  class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $periodo->id_control_informe_archivo_tab; ?>" aria-controls="tab_<?php echo $periodo->id_control_informe_archivo_tab; ?>" role="tab" data-toggle="tab"><?php echo $periodo->control_informe_archivo_tab_nombre; ?>
                                    </a></li>  
                                <?php
                                $x++;
                            }
                            ?>
                        </ul>
                        <hr>
                        <div class="tab-content">
                            <?php
                            foreach ($tabs as $periodo) {
                                $flag2 = FALSE;
                                ?>
                                <div role="tabpanel" class="tab-pane  <?php echo ($j == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo->id_control_informe_archivo_tab; ?>">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php foreach ($categorias as $cat) { ?>
                                            <?php
                                            $flag = FALSE;
                                            foreach ($archivos as $dato) {
                                                if ($dato->id_control_informe_archivo_categoria == $cat->id_control_informe_archivo_categoria) {
                                                    if ($dato->id_control_informe_archivo_categoria === $cat->id_control_informe_archivo_categoria) {
                                                        if ($dato->id_control_informe_archivo_tab == $periodo->id_control_informe_archivo_tab) {
                                                            if ($flag == FALSE) {
                                                                $flag = TRUE;
                                                                $flag2 = TRUE;
                                                                ?>
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="heading<?php echo $xx; ?>">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $xx; ?>" aria-expanded="true" aria-controls="collapse<?php echo $x; ?>">
                                                                                <?php echo $cat->control_informe_archivo_categoria_nombre; ?>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapse<?php echo $xx; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $xx; ?>" data-parent="#accordion">
                                                                        <div class="panel-body">
                                                                            <div class="table-responsive">
                                                                                <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                                                                    <thead>
                                                                                    <th  ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                                                    <th  ><span>Archivo</span></th>

                                                                                    </thead>
                                                                                    <tbody>

                                                                                        <?php
                                                                                        foreach ($archivos as $dato2) {
                                                                                            if ($dato2->id_control_informe_archivo_categoria === $cat->id_control_informe_archivo_categoria) {
                                                                                                if ($dato2->id_control_informe_archivo_tab == $periodo->id_control_informe_archivo_tab) {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                        <td ><?php echo $dato2->control_informe_archivo_nombre; ?></td>
                                                                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato2->control_informe_archivo_archivo; ?>" class="btn btn-primary btn-xs"><i class="fas fa-file-<?php echo obtenerFielType($dato2->control_informe_archivo_archivo); ?>"> </i> Ver Archivo</a></td>

                                                                                                    </tr>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        ?> 
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }

                                                $xx++;
                                            }
                                        }
                                        if ($flag2 == FALSE) {
                                            echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                                        }
                                        ?>
                                    </div>
                                    <?php ?>
                                </div>
                                <?php
                                $j++;
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>


