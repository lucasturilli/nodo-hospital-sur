<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Documentos de la Entidad</spam></h2>
                </div>
                <p>Documentos sobre y correspondientes a la entidad y sus funciones misionales en diferentes áreas y temas.</p>
                </hr>
                <?php if ($documentos != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Periodo</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por clasificación ">Clasificación</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por descripción">Descripción</span></th>
                                    <th ><span>Archivo</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $x = 0;
                                foreach ($documentos as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->entidad_documentos_nombre; ?></td>
                                        <td ><?php echo $dato->tipo; ?></td>
                                        <td ><?php echo $dato->entidad_documentos_descripcion; ?></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/documentos') . '/' . $dato->entidad_documentos_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->entidad_documentos_archivo); ?>"> </i> Ver Archivo</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>

                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>