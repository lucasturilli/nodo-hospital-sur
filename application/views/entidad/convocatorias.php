<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-12 col-lg-12">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Convocatorias</spam></h2>
                </div>
                <?php if ($convocatorias != FALSE) { ?>
                    <div class = "table-responsive">
                        <table  class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Titulo</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha de inicio ">Fecha de Inicio</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha de finalización">Fecha de Finalización</span></th>
                                    <th >Descripción</th>
                                    <th >Anexos</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                foreach ($convocatorias as $dato) {
                                    ?>
                                    <tr>
                                        <td ><?php echo $dato->convocatorias_nombre; ?></td>
                                        <td ><?php echo date('d', strtotime($dato->convocatorias_fecha_inicio)) . " de " . $meses[date('n', strtotime($dato->convocatorias_fecha_inicio)) - 1] . " del " . date('Y', strtotime($dato->convocatorias_fecha_inicio)); ?></td>
                                        <td ><?php echo date('d', strtotime($dato->convocatorias_fecha_fin)) . " de " . $meses[date('n', strtotime($dato->convocatorias_fecha_fin)) - 1] . " del " . date('Y', strtotime($dato->convocatorias_fecha_fin)); ?></td>
                                        <td ><a id="norma_<?php echo $dato->id_convocatorias; ?>" href="#" onclick="return false;" class="btn btn-primary btn-xs ver_descripcion"> Ver Descripción</a></td>
                                        <td ><a id="amron_<?php echo $dato->id_convocatorias; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_anexos" ><i class="fas fa-plus-square"> </i> Ver Anexos</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 

                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {

                    echo '<div class = "alert alert-info" role = "alert"><i class = "fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('.ver_descripcion').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(6)
        var url = "<?php echo site_url('entidad/convocatorias_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });

    });
    $('.ver_anexos').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(6)
        var url = "<?php echo site_url('entidad/convocatorias_anexos_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>