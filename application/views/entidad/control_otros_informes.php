<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Otros Informes</spam></h2>
                </div>
                <?php if ($informes != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por numero de proceso">Nombre</span></th>
                                    <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por Periodo">Periodo</span></th>
                                    <th class = "resize">Archivo</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $x = 0;
                                foreach ($informes as $dato) {
                                    ?>
                                    <tr>
                                        <td class="resize"><?php echo $dato->control_otros_informes_nombre; ?></td>
                                        <td class="resize"><?php echo $dato->control_otros_informes_periodo; ?></td>
                                        <td class="resize"><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_otros_informes_archivo; ?>" class="btn btn-primary btn-xs"><i class="fa fa-file-<?php echo obtenerFielType($dato->control_otros_informes_archivo); ?>-o"> </i> Ver Archivo</a></td>
                                    </tr>
                                    <?php
                                }
                                ?> 

                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>