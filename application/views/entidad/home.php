<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--   ========================== Main Content Area ===========================-->
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo"><?php echo $pagina->entidad_paginas_titulo; ?></spam></h2>
                </div>
                <?php
                switch ($tab) {
                    case 1:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Misión y Visión");
                        </script>
                        <h3 class="">Misión</h3>
                        <hr>
                        <?php echo $entidad_informacion->entidad_informacion_mision; ?>
                        <h3 class="">Visión</h3>
                        <hr>
                        <?php echo $entidad_informacion->entidad_informacion_vision; ?>
                        <?php
                        break;
                    case 2:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Objetivos y Funciones");
                        </script>
                        <h3 class="">Funciones</h3>
                        <hr>
                        <?php echo $entidad_informacion->entidad_informacion_funciones; ?>
                        <h3 class="">Objetivos</h3>
                        <hr>
                        <?php echo $entidad_informacion->entidad_informacion_objetivos; ?>
                        <?php
                        break;
                    case 3:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Organigrama");
                        </script>
                        <img class="img img-responsive" src="<?php echo site_url('uploads/entidad') . '/' . $entidad_informacion->entidad_informacion_organigrama; ?>" alt="Organigrama" />
                        <hr>
                        <?php if ($entidad_informacion->entidad_informacion_organigrama_acto != NULL) { ?>
                            <a type="button" target="_blank" href="<?php echo prep_url($entidad_informacion->entidad_informacion_organigrama_acto) ?>" class="btn button-u">Ver Acto Administrativo</a>
                        <?php } ?>
                        <a type="button" target="_blank" href="<?php echo site_url('uploads/entidad') . '/' . $entidad_informacion->entidad_informacion_organigrama; ?>" class="btn button-u">Descargar Imagen</a>
                        <hr>
                        <?php
                        break;
                    case 121:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Funcionarios");
                        </script>
                        <?php if ($funcionarios != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre de funcionario">Nombre</span></th>
                                            <th><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por cargo">Cargo</span></th>
                                            <th><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por dependencia">Dependencia</span></th>
                                            <th><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por correo electrónico">Correo Electrónico</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($funcionarios as $dato) {
                                            ?>
                                            <tr>
                                                <td><a href="<?php echo site_url('entidad/funcionario') . '/' . $dato->id_entidad_funcionario; ?>#menu"   data-toggle="tooltip" data-placement="top" title="Ver información completa del funcionario"><?php echo $dato->entidad_funcionario_nombres . ' ' . $dato->entidad_funcionario_apellidos; ?></a></td>
                                                <td><?php echo $dato->entidad_funcionario_cargo; ?></td>
                                                <td><a href="<?php echo site_url('entidad/dependencia') . '/' . $dato->id_entidad_dependencia; ?>#menu"   data-toggle="tooltip" data-placement="top" title="Ver información de la dependencia"><?php echo $dato->dependencia; ?></a></td>                                                                          
                                                <td><?php echo $dato->entidad_funcionario_email; ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 5:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Dependencias");
                        </script>
                        <?php if ($dependencias != FALSE) { ?>
                            <div class = "table-responsive">
                                <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                    <thead>
                                        <tr>
                                            <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por nombre de Dependencia">Dependencia</span></th>
                                            <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por responsable">Responsable</span></th>
                                            <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por dependencia">Teléfono</span></th>
                                            <th class = "resize"><span class = "jtooltip" data-toggle = "tooltip" data-placement = "top" title = "Clic para ordenar por teléfono">Correo Electrónico</span></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($dependencias as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><a href="<?php echo site_url('entidad') . '/dependencia/' . $dato->id_entidad_dependencia; ?>#menu"   data-toggle="tooltip" data-placement="top" title="Ver información de la dependencia"><?php echo $dato->entidad_dependencia_nombre; ?></a></td>
                                                <td   ><?php if ($dato->id_funcionario != NULL) { ?><a href="<?php echo site_url('entidad/funcionario') . '/' . $dato->id_funcionario; ?>#menu"   data-toggle="tooltip" data-placement="top" title="Ver información completa del funcionario"><?php echo $dato->nombres . ' ' . $dato->apellidos; ?></a>
                                                        <?php
                                                    } else {
                                                        echo 'Información no disponible';
                                                    }
                                                    ?></td>
                                                <td   ><?php echo $dato->entidad_dependencia_telefono; ?></td>
                                                <td   ><?php echo $dato->entidad_dependencia_email; ?></td>
                                            </tr>
                                        <?php }
                                        ?> 
                                    </tbody>
                                </table>
                            </div>
                        <?php } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 122:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Listado de Personal");
                        </script>
                        <p>Listado de todo el personal asociado a la entidad<br /><br />
                            <?php if ($planta->entidad_planta_personal_archivo != NULL) { ?>
                                <a href="<?php echo $planta->entidad_planta_personal_archivo; ?>" target="_blank" type="button" class="btn button-u" >Ver Listado</a>
                                <?php
                            } else {
                                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                            }
                            ?>
                        <p>Ingrese al siguiente sitio web (<a target="_blank" href="http://www.sigep.gov.co/directorio">http://www.sigep.gov.co/directorio</a>) o utilice la siguiente ventana para encontrar mayor información de los funcionarios.  Ingrese en el campo de texto ubicado en la parte inferior derecha el nombre, ciudad o entidad y luego de clic sobre el botón “Buscar”.</p>
                        <?php
                        break;
                    case 124:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Asignaciones Salariales");
                        </script>
                        <h3>Escala Salarial</h3>
                        <p>Escala salarial correspondiente a las categorías de todos los servidores que trabajan en la entidad, independientemente de su calidad de empleados, asesores, consultores o cualquier otra modalidad de contrato<br /><br />
                            <?php if ($asignaciones_salariales->entidad_asignaciones_salarial_escala_salarial != NULL) { ?><a target="_blank" href="<?php echo site_url('uploads/entidad/asignacion_salarial') . '/' . $asignaciones_salariales->entidad_asignaciones_salarial_escala_salarial; ?>"  class="btn button-u"><i class="fas fa-file-<?php echo obtenerFielType($asignaciones_salariales->entidad_asignaciones_salarial_escala_salarial); ?>"></i> Ver Documento</a></p>
                                <?php
                            } else {
                                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                            }
                            ?>
                        <hr>
                        <h3>Escala de Viáticos</h3>
                        <p>Escala de viáticos correspondiente a todos los servidores que trabajan en la entidad, independientemente de su calidad de empleados, asesores, consultores o cualquier otra modalidad de contrato<br /><br />
                            <?php if ($asignaciones_salariales->entidad_asignaciones_salarial_escala_viaticos != NULL) { ?><a target="_blank" href="<?php echo site_url('uploads/entidad/asignacion_salarial') . '/' . $asignaciones_salariales->entidad_asignaciones_salarial_escala_viaticos; ?>"  class="btn button-u"><i class="fas fa-file-<?php echo obtenerFielType($asignaciones_salariales->entidad_asignaciones_salarial_escala_viaticos); ?>"> </i> Ver Documento</a></p>
                                <?php
                            } else {
                                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                            }
                            ?>
                            <?php
                            break;
                        case 125:
                            ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Evaluación de Desempeño");
                        </script>
                        <p> Evaluación de Desempeño  del personal de la  entidad<br /><br />
                            <?php if ($evaluacion_desempeno->entidad_evaluacion_desempeno_archivo != NULL) { ?><a target="_blank" href="<?php echo site_url('uploads/entidad/funcionarios') . '/' . $evaluacion_desempeno->entidad_evaluacion_desempeno_archivo; ?>"  class="btn button-u "><i class="fas fa-file-<?php echo obtenerFielType($evaluacion_desempeno->entidad_evaluacion_desempeno_archivo); ?>"></i> Ver Documento</a></p>
                                <?php
                            } else {
                                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
                            }
                            ?>
                            <?php
                            break;

                        case 6066:
                            ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Informes al Concejo");
                        </script>
                        <p>Informes enviados al Honorable Concejo por parte de entidad. </p>
                        <hr>
                        <?php if ($informe_concejo != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table ">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th   ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($informe_concejo as $dato) {
                                            ?>
                                            <tr>
                                                <td><?php echo $dato->control_informe_concejo_nombre; ?></td>
                                                <td><?php echo $dato->control_informe_concejo_periodo; ?></td>
                                                <td><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informe_concejo_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informe_concejo_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 6069:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Informes de Rendición de Cuenta Fiscal");
                        </script>
                        <?php if ($informe_fiscal != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table ">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th   ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($informe_fiscal as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->control_informe_fiscal_nombre; ?></td>
                                                <td   ><?php echo $dato->control_informe_fiscal_periodo; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_informe_fiscal_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_informe_fiscal_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 69:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Metas e Indicadores de Gestión");
                        </script>
                        <p>Metas, indicadores de gestión y/o desempeño, de acuerdo a la planeación estratégica de la entidad. </p>
                        <hr>
                        <?php if ($indicadores_gestion != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodo">Periodo</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodicidad">Periodicidad</span></th>
                                            <th   ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($indicadores_gestion as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->control_indicadores_gestion_nombre; ?></td>
                                                <td   ><?php echo $dato->control_indicadores_gestion_periodo; ?></td>
                                                <td   ><?php echo $dato->control_indicadores_gestion_trimestre; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_indicadores_gestion_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_indicadores_gestion_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 71:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Otros Planes");
                        </script>
                        <p>Otros planes relacionados con temas anticorrupción, rendición de cuentas, servicio al ciudadano, antitrámites y los demás que solicite la normatividad vigente.</p>
                        <hr>
                        <?php if ($planes_otros != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por tipo">Tipo</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha">Fecha</span></th>
                                            <th   ><span>Archivo</span></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($planes_otros as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->control_planes_otros_nombre; ?></td>
                                                <td   ><?php echo $dato->control_planes_otros_tipo; ?></td>
                                                <td   ><?php echo $dato->control_planes_otros_fecha; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_planes_otros_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_planes_otros_archivo); ?>"> </i> Ver Archivo</a></td>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 6074:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Población Vulnerable");
                        </script>
                        <?php if ($poblacion_vulnerable->control_poblacion_vulnerable_descripcion != NULL) { ?>
                            <?php echo $poblacion_vulnerable->control_poblacion_vulnerable_descripcion; ?>
                        <?php } else { ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                            <?php
                        }
                        ?>     
                        <h3>Normas</h3>
                        <hr>
                        <?php if ($poblacion_vulnerable->control_poblacion_vulnerable_normas != NULL) { ?>
                            <?php echo $poblacion_vulnerable->control_poblacion_vulnerable_normas; ?>
                        <?php } else { ?>
                            <div class=" alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                        <?php }
                        ?>
                        <h3>Políticas</h3>
                        <hr>
                        <?php if ($poblacion_vulnerable->control_poblacion_vulnerable_politicas != NULL) { ?>
                            <?php echo $poblacion_vulnerable->control_poblacion_vulnerable_politicas; ?>
                        <?php } else { ?>
                            <div class=" alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                        <?php }
                        ?>
                        <h3>Programas</h3>
                        <hr>
                        <p>En el siguiente enlace podrá encontrar toda la información referente a los programas para la población vulnerable</p>
                        <a style=" margin-bottom: 20px;" href="<?php echo site_url('entidad/programas_poblacion_vulnerable') ?>" class="btn button-u">Ver programas</a>
                        <h3 class="internas_contenido_texto_titulo">Proyectos</h3>
                        <hr>
                        <p>En el siguiente enlace podrá encontrar toda la información referente a los proyectos para la población vulnerable</p>
                        <a href="<?php echo site_url('entidad/proyectos_poblacion_vulnerable') ?>" class="btn button-u">Ver proyectos</a>
                        <br><br>
                        <?php
                        break;
                    case 6077:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Programas Sociales");
                        </script>
                        <p>Listado de los  beneficiarios de los programas sociales.</p>
                        <hr>
                        <?php if ($programas_sociales != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table">
                                    <thead>
                                    <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por programa">Programa</span></th>
                                    <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por descripción">Descripción </span></th>
                                    <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha">Fecha </span></th>
                                    <th   ><span>Archivo</span></th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($programas_sociales as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->control_programas_sociales_programa; ?></td>
                                                <td   ><?php echo $dato->control_programas_sociales_descripcion; ?></td>
                                                <td   ><?php echo $dato->control_programas_sociales_fecha; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_programas_sociales_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_programas_sociales_archivo); ?>"> </i> Ver Archivo</a></td>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 6078:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Defensa Judicial");
                        </script>
                        <p>Demandas contra la entidad que incluye, número de demandas, estado en que se encuentra, pretensión o cuantía de la demanda, riesgo de pérdida.</p>
                        <hr>
                        <?php if ($defensa_judicial != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table ">
                                    <thead>
                                    <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por periodo">Periodo</span></th>
                                    <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por periodicidad">Periodicidad </span></th>
                                    <th   ><span>Archivo</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($defensa_judicial as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->control_defensa_judicial_periodo; ?></td>
                                                <td   ><?php echo $dato->control_defensa_judicial_trimestre; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_defensa_judicial_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_defensa_judicial_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 92:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Información SECOP");
                        </script>
                        <?php if ($secop->contratacion_contratos_secop_descripcion != NULL) { ?>
                            <?php echo $secop->contratacion_contratos_secop_descripcion; ?>
                            <hr>
                            <a target="_blank" href="<?php echo $secop->contratacion_contratos_secop_enlace; ?>" class="btn button-u "> Visitar Sitio</i></a>
                        <?php } else { ?>
                            <div class="  alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                            <?php
                        }
                        break;
                    case 103:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Inventario de Trámites y Servicios");
                        </script>
                        <?php if ($inventario->servicios_inventario_archivo != NULL) { ?>
                            <p>Inventario que contiene un listado de todos los trámites y servicios prestados por la entidad.</p>
                            <hr>
                            <a target="_blank" href="<?php echo site_url('uploads/entidad/servicios') . '/' . $inventario->servicios_inventario_archivo; ?>" class="btn button-u "><i class="fas fa-file-<?php echo obtenerFielType($inventario->servicios_inventario_archivo); ?>"> </i> Ver Archivo</a>
                        <?php } else { ?>
                            <div class="  alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                            <?php
                        }
                        break;
                    case 105:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Formularios de Trámites y Servicios");
                        </script>
                        <p>Formatos de descarga necesarios para la realización de trámites y servicios con la entidad.</p>
                        <hr>
                        <?php if ($formularios != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table ">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por descripción">Descripción </span></th>
                                            <th   ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($formularios as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->servicios_formatos_nombre; ?></td>
                                                <td   ><?php echo $dato->servicios_formatos_descripcion; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/servicios') . '/' . $dato->servicios_formatos_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->servicios_formatos_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 6079:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Informe de peticiones, quejas y reclamos");
                        </script>
                        <p>Informe de todas las peticiones, quejas, reclamos, denuncias y sugerencias recibidas y los tiempos de respuesta relacionados, junto con un análisis resumido de este mismo tema.</p>
                        <hr>
                        <?php if ($pqrdf != FALSE) { ?>
                            <div class="table-responsive">
                                <table id="myTable"  class="table table-striped table-hover table-bordered full_table ">
                                    <thead>
                                        <tr>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por periodo">Periodo</span></th>
                                            <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por periodicidad">Periodicidad</span></th>
                                            <th   ><span>Archivo</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($pqrdf as $dato) {
                                            ?>
                                            <tr>
                                                <td   ><?php echo $dato->servicios_pqrdf_periodo; ?></td>
                                                <td   ><?php echo $dato->servicios_pqrdf_semestre; ?></td>
                                                <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/servicios') . '/' . $dato->servicios_pqrdf_archivo; ?>" class="btn button-u btn-sm" ><i class="fas fa-file-<?php echo obtenerFielType($dato->servicios_pqrdf_archivo); ?>"> </i> Ver Archivo</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                        <?php } ?>
                        <?php
                        break;
                    case 131:
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Calidad");
                        </script>
                        <?php echo $calidad->entidad_calidad_descripcion; ?>
                        <?php
                        break;
                    default :
                        ?>
                        <script>
                            $("#titulo").empty();
                            $("#titulo").append("Presentación");
                        </script>
                        <?php if ($entidad_informacion->entidad_informacion_presentacion != NULL) { ?>
                            <?php echo $entidad_informacion->entidad_informacion_presentacion; ?>
                        <?php } else { ?>
                            <div class="  alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>
                            <?php
                        }
                        break;
                }
                ?>

            </div>
        </div>
    </div>
</main>

















