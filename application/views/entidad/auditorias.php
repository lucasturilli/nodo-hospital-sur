<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Auditorías</spam></h2>
                </div>
                <?php if ($auditorias != FALSE) { ?>
                    <ul class="nav nav-pills nav-fill" role="tablist" id="myTab">
                        <?php
                        $x = 0;
                        foreach ($auditorias_periodos as $periodo_auditoria) {
                            ?>
                            <li class="nav-item" role="tab_<?php echo $periodo_auditoria->control_auditorias_periodo; ?>" >
                                <a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $periodo_auditoria->control_auditorias_periodo; ?>" aria-controls="tab_<?php echo $periodo_auditoria->control_auditorias_periodo; ?>" role="tab" data-toggle="tab"><?php echo $periodo_auditoria->control_auditorias_periodo; ?>
                                </a></li>
                            <?php
                            $x++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <div class="tab-content">
                        <?php
                        $x = 0;
                        foreach ($auditorias_periodos as $periodo_auditoria) {
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo ($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo_auditoria->control_auditorias_periodo; ?>">
                                <div class="table-responsive">
                                    <table id="myTable"  class="table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                            <tr>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por ente">Ente de Control</span></th>
                                                <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha">Fecha de Auditoria</span></th>
                                                <th ><span>Periodo</span></th>
                                                <th ><span>Archivo</span></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($auditorias as $dato) {
                                                if ($dato->control_auditorias_periodo == $periodo_auditoria->control_auditorias_periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato->control_auditorias_nombre; ?></td>
                                                        <td ><?php echo $dato->ente; ?></td>
                                                        <td ><?php echo $dato->control_auditorias_fecha_auditoria; ?></td>
                                                        <td ><?php echo $dato->control_auditorias_periodo; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_auditorias_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_auditorias_archivo); ?>"> </i> Ver Archivo</a></td>

                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $x++;
                        }
                        ?>
                        <?php if ($informe_contraloria != FALSE) { ?>
                            <p><a  href="<?php echo prep_url($informe_contraloria->control_informe_contraloria_enlace); ?>" target="_blank" class="btn btn-secondary">Enlace al sitio aquí</a></p>
                            <?php echo $informe_contraloria->control_informe_contraloria_embedded; ?>

                        <?php }
                        ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> No hay información  disponible</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>