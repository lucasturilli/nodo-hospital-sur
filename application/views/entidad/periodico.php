<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Periódicos</spam></h2>
                </div>
                <?php
                if ($periodicos != FALSE) {
                    $x = 0;
                    $j = 0;
                    ?>
                    <div class="row">
                        <div class="col-md-12 column">
                            <ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
                                <?php
                                $x = 0;
                                foreach ($periodos as $periodo) {
                                    ?>
                                    <li  class="nav-item" role="tab_<?php echo $periodo->periodo; ?>"><a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>"   href="#tab_<?php echo $periodo->periodo; ?>" aria-controls="tab_<?php echo $periodo->periodo; ?>" role="tab" data-toggle="tab"><?php echo $periodo->periodo; ?></a></li>  
                                    <?php
                                    $x++;
                                }
                                ?>
                            </ul>
                            <hr>
                            <div class="tab-content">
                                <?php foreach ($periodos as $periodo) { ?>
                                    <div role="tabpanel" class="tab-pane    <?php echo ($j == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo->periodo; ?>">
                                        <div class = "table-responsive">
                                            <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                                <thead>
                                                <th style="cursor:pointer;" ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                <th style="cursor:pointer;" ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha de publicación ">Fecha de Publicación </span></th>
                                                <th ><span>Periódico</span></th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($periodicos as $dato2) {
                                                        if (date('Y', strtotime($dato2->entidad_periodico_fecha)) == $periodo->periodo) {
                                                            ?>
                                                            <tr>
                                                                <td ><?php echo $dato2->entidad_periodico_nombre; ?></td>
                                                                <td ><?php echo $dato2->entidad_periodico_fecha; ?></td>
                                                                <td ><a target="_blank" href="<?php echo site_url('uploads/periodico') . '/' . $dato2->entidad_periodico_archivo; ?>"><img width="150" class="img-responsive img-thumbnail" src="<?php echo site_url('uploads/periodico') . '/' . $dato2->entidad_periodico_portada; ?>" /></a></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php
                                    $j++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>

        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>

