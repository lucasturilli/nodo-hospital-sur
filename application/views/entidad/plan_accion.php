<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Plan de Acción</spam></h2>
                </div>
                <p>Plan de acción por dependencias, en el cual se especifican los objetivos, las estrategias, los proyectos, las metas, los responsables, los planes generales de compras y la distribución presupuestal de los  proyectos de inversión junto a los indicadores de gestión, de acuerdo con lo establecido en el artículo 74 de la Ley 1474 de 2011 (Estatuto Anticorrupción).</p>
                <?php
                if ($plan_accion != FALSE) {
                    $x = 0;
                    ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($dependencias as $dep) {
                            $flag = FALSE;
                            foreach ($plan_accion as $dato) {
                                if ($dato->id_entidad_dependencia === $dep->id_entidad_dependencia) {
                                    if ($flag === FALSE) {
                                        $flag = TRUE;
                                        ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab"  id="heading<?php echo $x; ?>">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $dep->id_entidad_dependencia; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $dep->id_entidad_dependencia; ?>">
                                                        <?php echo $dato->dependencia; ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_<?php echo $dep->id_entidad_dependencia; ?>" class=" collapse"  aria-labelledby="heading<?php echo $x; ?>" data-parent="#accordion">
                                                <div class="panel-body">
                                                    <div class=" table-responsive">
                                                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                                            <thead>
                                                                <tr>
                                                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por dependencia">Dependencia</span></th>
                                                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por periodo">Periodo</span></th>
                                                                    <th ><span>Archivo</span></th>
                                                                    <th ><span>Avances del Plan</span></th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                foreach ($plan_accion as $dato) {
                                                                    if ($dato->id_entidad_dependencia === $dep->id_entidad_dependencia) {
                                                                        ?>
                                                                        <tr>
                                                                            <td ><?php echo $dato->control_plan_accion_nombre; ?></td>
                                                                            <td ><?php echo $dato->dependencia; ?></td>
                                                                            <td ><?php echo $dato->control_plan_accion_periodo; ?></td>
                                                                            <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_plan_accion_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_plan_accion_archivo); ?>"> </i> Ver Archivo</a></td>
                                                                            <td ><a id="programa_<?php echo $dato->id_control_plan_accion; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_avances" ><i class="fas fa-plus-square"> </i> Ver Avances</a></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?> 
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                $x++;
                            }
                        }
                        ?>
                    </div>
                    <!-- Table -->
                </div>
                <?php
            } else {

                echo '<div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> Información no disponible</div>';
            }
            ?>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
<?php if ($tab_list != NULL) { ?>
        $(function () {
            $('#collapse_' +<?php echo $tab_list; ?>).collapse("show");
        });
<?php } ?>
    $('.ver_avances').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/plan_accion_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>