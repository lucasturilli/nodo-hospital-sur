<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Preguntas y Respuestas Frecuentes</spam></h2>
                </div>
                <?php
                if ($preguntas_frecuentes != FALSE) {
                    $x = 1;
                    ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <?php
                        foreach ($dependencias as $dependencia) {
                            ?>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading_<?php echo $dependencia->id_dependencia; ?>">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $dependencia->id_dependencia; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $dependencia->id_dependencia; ?>">
                                            <?php echo ($dependencia->dependencia == NULL) ? 'Temas comunes' : $dependencia->dependencia; ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse_<?php echo $dependencia->id_dependencia; ?>" class="panel-collapse collapse <?php echo ($tab_list != NULL ? ($tab_list == $dependencia->id_dependencia) ? 'in' : '' : ''); ?>" aria-labelledby="heading_<?php echo $dependencia->id_dependencia; ?>" data-parent="#accordion" role="tabpanel">
                                    <div class="panel-body">
                                        <div class = "table-responsive">
                                            <table id = "myTable_<?php echo $dependencia->id_dependencia; ?>" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                                <thead>
                                                <th>Pregunta</th>
                                                <th>Respuesta</th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($preguntas_frecuentes as $dato) {
                                                        if ($dato->id_dependencia == $dependencia->id_dependencia) {
                                                            ?>
                                                            <tr>
                                                                <th><?php echo $dato->entidad_preguntas_frecuentes_pregunta; ?></th>
                                                                <td class=""><?php echo $dato->entidad_preguntas_frecuentes_respuesta; ?></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>

                            <?php
                            $x++;
                        }
                        ?>
                    </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
