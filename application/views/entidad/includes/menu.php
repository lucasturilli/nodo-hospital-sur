<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="accourdion-area">
    <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">
        <div class="submenu_interno ">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed " data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-info-circle"></i> Información General</p>
                </a>
            </div>
            <div id="collapseTwo1" class="collapse <?php echo ($tab > 0 && $tab < 20) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 1) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/mision_vision') ?>" class=" ">Misión y Visión</a></li>
                        <li class="<?php echo ($tab == 2) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/objetivos_funciones') ?>" class=" ">Objetivos y Funciones</a></li>
                        <li class="<?php echo ($tab == 3) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/organigrama') ?>" class=" ">Organigrama</a></li>
                        <li class="<?php echo ($tab == 5) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/dependencias') ?>" class=" ">Dependencias</a></li>
                        <li class="<?php echo ($tab == 8) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/directorio_sucursales') ?>" class=" ">Directorio de Sucursales</a></li>
                        <li class="<?php echo ($tab == 9) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/directorio_entidades') ?>" class=" ">Directorio Entidades</a></li>
                        <li class="<?php echo ($tab == 10) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/directorio_agremiaciones') ?>" class=" ">Directorio de agremiaciones, asociaciones y otros grupos de interés</a></li>

                    </ul>
                </div>
            </div>
        </div>	
        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo2"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-podcast"></i> Servicios de Información</p>
                </a>
            </div>
            <div id="collapseTwo2" class="collapse <?php echo ($tab > 20 && $tab < 40) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 21) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/preguntas_frecuentes') ?>" class=" ">Preguntas y Respuestas Frecuentes</a></li>
                        <li class="<?php echo ($tab == 22) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/glosario') ?>" class=" ">Glosario</a></li>
                        <li class="<?php echo ($tab == 23) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('sitio/noticias') ?>" class=" ">Noticias</a></li>
                        <li class="<?php echo ($tab == 29) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/boletines') ?>" class=" ">Boletines</a></li>
        <!--                <li class="<?php // echo ($tab == 30) ? 'accourdion-area_active' : '';              ?>"><a href="<?php // echo site_url('entidad/periodico')              ?>" class=" ">Periódico</a></li>-->
                        <li class="<?php echo ($tab == 25) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('sitio/calendario') . '/' . date('Y') . '/' . date('m') ?>" class=" ">Calendario de Eventos</a></li>
                        <li class="<?php echo ($tab == 26) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('sitio/encuestas') ?>" class=" ">Encuestas</a></li>
                        <li class="<?php echo ($tab == 27) ? 'accourdion-area_active' : ''; ?>"><a target="_blank" href="<?php echo $configuracion->config_general_portal_ninos; ?>" class=" ">Portal para Niños y Niñas</a></li>
                        <li class="<?php echo ($tab == 28) ? 'accourdion-area_active' : ''; ?>"><a target="<?php echo $empleo_link->entidad_ofertas_empleo_target; ?>" href="<?php echo prep_url($empleo_link->entidad_ofertas_empleo_url); ?>" >Ofertas de Empleo</a></li>
                    </ul>
                </div>
            </div>
        </div>	

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo3"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-users"></i> Talento Humano</p>
                </a>
            </div>
            <div id="collapseTwo3" class="collapse <?php echo ($tab > 120 && $tab < 130) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <ul class="colapse_list list-unstyled">
                            <li class="<?php echo ($tab == 121) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/funcionarios') ?>" class=" ">Funcionarios Principales</a></li>
                            <li class="<?php echo ($tab == 122) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/listado_planta') ?>" class=" ">Listado de Personal</a></li>
                            <li class="<?php echo ($tab == 123) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/manual_funciones') ?>" class=" ">Manual de Funciones</a></li>
                            <li class="<?php echo ($tab == 125) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/evaluacion_desempeno') ?>" class=" ">Evaluación de Desempeño</a></li>
                            <li class="<?php echo ($tab == 124) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/asignaciones_salariales') ?>" class=" ">Asignaciones Salariales</a></li> 
                        </ul>
                    </ul>
                </div>
            </div>
        </div>

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo4"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-file"></i> Normatividad</p>
                </a>
            </div>
            <div id="collapseTwo4" class="collapse <?php echo ($tab > 40 && $tab < 50) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 41) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/leyes') ?>" class=" ">Leyes / Ordenanzas / Acuerdos</a></li>
                        <li class="<?php echo ($tab == 42) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/decretos') ?>" class=" ">Decretos</a></li>
                        <li class="<?php echo ($tab == 44) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/circulares') ?>" class=" ">Circulares y/u otros actos administrativos de carácter general</a></li>
                        <li class="<?php echo ($tab == 43) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/resoluciones') ?>" class=" ">Resoluciones</a></li>
                        <li class="<?php echo ($tab == 45) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/edictos') ?>" class=" ">Edictos y Avisos</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo5"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-clipboard"></i> Presupuesto</p>
                </a>
            </div>
            <div id="collapseTwo5" class="collapse <?php echo ($tab > 50 && $tab < 60) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 51) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/presupuesto') ?>" class=" ">Presupuesto Aprobado en Ejercicio</a></li>
                        <li class="<?php echo ($tab == 53) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/presupuesto_historico') ?>" class=" ">Información Histórica de Presupuestos</a></li>
                        <li class="<?php echo ($tab == 54) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/estados_financieros') ?>" class=" ">Estados Financieros</a></li>
        <!--                <li class="<?php //echo ($tab == 55) ? 'accourdion-area_active' : '';              ?>"><a href="<?php //echo site_url('entidad/presupuesto_estatuto_tributario')              ?>" class=" ">Estatuto Tributario</a></li>-->
                    </ul>
                </div>
            </div>
        </div>

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo6"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-signal"></i> Planeación</p>
                </a>
            </div>
            <div id="collapseTwo6" class="collapse <?php echo ($tab > 60 && $tab < 90) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 89) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/politicas') ?>" class=" ">Políticas / Lineamientos / Manuales</a></li>
                        <li class="<?php echo ($tab == 65) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/plan_anticorrupcion') ?>" class=" ">Plan Anticorrupción</a></li>
                        <li class="<?php echo ($tab == 62) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/plan_estrategico') ?>" class=" ">Plan de Desarrollo</a></li>
                        <li class="<?php echo ($tab == 63) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/plan_accion') ?>" class=" ">Plan de Acción</a></li>
                        <li class="<?php echo ($tab == 80) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/planeacion_plan_gasto_publico') ?>" class=" ">Plan de Gasto Público</a></li>
                        <li class="<?php echo ($tab == 64) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/programas_proyectos') ?>" class=" ">Programas y Proyectos en Ejecución</a></li>
                        <li class="<?php echo ($tab == 69) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/indicadores_gestion') ?>" class=" ">Metas e Indicadores de Gestión</a></li>
                        <!--<li class="<?php //echo ($tab == 81) ? 'accourdion-area_active' : '';           ?>"><a href="<?php // echo site_url('entidad/mipg')           ?>" class=" ">Modelo Integrado de Planeación y Gestión</a></li>-->
                        <li class="<?php echo ($tab == 71) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/planes_otros') ?>" class=" ">Otros Planes</a></li>
                        <li class="<?php echo ($tab == 73) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/informe_empalme') ?>" class=" ">Informes de Empalme</a></li>
                        <li class="<?php echo ($tab == 79) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/control_archivo') ?>" class=" ">Gestión Documental</a></li>

                    </ul>
                </div>
            </div>
        </div>	

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo7"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-clipboard"></i> Control</p>
                </a>
            </div>
            <div id="collapseTwo7" class="collapse <?php echo ($tab > 6060 && $tab < 6090) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">       
                        <li class="<?php echo ($tab == 6065) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/entes') ?>" class=" ">Entes de Control que Vigilan la Entidad</a></li>
                        <li class="<?php echo ($tab == 6067) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/auditorias') ?>" class=" ">Auditorías  </a></li>
                        <li class="<?php echo ($tab == 6066) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/informe_concejo') ?>" class=" ">Informes al Concejo</a></li>
                        <li class="<?php echo ($tab == 6079) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/informe_pqrs') ?>" class=" ">Informe de Peticiones, Quejas, Reclamos, Denuncias y Sugerencias</a></li>
                        <li class="<?php echo ($tab == 6069) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/informe_fiscal') ?>" class=" ">Informes de Rendición de Cuenta Fiscal </a></li>
                        <li class="<?php echo ($tab == 6068) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/informe_ciudadania') ?>" class=" ">Informes de Rendición de Cuenta a los Ciudadanos</a></li>
                        <li class="<?php echo ($tab == 6070) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/planes_mejoramiento') ?>" class=" ">Planes de Mejoramiento</a></li>
                        <li class="<?php echo ($tab == 6072) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/reportes_control_interno') ?>" class=" ">Reportes de Control Interno</a></li>
                        <li class="<?php echo ($tab == 6074) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/poblacion_vulnerable') ?>" class=" ">Información para Población Vulnerable</a></li>
                        <li class="<?php echo ($tab == 6077) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/programas_sociales') ?>" class=" ">Programas Sociales</a></li>
                        <li class="<?php echo ($tab == 6078) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/defensa_judicial') ?>" class=" ">Defensa Judicial</a></li>
                        <li class="<?php echo ($tab == 6080) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/otros_informes') ?>" class=" ">Otros Informes</a></li>
                    </ul>
                </div>
            </div>
        </div>	

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo8"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-id-badge"></i> Contratación</p>
                </a>
            </div>
            <div id="collapseTwo8" class="collapse <?php echo ($tab > 90 && $tab < 100) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 91) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/plan_compras') ?>" class=" ">Plan de Adquisiciones</a></li>
                        <li class="<?php echo ($tab == 92) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/secop') ?>" class=" ">Información SECOP</a></li>
                        <li class="<?php echo ($tab == 93) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/contratos') ?>" class=" ">Procesos de Contratación</a></li>
                        <li class="<?php echo ($tab == 95) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/avance_contractual') ?>" class=" ">Avances Contractuales</a></li>
        <!--                <li class="<?php //echo ($tab == 94) ? 'accourdion-area_active' : '';           ?>"><a href="<?php //echo site_url('entidad/convenios')           ?>" class=" ">Convenios</a></li>-->
                        <li class="<?php echo ($tab == 96) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/contratacion_lineamientos') ?>" class=" ">Lineamientos de Adquisiciones  y Compras</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo9"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-list-ol"></i> Trámites y Servicios</p>
                </a>
            </div>
            <div id="collapseTwo9" class="collapse <?php echo ($tab > 100 && $tab < 110) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 101) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/tramites') ?>" class=" ">Listado de Trámites y Servicios</a></li>
                        <li class="<?php echo ($tab == 104) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/tramites_en_linea') ?>" class=" ">Trámites en Línea</a></li>
                        <li class="<?php echo ($tab == 105) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/formularios_tramites') ?>" class=" ">Formularios de Trámites</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo12"
                   aria-expanded="false" aria-controls="collapseTwo12">
                    <p class="mb-0"><i class="fas fa-unlock-alt"></i> Datos Abiertos</p>
                </a>
            </div>
            <div id="collapseTwo12" class="collapse <?php echo ($tab > 140 && $tab < 150) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo12" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 141) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/datos_abiertos') ?>" class=" ">Datos Abiertos</a></li>
                        <li class="<?php echo ($tab == 142) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/catalogo_abiertos') ?>" class=" ">Catálogo de Datos Abiertos</a></li>
        <!--                <li class="<?php //echo ($tab == 143) ? 'accourdion-area_active' : '';           ?>"><a href="<?php //echo site_url('entidad/mapas_abiertos')           ?>" class=" ">Mapas de Datos Abiertos</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo10"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-check"></i> Calidad</p>
                </a>
            </div>
            <div id="collapseTwo10" class="collapse <?php echo ($tab > 130 && $tab < 140) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 131) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/index/calidad') ?>" class=" ">Sistema de Calidad</a></li>
                        <li class="<?php echo ($tab == 132) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/caracterizacion_procesos') ?>" class=" ">Caracterización de Procesos</a></li>
                        <li class="<?php echo ($tab == 133) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/procedimientos') ?>" class=" ">Procedimientos</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="submenu_interno">
            <div class="submenu_interno-header" role="tab" id="headingTwo1">
                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo11"
                   aria-expanded="false" aria-controls="collapseTwo1">
                    <p class="mb-0"><i class="fas fa-file-archive"></i> Documentos</p>
                </a>
            </div>
            <div id="collapseTwo11" class="collapse <?php echo ($tab > 110 && $tab < 120) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingTwo1" data-parent="#accordionEx1">
                <div class="submenu_interno-body">
                    <ul class="colapse_list list-unstyled">
                        <li class="<?php echo ($tab == 111) ? 'accourdion-area_active' : ''; ?>"><a href="<?php echo site_url('entidad/documentos') ?>" class=" ">Documentos de la Entidad</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


