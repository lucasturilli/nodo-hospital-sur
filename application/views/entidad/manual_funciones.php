<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Manual de funciones y competencias de la entidad</spam></h2>
                </div>
                <?php
                if ($manual_funciones != FALSE) {
                    $x = 0;
                    $j = 0;
                    ?>
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills nav-fill" id="myTab"  role="tablist">
                            <?php
                            $x = 0;
                            foreach ($periodos as $periodo) {
                                ?>
                                <li class="nav-item" role="tab_<?php echo $periodo->id_entidad_definicion_manual_de_funciones; ?>">
                                    <a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>"  href="#tab_<?php echo $periodo->id_entidad_definicion_manual_de_funciones; ?>" aria-controls="tab_<?php echo $periodo->id_entidad_definicion_manual_de_funciones; ?>" role="tab" data-toggle="tab"><?php echo $periodo->entidad_definicion_manual_de_funciones_nombre; ?></a></li>  
                                <?php
                                $x++;
                            }
                            ?>
                        </ul>
                        <hr>
                        <div class="tab-content">
                            <?php foreach ($periodos as $periodo) { ?>
                                <div role="tabpanel" class="tab-pane   <?php echo ($j == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo->id_entidad_definicion_manual_de_funciones; ?>">
                                    <div class = "table-responsive">
                                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                            <thead>
                                            <th><span btn-primary data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                            <th ><span>Archivo</span></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($manual_funciones as $dato2) {
                                                    if ($dato2->id_entidad_definicion_manual_de_funciones == $periodo->id_entidad_definicion_manual_de_funciones) {
                                                        ?>
                                                        <tr>
                                                            <td ><?php echo $dato2->entidad_manual_de_funciones_nombre; ?></td>
                                                            <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/funcionarios') . '/' . $dato2->entidad_manual_de_funciones_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato2->entidad_manual_de_funciones_archivo); ?>"> </i> Ver Archivo</a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php
                                $j++;
                            }
                            ?>
                        </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    </div>
</div>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>