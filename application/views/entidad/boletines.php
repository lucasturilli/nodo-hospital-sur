<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Boletines</spam></h2>
                </div>
                <?php
                if ($boletines != FALSE) {
                    $x = 0;
                    $j = 0;
                    ?>
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills nav-fill " id="myTab" role="tablist">
                        <?php
                        $x = 0;
                        foreach ($periodos as $periodo) {
                            ?>
                            <li class="nav-item" role="tab_<?php echo $periodo->periodo; ?>" >
                                <a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>"  href="#tab_<?php echo $periodo->periodo; ?>" aria-controls="tab_<?php echo $periodo->periodo; ?>" role="tab" data-toggle="tab"><?php echo $periodo->periodo; ?></a>
                            </li>  
                            <?php
                            $x++;
                        }
                        ?>
                    </ul>
                    <hr>
                    <div class="tab-content">
                        <?php foreach ($periodos as $periodo) { ?>
                            <div role="tabpanel" class="tab-pane  <?php echo ($j == 0) ? 'active' : ''; ?>" id="tab_<?php echo $periodo->periodo; ?>">
                                <div class = "table-responsive">
                                    <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                                        <thead>
                                        <th style="cursor:pointer;" ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                        <th style="cursor:pointer;" ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha de publicación ">Fecha de Publicación </span></th>
                                        <th ><span>Archivo</span></th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($boletines as $dato2) {
                                                if (date('Y', strtotime($dato2->entidad_boletines_fecha)) == $periodo->periodo) {
                                                    ?>
                                                    <tr>
                                                        <td ><?php echo $dato2->entidad_boletines_nombre; ?></td>
                                                        <td ><?php echo $dato2->entidad_boletines_fecha; ?></td>
                                                        <td ><a target="_blank" href="<?php echo site_url('uploads/boletines') . '/' . $dato2->entidad_boletines_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato2->entidad_boletines_archivo); ?>"> </i> Ver Archivo</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            $j++;
                        }
                        ?>
                    </div>

                    <?php
                } else {
                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($tab_list == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>