<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Directorio de Sucursales o Regionales</spam></h2>
                </div>
                <?php if ($sucursales != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre de la sucursal">Nombre</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por teléfono ">Teléfono</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Correo electrónico">Correo Electrónico</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Correo electrónico">Dirección </span></th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                foreach ($sucursales as $dato) {
                                    ?>
                                    <tr>
                                        <td ><a id="norma_<?php echo $dato->id_entidad_sucursales; ?>"  href="#" onclick="return false;"  data-toggle="tooltip" data-placement="top" title="Ver información completa de la sucursal"><?php echo $dato->entidad_sucursales_nombre; ?></a></td>
                                        <td ><?php echo $dato->entidad_sucursales_telefono; ?></td>
                                        <td ><?php echo $dato->entidad_sucursales_email; ?></td>
                                        <td ><?php echo $dato->entidad_sucursales_direccion; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?> 

                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {

                    echo '<div class = "alert alert-info" role = "alert"><i class = "fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
            </div>
        </div>
    </div>
</main>
<script>
    $('td a').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(6)
        var url = "<?php echo site_url('entidad/directorio_sucursales_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {

                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>