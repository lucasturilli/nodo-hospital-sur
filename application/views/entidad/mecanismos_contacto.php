<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Mecanismos de Contacto</a></h3>
        </div>
        <?php if ($section == 'mecanismos') { ?>
            <h3>Mecanismos para la atención al ciudadano</h3>
            <hr>
            <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-phone"></i>&nbsp;&nbsp;Teléfono: <?php echo $entidad_contacto->entidad_contacto_telefono; ?></li>
                <li class="list-group-item"><i class="fa fa-phone-square"></i>&nbsp;&nbsp;Línea Telefónica Gratuita: <?php echo $entidad_contacto->entidad_contacto_linea_gratuita; ?></li>
                <li class="list-group-item"><i class="fa fa-fax"></i>&nbsp;&nbsp;Fax: <?php echo $entidad_contacto->entidad_contacto_fax; ?></li>
                <li class="list-group-item"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Correo electrónico: <?php echo $entidad_contacto->entidad_contacto_email; ?></li>
                <li class="list-group-item"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Dirección : <?php echo $entidad_contacto->entidad_contacto_direccion; ?> Código Postal: 055412</li>
            </ul>
        <?php } ?>
        <?php if ($section == 'atencion') { ?>
            <h3>Atención en Línea</h3>
            <hr>
            La sala de chat es un lugar para comunicarse con la entidad y resolver sus dudas puntuales desde nuestro sitio Web. <br><br> En caso de no haber un operador disponible el enlace lo re direccionará al Sistema de peticiones, quejas y reclamos en donde podrá dejar su solicitud y posteriormente le daremos una respuesta por el mismo medio. <hr>
            <a class="btn btn-primary"  href="<?php echo $configuracion->config_general_url_chat; ?>">Abrir Chat</a>
        <?php } ?>
        <?php if ($section == 'buzon') { ?>
            <h3>Buzón de Contacto</h3>
            <hr>
            <p>El buzón de contáctenos , es un formulario para el ingreso de solicitudes de información para resolver sus dudas puntuales</p>
            <a class="btn btn-primary btn-lg" href="<?php echo site_url('contacto'); ?>"><i class="far fa-envelope"></i> Contáctenos</a>
        <?php } ?>
        <?php if ($section == 'pqrs') { ?>
            <h3>Solicitud y verificación de peticiones, quejas y reclamos </h3>
            <hr>
            <p>De clic en el siguiente botón para ingresar al sistema de recepción de peticiones, quejas, reclamos, denuncias y sugerencias </p>
            <a href="<?php echo $configuracion->config_general_url_pqrs;?>" target="_blank" class="btn btn-primary " ><i class="fas fa-comment-alt"></i> Clic Aquí</a>
        <?php } ?>
        <?php if ($section == 'informacion_publica') { ?>
            <h3>Solicitudes de información pública </h3>
            <hr>
            <p>De conformidad con el numera 5 del artículo 2.1.1.3.1.1 del decreto N° 1081 de 2015 se integró el formulario electrónico para la recepción de solicitudes de información pública con el sistema de recepción de peticiones, quejas y reclamos. Por favor ingrese por medio del siguiente enlace para realizar el registro de su solicitud. </p><hr>
            <a href="<?php echo $configuracion->config_general_url_pqrs; ?>" target="_blank" class="btn btn-primary">Clic Aquí</a>
        <?php } ?>
        <?php if ($section == 'localizacion') { ?>
            <h3>Localización física y días de atención al público</h3>
            <hr>
            <iframe src="<?php echo $entidad_contacto->entidad_contacto_google_mapa; ?>" width="100%" height="300" frameborder="0" style="border:0"></iframe>
            <strong><?php echo $seo->entidad_seo_titulo; ?><br />Antioquia - Colombia<br /></strong><br /><strong>Dirección:</strong> <?php echo $entidad_contacto->entidad_contacto_direccion; ?><br /><strong>Código Postal:</strong> 055460<br /><strong>Horarios de atención:</strong> <?php echo $entidad_contacto->entidad_contacto_horarios_atencion; ?> 
        <?php } ?>
        <?php if ($section == 'notificaciones_judiciales') { ?>
            <h3>Correo electrónico para notificaciones judiciales </h3>
            <hr>
            <p>De conformidad con lo establecido por el artículo 197 del Nuevo Código de Procedimiento Administrativo y de lo Contencioso Administrativo, se informa que la <?php echo $seo->entidad_seo_titulo; ?>, recibirá las notificaciones judiciales al correo electrónico:</p><hr>
            <h4><strong><?php echo $entidad_contacto->entidad_contacto_email_notificaciones_judiciales; ?></strong></h4>
        <?php } ?>
        <?php if ($section == 'politicas') { ?>
            <h3>Políticas de seguridad de la información del sitio web y protección de datos personales</h3>
            <hr>
            <ul class="list-unstyled">
                <?php
                if ($entidad_politicas_datos != FALSE) {
                    foreach ($entidad_politicas_datos as $dato) {
                        ?>
                        <li><a href="<?php echo site_url('uploads/entidad/politicas') . '/' . $dato->entidad_politicas_datos_archivo; ?>" target="_blank"><?php echo $dato->entidad_politicas_datos_nombre; ?></a></li>
                        <?php
                    }
                }
                ?>
            </ul>
        <?php } ?>
    </article>
</div>


