<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Reportes de Control Interno</spam></h2>
                </div>
                <?php if ($reportes_control_interno != FALSE) { ?>
                    <?php if ($tabs != FALSE) { ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php
                            foreach ($tabs as $cat) {
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab"  id="heading<?php echo $cat->id_control_reportes_control_interno_tab; ?>">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $cat->id_control_reportes_control_interno_tab; ?>" aria-expanded="true" aria-controls="collapse<?php echo $cat->id_control_reportes_control_interno_tab; ?>">
                                                <?php echo $cat->control_reportes_control_interno_tab_nombre; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?php echo $cat->id_control_reportes_control_interno_tab; ?>" class="panel-collapse collapse <?php echo ($tab_list != NULL) ? ($cat->id_control_reportes_control_interno_tab == $tab_list) ? 'show' : '' : ''; ?>" role="tabpanel" aria-labelledby="heading<?php echo $cat->id_control_reportes_control_interno_tab; ?>" data-parent="#accordion">
                                        <div class="panel-body">
                                            <ul class="nav nav-pills nav-fill" id="myTab_<?php echo $cat->id_control_reportes_control_interno_tab; ?>"  role="tablist">
                                                <?php
                                                $x = 0;
                                                foreach ($periodos_control_interno as $per_fin) {
                                                    ?>
                                                    <li class="nav-item" role="tab_<?php echo $per_fin->control_reportes_control_interno_periodo . $cat->id_control_reportes_control_interno_tab; ?>" ><a class="nav-link  <?php echo($x == 0) ? 'active' : ''; ?>" href="#tab_<?php echo $per_fin->control_reportes_control_interno_periodo . $cat->id_control_reportes_control_interno_tab; ?>" aria-controls="tab_<?php echo $per_fin->control_reportes_control_interno_periodo . $cat->id_control_reportes_control_interno_tab; ?>" role="tab" data-toggle="tab"><?php echo $per_fin->control_reportes_control_interno_periodo; ?></a></li>
                                                    <?php
                                                    $x++;
                                                }
                                                ?>
                                            </ul>
                                            <hr>
                                            <div class="tab-content">
                                                <?php
                                                $x = 0;
                                                foreach ($periodos_control_interno as $per_fin) {
                                                    ?>
                                                    <div role="tabpanel" class="tab-pane   <?php echo($x == 0) ? 'active' : ''; ?>" id="tab_<?php echo $per_fin->control_reportes_control_interno_periodo . $cat->id_control_reportes_control_interno_tab; ?>" >
                                                        <div class="table-responsive">
                                                            <table id="myTable"  class="table table-striped table-hover table-bordered full_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por nombre">Nombre</span></th>
                                                                        <th   ><span   data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Periodicidad">Periodicidad</span></th>
                                                                        <th   ><span>Archivo</span></th>

                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($reportes_control_interno as $dato) {
                                                                        if ($dato->id_control_reportes_control_interno_tab == $cat->id_control_reportes_control_interno_tab) {
                                                                            if ($dato->control_reportes_control_interno_periodo == $per_fin->control_reportes_control_interno_periodo) {
                                                                                ?>
                                                                                <tr>
                                                                                    <td   ><?php echo $dato->control_reportes_control_interno_nombre; ?></td>
                                                                                    <td   ><?php echo $dato->control_reportes_control_interno_cuatrimestre; ?></td>
                                                                                    <td   ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_reportes_control_interno_archivo; ?>" class="btn btn-secondary btn-sm" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_reportes_control_interno_archivo); ?>"> </i> Ver Archivo</a></td>

                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $x++;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    <?php } ?>

                    <?php
                } else {
                    ?>
                    <div class="alert alert-info" role="alert"><i class="fas fa-info-circle"></i> No hay información  disponible</div>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<script>
<?php if ($period == NULL) { ?>
        $(function () {
            $('#myTab a:first').tab('show');
        });
<?php } else { ?>
        $(document).ready(function () {
            $('a[href="#tab_<?php echo $period . $tab_list; ?>"]').tab('show');
        });
<?php } ?>
</script>