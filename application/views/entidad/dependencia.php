<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo"><?php echo $dependencia->entidad_dependencia_nombre; ?></spam></h2>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover    ">
                        <tr>
                            <th>Nombre de la Dependencia</th>
                            <td><?php echo $dependencia->entidad_dependencia_nombre; ?></td>
                        </tr>
                        <tr>
                            <th>Funcionario Responsable</th>
                            <td ><?php if ($dependencia->id_funcionario != NULL) { ?><a href="<?php echo site_url('alcaldia/funcionario') . '/' . $dependencia->id_funcionario; ?>"  data-toggle="tooltip" data-placement="top" title="Ver información completa del funcionario"><?php echo $dependencia->nombres . ' ' . $dependencia->apellidos; ?></a>
                                    <?php
                                } else {
                                    echo 'Información no disponible';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th>Correo Electrónico</th>
                            <td><?php echo $dependencia->entidad_dependencia_email; ?></td>
                        </tr>
                        <tr>
                            <th>Teléfono</th>
                            <td><?php echo $dependencia->entidad_dependencia_telefono; ?></td>
                        </tr>
                        <tr>
                            <th>Fax</th>
                            <td><?php echo ($dependencia->entidad_dependencia_fax != NULL) ? $dependencia->entidad_dependencia_fax : $entidad_contacto->entidad_contacto_fax; ?></td>
                        </tr>

                        <tr>
                            <th>Objetivos </th>
                            <td><?php echo ($dependencia->entidad_dependencia_objetivos != NULL) ? $dependencia->entidad_dependencia_objetivos : 'Información no disponible'; ?></td>
                        </tr>
                        <tr>
                            <th>Funciones </th>
                            <td><?php echo ($dependencia->entidad_dependencia_funciones != NULL) ? $dependencia->entidad_dependencia_funciones : 'Información no disponible'; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
