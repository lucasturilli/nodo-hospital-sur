<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-12 col-lg-12">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Esquema de Publicación</spam></h2>
                </div>
                <?php if ($registros != FALSE) { ?>
                    <div class = "table-responsive">
                        <table  class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                    <th >Archivo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td >Esquema de publicación </td>
                                    <td ><a href="<?php echo site_url('uploads/entidad/datos_abiertos') . '/' . $registros->esquema_publicacion_archivo; ?>" target="_blank" class="btn btn-primary btn-xs ">Ver Archivo</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {
                    echo '<div class = "alert alert-info" role = "alert"><i class = "fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>