<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<main class="main-content">
    <div class="container">
        <div class="row entidad_margen">
            <div class="col-md-4 col-lg-3 drawermenu-col">
                <?php include 'includes/menu.php'; ?>
            </div>
            <div class="col-md-8 col-lg-9">
                <div class="section-title">
                    <h2><i class="fas fa-circle"></i> <spam id="titulo">Programas y Proyectos en Ejecución</spam></h2>
                </div>
                <p>Proyectos de inversión o programas que se ejecutan en cada vigencia.  según lo establecido en el artículo 77 de la Ley 1474 de 2011 (Estatuto Anticorrupción).</p>
                <hr>
                <?php if ($programas != FALSE) { ?>
                    <div class = "table-responsive">
                        <table id = "myTable" class = "table table-striped table-hover table-bordered full_table fullwidth">
                            <thead>
                                <tr>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por Nombre">Nombre</span></th>
                                    <th ><span  data-toggle="tooltip" data-placement="top" title="Clic para ordenar por fecha de inscripción">Fecha de Inscripción</span></th>
                                    <th ><span>Archivo</span></th>
                                    <th ><span>Avances del Proyecto</span></th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                foreach ($programas as $dato) {
                                    ?>
                                    <tr>

                                        <td ><?php echo $dato->control_programas_nombre; ?></td>
                                        <td ><?php echo $dato->control_programas_fecha_inscripcion; ?></td>
                                        <td ><a target="_blank" href="<?php echo site_url('uploads/entidad/control') . '/' . $dato->control_programas_archivo; ?>" class="btn btn-primary btn-xs" ><i class="fas fa-file-<?php echo obtenerFielType($dato->control_programas_archivo); ?>"> </i> Ver Archivo</a></td>
                                        <td ><a id="programa_<?php echo $dato->id_control_programas; ?>" href="#" onclick="return false;" class="btn btn-warning btn-xs ver_avances" ><i class="fa fa-plus-square"> </i> Ver Avances</a></td>

                                    </tr>
                                    <?php
                                }
                                ?> 

                            </tbody>
                        </table>
                    </div>
                    <?php
                } else {

                    echo '<div class="alert alert-info" role="alert"><i class="fa fa-info-circle"></i> Información no disponible</div>';
                }
                ?>
            </div>
        </div>
    </div>
</main>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    </div>
</div>
<script>
    $('.ver_avances').click(function () {
        var name = $(this).attr("id");
        var id = name.substring(9)
        var url = "<?php echo site_url('entidad/programas_proyectos_ajax') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                $('.modal-dialog').empty();
                $('.modal-dialog').append(msg);
                $('#myModal').modal();
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });
</script>