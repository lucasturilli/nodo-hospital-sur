<?php

/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * @package    PQRS
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
?>
<?php $this->load->view('includes/header'); ?>
<?php if ($main_content != 'home') { ?>
    <?php $this->load->view('includes/breadcrumbs'); ?>
<?php } ?>
<?php $this->load->view($main_content); ?>
<?php $this->load->view('includes/footer');
?>

