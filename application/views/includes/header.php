<!DOCTYPE html>
<html lang="es">
    <!--flagEstadoSitio-->
    <head>
        <?php
        switch ($main_content) {
            case 'ver_noticia':
                $titulo_seo = $noticia->entidad_noticias_titulo;
                $imagen_seo = ($noticia->entidad_noticias_imagen != NULL) ? site_url('uploads/noticias') . '/' . $noticia->entidad_noticias_imagen : site_url('uploads/entidad') . '/' . $entidad_logo->entidad_informacion_logo;
                $descripcion_seo = $noticia->entidad_noticias_descripcion_corta;
                break;
            case 'ver_notificacion':
                $titulo_seo = $noticia->entidad_noticias_titulo;
                $imagen_seo = ($noticia->entidad_noticias_imagen != NULL) ? site_url('uploads/noticias') . '/' . $noticia->entidad_noticias_imagen : site_url('uploads/entidad') . '/' . $entidad_logo->entidad_informacion_logo;
                $descripcion_seo = $noticia->entidad_noticias_descripcion_corta;
                break;
            case 'ver_evento':
                $titulo_seo = $evento->entidad_calendario_nombre;
                $imagen_seo = ($evento->entidad_calendario_imagen != NULL) ? site_url('uploads/eventos') . '/' . $evento->entidad_calendario_imagen : site_url('uploads/entidad') . '/' . $entidad_logo->entidad_informacion_logo;
                $descripcion_seo = trim(strip_tags($evento->entidad_calendario_descripcion));
                break;
            default:
                $titulo_seo = $seo->entidad_seo_titulo;
                $imagen_seo = site_url('uploads/entidad') . '/' . $entidad_logo->entidad_informacion_logo;
                $descripcion_seo = $seo->entidad_seo_descripcion;
        }
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $seo->entidad_seo_titulo; ?></title>
        <meta property="og:title"        content="<?php echo $titulo_seo; ?>"/>
        <meta property="og:type"         content="article"/>
        <meta property="og:image"        content="<?php echo $imagen_seo; ?>"/>
        <meta property="og:site_name"    content="<?php echo $seo->entidad_seo_titulo; ?>"/>
        <meta property="og:locality"     content="<?php echo $seo->entidad_seo_ciudad; ?>"/>
        <meta property="og:region"       content="<?php echo $seo->entidad_seo_region; ?>"/>
        <meta property="og:country-name" content="<?php echo $seo->entidad_seo_country; ?>"/>
        <meta property="og:description" content="<?php echo $descripcion_seo; ?>"/>
        <?php if ($seo->entidad_seo_twitter_vcard != NULL) { ?>
            <meta name="twitter:card" content="<?php echo $seo->entidad_seo_twitter_vcard; ?>" />
            <meta name="twitter:title" content="<?php echo $titulo_seo; ?>" />
            <meta name="twitter:description" content="<?php echo $descripcion_seo; ?>" />
            <meta name="twitter:image" content="<?php echo $imagen_seo; ?>" />
        <?php } ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="lang" content="es" />
        <meta name="identifier-url" content="<?php echo site_url(); ?>" />
        <meta name="title" content="<?php echo $seo->entidad_seo_titulo; ?>" />
        <meta name="description" content="<?php echo $seo->entidad_seo_descripcion; ?>" />
        <meta name="abstract" content="<?php echo $seo->entidad_seo_descripcion; ?>" />
        <meta name="keywords" content="<?php echo $seo->entidad_seo_palabras; ?>" />
        <meta name="author" content="<?php echo $seo->entidad_seo_titulo; ?>" />
        <meta name="copyright" content="© Codweb SAS - http://codweb.co" />
        <meta name="application-name" content="<?php echo $seo->entidad_seo_titulo; ?>" />
        <meta name="robots" content="All" />
        <link rel="shortcut icon" href="<?php echo site_url('uploads/entidad') . '/' . $seo->entidad_seo_favicon; ?>">
        <!--       GOOGLE ANALYTICS-->
        <?php echo $seo->entidad_seo_google_analytics; ?>
        <!-- Bootstrap -->
        <link href="<?php echo site_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/yamm.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>fontawesome/css/all.css?ver=1.6" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,400,700|Nunito:700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
        <link href="<?php echo site_url(); ?>css/style.css?ver=1.6" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo site_url(); ?>js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/pace.js"></script>
        <script src="<?php echo site_url(); ?>js/sweetalert.js?ver=1.5"></script>
        <script src="<?php echo site_url(); ?>js/jquery.blockUI.js?ver=1.5"></script>
        <script src="<?php echo site_url(); ?>js/custom-drawer.js" type="text/javascript"></script>
        <link href="<?php echo site_url(); ?>plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo site_url(); ?>plugins/datatables/datatables.min.js" type="text/javascript"></script>

    </head>
    <body id="page-top" >
        <!--   ========================== Header Fixed Menu Space ===========================-->
        <div class="header-space"></div>
        <!--   ========================== Header Area===========================-->
        <header class="main-header">
            <div class="container">
                <div class="row">
                    <div class="navbar navbar-default yamm">
                        <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"><i class="fas fa-bars"></i><i class="far fa-times"></i></button><a href="<?php echo site_url(); ?>" class="navbar-brand" data-toggle="tooltip" data-placement="bottom" title="Regresar al Inicio"><img src="<?php echo site_url(); ?>images/logo.png" class="img-responsive" alt=""></a>
                        </div>
                        <div id="navbar-collapse-grid" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <!-- Grid 12 Menu -->
                                <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Nuestra Institución</a>
                                    <ul class="dropdown-menu">
                                        <li class="grid-demo">
                                            <div class="flex">
                                                <div class="col-md-6 col-xs-12 mega-drop-2">
                                                    <ul class="mega-drop ">
                                                        <li><a href="<?php echo site_url('entidad/'); ?>"><i class="fas fa-circle"></i> Presentación</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/19'); ?>"><i class="fas fa-circle"></i> Información General</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/39'); ?>"><i class="fas fa-circle"></i> Servicios de Información</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/129'); ?>"><i class="fas fa-circle"></i> Talento Humano</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/49'); ?>"><i class="fas fa-circle"></i> Normatividad</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/59'); ?>"><i class="fas fa-circle"></i> Presupuesto</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/88'); ?>"><i class="fas fa-circle"></i> Planeación</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6 col-xs-12 mega-drop-2">
                                                    <ul class="mega-drop ">
                                                        <li><a href="<?php echo site_url('entidad/index/6089'); ?>"><i class="fas fa-circle"></i> Control</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/99'); ?>"><i class="fas fa-circle"></i> Contratación</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/109'); ?>"><i class="fas fa-circle"></i> Trámites y Servicios</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/149'); ?>"><i class="fas fa-circle"></i> Datos Abiertos</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/139'); ?>"><i class="fas fa-circle"></i> Calidad</a></li>
                                                        <li><a href="<?php echo site_url('entidad/index/119'); ?>"><i class="fas fa-circle"></i> Documentos</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <!--With Offsets 
                                -->
                                <!--Aside Menu 
                                -->
                                <li class="dropdown yamm-fw"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Atención al Ciudadano</a>
                                    <ul class="dropdown-menu">
                                        <li class="grid-demo">
                                            <div class="flex">
                                                <div class="col-md-6 col-xs-12">
                                                    <h3>Información de Interés</h3>
                                                    <ul class="mega-drop">
                                                        <li><a href="<?php echo site_url('sitio/calendario'); ?>"><i class="fas fa-circle"></i> Calendario de eventos</a></li>
                                                        <li><a href="<?php echo site_url('entidad/convocatorias') ?>"><i class="fas fa-circle"></i> Convocatorias</a></li>
                                                        <li><a href="<?php echo site_url('entidad/estudios'); ?>"><i class="fas fa-circle"></i> Estudio, investigaciones y otras publicaciones</a></li>
                                                        <li><a href="<?php echo site_url('entidad/glosario'); ?>"><i class="fas fa-circle"></i> Glosario</a></li>
                                                        <li><a href="<?php echo site_url('entidad/informacion_adicional'); ?>"><i class="fas fa-circle"></i> Información adicional</a></li>
                                                        <li><a href="<?php echo $configuracion->config_general_portal_ninos; ?>" target="_blank"><i class="fas fa-circle"></i> Información para niños y jóvenes</a></li>
                                                        <li><a href="<?php echo site_url('sitio/noticias'); ?>"><i class="fas fa-circle"></i> Noticias</a></li>
                                                        <li><a href="<?php echo site_url('entidad/preguntas_frecuentes'); ?>"><i class="fas fa-circle"></i> Preguntas y respuestas frecuentes</a></li>
                                                        <li><a href="<?php echo site_url('entidad/datos_abiertos'); ?>"><i class="fas fa-circle"></i> Publicación de datos abiertos</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6 col-xs-12 mega-drop-2">
                                                    <h3>Mecanismos de Contacto</h3>
                                                    <ul class="mega-drop ">
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/mecanismos') ?>" ><i class="fas fa-circle"></i> Mecanismos para la atención al ciudadano</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/atencion') ?>"><i class="fas fa-circle"></i> Atención en Línea</a></li>
                                                        <li><a href="<?php echo site_url('contacto'); ?>"><i class="fas fa-circle"></i> Buzón de Contacto</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/pqrs') ?>"><i class="fas fa-circle"></i> Solicitud y verificación de peticiones, quejas y reclamos</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/informacion_publica') ?>"><i class="fas fa-circle"></i> Solicitudes de información pública</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/localizacion') ?>"><i class="fas fa-circle"></i> Localización física y días de atención al público</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/notificaciones_judiciales') ?>"><i class="fas fa-circle"></i> Correo electrónico para notificaciones judiciales</a></li>
                                                        <li><a href="<?php echo site_url('entidad/mecanismos_contacto/politicas') ?>"><i class="fas fa-circle"></i> Políticas de seguridad de la información y protección de datos personales</a></li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class=""><a href="<?php echo site_url('entidad/transparencia'); ?>" >Transparencia</a>

                                </li>
                                <li class=""><a href="<?php echo site_url('contacto'); ?>" >Contáctenos</a>

                            </ul>
                        </div>
                        <div class="share-area header-social pc clearfix">
                            <?php
                            if ($entidad_redes_sociales != FALSE) {
                                foreach ($entidad_redes_sociales as $dato) {
                                    echo '<a target="_blank" href="' . $dato->entidad_redes_sociales_link . '" data-toggle="tooltip" data-placement="bottom" title="Ver información en la red social ' . $dato->entidad_redes_sociales_definicion_nombre . '" class="text-capitalize"><i class="fab ' . $dato->entidad_redes_sociales_definicion_imagen . '"></i></a>';
                                }
                                echo '<a href="' . site_url('contacto') . '" data-toggle="tooltip" data-placement="bottom" title="Escríbenos"><i class="fa fa-envelope"></i></a>';
                            }
                            ?>
                        </div>

                        <div class="options">
                            <i data-toggle="tooltip" data-placement="bottom" title="Buscar" class="fas fa-search search"></i>
                            <i data-toggle="tooltip" data-placement="bottom" title="Redes Sociales" class="fas fa-share-alt share hidden-md hidden-lg"></i>
                            <div class="accessibilidad-area setting">
                                <i data-toggle="tooltip" data-placement="bottom" title="Opciones de Accesibilidad" class="fas fa-cog"></i><p>Accesibilidad</p>
                            </div>
                            <i data-toggle="tooltip" data-placement="bottom" title="Cerrar" class="fas fa-times hide-item"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-form option-area">
                <div class="container">
                    <?php
                    $attributes = array('id' => 'formulario_buscar', 'class' => 'search-top clearfix', 'role' => 'search');
                    echo form_open('buscar/index', $attributes);
                    ?>
                    <input type="text" name="texto" placeholder="Escribe lo que quieres buscar">
                    <button id="boton_search" onclick="buscar();" type="button">Buscar</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="share-area option-area">
                <div class="container">
                    <?php
                    if ($entidad_redes_sociales != FALSE) {
                        foreach ($entidad_redes_sociales as $dato) {
                            echo '<a target="_blank" href="' . $dato->entidad_redes_sociales_link . '" data-toggle="tooltip" data-placement="bottom" title="Ver información en la red social ' . $dato->entidad_redes_sociales_definicion_nombre . '" class="text-capitalize"><i class="fab ' . $dato->entidad_redes_sociales_definicion_imagen . '"></i>' . $dato->entidad_redes_sociales_definicion_nombre . '</a>';
                        }
                        echo '<a href="' . site_url('contacto') . '" data-toggle="tooltip" data-placement="bottom" title="Escríbenos"><i class="fa fa-envelope"></i>Escríbenos</a>';
                    }
                    ?>
                </div>
            </div>
            <div class="setting-area option-area">
                <div class="container flex">
                    <div class="google-translate">
                        <div id="google_translate_element"></div>
                        <div class="btn-group bootstrap-select goog-te-combo">
                            <button type="button" class="btn dropdown-toggle bs-placeholder btn-default">
                                <span class="filter-option pull-left">Seleccionar Idioma</span>
                                <span class="bs-caret"><span class="caret"></span></span>
                            </button>
                        </div>
                    </div>
                    <div class="contraste">
                        <span>Cambiar contraste</span>
                        <i class="fas fa-font black" onclick="changeFilter(0);" data-toggle="tooltip" data-placement="bottom" title="Cambiar contraste escala de grises"></i>
                        <i class="fas fa-font white" onclick="changeFilter(3);" data-toggle="tooltip" data-placement="bottom" title="Cambiar contraste normal"></i>
                        <i class="fas fa-font green" onclick="changeFilter(1);" data-toggle="tooltip" data-placement="bottom" title="Cambiar contraste medio"></i>
                        <i class="fas fa-font red" onclick="changeFilter(2);" data-toggle="tooltip" data-placement="bottom" title="Cambiar contraste alto"></i>
                    </div>
                    <div class="contraste Cambiar">
                        <span>Cambiar tamaño</span>
                        <button><i style="line-height: 27px;" class="fas red" id="btn-increase" data-toggle="tooltip" data-placement="bottom" title="Cambiar tamaño"><img src="<?php echo site_url(); ?>images/+A.png" alt=""></i></button>
                        <button><i style="line-height: 27px;" class="fas red" id="btn-decrease" data-toggle="tooltip" data-placement="bottom" title="Cambiar tamaño"><img src="<?php echo site_url(); ?>images/-A.png" alt=""></i></button>
                    </div>
                    <div class="contraste Cambiar">
                        <span>Más Opciones</span>
                        <button><i style="line-height: 27px;" class="fas fa-sign-language" data-toggle="modal" data-target="#accesibilidad" data-toggle="tooltip" data-placement="bottom" title="Más Opciones"></i></button>
                    </div>
                </div>
            </div>
        </header>
        <div class="modal fade" id="mecanismos_atencion">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Mecanismos para la atención al ciudadano</h4>
                    </div>
                    <div class="modal-body ">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-phone"></i> Teléfono: <?php echo $entidad_contacto->entidad_contacto_telefono; ?></li>
                            <li><i class="fa fa-phone-square"></i> Línea Telefónica Gratuita: <?php echo $entidad_contacto->entidad_contacto_linea_gratuita; ?></li>
                            <li><i class="fa fa-fax"></i> Fax: <?php echo $entidad_contacto->entidad_contacto_fax; ?></li>
                            <li><i class="fa fa-envelope"></i> Correo electrónico: <?php echo $entidad_contacto->entidad_contacto_email; ?></li>
                            <li><i class="fa fa-map-marker"></i> Dirección : <?php echo $entidad_contacto->entidad_contacto_direccion; ?> Código Postal: 055412</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="atencion_linea">
            <div class="modal-dialog  modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Atención en línea</h4>
                    </div>
                    <div class="modal-body ">
                        La sala de chat es un lugar para comunicarse con la entidad y resolver sus dudas puntuales desde nuestro sitio Web. <br><br> En caso de no haber un operador disponible el enlace lo re direccionará al Sistema de peticiones, quejas y reclamos en donde podrá dejar su solicitud y posteriormente le daremos una respuesta por el mismo medio. <hr>
                        <a class="btn btn-primary"  href="<?php echo $configuracion->config_general_url_chat; ?>">Abrir Chat</a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" id="informacion_publica">
            <div class="modal-dialog  modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Solicitudes de información pública</h4>
                    </div>
                    <div class="modal-body ">
                        <p>De conformidad con el numera 5 del artículo 2.1.1.3.1.1 del decreto N° 1081 de 2015 se integró el formulario electrónico para la recepción de solicitudes de información pública con el sistema de recepción de peticiones, quejas y reclamos. Por favor ingrese por medio del siguiente enlace para realizar el registro de su solicitud. </p><hr>
                        <a href="<?php echo $configuracion->config_general_url_pqrs; ?>" target="_blank" class="btn btn-primary">Clic Aquí</a>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="localizacion">
            <div class="modal-dialog  modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Localización física y días de atención al público</h4>
                    </div>
                    <div class="modal-body ">
                        <strong><?php echo $seo->entidad_seo_titulo; ?><br />Antioquia - Colombia<br /></strong><br /><strong>Dirección:</strong> <?php echo $entidad_contacto->entidad_contacto_direccion; ?><br /><strong>Código Postal:</strong> 055460<br /><strong>Horarios de atención:</strong> <?php echo $entidad_contacto->entidad_contacto_horarios_atencion; ?> 

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="notificaciones">
            <div class="modal-dialog  modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Correo electrónico para notificaciones judiciales</h4>
                    </div>
                    <div class="modal-body ">
                        <p>De conformidad con lo establecido por el artículo 197 del Nuevo Código de Procedimiento Administrativo y de lo Contencioso Administrativo, se informa que la <?php echo $seo->entidad_seo_titulo; ?>, recibirá las notificaciones judiciales al correo electrónico:</p><hr>
                        <h4><strong><?php echo $entidad_contacto->entidad_contacto_email_notificaciones_judiciales; ?></strong></h4>
                    </div>            
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

        <div class="modal fade" id="politicas">
            <div class="modal-dialog  modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Políticas de seguridad de la información del sitio web y protección de datos personales</h4>
                    </div>
                    <div class="modal-body ">
                        <ul class="list-unstyled">
                            <?php
                            if ($entidad_politicas_datos != FALSE) {
                                foreach ($entidad_politicas_datos as $dato) {
                                    ?>
                                    <li><a href="<?php echo site_url('uploads/entidad/politicas') . '/' . $dato->entidad_politicas_datos_archivo; ?>" target="_blank"><?php echo $dato->entidad_politicas_datos_nombre; ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>            
                    <div class="modal-footer">
                        <button type="button" class="btn button-u" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->

        <div class="modal fade" id="accesibilidad" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header alert alert-info">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Opciones de Accesibilidad</h4>
                    </div>
                    <div class="modal-body ">
                        <div class="list-group">
                            <a href="http://www.convertic.gov.co/641/w3-propertyvalue-15339.html" target="_blank" type="button" class="list-group-item list-group-item-action"><i class="fas fa-cloud-download-alt"></i> <kbd>JAWS</kbd> software que convierte a voz la información que se muestra en la pantalla, permitiendo a las personas ciegas hacer un uso autónomo del computador y sus aplicaciones</a>
                            <a href="http://convertic.gov.co/641/w3-propertyvalue-15340.html" target="_blank" type="button" class="list-group-item list-group-item-action"><i class="fas fa-cloud-download-alt"></i> <kbd>ZoomText</kbd> software que amplía hasta 16 veces el tamaño de las letras en pantalla y permite variar color y contraste, beneficiando a personas con baja visión.</a>
                            <a href="http://micrositios.mintic.gov.co/convertic/" target="_blank" type="button" class="list-group-item list-group-item-action"><i class="fas fa-cloud-download-alt"></i> <kbd>CONVERTIC</kbd> #TecnologíaParaVer</a>
                            <a href="http://www.essentialaccessibility.com/es/download" target="_blank"type="button" class="list-group-item list-group-item-action"><i class="fas fa-cloud-download-alt"></i> <kbd>eSSENTIAL Accessibility</kbd> Herramientas para que las personas con discapacidades naveguen por la web</a>
                            <a href="<?php echo site_url("sitio/mapa_del_sitio"); ?>" type="button" class="list-group-item list-group-item-action"><i class="fas fa-sitemap"></i> Ver mapa del sitio</a>
                            <a href="https://centroderelevo.gov.co/632/w3-channel.html" target="_blank" type="button" class="list-group-item list-group-item-action"><i class="fas fa-sign-language"></i> Centro de Relevo</a>
                        </div>
                    </div>            
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
