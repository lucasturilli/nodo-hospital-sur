<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!--   ========================== Nuestro Slider ===========================-->
<section class="nuestro">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-center">
                    <h2><i class="fas fa-circle"></i> Enlaces de Interés</h2>
                </div>
            </div>
        </div>
        <?php if ($carrousel != FALSE) { ?>
            <div class="partner-slider-overlay">
                <div class="patner-slider">
                    <?php foreach ($carrousel as $dato) { ?>
                        <div class="item">
                            <a data-toggle="tooltip" data-placement="right"  title="Vista el Sitio de <?php echo $dato->entidad_carrousel_titulo; ?>" href="<?php echo $dato->entidad_carrousel_url; ?>"><img src="<?php echo site_url('uploads/carrousel') . '/' . $dato->entidad_carrousel_imagen; ?>" alt="brand_<?php echo $dato->entidad_carrousel_titulo; ?>"></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

    </div>
</section>
<!--   ========================== Main Footer ===========================-->
<footer class="main-footer">
    <iframe class="map" src="https://www.google.com/maps/d/embed?mid=1Nkui2b-WwMW2GdQzIFVW9tGnpe1B9tCr&hl=es" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="widget">
                    <p><b>ESE Hospital del Sur Itagüí “Gabriel Jaramillo Piedrahita”</b></p>
                    <img src="<?php echo site_url(); ?>images/11-layers.png" class="img-responsive" alt="">
                </div>
                <div class="row flex">
                    <!--   ========================== Widget ===========================-->
                    <div class="col-md-6 col-sm-6 col-ss-6 col-xs-12">
                        <div class="widget"> <b>NIT</b>: <?php echo $informacion_general->entidad_informacion_nit; ?><br><b>PBX</b>: <?php echo $entidad_contacto->entidad_contacto_telefono; ?> <br> <b>FAX</b>: <?php echo $entidad_contacto->entidad_contacto_fax; ?> <br>
                            <?php echo $entidad_contacto->entidad_contacto_lineas; ?></div>
                    </div>
                    <!--   ========================== Widget ===========================-->
                    <div class="col-md-6 col-sm-6 col-ss-6 col-xs-12">
                        <div class="widget"> <b>Dirección:</b><br>
                            <?php echo $entidad_contacto->entidad_contacto_direccion; ?>
                            Itagüí - Antioquia <br>


                        </div>
                    </div>
                    <!--   ========================== Widget ===========================-->
                    <div class="col-md-6 col-sm-6 col-ss-6 col-xs-12">
                        <div class="widget"><b>Horario de atención:</b> <br>
                            <?php echo $entidad_contacto->entidad_contacto_horarios_atencion; ?>
                            <br><br><b>Correo electrónico</b> <br>
                            <?php echo $entidad_contacto->entidad_contacto_email; ?><br>
                            <br><b>Correo de notificación judicial</b> <br>
                            <?php echo $entidad_contacto->entidad_contacto_email_notificaciones_judiciales; ?>
                            <div class="share-area footer-social pc clearfix">
                                <?php
                                if ($entidad_redes_sociales != FALSE) {
                                    foreach ($entidad_redes_sociales as $dato) {
                                        echo '<a target="_blank" href="' . $dato->entidad_redes_sociales_link . '" data-toggle="tooltip" data-placement="bottom" title="Ver información en la red social ' . $dato->entidad_redes_sociales_definicion_nombre . '" class="text-capitalize"><i class="fab ' . $dato->entidad_redes_sociales_definicion_imagen . '"></i></a>';
                                    }
                                    echo '<a href="' . site_url('contacto') . '" data-toggle="tooltip" data-placement="bottom" title="Escríbenos"><i class="fa fa-envelope"></i></a>';
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                    <!--   ========================== Widget ===========================-->
                    <div class="col-md-6 col-sm-6 col-ss-6 col-xs-12">
                        <div class="widget">
                            <?php
                            if ($enlaces_footer != FALSE) {
                                foreach ($enlaces_footer as $dato) {
                                    ?>
                                    <a target="<?php echo $dato->entidad_enlaces_footer_tipo_enlace; ?>" href="<?php echo prep_url(str_replace("[URL]", site_url(), $dato->entidad_enlaces_footer_enlace)); ?>" href="http://www.itagui.gov.co"><?php echo $dato->entidad_enlaces_footer_nombre; ?></a> <br>
                                    <?php
                                }
                            }
                            ?>
                            <br>
                            <br>
                            <?php
                            if ($entidad_politicas_datos != FALSE) {
                                foreach ($entidad_politicas_datos as $dato) {
                                    ?>
                                    <a href="<?php echo site_url('uploads/entidad/politicas') . '/' . $dato->entidad_politicas_datos_archivo; ?>" target="_blank"><?php echo $dato->entidad_politicas_datos_nombre; ?></a><br>
                                    <?php
                                }
                            }
                            ?>
                            <p><br>Contador de visitas: <strong><?php echo number_format($visitas); ?></strong></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<!--   ========================== Footer Bottom ===========================-->
<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="copy develop_by">Ultima actualización: <?php echo date('d', strtotime($actualizacion->actualizacion)) . " de " . $meses[date('n', strtotime($actualizacion->actualizacion)) - 1] . " del " . date('Y', strtotime($actualizacion->actualizacion)); ?> <spam class="float-right">Powered by <a target="_blank" href="https://codweb.co"><img src="<?php echo site_url(); ?>images/develop.png" alt="develop_image"></a></spam></div>
            </div>
        </div>
    </div>
</div>

<span id="top-link-block" class="affix">
    <a href="#top" title="Subir pantalla " class="well well-sm" onclick="$('html,body').animate({scrollTop: 0}, 'slow');
        return false;">
        <i class="fa fa-angle-double-up"></i>
    </a>
</span>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script src="<?php echo site_url(); ?>js/custom.js" type="text/javascript"></script>
<?php
if ($configuracion->config_general_script_chat != NULL) {
    echo $configuracion->config_general_script_chat;
}
?>
<script>
        $(function () {
        $('.full_table').dataTable({
        "bPaginate": true,
                "bLengthChange": true,
                "responsive": true,
                "bFilter": true,
                "bSort": true,
<?php
if (isset($shorting)) {
    if ($shorting != NULL) {
        echo "'aaSorting': [[$shorting, '$shorting_type']],";
    }
}
if (isset($search)) {
    if ($search != NULL) {
        echo "'oSearch': {'sSearch': '$search'},";
    }
}
?>
        "bInfo": true,
                "bAutoWidth": false,
                "oLanguage": {
                "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando del _START_ al _END_ de un total de _TOTAL_",
                        "sInfoEmpty": "Mostrando del 0 al 0 de un total de 0",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ )",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                        "sFirst": "Primero",
                                "sLast": "Último",
                                "sNext": "Siguiente",
                                "sPrevious": "Anterior"
                        },
                        "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                }
        });
        });</script>
</body>
</html>

