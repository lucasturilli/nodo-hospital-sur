<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
            <h2><i class="fas fa-circle"></i> Encuestas</h2>
        </div>
        <?php if ($all_survey != FALSE) { ?>
                    <div class="dropdown">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Seleccione la Encuesta 
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach ($all_survey as $dato) {
                                ?>
                                <li><a class="dropdown-item" href="<?php echo site_url('sitio/encuestas') . '/' . $dato->id_survey; ?>"><?php echo $dato->survey_encuesta; ?></a></li>
                            <?php }
                            ?>
                        </ul>
                    </div>
                    <hr>
                    <?php if ($survey != FALSE) { ?>
                        <h3><?php echo $survey->survey_encuesta; ?></h3>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <dl class="dl-horizontal">
                            <dt style="text-align: left;">Fecha de inicio </dt>
                            <dd><?php echo $dias[date('w', strtotime($survey->survey_fecha_publicacion))] . ' ' . date('d', strtotime($survey->survey_fecha_publicacion)) . " de " . $meses[date('n', strtotime($survey->survey_fecha_publicacion)) - 1] . " del " . date('Y', strtotime($survey->survey_fecha_publicacion)); ?></dd>
                            <dt style="text-align: left;">Fecha de finalización </dt>
                            <dd><?php echo $dias[date('w', strtotime($survey->survey_fecha_publicacion))] . ' ' . date('d', strtotime($survey->survey_fecha_expiracion)) . " de " . $meses[date('n', strtotime($survey->survey_fecha_expiracion)) - 1] . " del " . date('Y', strtotime($survey->survey_fecha_expiracion)); ?></dd>
                        </dl>
                        <?php
                        $x = 1;
                        $estilos = array('bg-success ', 'bg-info ', 'bg-warning ', 'bg-danger ');
                        $texto = '<dl>';
                        foreach ($total[0] as $dato) {
                            if ($total[1] == 0) {
                                $total[1] = 1;
                            }
                            $valor = round(($dato->total_pregunta / $total[1]) * 100, 0);
                            $color = $estilos[fmod($x, 4)];
                            $texto .= '
                            <dt>' . $dato->survey_preguntas_pregunta . '</dt>
                            <dd><div class="progress survey_progress">
                                    <div class="progress-bar active ' . $color . ' progress-bar-striped"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: ' . $valor . '%">
                                    </div>
                                </div>
                                <span class="">' . $dato->total_pregunta . ' Votos (' . round(($dato->total_pregunta / $total[1]) * 100, 0) . '%)</span>
                            </dd>';
                            $x++;
                        }
                        $texto .= '</dl><hr style="border:solid; margin-bottom: 10px;"><span class="">Total Votos ' . $total[1] . '</span>';
                        echo $texto;
                    }
                } else {
                    ?>
                    <div class="jumbotron" style=" margin-bottom: 15px; padding: 0 30px;">
                        <h2>Encuestas!</h2>
                        <p>No hay encuestas disponibles espera muchas más próximamente.</p>
                    </div>
                <?php } ?>
     
    </article>
</div>
