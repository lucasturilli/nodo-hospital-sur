<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$flag = FALSE;
?>


<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Búsqueda</a></h3>
        </div>
        <div class="col-md-12" >
            <h3 class="text-center"><i class="fab fa-searchengin"></i> Resultados de la búsqueda por <strong><em>“<?php echo $texto; ?>”</em></strong></h3>
            <hr>
            <!--/*SECCIONES*/-->
            <?php
            if ($secciones != NULL) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Secciones</h4>
                        <ul>
                            <?php foreach ($secciones as $key => $seccion) { ?>
                                <li><a href="<?php echo site_url($key); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', $seccion); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************DEPENDENCIAS/////**************-->
            <?php
            if ($dependencias != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Estructura Administrativa</h4>
                        <ul>
                            <?php foreach ($dependencias as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/dependencias/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', $dato->entidad_dependencia_nombre); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********SUCURSALES ****************-->
            <?php
            if ($sucursales != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Sucursales o Regionales</h4>
                        <ul>
                            <?php foreach ($sucursales as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/directorio_sucursales/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_sucursales_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********ENTIDADES ****************-->
            <?php
            if ($entidades != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Entidades</h4>
                        <ul>
                            <?php foreach ($entidades as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/directorio_entidades/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_entidades_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********AGREMIACIONES ****************-->
            <?php
            if ($agremiaciones != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Agremiaciones, asociaciones y otros grupos de interés</h4>
                        <ul>
                            <?php foreach ($agremiaciones as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/directorio_agremiaciones/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_agremiaciones_nombre . ' - ' . $dato->entidad_agremiaciones_actividad, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********PREGUNTAS FRECUENTES ****************-->
            <?php
            if ($preguntas_frecuentes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Preguntas y Respuestas Frecuentes</h4>
                        <ul>
                            <?php foreach ($preguntas_frecuentes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/preguntas_frecuentes/' . $dato->id_dependencia . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_preguntas_frecuentes_pregunta . ' - ' . strip_tags($dato->entidad_preguntas_frecuentes_respuesta), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********GLOSARIO ****************-->
            <?php
            if ($glosario != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Glosario</h4>
                        <ul>
                            <?php foreach ($glosario as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/glosario/' . NULL . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_glosario_palabra . ' - ' . strip_tags($dato->entidad_glosario_descripcion), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>

            <!--/************NOTICIAS/////**************-->
            <?php
            if ($noticias != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Noticias, notificaciones y anuncios </h4>
                        <ul>
                            <?php foreach ($noticias as $dato) { ?>
                                <li><a  href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias; ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_noticias_titulo . ' - ' . strip_tags($dato->entidad_noticias_descripcion_corta), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********BOLETINES ****************-->
            <?php
            if ($boletines != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Boletines</h4>
                        <ul>
                            <?php foreach ($boletines as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/boletines/' . date('Y', strtotime($dato->entidad_boletines_fecha)) . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_boletines_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************CALENDARIO/////**************-->
            <?php
            if ($calendario != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Actividades y eventos</h4>
                        <ul>
                            <?php foreach ($calendario as $dato) { ?>
                                <li><a  href="<?php echo site_url('sitio/calendario_evento') . '/' . $dato->entidad_calendario_alias; ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_calendario_nombre . ' - ' . strip_tags($dato->entidad_calendario_descripcion), NULL, 200)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************FUNCIONARIOS/////**************-->
            <?php
            if ($funcionarios != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Funcionarios</h4>
                        <ul>
                            <?php foreach ($funcionarios as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/funcionarios/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_funcionario_nombres . ' ' . $dato->entidad_funcionario_apellidos . ' - ' . strip_tags($dato->entidad_funcionario_email) . ' - ' . strip_tags($dato->entidad_funcionario_cargo), NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************MANUAL DE FUNCIONES/////**************-->
            <?php
            if ($manual_funciones != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Manual de funciones y competencias</h4>
                        <ul>
                            <?php foreach ($manual_funciones as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/manual_funciones/' . $dato->id_entidad_definicion_manual_de_funciones . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_manual_de_funciones_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************LEYES/////**************-->
            <?php
            if ($leyes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Leyes / Ordenanzas / Acuerdos</h4>
                        <ul>
                            <?php foreach ($leyes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/leyes/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_leyes_nombre . ' - ' . $dato->normatividad_leyes_tematica, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************DECRETOS/////**************-->
            <?php
            if ($decretos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Decretos</h4>
                        <ul>
                            <?php foreach ($decretos as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/decretos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_decretos_nombre . ' - ' . $dato->normatividad_decretos_tematica, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************CIRCULARES/////**************-->
            <?php
            if ($circulares != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Circulares y/u otros actos administrativos de carácter general</h4>
                        <ul>
                            <?php
                            foreach ($circulares as $dato) {
                                if ($dato->normatividad_resoluciones_tipo != 'Resolución') {
                                    ?>
                                    <li><a  href="<?php echo site_url('entidad/circulares/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_resoluciones_nombre . ' - ' . $dato->normatividad_resoluciones_tematica, NULL, 140)); ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************RESOLUCIONES/////**************-->
            <?php
            if ($resoluciones != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Resoluciones</h4>
                        <ul>
                            <?php
                            foreach ($resoluciones as $dato) {
                                if ($dato->normatividad_resoluciones_tipo == 'Resolución') {
                                    ?>
                                    <li><a  href="<?php echo site_url('entidad/resoluciones/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_resoluciones_nombre . ' - ' . $dato->normatividad_resoluciones_tematica, NULL, 140)); ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************EDICTOS/////**************-->
            <?php
            if ($edictos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Edictos y Avisos</h4>
                        <ul>
                            <?php foreach ($edictos as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/edictos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_edictos_nombre . ' - ' . $dato->normatividad_edictos_tematica, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************ESTADOS FINANCIEROS/////**************-->
            <?php
            if ($estados_financieros != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Estados Financieros</h4>
                        <ul>
                            <?php foreach ($estados_financieros as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/estados_financieros/' . $dato->financiera_estados_financieros_periodo . "/" . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->financiera_estados_financieros_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************ESTATUTO TRIBUTARIO /////**************-->
            <?php
            if ($estatuto_tributario != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Estatuto Tributario</h4>
                        <ul>
                            <?php foreach ($estatuto_tributario as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/presupuesto_estatuto_tributario/' . $dato->financiera_estatuto_tributario_periodo . "/" . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->financiera_estatuto_tributario_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************POLITICAS/////**************-->
            <?php
            if ($politicas != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Políticas / Lineamientos / Manuales</h4>
                        <ul>
                            <?php foreach ($politicas as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/politicas/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->normatividad_politicas_nombre . ' - ' . $dato->normatividad_politicas_tematica, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************ANTICORRUPCUION/////**************-->
            <?php
            if ($anticorrupcion != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Plan Anticorrupción</h4>
                        <ul>
                            <?php foreach ($anticorrupcion as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/plan_anticorrupcion/' . $dato->anticorrupcion_definicion_periodo . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->anticorrupcion_definicion_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************PLAN ACCION/////**************-->
            <?php
            if ($plan_accion != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Plan de Acción</h4>
                        <ul>
                            <?php foreach ($plan_accion as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/plan_accion/' . $dato->id_entidad_dependencia . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_plan_accion_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************PLAN GASTO PUBLICO/////**************-->
            <?php
            if ($plan_gasto_publico != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Plan de Gasto Público</h4>
                        <ul>
                            <?php foreach ($plan_gasto_publico as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/planeacion_plan_gasto_publico/' . $dato->planeacion_plan_gasto_publico_periodo . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->planeacion_plan_gasto_publico_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********PROGRAMAS ****************-->
            <?php
            if ($programas != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Programas y Proyectos en Ejecución</h4>
                        <ul>
                            <?php foreach ($programas as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/programas_proyectos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_programas_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
                <?php
            }
            ?>
            <!--OTROS PLANES *************************************-->
            <?php
            if ($otros_planes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Otros Planes</h4>
                        <ul>
                            <?php foreach ($otros_planes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/planes_otros/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_planes_otros_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--INFORME ENPALME *************************************-->
            <?php
            if ($infore_enpalme != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Empalme</h4>
                        <ul>
                            <?php foreach ($infore_enpalme as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/informe_empalme/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informe_empalme_nombre_archivo, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--INFORME ARCHIVO *************************************-->
            <?php
            if ($gestion_documental != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Gestión Documental</h4>
                        <ul>
                            <?php foreach ($gestion_documental as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/control_archivo/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informe_archivo_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--MIPG PLAN *************************************-->
            <?php
            if ($mipg_plan != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Modelo Integrado de Planeación y Gestión “MIPG”</h4>
                        <ul>
                            <?php foreach ($mipg_plan as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/mipg/1/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_mipg_plan_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--MIPG PLANES *************************************-->
            <?php
            if ($mipg_planes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Modelo Integrado de Planeación y Gestión “MIPG”</h4>
                        <ul>
                            <?php foreach ($mipg_planes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/mipg/2/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_mipg_planes_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--METAS E INDICADORES DE GESTION *************************************-->
            <?php
            if ($metas != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Metas e Indicadores de Gestión</h4>
                        <ul>
                            <?php foreach ($metas as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/indicadores_gestion/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_indicadores_gestion_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--ENTES *************************************-->
            <?php
            if ($entes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Entes de Control que Vigilan la Entidad</h4>
                        <ul>
                            <?php foreach ($entes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/entes/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_entes_nombre . ' - ' . $dato->control_entes_email_contacto, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************AUDITORIAS/////**************-->
            <?php
            if ($auditorias != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Auditorías </h4>
                        <ul>
                            <?php foreach ($auditorias as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/auditorias/' . $dato->control_auditorias_periodo . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_auditorias_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME FISCAL/////**************-->
            <?php
            if ($informe_concejo != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes al Concejo</h4>
                        <ul>
                            <?php foreach ($informe_concejo as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/informe_concejo/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informe_concejo_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME FISCAL/////**************-->
            <?php
            if ($informe_fiscal != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Rendición de Cuenta Fiscal</h4>
                        <ul>
                            <?php foreach ($informe_fiscal as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/informe_fiscal/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informe_fiscal_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME CIUDADANIA/////**************-->
            <?php
            if ($informe_ciudadania != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Rendición de Cuenta a los Ciudadanos</h4>
                        <ul>
                            <?php foreach ($informe_ciudadania as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/informe_ciudadania/1/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informe_ciudadania_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME CIUDADANIA - GESTION/////**************-->
            <?php
            if ($informe_ciudadania_gestion != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Rendición de Cuenta a los Ciudadanos</h4>
                        <ul>
                            <?php foreach ($informe_ciudadania_gestion as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/informe_ciudadania/2/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informes_gestion_ciudadania_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME CIUDADANIA - EVALUACION/////**************-->
            <?php
            if ($informe_ciudadania_evaluacion != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Rendición de Cuenta a los Ciudadanos</h4>
                        <ul>
                            <?php foreach ($informe_ciudadania_evaluacion as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/informe_ciudadania/4/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informes_ciudadania_evaluacion_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/************INFORME CIUDADANIA - CRONOGRAMA/////**************-->
            <?php
            if ($informes_cronograma != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Informes de Rendición de Cuenta a los Ciudadanos</h4>
                        <ul>
                            <?php foreach ($informes_cronograma as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/informe_ciudadania/3/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_informes_cronograma_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//PLANES DE MEJORAMIENTO ******************************************-->
            <?php
            if ($planes_mejoramiento != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Planes de Mejoramiento</h4>
                        <ul>
                            <?php foreach ($planes_mejoramiento as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/planes_mejoramiento/' . $dato->control_planes_mejoramiento_periodo . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_planes_mejoramiento_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//REPORTES CONTROL INTERNO ******************************************-->
            <?php
            if ($reportes_control_interno != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Reportes de Control Interno</h4>
                        <ul>
                            <?php foreach ($reportes_control_interno as $dato) { ?>
                                <li><a  href="<?php echo site_url("entidad/reportes_control_interno/$dato->id_control_reportes_control_interno_tab/$dato->control_reportes_control_interno_periodo/$texto"); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_reportes_control_interno_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//PROGRMAMS POBLACION VULNERABLE ******************************************-->
            <?php
            if ($poblacion_vulnerable_programas != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Programas para la Población Vulnerable</h4>
                        <ul>
                            <?php foreach ($poblacion_vulnerable_programas as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/programas_poblacion_vulnerable/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_poblacion_vulnerable_programas_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//PROYECTOS POBLACION VULNERABLE ******************************************-->
            <?php
            if ($poblacion_vulnerable_proyectos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Proyectos para la Población Vulnerable</h4>
                        <ul>
                            <?php foreach ($poblacion_vulnerable_proyectos as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/proyectos_poblacion_vulnerable/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_poblacion_vulnerable_proyectos_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********PROGRAMAS SOCIALES ****************-->
            <?php
            if ($programas_sociales != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Programas Sociales</h4>
                        <ul>
                            <?php foreach ($programas_sociales as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/index/programas_sociales/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_programas_sociales_programa, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/***********OTROS INFORMES****************-->
            <?php
            if ($otros_informes != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Otros Informes</h4>
                        <ul>
                            <?php foreach ($otros_informes as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/otros_informes/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->control_otros_informes_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/PLAN COMPRAS**********************************************-->
            <?php
            if ($plan_compras != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Plan de Adquisiciones</h4>
                        <ul>
                            <?php foreach ($plan_compras as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/plan_compras/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->contratacion_plan_compras_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/CONTRATOS**********************************************-->
            <?php
            if ($contratos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Contratos</h4>
                        <ul>
                            <?php foreach ($contratos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/contratos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras('Proceso # ' . $dato->contratacion_contratos_numero_proceso . ' - ' . $dato->contratacion_contratos_objeto, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//AVANCES CONTRACTUALES ******************************************-->
            <?php
            if ($avances_contractuales != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Avances Contractuales</h4>
                        <ul>
                            <?php foreach ($avances_contractuales as $dato) { ?>
                                <li><a  href="<?php echo site_url('entidad/avance_contractual/' . $dato->contratacion_avance_contractual_periodo . '/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->contratacion_avance_contractual_nombre, NULL, 140)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//CONVENIOS******************************************-->
            <?php
            if ($convenios != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Convenios</h4>
                        <ul>
                            <?php foreach ($convenios as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/convenios/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->contratacion_convenios_numero_proceso . ' - ' . $dato->contratacion_convenios_objeto, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//LINEAMIENTOS******************************************-->
            <?php
            if ($lineamientos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Lineamientos de Adquisiciones y Compras</h4>
                        <ul>
                            <?php foreach ($lineamientos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/contratacion_lineamientos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->contratacion_lineamientos_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/TRAMITES**********************************************-->
            <?php
            if ($tramites != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Trámites y Servicios</h4>
                        <ul>
                            <?php foreach ($tramites as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/tramites/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->servicios_tramites_nombre . ' - ' . strip_tags($dato->servicios_tramites_descripcion), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/TRAMITES EN LINEA**********************************************-->
            <?php
            if ($tramites_en_linea != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Trámites y Servicios en línea</h4>
                        <ul>
                            <?php foreach ($tramites_en_linea as $dato) { ?>
                                <li><a href="<?php echo site_url("entidad/tramites_en_linea/$dato->id_entidad_dependencia/" . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->servicios_tramites_en_linea_nombre . ' - ' . strip_tags($dato->servicios_tramites_en_linea_descripcion), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/FORMATOS**********************************************-->
            <?php
            if ($formatos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Formularios de Trámites y Servicios</h4>
                        <ul>
                            <?php foreach ($formatos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/index/formularios_tramites/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->servicios_formatos_nombre . ' - ' . strip_tags($dato->servicios_formatos_descripcion), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--/ARCHVISO DATOS ABIERTOS**********************************************-->
            <?php
            if ($archivos_datos_abiertos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Datos Abiertos</h4>
                        <ul>
                            <?php foreach ($archivos_datos_abiertos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/datos_abiertos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->archivos_datos_abiertos_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>
            <!--//DOCUMENTOS ****************************-->
            <?php
            if ($documentos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Documentos de la Alcaldía</h4>
                        <ul>
                            <?php foreach ($documentos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/documentos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_documentos_nombre . ' - ' . strip_tags($dato->entidad_documentos_descripcion), NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>

            <!--//CATACTERIZACION PROCESOS ****************************-->
            <?php
            if ($caracterizacion_procesos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Caracterización de Procesos</h4>
                        <ul>
                            <?php foreach ($caracterizacion_procesos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/caracterizacion_procesos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_caracterizacion_procesos_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>

            <!--//PROCEDIMIENTOS****************************-->
            <?php
            if ($procedimientos != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Procedimientos</h4>
                        <ul>
                            <?php foreach ($procedimientos as $dato) { ?>
                                <li><a href="<?php echo site_url('entidad/procedimientos/' . $texto); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->entidad_procedimientos_nombre, NULL, 150)); ?></a></li>
                            <?php } ?>
                        </ul>                                </div>
                </div>
            <?php } ?>

            <!--            //DOCUEMNTOS MUNICIPIO ****************************
            -->

            <?php
            if ($documentos_municipio != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Documentos de la Alcaldía</h4>
                        <ul>
                            <?php foreach ($documentos_municipio as $dato) { ?>

                                <li><a target="_blank" href="<?php echo site_url('uploads/municipio/documentos') . '/' . $dato->municipio_documentos_archivo; ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->municipio_documentos_nombre . ' - ' . $dato->municipio_documentos_descripcion, NULL, 150)); ?></a></li>

                            <?php } ?>
                        </ul>                                </div>
                </div>

                <?php
            }
            ?>


            <!--/***********MAPAS ****************-->

            <?php
            if ($mapas != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Mapas</h4>
                        <ul>
                            <?php foreach ($mapas as $dato) { ?>

                                <li><a  href="<?php echo site_url('municipio/mapas'); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->municipio_galeria_mapas_nombre, NULL, 150)); ?></a></li>

                            <?php } ?>
                        </ul>                                </div>
                </div>

                <?php
            }
            ?>


            <!--/***********SITIOS INTEREZ ****************-->

            <?php
            if ($sitios != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Sitios de Interés</h4>
                        <ul>
                            <?php foreach ($sitios as $dato) { ?>

                                <li><a  href="<?php echo site_url('municipio/sitios'); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->municipio_sitios_interes_nombre, NULL, 150)); ?></a></li>

                            <?php } ?>
                        </ul>                                </div>
                </div>

                <?php
            }
            ?>

            <!--/***********SITIOS INTEREZ ****************-->

            <?php
            if ($territorios != FALSE) {
                $flag = TRUE;
                ?>
                <div class="col-md-12 column text-left">
                    <div class="alert alert-secondary" role="alert"><h4>Territorios</h4>
                        <ul>
                            <?php foreach ($territorios as $dato) { ?>

                                <li><a  href="<?php echo site_url('municipio/territorios'); ?>"><?php echo str_ireplace($texto, '<span style="color:#990100;"><strong><em>' . $texto . '</em></strong></span>', limitarPalabras($dato->municipio_territorios_nombre . ' - ' . $dato->municipio_territorios_tipo, NULL, 150)); ?></a></li>

                            <?php } ?>
                        </ul>                                </div>

                </div>

                <?php
            }
            ?>
            <?php if ($flag == FALSE) { ?>
                <div class="alert alert-primary" role="alert">
                    <h4><i class="fa fa-exclamation"></i> No se encontraron resultados, por favor intenta de nuevo con otra palabra o texto clave </h4>
                </div>
            <?php } ?>

            <div class="col-md-12 column text-center ">
                <hr>
                <p>Si la documentación buscada no es listada, por favor utilice el siguiente formulario de búsqueda avanzada.</p>
                <div class="alert alert-secondary" role="alert"><h4>Búsqueda Avanzada</h4>
                    <script async src="https://cse.google.com/cse.js?cx=001723670691183947285:4wel63vff1z"></script>
                    <div class="gcse-search"></div>
                </div>
            </div>
        </div>
    </article>
</div>

