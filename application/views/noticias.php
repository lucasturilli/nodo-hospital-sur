<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Noticias</a></h3>
        </div>
        <?php
        $flag = FALSE;
        if ($noticias != FALSE) {
            $x = 0;
            ?>
            <div class="row">
                <?php
                foreach ($noticias as $dato) {
                    if ($x < ($paginas + 12) && $x >= $paginas) {
                        if (fmod($x, 4) == 0) {
                            echo '<div class="row">';
                        }
                        ?>
                        <div class="col-md-3 col-sm-6 col-ss-6 col-xs-12">
                            <div class="notice-content">
                                <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><img src="<?php echo site_url('uploads/noticias') . '/' . $dato->entidad_noticias_imagen; ?>" alt=""></a>
                                <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>" class="title2" href="#"><?php echo $dato->entidad_noticias_titulo; ?></a>
                                <p><?php echo limitarPalabras($dato->entidad_noticias_descripcion_corta, 30, 80); ?></p>
                                <div class="social flex">
                                    <div class="share">
                                        <a data-toggle="tooltip" title="Compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><i class="fab fa-facebook-f"></i></a>
                                        <a data-toggle="tooltip" title="Compartir" href="https://twitter.com/intent/tweet?text=E.S.E Hospital del Sur <?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?> vía <?php echo $seo->entidad_seo_twitter_vcard; ?>"><i class="fab fa-twitter"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    $x++;
                                        if (fmod($x, 4) == 0) {
                        echo '</div>';
                    }
                }
                ?>
            </div><?php
        } else {
            ?>
            <div class="row">
                <div class="jumbotron">
                    <h2>Noticias!</h2>
                    <p>Espera muchas más noticias próximamente.</p>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <?php echo $paginacion; ?>
            </div>
        </div>
    </article>
</div>
