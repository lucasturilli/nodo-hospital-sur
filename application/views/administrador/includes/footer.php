<footer>
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-inverse navbar-fixed-bottom"  role="navigation">
                <ul class="nav navbar-nav">
                    <li>
                        <p class="navbar-text"><b>Copyright © <a class="navbar-link" target="_blank" href="http://codweb.co">Codweb</a></b></p>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <p class="navbar-text"> CI <?php echo CI_VERSION; ?> Page rendered in <strong>{elapsed_time}</strong> seconds &nbsp&nbsp</p> 
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</footer>
</body>
</html>