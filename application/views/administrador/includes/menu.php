<?php
$permiso = array();
if ($permisos != FALSE) {
    foreach ($permisos as $row) {
        array_push($permiso, $row->id_permisos);
    }
}
if ($menu_lateral == TRUE) {
    ?>
    <div class="col-md-3">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 10px;">
            <li class="<?php echo ($menu == 'entidad') ? 'active' : ''; ?>"><a href="#entidad" role="tab" data-toggle="tab">Información Entidad</a></li>
            <li class="<?php echo ($menu == 'personeria') ? 'active' : ''; ?>"><a href="#personeria" role="tab" data-toggle="tab">Aplicaciones</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <?php
        }
        if (!isset($tab)) {
            $tab = 0;
        }
        $permiso = array();
        if ($permisos != FALSE) {
            foreach ($permisos as $row) {
                array_push($permiso, $row->id_permisos);
            }
        }
        ?>
        <!--/*********INFORMACION ENTIDAD********************/-->
        <div class="tab-pane <?php echo ($menu == 'entidad') ? 'active' : ''; ?>" id="entidad">
            <div class=" sidebar">
                <div class="panel-group" id="accordion">
                    <?php if (in_array(1, $permiso) || in_array(2, $permiso) || in_array(3, $permiso) || in_array(4, $permiso) || in_array(5, $permiso) || in_array(6, $permiso) || in_array(7, $permiso) || in_array(8, $permiso) || in_array(9, $permiso) || in_array(10, $permiso) || in_array(11, $permiso) || in_array(12, $permiso) || in_array(13, $permiso)) { ?>
                        <?php if (in_array(1, $permiso) || in_array(2, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                1. INFORMACIÓN GENERAL DE LA ENTIDAD
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse <?php echo ($tab > 0 && $tab <= 50) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_informacion" class="list-group-item <?php echo ($tab == 1) ? 'active' : ''; ?>"><strong>1.1</strong> INFORMACIÓN GENERAL</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_contacto" class="list-group-item <?php echo ($tab == 2) ? 'active' : ''; ?>"><strong>1.2</strong> DATOS DE CONTACTO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_politicas_datos" class="list-group-item <?php echo ($tab == 3) ? 'active' : ''; ?>"><strong>1.3</strong> POLÍTICAS Y PROTECCIÓN DE DATOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_redes_sociales" class="list-group-item <?php echo ($tab == 9) ? 'active' : ''; ?>"><strong>1.4</strong> REDES SOCIALES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_dependencia" class="list-group-item <?php echo ($tab == 5) ? 'active' : ''; ?>"><strong>1.5</strong> DEPENDENCIAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_sucursales" class="list-group-item <?php echo ($tab == 6) ? 'active' : ''; ?>"><strong>1.6</strong> SUCURSALES O REGIONALES </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_entidades" class="list-group-item <?php echo ($tab == 7) ? 'active' : ''; ?>"><strong>1.7</strong> DIRECTORIO DE ENTIDADES  </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_agremiaciones" class="list-group-item <?php echo ($tab == 8) ? 'active' : ''; ?>"><strong>1.8</strong> DIRECTORIO DE AGREMIACIONES  </a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(3, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                2. SERVICIOS DE INFORMACIÓN
                                            </a> </strong>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse <?php echo ($tab > 50 && $tab <= 100 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_preguntas_frecuentes" class="list-group-item <?php echo ($tab == 51) ? 'active' : ''; ?>"><strong>2.1</strong> PREGUNTAS FRECUENTES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_glosario" class="list-group-item <?php echo ($tab == 52) ? 'active' : ''; ?>"><strong>2.2</strong> GLOSARIO </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_noticias" class="list-group-item <?php echo ($tab == 53) ? 'active' : ''; ?>"><strong>2.3</strong> NOTICIAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_calendario" class="list-group-item <?php echo ($tab == 54) ? 'active' : ''; ?>"><strong>2.4</strong> CALENDARIO DE ACTIVIDADES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_ofertas_empleo" class="list-group-item <?php echo ($tab == 55) ? 'active' : ''; ?>"><strong>2.5</strong> OFERTAS DE EMPLEO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_boletines" class="list-group-item <?php echo ($tab == 56) ? 'active' : ''; ?>"><strong>2.6</strong> BOLETINES</a>
        <!--                                        <a style="font-size: 12px;" href="<?php //echo site_url();               ?>administrador/datos_entidad/entidad_periodico" class="list-group-item <?php //echo ($tab == 57) ? 'active' : '';               ?>"><strong>2.7</strong> PERIÓDICO</a>-->
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/convocatorias" class="list-group-item <?php echo ($tab == 58) ? 'active' : ''; ?>"><strong>2.7</strong> CONVOCATORIAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/convocatorias_anexos" class="list-group-item <?php echo ($tab == 61) ? 'active' : ''; ?>"><strong>2.7.1</strong> CONVOCATORIAS ANEXOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/estudios" class="list-group-item <?php echo ($tab == 59) ? 'active' : ''; ?>"><strong>2.8</strong> ESTUDIOS, INVESTIGACIONES Y OTRAS PUBLICACIONES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/informacion_adicional" class="list-group-item <?php echo ($tab == 60) ? 'active' : ''; ?>"><strong>2.9</strong> INFORMACIÓN ADICIONAL </a>
                                    </div>
                                </div>
                            </div>

                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(4, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo2">
                                                3. TALENTO HUMANO
                                            </a> </strong>
                                    </h4>
                                </div>
                                <div id="collapseTwo2" class="panel-collapse <?php echo ($tab > 100 && $tab <= 150 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_funcionario" class="list-group-item <?php echo ($tab == 101) ? 'active' : ''; ?>"><strong>3.1</strong> FUNCIONARIOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_definicion_manual_de_funciones" class="list-group-item <?php echo ($tab == 106) ? 'active' : ''; ?>"><strong>3.2</strong> TIPOS DE MANUAL DE FUNCIONES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_manual_de_funciones" class="list-group-item <?php echo ($tab == 102) ? 'active' : ''; ?>"><strong>3.2.1</strong> MANUAL DE FUNCIONES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_asignaciones_salarial" class="list-group-item <?php echo ($tab == 103) ? 'active' : ''; ?>"><strong>3.3</strong> ASIGNACIONES SALARIALES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_planta_personal" class="list-group-item <?php echo ($tab == 104) ? 'active' : ''; ?>"><strong>3.4</strong> LISTADO DE PERSONAL DE PLANTA</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_evaluacion_desempeno" class="list-group-item <?php echo ($tab == 105) ? 'active' : ''; ?>"><strong>3.5</strong> EVALUACIÓN DE DESEMPEÑO GENERAL</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(5, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong> <a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                4. INFORMACIÓN PARA NIÑOS
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="panel-collapse <?php echo ($tab > 150 && $tab <= 200) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" target="_blank" href="<?php echo ($configuracion->config_general_portal_ninos != NULL) ? $configuracion->config_general_portal_ninos : '#' ?>" class="list-group-item "><strong>4.1</strong> ADMINISTRAR SITIO</a>
        <!--                                        <a style="font-size: 12px;" href="<?php //echo site_url();                                 ?>administrador/datos_entidad/entidad_informacion_ninos" class="list-group-item <?php //echo ($tab == 151) ? 'active' : '';                                 ?>"><strong>4.1</strong> INFORMACIÓN GENERAL</a>
                                        <a style="font-size: 12px;" href="<?php //echo site_url();                                 ?>administrador/datos_entidad/entidad_noticias_ninos" class="list-group-item <?php //echo ($tab == 152) ? 'active' : '';                                 ?>"><strong>4.2</strong> NOTICIAS </a>
                                        <a style="font-size: 12px;" href="<?php //echo site_url();                                 ?>administrador/datos_entidad/entidad_calendario_ninos" class="list-group-item <?php //echo ($tab == 153) ? 'active' : '';                                 ?>"><strong>4.3</strong> CALENDARIO DE ACTIVIDADES</a>
                                        <a style="font-size: 12px;" href="<?php //echo site_url();                                 ?>administrador/datos_entidad/entidad_juegos_ninos" class="list-group-item <?php //echo ($tab == 154) ? 'active' : '';                                 ?>"><strong>4.4</strong> JUEGOS</a>
                                        <a style="font-size: 12px;" href="<?php //echo site_url();                                 ?>administrador/datos_entidad/entidad_sitios_ninos" class="list-group-item <?php //echo ($tab == 155) ? 'active' : '';                                 ?>"><strong>4.5</strong> SITIOS RECOMENDADOS</a>
                                        --> </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(6, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong> <a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                                5. NORMATIVIDAD
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="panel-collapse <?php echo ($tab > 200 && $tab <= 250) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/normatividad/normatividad_leyes" class="list-group-item <?php echo ($tab == 201) ? 'active' : ''; ?>"><strong>5.1</strong> LEYES / ORDENANZAS / ACUERDOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/normatividad/normatividad_decretos" class="list-group-item <?php echo ($tab == 202) ? 'active' : ''; ?>"><strong>5.2</strong> DECRETOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/normatividad/normatividad_resoluciones" class="list-group-item <?php echo ($tab == 203) ? 'active' : ''; ?>"><strong>5.3</strong> RESOLUCIONES / CIRCULARES / OTROS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/normatividad/normatividad_edictos" class="list-group-item <?php echo ($tab == 205) ? 'active' : ''; ?>"><strong>5.4</strong> EDICTOS</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(7, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                                                6. PRESUPUESTO 
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="panel-collapse <?php echo ($tab > 250 && $tab <= 300 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/financiera/financiera_presupuesto" class="list-group-item <?php echo ($tab == 251) ? 'active' : ''; ?>"><strong>6.1</strong> PRESUPUESTO APROBADO EN EJERCICIO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/financiera/financiera_presupuesto_ejecucion" class="list-group-item <?php echo ($tab == 252) ? 'active' : ''; ?>"><strong>6.1.1</strong> EJECUCIÓN DEL PRESUPUESTO</a>
                <!--                                <a style="font-size: 12px;" href="<?php // echo site_url();                                                         ?>administrador/financiera/financiera_historico_presupuesto" class="list-group-item <?php // echo ($tab == 253) ? 'active' : '';                                                         ?>"><strong>6.2</strong> INFORMACIÓN HISTÓRICA DE PRESUPUESTOS</a>-->
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/financiera/financiera_estados_financieros" class="list-group-item <?php echo ($tab == 254) ? 'active' : ''; ?>"><strong>6.2</strong> ESTADOS FINANCIEROS</a>
        <!--                                        <a style="font-size: 12px;" href="<?php //echo site_url();               ?>administrador/financiera/financiera_estatuto_tributario" class="list-group-item <?php //echo ($tab == 255) ? 'active' : '';               ?>"><strong>6.3</strong> ESTATUTO TRIBUTARIO</a>-->
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(8, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                                7. PLANEACIÓN 
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="panel-collapse <?php echo ($tab > 300 && $tab <= 350) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/normatividad_politicas" class="list-group-item <?php echo ($tab == 301) ? 'active' : ''; ?>"><strong>7.1</strong> POLÍTICAS / LINEAMIENTOS / MANUALES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/anticorrupcion_definicion" class="list-group-item <?php echo ($tab == 302) ? 'active' : ''; ?>"><strong>7.2</strong> PLAN ANTICORRUPCIÓN </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/anticorrupcion_definicion_tipo" class="list-group-item <?php echo ($tab == 320) ? 'active' : ''; ?>"><strong>7.2.1</strong> TIPOS DE PLAN ANTICORRUPCIÓN </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/anticorrupcion_definicion_anexos" class="list-group-item <?php echo ($tab == 321) ? 'active' : ''; ?>"><strong>7.2.2</strong> ANEXOS AL PLAN ANTICORRUPCIÓN </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_plan_estrategico" class="list-group-item <?php echo ($tab == 304) ? 'active' : ''; ?>"><strong>7.3</strong> PLAN DE DESARROLLO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_plan_estrategico_avances" class="list-group-item <?php echo ($tab == 322) ? 'active' : ''; ?>"><strong>7.3.1</strong> ANEXOS AL PLAN DE DESARROLLO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_plan_accion" class="list-group-item <?php echo ($tab == 305) ? 'active' : ''; ?>"><strong>7.4</strong> PLAN DE ACCIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_plan_accion_avances" class="list-group-item  <?php echo ($tab == 306) ? 'active' : ''; ?>"><strong>7.4.1</strong> AVANCES DEL PLAN DE ACCIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_programas" class="list-group-item <?php echo ($tab == 307) ? 'active' : ''; ?>"><strong>7.5</strong> PROGRAMAS Y PROYECTOS EN EJECUCIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_programas_avances" class="list-group-item  <?php echo ($tab == 308) ? 'active' : ''; ?>"><strong>7.5.1</strong> AVANCES DE LOS PROGRAMAS Y PROYECTOS EN EJECUCIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_indicadores_gestion" class="list-group-item <?php echo ($tab == 309) ? 'active' : ''; ?>"><strong>7.6</strong> METAS E INDICADORES DE GESTIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_planes_otros" class="list-group-item <?php echo ($tab == 310) ? 'active' : ''; ?>"><strong>7.7</strong> OTROS PLANES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_informe_empalme" class="list-group-item <?php echo ($tab == 311) ? 'active' : ''; ?>"><strong>7.8</strong> INFORMES DE EMPALME</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_informe_empalme_anexos" class="list-group-item <?php echo ($tab == 312) ? 'active' : ''; ?>"><strong>7.8.1</strong> ANEXOS DE INFORMES DE EMPALME</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_informe_archivo_categoria" class="list-group-item <?php echo ($tab == 313) ? 'active' : ''; ?>"><strong>7.9.1</strong> CATEGORIAS ARCHIVO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_informe_archivo_tab" class="list-group-item <?php echo ($tab == 314) ? 'active' : ''; ?>"><strong>7.9.2</strong> PESTAÑAS ARCHIVO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/control_informe_archivo" class="list-group-item <?php echo ($tab == 315) ? 'active' : ''; ?>"><strong>7.9.3</strong> INFORMES DE ARCHIVO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/planeacion/planeacion_plan_gasto_publico" class="list-group-item <?php echo ($tab == 316) ? 'active' : ''; ?>"><strong>7.10</strong> PLAN DE GASTO PUBLICO</a>
        <!--                                        <a style="font-size: 12px;" href="<?php // echo site_url();           ?>administrador/adicional/entidad_mipg_plan" class="list-group-item <?php // echo ($tab == 317) ? 'active' : '';           ?>"><strong>7.11</strong> PLAN DE ACCIÓN MIPG</a>
                                        <a style="font-size: 12px;" href="<?php // echo site_url();           ?>administrador/adicional/entidad_mipg_plan_avances" class="list-group-item <?php // echo ($tab == 318) ? 'active' : '';           ?>"><strong>7.11.1</strong> AVANCES PLAN DE ACCIÓN MIPG</a>
                                        <a style="font-size: 12px;" href="<?php // echo site_url();           ?>administrador/adicional/entidad_mipg_planes" class="list-group-item <?php // echo ($tab == 319) ? 'active' : '';           ?>"><strong>7.12</strong> PLANES DE MIPG</a>-->
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (in_array(1, $permiso) || in_array(20, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseSix2">
                                                8. CONTROL 
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseSix2" class="panel-collapse <?php echo ($tab > 350 && $tab <= 400) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_entes" class="list-group-item <?php echo ($tab == 351) ? 'active' : ''; ?>"><strong>8.1</strong> ENTES DE CONTROL QUE VIGILAN LA ENTIDAD</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_auditorias" class="list-group-item <?php echo ($tab == 352) ? 'active' : ''; ?>"><strong>8.2</strong> AUDITORIAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informe_concejo" class="list-group-item <?php echo ($tab == 353) ? 'active' : ''; ?>"><strong>8.3</strong> INFORME AL  CONGRESO/ASAMBLEA/CONCEJO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informe_ciudadania" class="list-group-item <?php echo ($tab == 354) ? 'active' : ''; ?>"><strong>8.4</strong> INFORME A LOS CIUDADANOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informe_ciudadania_anexos" class="list-group-item <?php echo ($tab == 368) ? 'active' : ''; ?>"><strong>8.4.1</strong> ANEXOS A LOS INFORME A LOS CIUDADANOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informes_gestion_ciudadania" class="list-group-item <?php echo ($tab == 367) ? 'active' : ''; ?>"><strong>8.4.2</strong> INFORME DE GESTIÓN  A LOS CIUDADANOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informes_cronograma" class="list-group-item <?php echo ($tab == 369) ? 'active' : ''; ?>"><strong>8.4.3</strong> CRONOGRAMA DE RENDICIÓN DE CUENTAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informes_ciudadania_evaluacion" class="list-group-item <?php echo ($tab == 370) ? 'active' : ''; ?>"><strong>8.4.4</strong> EVALUACION DE RENDICIÓN DE CUENTAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informes_anexos" class="list-group-item <?php echo ($tab == 371) ? 'active' : ''; ?>"><strong>8.4.4.1</strong> ANEXOS EVALUACION DE RENDICIÓN DE CUENTAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_informe_fiscal" class="list-group-item <?php echo ($tab == 366) ? 'active' : ''; ?>"><strong>8.5</strong> INFORMES DE RENDICIÓN FISCAL</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_planes_mejoramiento" class="list-group-item <?php echo ($tab == 355) ? 'active' : ''; ?>"><strong>8.6</strong> PLANES DE MEJORAMIENTO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_reportes_control_interno" class="list-group-item <?php echo ($tab == 356) ? 'active' : ''; ?>"><strong>8.7</strong> REPORTES DE CONTROL INTERNO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_reportes_control_interno_tab" class="list-group-item <?php echo ($tab == 372) ? 'active' : ''; ?>"><strong>8.7.1</strong> TIPOS DE REPORTES DE CONTROL INTERNO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_poblacion_vulnerable" class="list-group-item <?php echo ($tab == 357) ? 'active' : ''; ?>"><strong>8.8</strong> INFORMACIÓN PARA POBLACIÓN VULNERABLE</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_poblacion_vulnerable_programas" class="list-group-item <?php echo ($tab == 358) ? 'active' : ''; ?>"><strong>8.9</strong> PROGRAMAS PARA POBLACIÓN VULNERABLE</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_poblacion_vulnerable_programas_avances" class="list-group-item <?php echo ($tab == 359) ? 'active' : ''; ?>"><strong>8.9.1</strong> AVANCES DE LOS PROGRAMAS PARA POBLACIÓN VULNERABLE</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_poblacion_vulnerable_proyectos" class="list-group-item <?php echo ($tab == 360) ? 'active' : ''; ?>"><strong>8.10</strong> PROYECTOS PARA POBLACIÓN VULNERABLE</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_poblacion_vulnerable_proyectos_avances" class="list-group-item <?php echo ($tab == 361) ? 'active' : ''; ?>"><strong>8.10.1</strong> AVANCES DE LOS PROYECTOS PARA POBLACIÓN VULNERABLE</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_programas_sociales" class="list-group-item <?php echo ($tab == 362) ? 'active' : ''; ?>"><strong>8.11</strong> PROGRAMAS SOCIALES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_defensa_judicial" class="list-group-item <?php echo ($tab == 363) ? 'active' : ''; ?>"><strong>8.12</strong> DEFENSA JUDICIAL</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_otros_informes" class="list-group-item <?php echo ($tab == 365) ? 'active' : ''; ?>"><strong>8.13</strong> OTROS INFORMES</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/control/control_anuario_estadistico" class="list-group-item <?php echo ($tab == 373) ? 'active' : ''; ?>"><strong>8.14</strong> ANUARIO ESTADÍSTICO </a>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (in_array(1, $permiso) || in_array(9, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                                                9. CONTRATACIÓN
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseSeven" class="panel-collapse <?php echo ($tab > 400 && $tab <= 450 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/contratacion/contratacion_plan_compras" class="list-group-item <?php echo ($tab == 401) ? 'active' : ''; ?>"><strong>9.1</strong> PLAN DE COMPRAS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/contratacion/contratacion_contratos_secop" class="list-group-item <?php echo ($tab == 403) ? 'active' : ''; ?>"><strong>9.2</strong> INFORMACIÓN SECOP</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/contratacion/contratacion_contratos" class="list-group-item <?php echo ($tab == 404) ? 'active' : ''; ?>"><strong>9.3</strong> PROCESOS DE CONTRATACIÓN</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/contratacion/contratacion_avance_contractual" class="list-group-item <?php echo ($tab == 406) ? 'active' : ''; ?>"><strong>9.3.1</strong> AVANCES CONTRACTUALES </a>
                                        <!--<a style="font-size: 12px;" href="<?php //echo site_url();               ?>administrador/contratacion/contratacion_convenios" class="list-group-item <?php //echo ($tab == 405) ? 'active' : '';               ?>"><strong>9.4</strong> CONVENIOS</a>-->
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/contratacion/contratacion_lineamientos" class="list-group-item <?php echo ($tab == 407) ? 'active' : ''; ?>"><strong>9.4</strong> LINEAMIENTOS DE ADQUISICIONES  Y COMPRAS</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(10, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                                                10. TRÁMITES Y SERVICIOS 
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseEight" class="panel-collapse <?php echo ($tab > 450 && $tab <= 500 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/servicios/servicios_tramites" class="list-group-item <?php echo ($tab == 451) ? 'active' : ''; ?>"><strong>10.1</strong> LISTADO DE TRÁMITES Y SERVICIOS </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/servicios/servicios_tramites_en_linea" class="list-group-item <?php echo ($tab == 452) ? 'active' : ''; ?>"><strong>10.2</strong> TRAMITES EN LÍNEA </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/servicios/servicios_formatos" class="list-group-item <?php echo ($tab == 453) ? 'active' : ''; ?>"><strong>10.3</strong> FORMULARIOS DE TRAMITES </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/servicios/servicios_pqrdf" class="list-group-item <?php echo ($tab == 455) ? 'active' : ''; ?>"><strong>10.4</strong> INFORME DE PETICIONES, QUEJAS Y RECLAMOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/servicios/servicios_pqrdf_anexos" class="list-group-item <?php echo ($tab == 456) ? 'active' : ''; ?>"><strong>10.4.1</strong> ANEXOS A AL INFORME DE PETICIONES, QUEJAS Y RECLAMOS</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(11, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseEight8">
                                                11. CALIDAD
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseEight8" class="panel-collapse <?php echo ($tab > 500 && $tab <= 550 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/calidad/entidad_calidad" class="list-group-item <?php echo ($tab == 501) ? 'active' : ''; ?>"><strong>11.1</strong> SISTEMA DE CALIDAD </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/calidad/entidad_caracterizacion_procesos" class="list-group-item <?php echo ($tab == 502) ? 'active' : ''; ?>"><strong>11.2</strong> CARACTERIZACIÓN DE PROCESOS  </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/calidad/entidad_procedimientos" class="list-group-item <?php echo ($tab == 503) ? 'active' : ''; ?>"><strong>11.3</strong> PROCEDIMIENTOS </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(12, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseEightabiertos">
                                                12. DATOS ABIERTOS
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseEightabiertos" class="panel-collapse <?php echo ($tab > 550 && $tab <= 600 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_abiertos/catalogo_datos_abiertos" class="list-group-item <?php echo ($tab == 551) ? 'active' : ''; ?>"><strong>12.1</strong> Catalogo de Datos Abiertos </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_abiertos/archivos_datos_abiertos" class="list-group-item <?php echo ($tab == 552) ? 'active' : ''; ?>"><strong>12.2</strong> Archivos de Datos Abiertos </a>
                                        <!--<a style="font-size: 12px;" href="<?php // echo site_url();               ?>administrador/datos_abiertos/mapas_datos_abiertos" class="list-group-item <?php //echo ($tab == 556) ? 'active' : '';               ?>"><strong>12.3</strong> Mapas de Datos Abiertos </a>-->
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_abiertos/registro_activos_informacion" class="list-group-item <?php echo ($tab == 553) ? 'active' : ''; ?>"><strong>12.3</strong> Registro de Activos de Información</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_abiertos/informacion_clasificada_reservada" class="list-group-item <?php echo ($tab == 554) ? 'active' : ''; ?>"><strong>12.4</strong> Índice de Información Clasificada y Reservada</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_abiertos/esquema_publicacion" class="list-group-item <?php echo ($tab == 555) ? 'active' : ''; ?>"><strong>12.5</strong> Esquema de Publicación</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(13, $permiso)) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                                                13. DOCUMENTOS DE LA ENTIDAD
                                            </a></strong>
                                    </h5>
                                </div>
                                <div id="collapseNine" class="panel-collapse <?php echo ($tab > 600 && $tab <= 650 ) ? 'collapse in' : 'collapse'; ?>">
                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_tipo_documentos" class="list-group-item <?php echo ($tab == 601) ? 'active' : ''; ?>"><strong>13.1</strong> TIPO DE DOCUMENTOS </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/datos_entidad/entidad_documentos" class="list-group-item <?php echo ($tab == 602) ? 'active' : ''; ?>"><strong>13.1.1</strong> DOCUMENTOS</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="panel  panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">No tiene permisos para ver esta sección</h4>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>


        <!--/*********APLICACIONES***************************/-->
        <div class="tab-pane  <?php echo ($menu == 'aplicaciones') ? 'active' : ''; ?>" id="personeria">
            <div class=" sidebar">
                <div class="panel-group" id="accordion2">
                    <?php if (in_array(1, $permiso) || in_array(16, $permiso) || in_array(17, $permiso) || in_array(18, $permiso) || in_array(19, $permiso) || in_array(20, $permiso)) { ?>
                        <?php if (in_array(1, $permiso) || in_array(15, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2">
                                                1. APLICACIONES
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne2" class="panel-collapse <?php echo ($tab > 800 && $tab <= 850) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_widgets" class="list-group-item <?php echo ($tab == 801) ? 'active' : ''; ?>"><strong>1.1</strong> WIDGETS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_banners_imagenes/principal" class="list-group-item <?php echo ($tab == 802) ? 'active' : ''; ?>"><strong>1.2</strong> BANNER PRINCIPAL</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_banner_aviso" class="list-group-item <?php echo ($tab == 807) ? 'active' : ''; ?>"><strong>1.3</strong> BANNER AVISO</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_carrousel/1" class="list-group-item <?php echo ($tab == 808) ? 'active' : ''; ?>"><strong>1.4</strong> CARRUSEL</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_iconos_home" class="list-group-item <?php echo ($tab == 810) ? 'active' : ''; ?>"><strong>1.5</strong> SERVICIOS</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_enlaces_footer" class="list-group-item <?php echo ($tab == 811) ? 'active' : ''; ?>"><strong>1.6</strong> ENLACES FOOTER</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_sub_menu" class="list-group-item <?php echo ($tab == 812) ? 'active' : ''; ?>"><strong>1.7</strong> SUB MENU HOME</a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/embebido_home" class="list-group-item <?php echo ($tab == 813) ? 'active' : ''; ?>"><strong>1.8</strong> EMBEDDED HOME</a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(16, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3">
                                                2. ENCUESTAS
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne3" class="panel-collapse <?php echo ($tab > 850 && $tab <= 900) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/survey" class="list-group-item <?php echo ($tab == 851) ? 'active' : ''; ?>"><strong>2.1</strong> ENCUESTAS</a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(17, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne33">
                                                3. SUSCRIPTORES
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne33" class="panel-collapse <?php echo ($tab > 900 && $tab <= 950) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/newsletter" class="list-group-item <?php echo ($tab == 901) ? 'active' : ''; ?>"><strong>3.1</strong> SUSCRIPTORES</a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(18, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne334">
                                                4. PAGINAS
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne334" class="panel-collapse <?php echo ($tab > 950 && $tab <= 1000) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/entidad_paginas" class="list-group-item <?php echo ($tab == 951) ? 'active' : ''; ?>"><strong>4.1</strong> PAGINAS</a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(19, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne335">
                                                5. NOTIFICACIONES ELECTRÓNICAS 
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne335" class="panel-collapse <?php echo ($tab > 1000 && $tab <= 1050) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/notificaciones_electronicas_tipo" class="list-group-item <?php echo ($tab == 1001) ? 'active' : ''; ?>"><strong>5.1</strong> TIPO NOTIFICACIONES ELECTRÓNICAS </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/aplicaciones/notificaciones_electronicas" class="list-group-item <?php echo ($tab == 1002) ? 'active' : ''; ?>"><strong>5.1.1</strong> NOTIFICACIONES ELECTRÓNICAS </a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                        <?php if (in_array(1, $permiso) || in_array(20, $permiso)) { ?>
                            <div class="panel  panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <strong><a style="font-size: 12px;" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne336">
                                                6. APP
                                            </a></strong>
                                    </h4>
                                </div>
                                <div id="collapseOne336" class="panel-collapse <?php echo ($tab > 1100 && $tab <= 1150) ? 'collapse in' : 'collapse'; ?>">

                                    <div class="list-group" style="margin-bottom: 0px;">
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/aseguramiento" class="list-group-item <?php echo ($tab == 1101) ? 'active' : ''; ?>"><strong>6.1</strong> ASEGURAMIENTO </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/pacientes" class="list-group-item <?php echo ($tab == 1102) ? 'active' : ''; ?>"><strong>6.2</strong> PACIENTES </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/agendamiento" class="list-group-item <?php echo ($tab == 1103) ? 'active' : ''; ?>"><strong>6.3</strong> AGENDAMIENTO </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/citas" class="list-group-item <?php echo ($tab == 1104) ? 'active' : ''; ?>"><strong>6.4</strong> CITAS </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/sedes" class="list-group-item <?php echo ($tab == 1105) ? 'active' : ''; ?>"><strong>6.5</strong> SEDES </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/servicios" class="list-group-item <?php echo ($tab == 1106) ? 'active' : ''; ?>"><strong>6.6</strong> SERVICIOS </a>
                                        <a style="font-size: 12px;" href="<?php echo site_url(); ?>administrador/app/historico_citas" class="list-group-item <?php echo ($tab == 1107) ? 'active' : ''; ?>"><strong>6.7</strong> HISTORICO </a>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="panel  panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">No tiene permisos para ver esta sección</h4>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>


