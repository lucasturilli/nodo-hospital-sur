<?php
/**
 * Autor:      Lucas Fernandez R.
 * Email:      lucasturilli@gmail.com
 * Web:        www.codweb.co
 * @package    PQRS
 * @author     Lucas Fernandez R. < lucasturilli@gmail.com>
 * @version    1.0
 * @copyright  2014 <Lucas Fernandez Roldan>
 */
?>
<?php $this->load->view('administrador/includes/header'); ?>
<section>
    <div class="container-fluid">
        <div class="row">
            <?php
            if ($menu_lateral == TRUE) {
                $this->load->view('administrador/includes/menu');
            }
            ?>
            <?php
            if (!isset($output)) {
                $this->load->view($main_content);
            } else {
                $this->load->view($main_content, $output);
            }
            ?>
        </div>
    </div>
</section>
<?php
$this->load->view('administrador/includes/footer');
?>