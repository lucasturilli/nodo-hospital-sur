<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="NODO SITIOS WEB INSTITUCIONALES">
        <meta name="author" content="CODWEB SAS">
        <link rel="shortcut icon" href="<?php echo site_url(); ?>/images/favicon.ico">
        <title>Nodo - <?php echo $seo->entidad_seo_titulo; ?></title>
        <link rel="shortcut icon" href="<?php echo site_url('uploads/entidad') . '/' . $seo->entidad_seo_favicon; ?>" />
        <!-- Bootstrap -->
        <link href="<?php echo site_url(); ?>css/administrador/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/administrador/AdminLTE.min.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/administrador/morris.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,900|Roboto:400,500,700" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo site_url(); ?>js/administrador/jquery.js?ver=1.5" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/administrador/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/administrador/sweetalert.js"></script>
        <script src="<?php echo site_url(); ?>js/administrador/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo site_url(); ?>js/administrador/raphael.js"></script>
        <script src="<?php echo site_url(); ?>js/administrador/sweetalert.js"></script>
        <link href="<?php echo site_url(); ?>plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo site_url(); ?>plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo site_url(); ?>css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>
        <script src="<?php echo site_url(); ?>js/plugins/fullcalendar/moment.min.js"></script>
        <!-- Full Calendar -->
        <script src="<?php echo site_url(); ?>js/plugins/fullcalendar/fullcalendar.min.js"></script>
        <script src="<?php echo site_url(); ?>js/plugins/fullcalendar/locale/es.js"></script>
        <script src="<?php echo site_url(); ?>js/jquery.blockUI.js?ver=1.5"></script>

    </head>
    <body>
        <?php
        $permiso = array();
        if ($permisos != FALSE) {
            foreach ($permisos as $row) {
                array_push($permiso, $row->id_permisos);
            }
        }
        ?>
        <header>
            <div class="container-fluid">
                <div class="row">
                    <div class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="" href="<?php echo site_url('administrador/principal'); ?>"><img src="<?php echo site_url(); ?>images/administrador/nodo.png" height="48" /></a>
                        </div>
                        <p class="navbar-text hidden-xs"><b>Bienvenido, <?php echo $this->session->userdata('administrador_nombres') . ' ' . $this->session->userdata('administrador_apellidos'); ?></b></p>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="<?php echo site_url('administrador/principal'); ?>"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
                                <li><a href="<?php echo site_url('administrador/referenciales_nodo/definicion_mi_usuario'); ?>"><i class="far fa-user"></i> Mi Perfil</a></li>
                                <?php if ($this->session->userdata('id_administrador_rol') == 1) { ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cogs"></i> Administración</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="<?php echo site_url('administrador/referenciales_nodo/definicion_usuarios'); ?>">Usuarios</a></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_nodo/audit_administrador'); ?>">Logs Usuarios</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_entidad/entidad_seo'); ?>">SEO</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_entidad/entidad_redes_sociales_definicion'); ?>">Redes Sociales</a></li>
                                            <li><a  href="<?php echo site_url('administrador/principal'); ?>/filemanager">Administrador de Archivos</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_nodo/smtp'); ?>">SMTP</a></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_nodo/config_general'); ?>">Otras Configuraciones</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php echo site_url('administrador/referenciales_nodo/recordatorios'); ?>">Alertas y Recordatorios</a></li>
                                        </ul>
                                    </li>
                                    <?php
                                } else {
                                    if (in_array(22, $permiso)) {
                                        ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cogs"></i> Administración</a>
                                            <ul class="dropdown-menu">
                                                <li><a  href="<?php echo site_url('administrador/principal'); ?>/filemanager">Administrador de Archivos</a></li>

                                            </ul>
                                        </li>
                                        <?php
                                    }
                                    if (in_array(21, $permiso)) {
                                        ?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-cogs"></i> Administración</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('administrador/referenciales_nodo/recordatorios'); ?>">Alertas y Recordatorios</a></li>

                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                                <li><a target="_blank" href="<?php echo site_url(); ?>"><i class="fas fa-external-link-alt"></i> Ver Sitio</a></li>
                                <li><a target="_blank" href="<?php echo site_url(); ?>/media/manual_alcaldia.pdf"><i class="fas fa-info-circle"></i> Ayuda</a></li>
                                <li><a target="_blank" href="http://codweb.co/soporte"><i class="far fa-life-ring"></i> Soporte </a></li>
                                <li><a href="<?php echo site_url() ?>administrador/login/logout"><i class="fas fa-sign-out-alt"></i> Salir</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?php
        ?>
        <section>
            <div class=" container-fluid">
                <div class="row hidden-xs">
                    <?php
                    echo $breadcrumbs;
                    ?>
                </div>
            </div>
        </section>