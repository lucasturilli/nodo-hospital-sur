<!--        /*TABLES*/-->

<link href="<?php echo site_url(); ?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="<?php echo site_url(); ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo site_url(); ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<div class="col-md-12">
    <div class="row clearfix">
        <div class="col-md-12 column ">
            <?php if ($audits != FALSE) { ?>
             
                    <div class = "table-responsive">
                        <table  class="table table-striped table-bordered table-condensed table-hover full_table_audits  ">
                            <thead>
                                <tr>
                                    <th class="resize">Fecha</th>
                                    <th class="resize">Tipo</th>
                                    <th class="resize">Sección</th>
                                    <th class="resize">Identificador</th>
                                    <th class="resize">Usuario</th>
                                    <th class="resize">Datos</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($audits as $dato) {
                                    ?>
                                    <tr>
                                        <td class="resize"><?php echo $dato->audit_fecha; ?></td>
                                        <td class="resize"><?php echo $dato->audit_type; ?></td>
                                        <td class="resize"><?php echo str_replace("_", " ", "$dato->audit_table"); ?></td>
                                        <td class="resize"><?php echo $dato->audit_key_in_table; ?></td>
                                        <td class="resize"><?php echo $dato->nombres . ' ' . $dato->apellidos; ?></td>
                                        <td class="resize"><?php echo strip_tags($dato->audit_data); ?></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php }
            ?>
    </div>



    <?php /* echo 'Aqui';
      print_r($this->session->userdata('contenido'));
      $this->session->unset_userdata('contenido'); */
    ?>

    <script>
        $(function () {
            $('.full_table_audits').dataTable({
                "bPaginate": true,
                "aaSorting": [[0, 'desc']],
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });
    </script>