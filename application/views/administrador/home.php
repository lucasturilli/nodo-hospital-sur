
<div class="col-md-9">
    <div class="row ">
        <div class="col-md-12 " >
            <div class="row ">
                <div class="col-md-12 column text-left">
                    <div  class="page-header">
                        <h3 ><img class="img-thumbnail" width="100" src="<?php echo site_url('uploads/entidad') . '/' . $entidad_logo->entidad_informacion_logo; ?>"> Gestor de Contenidos <small><em> <?php echo $seo->entidad_seo_titulo; ?></em></small></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo number_format($visitas); ?></h3>
                            <p>Visitas Totales</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-globe"></i>
                        </div>
                        <a href="<?php echo site_url('administrador/referenciales_nodo/entidad_contador_visitas_dia'); ?>" class="small-box-footer">Ver mas <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><?php echo number_format($suscriptores); ?></h3>
                            <p>Suscriptores</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-users"></i>
                        </div>
                        <a href="<?php echo site_url('administrador/aplicaciones/newsletter'); ?>" class="small-box-footer">Ver mas <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo number_format($audits_count); ?></h3>
                            <p>Registro de Publicaciones</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-pencil-alt"></i>
                        </div>
                        <a href="<?php echo site_url('administrador/referenciales_nodo/audits'); ?>" class="small-box-footer">Ver mas <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo round(((($contador[2]) / $total) * 100), 2); ?> %</h3>
                            <p>Estado de Publicación</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-check-circle"></i>
                        </div>
                        <a href="<?php echo site_url('administrador/principal/estado_publicacion'); ?>" class="small-box-footer">Ver mas <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Visitas por mes</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="line-chart" style="height: 300px;"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</div>

<script>
    // LINE CHART
    var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
        data: [
<?php
$x = 1;
foreach ($graficas as $dato) {
    ?>
    <?php
    $zero = ($dato[2] < 10) ? '0' : '';
    echo "{month: '" . $dato[0] . '-' . $zero . $dato[2] . "' , value: " . $dato[1] . "}";
    echo($x != 6) ? ',' : '';
    ?>
    <?php
    $x++;
}
?>
        ],
        xkey: 'month',
                ykeys: ['value'],
        labels: ['Visitas'],
                lineColors: ['#3c8dbc'],
        xLabels:'month',
                hideHover: 'auto'
    });
</script>