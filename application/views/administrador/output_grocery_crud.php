<?php foreach ($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach ($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php if ($menu_lateral == TRUE) { ?>
    <div class="col-md-9">
        <?php if (isset($texto)) { ?>
            <a class="btn btn-info btn-block" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-info-circle"></i> <?php echo $texto_titulo; ?>
            </a>
            <div class="collapse" id="collapseExample">
                <div class="well alert-info">
                    <?php echo $texto_titulo; ?></strong><br><?php echo $texto; ?>
                </div>
            </div>
        <?php } ?>
        <?php echo $output; ?>
    </div>
<?php } else { ?>
    <div class="col-md-12">
        <?php echo $output; ?>
    </div>
<?php } ?>
<script>
    $(document).ready(function () {
        $('.bad').parent().addClass("danger");
        $('.good').parent().addClass("success");
        $('.wait').parent().addClass("warning");
    });


    $("a.image-zoom").fancybox();
    $(document).on('click', '.noticias-newsletter', function () {
        var name = $(this).attr("id");
        var id = name.substring(8);
        var url = "<?php echo site_url('administrador/cron_newsletter/send') . '/' ?>" + id;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                swal({
                    title: "Éxito",
                    text: "La noticia quedo inscrita correctamente para su envío",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                }, function () {
                    location.reload();
                });
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });

    $(document).on('click', '.aseguramiento-sincronizar', function () {
        var url = "<?php echo site_url('administrador/Cron_aseguramiento/sincronizar') ?>";
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (msg) {
                swal({
                    title: "Éxito",
                    text: "La sincronización de la base de datos está en curso ",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                }, function () {
                    location.reload();
                });
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    alert('No Conecta.\n Verifique su conexión a internet.');
                } else if (jqXHR.status == 404) {
                    alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                } else if (jqXHR.status == 500) {
                    alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'parsererror') {
                    alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'timeout') {
                    alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                } else if (exception === 'abort') {
                    alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                }
            }
        });
    });

</script>