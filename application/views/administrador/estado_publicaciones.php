<?php
$estados = array(1 => '<i class="fas fa-times-circle-o"></i> Sin Resgistro',
    2 => '<i class="fas fa-check-circle-o"></i> Registro Completo',
    3 => '<i class="fas fa-exclamation-triangle"></i> Registro Incompleto',
    4 => '<i class="fas fa-info-circle"></i> Registros sin Avances');
$clase = array(1 => 'danger',
    2 => 'success',
    3 => 'warning',
    4 => 'info');
?>
<div class="col-md-12">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo $contador[2] . ' de ' . $total; ?></h3>
                    <p>Registros Completos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-check-circle"></i>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?php echo $contador[4]; ?></h3>
                    <p>Registros sin Avances</p>
                </div>
                <div class="icon">
                    <i class="fas fa-info-circle"></i>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?php echo $contador[3]; ?></h3>
                    <p>Registro Incompleto</p>
                </div>
                <div class="icon">
                    <i class="fas fa-exclamation-triangle"></i>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo $contador[1]; ?></h3>
                    <p>Sin Registro</p>
                </div>
                <div class="icon">
                    <i class="fas fa-times-circle"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column ">
            <div class=" table-responsive">
                <table class="table table-condensed table-bordered table-hover">
                    <tr>
                        <th>Seccion</th>
                        <th>Item</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Registros</th>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_informacion[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.1</td>
                        <td>Información General</td>
                        <td><?php echo $estados[$entidad_informacion[0]]; ?></td>
                        <td><?php echo $entidad_informacion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_contacto[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.2</td>
                        <td>Datos de Contacto</td>
                        <td><?php echo $estados[$entidad_contacto[0]]; ?></td>
                        <td><?php echo $entidad_contacto[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_politicas_datos[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.3</td>
                        <td>Políticas y Protección de Datos</td>
                        <td><?php echo $estados[$entidad_politicas_datos[0]]; ?></td>
                        <td><?php echo $entidad_politicas_datos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_redes_sociales[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.4</td>
                        <td>Redes Sociales</td>
                        <td><?php echo $estados[$entidad_redes_sociales[0]]; ?></td>
                        <td><?php echo $entidad_redes_sociales[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_dependencia[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.5</td>
                        <td>Dependencias</td>
                        <td><?php echo $estados[$entidad_dependencia[0]]; ?></td>
                        <td><?php echo $entidad_dependencia[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_sucursales[0]]; ?>">
                        <td>Información General de la Entidad</td>
                        <td>1.6</td>
                        <td>Sucursales o Regionales</td>
                        <td><?php echo $estados[$entidad_sucursales[0]]; ?></td>
                        <td><?php echo $entidad_sucursales[1]; ?></td>
                    </tr>

                    <!--                ********************SERVICIOS DE INFORMACION ************-->
                    <tr class="<?php echo $clase[$entidad_preguntas_frecuentes[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.1</td>
                        <td>Preguntas y Respuestas Frecuentes</td>
                        <td><?php echo $estados[$entidad_preguntas_frecuentes[0]]; ?></td>
                        <td><?php echo $entidad_preguntas_frecuentes[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_glosario[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.2</td>
                        <td>Glosario</td>
                        <td><?php echo $estados[$entidad_glosario[0]]; ?></td>
                        <td><?php echo $entidad_glosario[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_noticias[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.3</td>
                        <td>Noticias</td>
                        <td><?php echo $estados[$entidad_noticias[0]]; ?></td>
                        <td><?php echo $entidad_noticias[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_calendario[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.4</td>
                        <td>Calendario de Actividades</td>
                        <td><?php echo $estados[$entidad_calendario[0]]; ?></td>
                        <td><?php echo $entidad_calendario[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_ofertas_empleo[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.5</td>
                        <td>Ofertas de Empleo</td>
                        <td><?php echo $estados[$entidad_ofertas_empleo[0]]; ?></td>
                        <td><?php echo $entidad_ofertas_empleo[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_boletines[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.6</td>
                        <td>Boletines</td>
                        <td><?php echo $estados[$entidad_boletines[0]]; ?></td>
                        <td><?php echo $entidad_boletines[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$convocatorias[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.8</td>
                        <td>Convocatorias</td>
                        <td><?php echo $estados[$convocatorias[0]]; ?></td>
                        <td><?php echo $convocatorias[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$estudios[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.9</td>
                        <td>Estudios, investigaciones y otras publicaciones</td>
                        <td><?php echo $estados[$estudios[0]]; ?></td>
                        <td><?php echo $estudios[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$informacion_adicional[0]]; ?>">
                        <td>Servicios de Información</td>
                        <td>2.10</td>
                        <td>Información adicional</td>
                        <td><?php echo $estados[$informacion_adicional[0]]; ?></td>
                        <td><?php echo $informacion_adicional[1]; ?></td>
                    </tr>
                    <!--                ********************TALENTO HUMANO ************-->
                    <tr class="<?php echo $clase[$entidad_funcionario[0]]; ?>">
                        <td>Talento Humano</td>
                        <td>3.1</td>
                        <td>Funcionarios</td>
                        <td><?php echo $estados[$entidad_funcionario[0]]; ?></td>
                        <td><?php echo $entidad_funcionario[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_manual_funciones[0]]; ?>">
                        <td>Talento Humano</td>
                        <td>3.2</td>
                        <td>Manual de Funciones</td>
                        <td><?php echo $estados[$entidad_manual_funciones[0]]; ?></td>
                        <td><?php echo $entidad_manual_funciones[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_asignaciones_salarial[0]]; ?>">
                        <td>Talento Humano</td>
                        <td>3.3</td>
                        <td>Asignaciones Salariales</td>
                        <td><?php echo $estados[$entidad_asignaciones_salarial[0]]; ?></td>
                        <td><?php echo $entidad_asignaciones_salarial[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_planta_personal[0]]; ?>">
                        <td>Talento Humano</td>
                        <td>3.4</td>
                        <td>Planta de Personal</td>
                        <td><?php echo $estados[$entidad_planta_personal[0]]; ?></td>
                        <td><?php echo $entidad_planta_personal[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_evaluacion_desempeno[0]]; ?>">
                        <td>Talento Humano</td>
                        <td>3.5</td>
                        <td>Evaluación de Desempeño</td>
                        <td><?php echo $estados[$entidad_evaluacion_desempeno[0]]; ?></td>
                        <td><?php echo $entidad_evaluacion_desempeno[1]; ?></td>
                    </tr>
                    <!--                ********************NORMATIVIDAD ************-->
                    <tr class="<?php echo $clase[$normatividad_leyes[0]]; ?>">
                        <td>Normatividad</td>
                        <td>5.1</td>
                        <td>Ley/Ordenanza/Acuerdo</td>
                        <td><?php echo $estados[$normatividad_leyes[0]]; ?></td>
                        <td><?php echo $normatividad_leyes[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$normatividad_decretos[0]]; ?>">
                        <td>Normatividad</td>
                        <td>5.2</td>
                        <td>Decretos</td>
                        <td><?php echo $estados[$normatividad_decretos[0]]; ?></td>
                        <td><?php echo $normatividad_decretos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$normatividad_resoluciones[0]]; ?>">
                        <td>Normatividad</td>
                        <td>5.3</td>
                        <td>Resolución/Circular/Otro</td>
                        <td><?php echo $estados[$normatividad_resoluciones[0]]; ?></td>
                        <td><?php echo $normatividad_resoluciones[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$normatividad_edictos[0]]; ?>">
                        <td>Normatividad</td>
                        <td>5.4</td>
                        <td>Edictos</td>
                        <td><?php echo $estados[$normatividad_edictos[0]]; ?></td>
                        <td><?php echo $normatividad_edictos[1]; ?></td>
                    </tr>
                    <!--                ********************PRESUPUESTO ************-->
                    <tr class="<?php echo $clase[$financiera_presupuesto[0]]; ?>">
                        <td>Presupuesto</td>
                        <td>6.1</td>
                        <td>Presupuesto Aprobado en Ejercicio</td>
                        <td><?php echo $estados[$financiera_presupuesto[0]]; ?></td>
                        <td><?php echo $financiera_presupuesto[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$financiera_presupuesto_ejecucion[0]]; ?>">
                        <td>Presupuesto</td>
                        <td>6.1.1</td>
                        <td>Ejecución de Presupuesto Aprobado en Ejercicio</td>
                        <td><?php echo $estados[$financiera_presupuesto_ejecucion[0]]; ?></td>
                        <td><?php echo $financiera_presupuesto_ejecucion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$financiera_estados_financieros[0]]; ?>">
                        <td>Presupuesto</td>
                        <td>6.2</td>
                        <td>Estados Financieros </td>
                        <td><?php echo $estados[$financiera_estados_financieros[0]]; ?></td>
                        <td><?php echo $financiera_estados_financieros[1]; ?></td>
                    </tr>

                    <!--                ********************PLANEACION ************-->
                    <tr class="<?php echo $clase[$normatividad_politicas[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.1</td>
                        <td>Políticas/Lineamientos/Manuales</td>
                        <td><?php echo $estados[$normatividad_politicas[0]]; ?></td>
                        <td><?php echo $normatividad_politicas[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$anticorrupcion_definicion[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.2</td>
                        <td>Plan Anticorrupción</td>
                        <td><?php echo $estados[$anticorrupcion_definicion[0]]; ?></td>
                        <td><?php echo $anticorrupcion_definicion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_plan_estrategico[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.3</td>
                        <td>Plan de desarrollo Vigente</td>
                        <td><?php echo $estados[$control_plan_estrategico[0]]; ?></td>
                        <td><?php echo $control_plan_estrategico[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_plan_accion[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.4</td>
                        <td>Plan de Acción</td>
                        <td><?php echo $estados[$control_plan_accion[0]]; ?></td>
                        <td><?php echo $control_plan_accion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_plan_accion_avances[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.4.1</td>
                        <td>Avances del Plan de Acción</td>
                        <td><?php echo $estados[$control_plan_accion_avances[0]]; ?></td>
                        <td><?php echo $control_plan_accion_avances[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_programas[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.5</td>
                        <td>Programas y Proyectos en Ejecución </td>
                        <td><?php echo $estados[$control_programas[0]]; ?></td>
                        <td><?php echo $control_programas[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_programas_avances[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.5.1</td>
                        <td>Avances de Programas y Proyectos en Ejecución </td>
                        <td><?php echo $estados[$control_programas_avances[0]]; ?></td>
                        <td><?php echo $control_programas_avances[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_indicadores_gestion[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.6</td>
                        <td>Metas e Indicadores de Gestión</td>
                        <td><?php echo $estados[$control_indicadores_gestion[0]]; ?></td>
                        <td><?php echo $control_indicadores_gestion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_planes_otros[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.7</td>
                        <td>Otros Planes</td>
                        <td><?php echo $estados[$control_planes_otros[0]]; ?></td>
                        <td><?php echo $control_planes_otros[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_empalme[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.8</td>
                        <td>Informes de Empalme</td>
                        <td><?php echo $estados[$control_informe_empalme[0]]; ?></td>
                        <td><?php echo $control_informe_empalme[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_empalme_anexos[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.8.1</td>
                        <td>Anexos de Informes de Empalme</td>
                        <td><?php echo $estados[$control_informe_empalme_anexos[0]]; ?></td>
                        <td><?php echo $control_informe_empalme_anexos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_archivo[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.9</td>
                        <td>Informes de Archivo</td>
                        <td><?php echo $estados[$control_informe_archivo[0]]; ?></td>
                        <td><?php echo $control_informe_archivo[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$planeacion_plan_gasto_publico[0]]; ?>">
                        <td>Planeación</td>
                        <td>7.10</td>
                        <td>Plan de Gasto Público</td>
                        <td><?php echo $estados[$planeacion_plan_gasto_publico[0]]; ?></td>
                        <td><?php echo $planeacion_plan_gasto_publico[1]; ?></td>
                    </tr>
                    <!--                ********************CONTROL ************-->
                    <tr class="<?php echo $clase[$control_entes[0]]; ?>">
                        <td>Control</td>
                        <td>8.1</td>
                        <td>Entes de Control que Vigilan la Entidad</td>
                        <td><?php echo $estados[$control_entes[0]]; ?></td>
                        <td><?php echo $control_entes[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_auditorias[0]]; ?>">
                        <td>Control</td>
                        <td>8.2</td>
                        <td>Auditorias</td>
                        <td><?php echo $estados[$control_auditorias[0]]; ?></td>
                        <td><?php echo $control_auditorias[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_concejo[0]]; ?>">
                        <td>Control</td>
                        <td>8.3</td>
                        <td>Informe al Congreso/Asamblea/Concejo </td>
                        <td><?php echo $estados[$control_informe_concejo[0]]; ?></td>
                        <td><?php echo $control_informe_concejo[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_ciudadania[0]]; ?>">
                        <td>Control</td>
                        <td>8.4</td>
                        <td>Informe de Rendición de Cuenta a los Ciudadanos </td>
                        <td><?php echo $estados[$control_informe_ciudadania[0]]; ?></td>
                        <td><?php echo $control_informe_ciudadania[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_informe_fiscal[0]]; ?>">
                        <td>Control</td>
                        <td>8.5</td>
                        <td>Informes de Rendición Fiscal</td>
                        <td><?php echo $estados[$control_informe_fiscal[0]]; ?></td>
                        <td><?php echo $control_informe_fiscal[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_planes_mejoramiento[0]]; ?>">
                        <td>Control</td>
                        <td>8.6</td>
                        <td>Planes de Mejoramiento</td>
                        <td><?php echo $estados[$control_planes_mejoramiento[0]]; ?></td>
                        <td><?php echo $control_planes_mejoramiento[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_reportes_control_interno[0]]; ?>">
                        <td>Control</td>
                        <td>8.7</td>
                        <td>Reportes de Control Interno</td>
                        <td><?php echo $estados[$control_reportes_control_interno[0]]; ?></td>
                        <td><?php echo $control_reportes_control_interno[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_poblacion_vulnerable[0]]; ?>">
                        <td>Control</td>
                        <td>8.8</td>
                        <td>Información para Población Vulnerable </td>
                        <td><?php echo $estados[$control_poblacion_vulnerable[0]]; ?></td>
                        <td><?php echo $control_poblacion_vulnerable[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_poblacion_vulnerable_programas[0]]; ?>">
                        <td>Control</td>
                        <td>8.9</td>
                        <td>Programas para Población Vulnerable </td>
                        <td><?php echo $estados[$control_poblacion_vulnerable_programas[0]]; ?></td>
                        <td><?php echo $control_poblacion_vulnerable_programas[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_poblacion_vulnerable_programas_avances[0]]; ?>">
                        <td>Control</td>
                        <td>8.9.1</td>
                        <td>Avances de los Programas para la Población Vulnerable</td>
                        <td><?php echo $estados[$control_poblacion_vulnerable_programas_avances[0]]; ?></td>
                        <td><?php echo $control_poblacion_vulnerable_programas_avances[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_poblacion_vulnerable_proyectos[0]]; ?>">
                        <td>Control</td>
                        <td>8.10</td>
                        <td>Proyectos para Población Vulnerable </td>
                        <td><?php echo $estados[$control_poblacion_vulnerable_proyectos[0]]; ?></td>
                        <td><?php echo $control_poblacion_vulnerable_proyectos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_poblacion_vulnerable_proyectos_avances[0]]; ?>">
                        <td>Control</td>
                        <td>8.10.1</td>
                        <td>Avances de los Proyectos para la Población Vulnerable</td>
                        <td><?php echo $estados[$control_poblacion_vulnerable_proyectos_avances[0]]; ?></td>
                        <td><?php echo $control_poblacion_vulnerable_proyectos_avances[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_programas_sociales[0]]; ?>">
                        <td>Control</td>
                        <td>8.11</td>
                        <td>Programas Sociales</td>
                        <td><?php echo $estados[$control_programas_sociales[0]]; ?></td>
                        <td><?php echo $control_programas_sociales[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_defensa_judicial[0]]; ?>">
                        <td>Control</td>
                        <td>8.12</td>
                        <td>Defensa Judicial</td>
                        <td><?php echo $estados[$control_defensa_judicial[0]]; ?></td>
                        <td><?php echo $control_defensa_judicial[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$control_otros_informes[0]]; ?>">
                        <td>Control</td>
                        <td>8.12</td>
                        <td>Otros Informes</td>
                        <td><?php echo $estados[$control_otros_informes[0]]; ?></td>
                        <td><?php echo $control_otros_informes[1]; ?></td>
                    </tr>
                    <!--                ********************CONTRATACION ************-->
                    <tr class="<?php echo $clase[$contratacion_plan_compras[0]]; ?>">
                        <td>Contratación</td>
                        <td>9.1</td>
                        <td>Plan de Compras</td>
                        <td><?php echo $estados[$contratacion_plan_compras[0]]; ?></td>
                        <td><?php echo $contratacion_plan_compras[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$contratacion_contratos_secop[0]]; ?>">
                        <td>Contratación</td>
                        <td>9.2</td>
                        <td>Información SECOP</td>
                        <td><?php echo $estados[$contratacion_contratos_secop[0]]; ?></td>
                        <td><?php echo $contratacion_contratos_secop[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$contratacion_contratos[0]]; ?>">
                        <td>Contratación</td>
                        <td>9.3</td>
                        <td>Procesos de Contratación</td>
                        <td><?php echo $estados[$contratacion_contratos[0]]; ?></td>
                        <td><?php echo $contratacion_contratos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$contratacion_avance_contractual[0]]; ?>">
                        <td>Contratación</td>
                        <td>9.3.1</td>
                        <td>Avances Contractuales</td>
                        <td><?php echo $estados[$contratacion_avance_contractual[0]]; ?></td>
                        <td><?php echo $contratacion_avance_contractual[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$contratacion_lineamientos[0]]; ?>">
                        <td>Contratación</td>
                        <td>9.4</td>
                        <td>Lineamientos de Adquisiciones  y Compras</td>
                        <td><?php echo $estados[$contratacion_lineamientos[0]]; ?></td>
                        <td><?php echo $contratacion_lineamientos[1]; ?></td>
                    </tr>
                    <!--                ********************TRAMITES Y SERVICIOS ************-->
                    <tr class="<?php echo $clase[$servicios_tramites[0]]; ?>">
                        <td>Trámites y Servicios</td>
                        <td>10.1</td>
                        <td>Listado de Trámites y Servicios</td>
                        <td><?php echo $estados[$servicios_tramites[0]]; ?></td>
                        <td><?php echo $servicios_tramites[1]; ?></td>
                    </tr>
                    </tr>
                    <tr class="<?php echo $clase[$servicios_tramites_en_linea[0]]; ?>">
                        <td>Trámites y Servicios</td>
                        <td>10.2</td>
                        <td>Listado de Trámites y Servicios en línea </td>
                        <td><?php echo $estados[$servicios_tramites_en_linea[0]]; ?></td>
                        <td><?php echo $servicios_tramites_en_linea[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$servicios_formatos[0]]; ?>">
                        <td>Trámites y Servicios</td>
                        <td>10.3</td>
                        <td>Formularios de trámites y servicios</td>
                        <td><?php echo $estados[$servicios_formatos[0]]; ?></td>
                        <td><?php echo $servicios_formatos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$servicios_pqrdf[0]]; ?>">
                        <td>Trámites y Servicios</td>
                        <td>10.4</td>
                        <td>Informe de Peticiones, Quejas, Reclamos, Denuncias y Felicitaciones (PQRDF) </td>
                        <td><?php echo $estados[$servicios_pqrdf[0]]; ?></td>
                        <td><?php echo $servicios_pqrdf[1]; ?></td>
                    </tr>
                    <!--                ********************CALIDAD ************-->
                    <tr class="<?php echo $clase[$entidad_calidad[0]]; ?>">
                        <td>Calidad</td>
                        <td>11.1</td>
                        <td>SISTEMA DE CALIDAD</td>
                        <td><?php echo $estados[$entidad_calidad[0]]; ?></td>
                        <td><?php echo $entidad_calidad[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_caracterizacion_procesos[0]]; ?>">
                        <td>Calidad</td>
                        <td>11.2</td>
                        <td>Caracterización de Procesos</td>
                        <td><?php echo $estados[$entidad_caracterizacion_procesos[0]]; ?></td>
                        <td><?php echo $entidad_caracterizacion_procesos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$entidad_procedimientos[0]]; ?>">
                        <td>Calidad</td>
                        <td>11.3</td>
                        <td>Procedimientos</td>
                        <td><?php echo $estados[$entidad_procedimientos[0]]; ?></td>
                        <td><?php echo $entidad_procedimientos[1]; ?></td>
                    </tr>
                    <!--                ********************DATOS ABIERTOS ************-->
                    <tr class="<?php echo $clase[$catalogo_datos_abiertos[0]]; ?>">
                        <td>Datos Abiertos</td>
                        <td>12.1</td>
                        <td>Catalogo de Datos Abiertos</td>
                        <td><?php echo $estados[$catalogo_datos_abiertos[0]]; ?></td>
                        <td><?php echo $catalogo_datos_abiertos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$archivos_datos_abiertos[0]]; ?>">
                        <td>Datos Abiertos</td>
                        <td>12.2</td>
                        <td>Archivos de Datos Abiertos</td>
                        <td><?php echo $estados[$archivos_datos_abiertos[0]]; ?></td>
                        <td><?php echo $archivos_datos_abiertos[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$registro_activos_informacion[0]]; ?>">
                        <td>Datos Abiertos</td>
                        <td>12.3</td>
                        <td>Registro de Activos de Información</td>
                        <td><?php echo $estados[$registro_activos_informacion[0]]; ?></td>
                        <td><?php echo $registro_activos_informacion[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$informacion_clasificada_reservada[0]]; ?>">
                        <td>Datos Abiertos</td>
                        <td>12.4</td>
                        <td>Índice de Información Clasificada y Reservada</td>
                        <td><?php echo $estados[$informacion_clasificada_reservada[0]]; ?></td>
                        <td><?php echo $informacion_clasificada_reservada[1]; ?></td>
                    </tr>
                    <tr class="<?php echo $clase[$esquema_publicacion[0]]; ?>">
                        <td>Datos Abiertos</td>
                        <td>12.5</td>
                        <td>Esquema de Publicación </td>
                        <td><?php echo $estados[$esquema_publicacion[0]]; ?></td>
                        <td><?php echo $esquema_publicacion[1]; ?></td>
                    </tr>
                    <!--                ********************DOCUMENTOS************-->
                    <tr class="<?php echo $clase[$entidad_documentos[0]]; ?>">
                        <td>Documentos de la entidad</td>
                        <td>13.1</td>
                        <td>Documentos de la entidad</td>
                        <td><?php echo $estados[$entidad_documentos[0]]; ?></td>
                        <td><?php echo $entidad_documentos[1]; ?></td>
                    </tr>


                </table>
            </div>
        </div>
    </div>
