<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Nodo - <?php echo $seo->entidad_seo_titulo; ?></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="shortcut icon" href="<?php echo site_url('uploads/entidad') . '/' . $seo->entidad_seo_favicon; ?>" />
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
        <script src="<?php echo site_url(); ?>plugins/validate/jquery.validate.min.js"></script>
        <!-- BLOCKUI -->
        <script src="<?php echo site_url(); ?>js/jquery.blockUI.js"></script>
        <style type="text/css">
            body {
                color: #4e4e4e;
                background: #e2e2e2;
                font-family: 'Roboto', sans-serif;
            }
            .form-control {
                background: #f2f2f2;
                font-size: 16px;
                border-color: transparent;
                box-shadow: none !important;
            }
            .form-control:focus {
                border-color: #F79FA6;
                background: #FFE8EA;
            }
            .form-control, .btn {        
                border-radius: 2px;
            }
            .login-form {
                width: 380px;
                margin: 0 auto;
            }
            .login-form h2 {
                margin: 0;
                padding: 30px 0;
                font-size: 34px;
            }
            .login-form .avatar {
                margin: 0 auto 30px;
                width: 100px;
                height: 100px;
                border-radius: 50%;
                z-index: 9;
                background: #4aba70;
                padding: 15px;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
            }
            .login-form .avatar img {
                width: 100%;
            }
            .login-form form {
                color: #7a7a7a;
                border-radius: 4px;
                margin-bottom: 20px;
                background: #fff;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                padding: 30px;		
            }
            .login-form .btn {
                font-size: 16px;
                line-height: 26px;
                min-width: 120px;
                font-weight: bold;

                border: none;		
            }
            .login-form .btn:hover, .login-form .btn:focus{

                outline: none !important;
            }
            .checkbox-inline {
                margin-top: 14px;
            }
            .checkbox-inline input {
                margin-top: 3px;
            }
            .login-form a {
                color: #4aba70;
            }	
            .login-form a:hover {
                text-decoration: underline;
            }
            .hint-text {
                color: #999;
                text-align: center;
                padding-bottom: 15px;
            }
            .sk-spinner-cube-grid {
                /*
                 * Spinner positions
                 * 1 2 3
                 * 4 5 6
                 * 7 8 9
                 */
            }
            .sk-spinner-cube-grid.sk-spinner {
                width: 30px;
                height: 30px;
                margin: 0 auto;
            }
            .sk-spinner-cube-grid .sk-cube {
                width: 33%;
                height: 33%;
                background-color: #DD4B39;
                float: left;
                -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
                animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(1) {
                -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(2) {
                -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(3) {
                -webkit-animation-delay: 0.4s;
                animation-delay: 0.4s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(4) {
                -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(5) {
                -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(6) {
                -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(7) {
                -webkit-animation-delay: 0s;
                animation-delay: 0s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(8) {
                -webkit-animation-delay: 0.1s;
                animation-delay: 0.1s;
            }
            .sk-spinner-cube-grid .sk-cube:nth-child(9) {
                -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
            }
            @-webkit-keyframes sk-cubeGridScaleDelay {
                0%,
                70%,
                100% {
                    -webkit-transform: scale3D(1, 1, 1);
                    transform: scale3D(1, 1, 1);
                }
                35% {
                    -webkit-transform: scale3D(0, 0, 1);
                    transform: scale3D(0, 0, 1);
                }
            }
            @keyframes sk-cubeGridScaleDelay {
                0%,
                70%,
                100% {
                    -webkit-transform: scale3D(1, 1, 1);
                    transform: scale3D(1, 1, 1);
                }
                35% {
                    -webkit-transform: scale3D(0, 0, 1);
                    transform: scale3D(0, 0, 1);
                }
            }
        </style>
    </head>
    <body>
        <div class="login-form">
            <h2 class="text-center">Formulario de Ingreso</h2>
            <?php
            echo form_open('administrador/login/validar_usuario', 'id="form" name="login-form"');
            ?> 
            <img class="center-block" src="<?php echo site_url(); ?>images/administrador/nodo.png" height="80" />  
            <br>
            <div class="form-group">	
                <?php echo form_input('numero', $id, 'type="text" class="form-control input-lg" placeholder="Identificación " required="required"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_password('password', $pass, 'type="password" class="form-control input-lg" placeholder="Contraseña" required="required"'); ?>
            </div>        
            <div class="form-group clearfix">
<!--                <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>-->
                <button type="submit" class="btn btn-primary btn-lg pull-right"><i class="fas fa-sign-in-alt"></i> Ingresar</button>
            </div>		
            <?php
            if (isset($mensaje)) {
                echo '<div class="alert alert-danger" role="alert"><i class="fas fa-exclamation-triangle"></i> ' . $mensaje . '</div>';
            }

            echo validation_errors('<div class="alert alert-danger" role="alert"><i class="fas fa-exclamation-triangle"></i> ', '</div>');
            ?>
            <?php
            echo form_close();
            ?>
            <div class="hint-text">¿Olvido su contraseña? <a data-toggle="modal" data-target="#enviar" href="#">Clic Aquí</a></div>
        </div>

        <div class="modal fade" id="enviar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content ">
                    <div class="modal-header alert-message">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-key"></i> Recuperar Contraseña</h4>
                    </div>
                    <form id="form_correo">
                        <div class="modal-body contenido_modal_delete">
                            <label>Ingrese su número de identificación </label>
                            <input id="id_recuperar" name="id_recuperar" type="text" placeholder="Escriba aquí" class="form-control">
                            <br>
                            <label>Ingrese el correo electrónico registrado </label>
                            <input id="email_recuperar" name="email_recuperar" type="email" placeholder="Escriba aquí" class="form-control">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
                            <button type="submit" id="submit_prueba"  class="btn btn-danger">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>    




<script>


    $(document).ready(function () {
        $("#form_formulario").validate({
            rules: {
                numero: {
                    required: true
                },
                password: {
                    required: true
                }
            }, submitHandler: function (form) {
                $.blockUI();
                form.submit();
            }
        });



        $("#form_correo").validate({
            rules: {
                email_recuperar: {
                    required: true,
                    email: true
                },
                id_recuperar: {
                    required: true,
                    number: true
                }
            }, submitHandler: function (form) {

                var email = $('#email_prueba').val();
                var url = "<?php echo site_url('administrador/login/enviar_email'); ?>";
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    data: {
                        email: $('input[name=email_recuperar]').val(),
                        id: $('input[name=id_recuperar]').val()
                    },
                    success: function (msg) {
                        if (msg.estado == 2) {
                            alert(msg.error);
                        } else {
                            $('#enviar').modal('toggle');
                            alert("Los datos de ingreso fueron enviados al correo electrónico " + msg.error + ", por favor verifíquelos e ingrese nuevamente. ");
                        }
                    },
                    error: function (jqXHR, exception) {
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        alert(msg);
                    }
                });

            }
        });

    });

</script>