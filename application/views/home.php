<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($aviso->entidad_banner_aviso_activo == 1) {
    if ($this->session->userdata('aviso') == FALSE) {
        $this->session->set_userdata('aviso', TRUE);
        ?>
        <div class="modal fade " id="modal_aviso" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalCenterTitle"><?php echo $aviso->entidad_banner_aviso_titulo; ?></h4>
                    </div>
                    <div class="modal-body">
                        <?php if ($aviso->entidad_banner_aviso_url != NULL) { ?>
                            <a href="<?php echo prep_url($aviso->entidad_banner_aviso_url); ?>" target="<?php echo $aviso->entidad_banner_aviso_tipo_enlace; ?>"><img width="100%" src="<?php echo site_url(); ?>uploads/banners/<?php echo $aviso->entidad_banner_aviso_archivo; ?>" class="thumbnails"></a>
                        <?php } else { ?>
                            <img width="100%" src="<?php echo site_url(); ?>uploads/banners/<?php echo $aviso->entidad_banner_aviso_archivo; ?>" class="thumbnails">
                        <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?> 

<!--   ========================== Banner Slider ===========================-->
<section class="banner-slider">

    <?php if ($embebido_home->embebido_home != NULL) { ?>
        <div class="container text-center">
            <div class="embed-responsive embed-responsive-16by9">
                <?php echo $embebido_home->embebido_home; ?>
            </div>
        </div>
    <?php } else { ?>
        <?php
        if ($banner_principal != FALSE) {
            foreach ($banner_principal as $dato) {
                ?>
                <div class="slider-img flex" style="background-image: url(<?php echo site_url('uploads/banners') . '/' . $dato->entidad_banners_imagenes_imagen; ?>);">
                    <?php if ($dato->entidad_banners_imagenes_texto != NULL) { ?>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="item">
                                        <div class="overlay">
                                            <p><?php echo $dato->entidad_banners_imagenes_texto; ?></p>
                                        </div>
                                        <?php if ($dato->entidad_banners_imagenes_url != NULL) { ?>
                                            <div><a class="btn button-u" href="<?php echo prep_url(str_replace("[URL]", site_url(), $dato->entidad_banners_imagenes_url)); ?>" title="<?php echo strip_tags($dato->entidad_banners_imagenes_texto); ?>" target="<?php echo $dato->entidad_banners_imagenes_target; ?>">Leer más</a></div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
        }
    }
    ?>
</section>
<!--   ========================== Tramites Section ===========================-->
<section class="tramites">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <!--                <div class="section-title">
                                    <h2><i class="fas fa-circle"></i> Atajos</h2>
                                </div>-->
            </div>
            <div class="col-xs-12">
                <!--   ========================== Tramites Menu Button ===========================-->
                <ul class="tramites-menu">
                    <?php
                    if ($enlaces_sub_menu != FALSE) {
                        ?>
                        <?php
                        foreach ($enlaces_sub_menu as $dato) {
                            ?>
                            <li>
                                <a href="<?php echo prep_url(str_replace("[URL]", site_url(), $dato->entidad_sub_menu_enlace)); ?>" target="<?php echo $dato->entidad_sub_menu_tipo_enlace; ?>" class="clearfix">
                                    <div class="icon">
                                        <div>
                                            <img src="<?php echo site_url("uploads/iconos/$dato->entidad_sub_menu_imagen"); ?>" alt="">
                                        </div>
                                    </div> <span><?php echo $dato->entidad_sub_menu_nombre; ?></span>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row tramites2">
            <div class="col-md-4 col-xs-12 ">
                <div class="row">
                    <!--   ========================== Poll Area ===========================-->
                    <div class="Encuestas col-sm-6 col-md-12">
                        <div class="section-title">
                            <h2><i class="fas fa-circle"></i> Encuestas</h2>
                        </div>
                        <?php if ($survey != NULL) { ?>
                            <div class="poll">
                                <p><?php echo $survey->survey_encuesta; ?></p>
                                <?php if ($survey_ip == TRUE || $this->input->cookie('voto') == TRUE) { ?>
                                    <form action="#">
                                        <?php echo $survey_texto; ?>
                                    </form>
                                </div>
                            <?php } else {
                                ?>
                                <form action="#" class="contenido_encuestas">
                                    <?php
                                    if ($survey_preguntas != FALSE) {
                                        $x = 1;
                                        foreach ($survey_preguntas as $dato) {
                                            ?>
                                            <label class="clearfix" for=""><input type="radio" name="confirm" id="optionsRadios<?php echo $dato->id_survey_preguntas; ?>" <?php echo ($x == 1) ? 'checked' : ''; ?>> <span><?php echo $dato->survey_preguntas_pregunta; ?></span></label>
                                            <?php
                                            $x++;
                                        }
                                        ?>
                                        <div class="buttons">
                                            <a href="<?php echo site_url('sitio/encuestas'); ?>" data-toggle="tooltip" title="Ver Todas las Encuestas" class="btn button-u gray">Ver Todas</a>
                                            <a onclick="return false;" class="btn button-u" id="encuesta_boton">Votar</a>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="jumbotron">
                            <h2>Encuestas!</h2>
                            <p>Espera muchas más encuestas próximamente.</p>
                        </div>
                        <div class="buttons">
                            <a href="<?php echo site_url('sitio/encuestas'); ?>" data-toggle="tooltip" title="Ver Todas las Encuestas" class="btn button-u gray">Ver Todas</a>
                        </div>
                    <?php } ?>
                </div>
                <!--   ========================== Evento Slider ===========================-->
                <div class="eventos col-sm-6 col-md-12">
                    <div class="section-title">
                        <h2><i class="fas fa-circle"></i> Eventos</h2>
                    </div>
                    <?php if ($calendario != NULL) { ?>
                        <div class="items evento-slider clearfix">
                            <?php foreach ($calendario as $dato) { ?>
                                <div class="item">
                                    <div class="img">
                                        <a href="<?php echo site_url('sitio/calendario_evento') . '/' . $dato->entidad_calendario_alias; ?>"><img src="<?php echo site_url("uploads/eventos/notificaciones/$dato->entidad_calendario_imagen"); ?>" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <date><i class="far fa-calendar"></i> <?php echo date("Y/m/d", strtotime($dato->entidad_calendario_fecha_inicio)); ?></date>
                                        <p><a href="<?php echo site_url('sitio/calendario_evento') . '/' . $dato->entidad_calendario_alias; ?>"><?php echo $dato->entidad_calendario_nombre; ?></a></p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="jumbotron d-print-none" >
                            <h2>Eventos!</h2>
                            <p>No hay eventos programados para este mes, espera más información   próximamente.</p>
                        </div>
                    <?php } ?>
                    <a href="<?php echo site_url('sitio/calendario'); ?>" class="btn button-u gray" data-toggle="tooltip" title="Ver todos los eventos">Ver Todos</a>
                </div>
            </div>
        </div>
        <!--   ========================== Notice Content ===========================-->
        <div class="col-md-8 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title">
                        <h2><i class="fas fa-circle"></i> Noticias</h2>
                    </div>
                </div>
                <?php
                $flag = FALSE;
                $x = 0;
                if ($noticias != FALSE) {
                    $tope = 6;
                    ?>
                    <?php
                    foreach ($noticias as $dato) {
                        if ($x < $tope) {
                            ?>
                            <?php echo ($x == 3) ? '</div>' : '' ?>
                            <?php echo ($x == 3 || $x == 0) ? '<div class="row">' : '' ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="notice-content">
                                    <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><img src="<?php echo site_url('uploads/noticias') . '/' . $dato->entidad_noticias_imagen; ?>" alt=""></a>
                                    <a href="<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>" class="title" href="#"><?php echo $dato->entidad_noticias_titulo; ?></a>
                                    <p><?php echo limitarPalabras($dato->entidad_noticias_descripcion_corta, 40, 200); ?></p>
                                    <div class="social flex">
                                        <div class="share">
                                            <a data-toggle="tooltip" title="Compartir" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?>"><i class="fab fa-facebook-f"></i></a>
                                            <a data-toggle="tooltip" title="Compartir" href="https://twitter.com/intent/tweet?text=E.S.E Hospital del Sur <?php echo site_url('sitio/ver_noticia') . '/' . $dato->entidad_noticias_alias ?> vía <?php echo $seo->entidad_seo_twitter_vcard; ?>"><i class="fab fa-twitter"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo ($x == 6) ? '</div>' : '' ?>
                            <?php
                        }
                        $x++;
                    }
                }
                ?>
                <div class="col-xs-12">
                    <a href="<?php echo site_url('sitio/noticias'); ?>" class="more" data-toggle="tooltip" title="Ver Todas las Noticias">+ Ver Todas las Noticias</a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--   ========================== Service Section ===========================-->
<section class="service">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-center">
                    <h2><i class="fas fa-circle"></i> Servicios</h2>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis aliquam elit mi, eu ultrices justo vehicula in. Fusce non enim at diam semper dapibus congue vitae elit. Fusce placerat eget felis at pellentesque. Maecenas auctor lacinia ornare. Phasellus at finibus mi, sed consectetur urna.</p>-->
                </div>
            </div>
        </div>
    </div>
    <div class="service-wraper">
        <div class="container">
            <div class="row flex">
                <?php if ($iconos != FALSE) { ?>
                    <?php foreach ($iconos as $icono) { ?>
                        <div class="col-md-6 col-xs-12">
                            <div class="item clearfix">
                                <div class="img"><img src="<?php echo site_url("uploads/iconos") . '/' . $icono->entidad_iconos_home_imagen; ?>" alt=""></div>
                                <div class="text">
                                    <a href="#" class="title"><?php echo $icono->entidad_iconos_home_nombre; ?></a>
                                    <p><?php echo $icono->entidad_iconos_home_texto_corto; ?></p>
                                    <div><a class="button-u gray"  href="<?php echo site_url("sitio/servicio/$icono->entidad_iconos_home_alias"); ?>">Leer más</a></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <!--   ========================== Service Items ===========================-->
            </div>
        </div>
    </div>
</section>

<!--   ========================== REDES Section ===========================-->
<section class="tramites">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-center">
                    <h2><i class="fas fa-circle"></i> Redes Sociales</h2>
                    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis aliquam elit mi, eu ultrices justo vehicula in. Fusce non enim at diam semper dapibus congue vitae elit. Fusce placerat eget felis at pellentesque. Maecenas auctor lacinia ornare. Phasellus at finibus mi, sed consectetur urna.</p>-->
                </div>
            </div>
        </div>
    </div>
    <div class="tramites-wraper">
        <div class="container">
            <div class="row flex">
                <div class="col-md-6 col-xs-12">
                    <div class="video">
                        <iframe class="big" frameborder="0" scrolling="no" allowtransparency="true"  src="<?php echo $widgets->entidad_widgets_youtube_principal; ?>"></iframe>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="facebook_area">
                        <center> <?php echo $widgets->entidad_widgets_facebook; ?> </center>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="video">
                        <iframe frameborder="0" scrolling="no" allowtransparency="true" class="small" src="<?php echo $widgets->entidad_widgets_twitter; ?>embed"></iframe>
                    </div>
                </div>
                <!--   ========================== Service Items ===========================-->
            </div>
        </div>
    </div>
</section>
<script>
<?php
if ($survey != NULL) {
    if ($survey_ip != TRUE && $this->input->cookie('voto') != TRUE) {
        ?>
            $('#encuesta_boton').click(function () {
                var respuesta = $('input[name="confirm"]:checked').attr('id');
                var short = respuesta.substring(13);
                swal({
                    title: "",
                    text: "Tu voto fue recibido correctamente recuerda que solo podrás votar una vez por día.",
                    confirmButtonText: "Continuar",
                    type: "success",
                    showCancelButton: false,
                    allowOutsideClick: false
                }).then((result) => {
                    var url = "<?php echo site_url('sitio/survey') . '/' . $survey->id_survey . '/' . $this->session->all_userdata()['__ci_last_regenerate'] . '/'; ?>" + short;
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        success: function (msg) {
                            $('#encuesta_boton').remove();
                            $('.contenido_encuestas').empty();
                            $('.contenido_encuestas').append(msg);
                        },
                        error: function (jqXHR, exception) {
                            if (jqXHR.status === 0) {
                                alert('No Conecta.\n Verifique su conexión a internet.');
                            } else if (jqXHR.status == 404) {
                                alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                            } else if (jqXHR.status == 500) {
                                alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'parsererror') {
                                alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'timeout') {
                                alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'abort') {
                                alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else {
                                alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                            }

                        }

                    });
                });

            });
        <?php
    }
}
?>
</script>