<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<link href="<?php echo site_url(); ?>css/social-share-kit.css" rel="stylesheet">
<script src="<?php echo site_url(); ?>js/social-share-kit.min.js"></script>


<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#"><?php echo $evento->entidad_calendario_nombre; ?></a></h3>
        </div>
        <div class="single-post">
            <div class="post_time">
                <ul class="list-unstyled">
                    <li><i class="fa fa-calendar" aria-hidden="true"></i>Fecha de Ingreso: <?php echo date('d', strtotime($evento->entidad_calendario_fecha_ingreso)) . " de " . $meses[date('n', strtotime($evento->entidad_calendario_fecha_ingreso)) - 1] . " del " . date('Y', strtotime($evento->entidad_calendario_fecha_ingreso)); ?></li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i>Última actualización: <?php echo date('d', strtotime($evento->entidad_calendario_actualizacion)) . " de " . $meses[date('n', strtotime($evento->entidad_calendario_actualizacion)) - 1] . " del " . date('Y', strtotime($evento->entidad_calendario_actualizacion)); ?></li>
                </ul>
            </div>
            <?php if ($evento->entidad_calendario_imagen != NULL) { ?>
                <?php if ($evento->entidad_calendario_enlace_imagen == NULL) { ?>
                    <img class="img-responsive center-block"  src="<?php echo site_url('uploads/eventos') . '/' . $evento->entidad_calendario_imagen ?>">
                <?php } else { ?>
                    <a href="<?php echo prep_url($evento->entidad_calendario_enlace_imagen); ?>" target="<?php echo $evento->entidad_calendario_enlace_tipo; ?>"><img class="img-responsive center-block"  src="<?php echo site_url('uploads/eventos') . '/' . $evento->entidad_calendario_imagen ?>"></a>
                <?php } ?>
            <?php } ?>
            <div class="post_content">
                <?php echo $evento->entidad_calendario_descripcion; ?>
                <?php if ($evento->entidad_calendario_dia == 2) { ?>
                    <table class="table table-bordered table-striped  ">
                        <tr>
                            <td>Fecha de inicio</td>
                            <td class="text-lowercase"><?php echo $dias[date('w', strtotime($evento->entidad_calendario_fecha_inicio))] . ' ' . date('d', strtotime($evento->entidad_calendario_fecha_inicio)) . " de " . $meses[date('n', strtotime($evento->entidad_calendario_fecha_inicio)) - 1] . " del " . date('Y', strtotime($evento->entidad_calendario_fecha_inicio)); ?></td>
                        </tr>
                        <tr>
                            <td>Hora</td>
                            <td><?php echo date('h', strtotime($evento->entidad_calendario_fecha_inicio)) . ':' . date('i', strtotime($evento->entidad_calendario_fecha_inicio)) . ' ' . date('a', strtotime($evento->entidad_calendario_fecha_inicio)); ?></td>
                        </tr>
                        <tr>
                            <td>Fecha de finalización</td>
                            <td class="text-lowercase"><?php echo $dias[date('w', strtotime($evento->entidad_calendario_fecha_fin))] . ' ' . date('d', strtotime($evento->entidad_calendario_fecha_fin)) . " de " . $meses[date('n', strtotime($evento->entidad_calendario_fecha_fin)) - 1] . " del " . date('Y', strtotime($evento->entidad_calendario_fecha_fin)); ?></td>
                        </tr>
                        <tr>
                            <td>Hora</td>
                            <td><?php echo date('h', strtotime($evento->entidad_calendario_fecha_fin)) . ':' . date('i', strtotime($evento->entidad_calendario_fecha_fin)) . ' ' . date('a', strtotime($evento->entidad_calendario_fecha_fin)); ?></td>
                        </tr>
                    </table>
                <?php } else { ?>
                    <table class="table table-bordered table-striped  ">
                        <tr>
                            <td>Fecha de inicio</td>
                            <td class="text-lowercase"><?php echo $dias[date('w', strtotime($evento->entidad_calendario_fecha_inicio))] . ' ' . date('d', strtotime($evento->entidad_calendario_fecha_inicio)) . " de " . $meses[date('n', strtotime($evento->entidad_calendario_fecha_inicio)) - 1] . " del " . date('Y', strtotime($evento->entidad_calendario_fecha_inicio)); ?></td>
                        </tr>
                        <tr>
                            <td>Hora</td>
                            <td>Todo el día</td>
                        </tr>
                    </table>
                <?php } ?>
            </div>
        </div>
        <div class="text-right">
            <div class="ssk-group ssk-count  ssk-round">
                <a href="" class="ssk ssk-facebook"></a>
                <a href="" class="ssk ssk-twitter"></a>
                <a href="" class="ssk ssk-google-plus"></a>
                <a href="" class="ssk ssk-pinterest"></a>
                <a href="" class="ssk ssk-vk"></a>
                <a href="" class="ssk ssk-linkedin"></a>
                <a href="" class="ssk ssk-buffer"></a>
                <a href="" class="ssk ssk-email"></a>
            </div>
        </div>
    </article>
    <article class="blog-item">
        <?php if ($calendario != NULL) { ?>
            <div class="section-title">
                <h2><i class="fas fa-circle"></i> Próximos Eventos</h2>
            </div>
            <?php
            foreach ($calendario as $dato) {
                if ($dato->id_entidad_calendario != $evento->id_entidad_calendario) {
                    ?>
                    <div class="col-md-4">
                        <div class="notice-content">
                            <a href="<?php echo site_url('sitio/calendario_evento') . '/' . $dato->entidad_calendario_alias; ?>"><img src="<?php echo site_url("uploads/eventos/notificaciones/$dato->entidad_calendario_imagen"); ?>" alt="anouncios_<?php echo $dato->entidad_calendario_alias; ?>" data-toggle="tooltip" data-placement="top" title="Ampliar Información"></a>
                            <a class="title2" href="<?php echo site_url('sitio/calendario_evento') . '/' . $dato->entidad_calendario_alias; ?>"><?php echo $dato->entidad_calendario_nombre; ?></a>
                            <date><i class="far fa-calendar"></i> <?php echo date("Y/m/d", strtotime($dato->entidad_calendario_fecha_inicio)); ?></date>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        <?php } ?>
        <div class="col-xs-12">
            <a href="<?php echo site_url('sitio/calendario'); ?>" class="more" data-toggle="tooltip" title="Ver Todos los Eventos">+ Ver Calendario de Eventos</a>
        </div>
    </article>
</div>
<script type="text/javascript">
    SocialShareKit.init();
</script>
