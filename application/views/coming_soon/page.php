<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <!--flagEstadoSitio-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $seo->entidad_seo_titulo; ?></title>
        <link href="<?php echo site_url(); ?>css/coming_soon.css" rel="stylesheet" type="text/css" media="screen" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script> 
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto+Slab:700' rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no" />
        <link rel="shortcut icon" href="<?php echo site_url('uploads/entidad') . '/' . $seo->entidad_seo_favicon; ?>" />
    </head>
    <body>
        <div class="wrapper">
            <div class="left-section"><img src="images/watch.png" /></div>
            <div class="right-section">
                <h1 id="logo"><img src="<?php echo site_url('uploads/entidad') . '/' . $logo->entidad_informacion_logo; ?>" /></h1>
                <div class="seprator"></div>
                <div class="main-content">
                    <h2>Nuestro sitio web estará disponible próximamente</h2>
                    <p>Nos estamos renovando para mejorar</p>
                </div>
                <ul class="social">
                    <?php
                    if ($social != FALSE) {
                        foreach ($social as $dato) {
                            echo '<li><a href="' . $dato->entidad_redes_sociales_link . '" data-toggle="tooltip" data-placement="bottom" title="Ver información en la red social ' . $dato->entidad_redes_sociales_definicion_nombre . '"><i class="fa ' . $dato->entidad_redes_sociales_definicion_imagen . '"></i></a></li>';
                        }
                    }
                    ?>
                </ul>
                <ul class="info">
                    <li><i class="fa fa-phone-square"></i><?php echo $entidad_contacto->entidad_contacto_telefono; ?></li>
                    <li><i class="fa fa-map-marker"></i><?php echo $entidad_contacto->entidad_contacto_direccion . ' / ' . $seo->entidad_seo_ciudad . ' - ' . ucfirst(strtolower($seo->entidad_seo_country)); ?></li>
                    <li><i class="fa fa-envelope"></i><a href="#"><?php echo $entidad_contacto->entidad_contacto_email; ?></a></li>
                </ul>
            </div>
            <div class="watch-section"><img src="images/watch.png" /></div>
        </div>
    </body>
</html>
