<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="blog-content">
    <!--   ========================== Blog Content ===========================-->
    <article class="blog-item">
        <div class="section-title">
        </div>
        <div class="title">
            <h3><a href="#">Encuesta</a></h3>
        </div>
        <div class="col-xl-12 col-md-12">
                <div class="enquestas_area">
                    <?php if ($survey != NULL) { ?>
                        <div class="enquestas_item">
                            <h3><?php echo $survey->survey_encuesta; ?></h3>
                            <?php if ($survey_ip == TRUE || $this->input->cookie('voto') == TRUE) { ?>
                                <form action="#" class="poll-form">
                                    <div class="check-list resultado_encuesta">
                                        <?php echo $survey_texto; ?>
                                    </div>
                                </form>
                            </div>
                        <?php } else {
                            ?>
                            <form action="#" class="poll-form">
                                <div class="check-list">
                                    <?php
                                    if ($survey_preguntas != FALSE) {
                                        $x = 1;
                                        foreach ($survey_preguntas as $dato) {
                                            ?>
                                            <div class="<?php echo ($x == 1) ? 'check-item2 check-item' : 'check-item'; ?>">
                                                <label class="<?php echo ($x == 1) ? 'check-box-area2' : 'check-box-area'; ?> check-box-area"><span><?php echo $dato->survey_preguntas_pregunta; ?></span>
                                                    <input type="radio" name="confirm" id="optionsRadios<?php echo $x; ?>" <?php echo ($x == 1) ? 'checked' : ''; ?>>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <?php
                                            $x++;
                                        }
                                        ?>
                                    </div>
                                    <div class="encuesta_boton">
                                        <a href="#" class="btn btn-secondary btn-block" data-toggle="tooltip" title="Guardar Voto"  id="encuesta_boton" onclick="return false;" >Votar</a>
                                    </div>
                                </form>
                            <?php } ?>
                            <div class="link_button2 link_button">
                                <a href="<?php echo site_url('sitio/encuestas'); ?>" data-toggle="tooltip" title="Ver Todas las Encuestas">Ver Todas las Encuestas</a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="jumbotron">
                        <h2>Encuestas!</h2>
                        <p>Espera muchas más encuestas próximamente.</p>
                    </div>
                    <div class="link_button2 link_button">
                        <a href="<?php echo site_url('sitio/encuestas'); ?>" data-toggle="tooltip" title="Ver Todas las Encuestas">Ver Todas las Encuestas</a>
                    </div>
                <?php } ?>
            </div>
    </article>
</div>


<script>
<?php
if ($survey != NULL) {
    if ($survey_ip != TRUE && $this->input->cookie('voto') != TRUE) {
        ?>
            $('#encuesta_boton').click(function () {
                var respuesta = $('input[name="optionsRadios"]:checked').val();
                swal({
                    title: "",
                    text: "Tu voto fue recibido correctamente recuerda que solo podrás votar una vez por día.",
                    confirmButtonText: "Continuar",
                    type: "success",
                    showCancelButton: false,
                    allowOutsideClick: false
                }).then((result) => {
                    var url = "<?php echo site_url('sitio/survey') . '/' . $survey->id_survey . '/' . $this->session->all_userdata()['__ci_last_regenerate'] . '/'; ?>" + respuesta;
                    $.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        success: function (msg) {
                            $('#encuesta_boton').remove();
                            $('.check-list').empty();
                            $('.check-list').append(msg);
                        },
                        error: function (jqXHR, exception) {
                            if (jqXHR.status === 0) {
                                alert('No Conecta.\n Verifique su conexión a internet.');
                            } else if (jqXHR.status == 404) {
                                alert('Página buscada no encontrada . [404]\nPóngase en contacto con el administrador para solucionar el problema. ');
                            } else if (jqXHR.status == 500) {
                                alert('Error en el servidor interno [500]\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'parsererror') {
                                alert('Error JSON.\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'timeout') {
                                alert('Tiempo de espera agotado.\nIntente nuevamente o póngase en contacto con el administrador para solucionar el problema.');
                            } else if (exception === 'abort') {
                                alert('Ajax Solicitud abortada.\nPóngase en contacto con el administrador para solucionar el problema.');
                            } else {
                                alert('Uncaught Error.\n' + jqXHR.responseText + '\nPóngase en contacto con el administrador para solucionar el problema.');
                            }

                        }

                    });
                });

            });
        <?php
    }
}
?>
</script>